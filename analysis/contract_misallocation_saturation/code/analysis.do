* Saturation model for the Productivity Differences

cd "/Users/felipe/Dropbox (Brown)/Research/corruption_inc/analysis/contract_misallocation_saturation/code"
set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/felipe/Dropbox (Brown)/corruption_inc_copy"

cap: mkdir "../output"

program main
    local reps 30

    foreach stub in  "_govSalesMarkup" {

   *    estimation, reps(`reps')
    }

	
    * Build misallocation table
    file open excess_costs_table using "../output/excess_costs.tex", write replace
    file write excess_costs_table "& (1) & (2) & (3) \\" _n

    build_misallocation_table
    file close excess_costs_table
	

end



program make_winners_losers

	* Winners	
	use "$data_dir/proc_contracts/tables/winners_table.dta",clear
	keep internal_firm_id contract_link
	gduplicates drop
	
	* Merge with All Competitors
	merge 1:1 contract_link internal_firm_id using "$data_dir/proc_contracts/tables/contracts_competitors.dta", keep(1 2 3)
	gen winner = _merge!=2
	bysort contract_link: gen num_competitors = _N
	drop if num_competitors == 1
	drop _merge provider_name
	
	save_data "../temp/competitors", key(internal_firm_id contract_link) replace
	
	* Add Contract Info
	use "$data_dir/proc_contracts/tables/contracts_master_with_restrictions", clear
	keep contract_link agency_name province agg_contract_value contract_budget contract_year  object_type contract_cat
	gduplicates drop
	
	merge 1:m contract_link  using "../temp/competitors", keep(3) nogen
	
	save_data "../temp/contract_data_and_competitors", key(internal_firm_id contract_link) replace
	
end 


program estimation 
	syntax, reps(integer)
	
	use "/Volumes/Research Data/Excess/balance_sheet_govSalesMarkup_tfp.dta", clear
	
	drop totalWagesIESS101 totalDirecttWages101 totalWages101 materials otherMaterials fuel energy services services2 ///
		capital totalRevenue totalAsset totalDebt Materials1 Materials2 Materials3 variable_inputs active startYear ///
		isic contractor priceindex revenue_asset_ratio liess ldir m ms ln_totalDebt ln_variable_inputs ln_totalAsset ///
		age large_shareholder is_provider state2 state3 proxy2 proxy3 state_proxy state2_proxy state_proxy2 state2_proxy2 ///
		dyear1 dyear3 dyear2 dyear4 dyear5 dyear6 dyear7 dyear8 dyear9 L_free L_state L2_state L_state2 L2_state2 L_state3 ///
		L2_state3 L_state_proxy L2_state_proxy L_state2_proxy L2_state2_proxy L_state_proxy2 L2_state_proxy2 L_state2_proxy2 ///
		L2_state2_proxy2 L_proxy L2_proxy L_proxy2 L2_proxy2 L_proxy3 L2_proxy3
	
	drop bP1_Woold bPl1_Woold bP2_Woold bPl2_Woold bP3_Woold bPl3_Woold bP4_Woold bPl4_Woold bP5_Woold bPl5_Woold ///
		bP6_Woold bPl6_Woold bP7_Woold bPl7_Woold bP8_Woold bPl8_Woold bP9_Woold bPl9_Woold ba0c_Woold bb0c_Woold by1_Woold ///
		by2_Woold by3_Woold by4_Woold by5_Woold by6_Woold bC1_Woold in_sample_Ols firms_Ols sampleSize_Ols bl_Ols bm_Ols ///
		bk_Ols cons_Ols by1_Ols by2_Ols by3_Ols by4_Ols by5_Ols by6_Ols by7_Ols by8_Ols bC1_Ols
	
	gen contract_year = year
	cap: replace internal_firm_id = firm_id if internal_firm_id ==""
	*gcollapse (mean) tfp_Woold, by(internal_firm_id isic2_nbr contract_year)
	*gduplicates drop internal_firm_id contract_year, force
	
	* Add competitors
	joinby internal_firm_id contract_year using "../temp/contract_data_and_competitors"

	* Add connection info
	merge m:1 internal_firm_id using "$data_dir/providers_connections/master/providers_connections_all", keep(1 3)
	gen connected = _merge == 3
	
    * Capital Share of Revenue Gap
    gen s_k = state-r if rep == 0
    winsor2 s_k, cuts(1 99) replace by(rep)
    bys internal_firm_id year (rep): replace s_k = s_k[1]

    * Cost share
    gen cost_share = (exp(proxy) + exp(free))/exp(r) if rep == 0
    winsor2 cost_share, cuts(1 99) replace by(rep)
    bys internal_firm_id year (rep): replace cost_share = cost_share[1]
    gen cost_share_uncon = cost_share if connected == 0
	
	drop if s_k == . | tfp_Woold == .

	* Estimation 
	
	*	preserve
	*		keep if isic2_nbr == `sector'

			* Assume CRTS and initialize variables
			gen bk_Woold_crts = bk_Woold 
			gen bl_Woold_crts = bl_Woold 
			gen bm_Woold_crts = bm_Woold 

			gen fixed_capital_Woold_con1 = .
			gen fixed_capital_Woold_win1 = .
			gen sampleSize_fixed_Woold_1 = .
			gen uniqueFirms_fixed_Woold_1 = .

			gen fixed_capital_Woold_con2 = .
			gen fixed_capital_Woold_win2 = .
			gen sampleSize_fixed_Woold_2 = .
			gen uniqueFirms_fixed_Woold_2 = .

			
			gen fixed_capital_Woold_con3 = .
			gen fixed_capital_Woold_win3 = .
			gen sampleSize_fixed_Woold_3 = .
			gen uniqueFirms_fixed_Woold_3 = .

	qui levelsof isic2_nbr, local(sectors)
	foreach sector in `sectors' {

			forvalues rep = 0(1)`reps'{
	
					dis "Specification Woold, Repetition `rep', Sector `sector'"
					
					
					* No Fixed Effects
						local tfp_gap_Woold_mean_con = .
						local tfp_gap_Woold_mean_win = .
						local sample_fixed_Woold = .
						local s_k_gap_Woold_mean_con = .
						local s_k_gap_Woold_mean_win = .
						cap noisily: reghdfe tfp_Woold winner 1.connected#1.winner  if rep == `rep' & isic2_nbr == `sector', absorb(isic3_nbr contract_year)
						cap{ 
							local tfp_gap_Woold_mean_con = _b[1.connected#1.winner]
							local tfp_gap_Woold_mean_win = _b[winner]
							local sample_fixed_Woold = e(N)
							cap: drop tagtemp
							cap: drop sumtemp
							cap: drop insampletemp
							gen insampletemp = e(sample)
							gegen tagtemp = tag(internal_firm_id) if insample == 1
							gegen sumtemp = sum(tagtemp)
						}
					
						cap noisily: reghdfe s_k winner 1.connected#1.winner  if rep == `rep' & isic2_nbr == `sector', absorb(isic3_nbr contract_year)
						cap{
							local s_k_gap_Woold_mean_con = _b[1.connected#1.winner ]
							local s_k_gap_Woold_mean_win = _b[winner]
						}

						cap: replace fixed_capital_Woold_con1 = exp(-(bk_Woold_crts/(bk_Woold_crts+bl_Woold_crts+bm_Woold_crts))*`s_k_gap_Woold_mean_con' - ///
							1/(bk_Woold_crts+bl_Woold_crts+bm_Woold_crts)*`tfp_gap_Woold_mean_con')-1 if rep == `rep' & isic2_nbr == `sector'
						
						cap: replace fixed_capital_Woold_win1 = exp(-(bk_Woold_crts/(bk_Woold_crts+bl_Woold_crts+bm_Woold_crts))*`s_k_gap_Woold_mean_win' - ///
							1/(bk_Woold_crts+bl_Woold_crts+bm_Woold_crts)*`tfp_gap_Woold_mean_win')-1 if rep == `rep' & isic2_nbr == `sector'

						cap: replace sampleSize_fixed_Woold_1 = `sample_fixed_Woold' if  rep == `rep' & isic2_nbr == `sector'
						cap: replace uniqueFirms_fixed_Woold_1 = sumtemp if  rep == `rep' & isic2_nbr == `sector'

					
					* Fixed Effects for Agencies, Province, Contract Category
						local tfp_gap_Woold_mean_con = .
						local tfp_gap_Woold_mean_win = .
						local sample_fixed_Woold = .
						local s_k_gap_Woold_mean_con = .
						local s_k_gap_Woold_mean_win = .
						cap noisily: reghdfe tfp_Woold winner 1.connected#1.winner  if rep == `rep' & isic2_nbr == `sector', absorb(isic3_nbr ///
							agency_name province contract_cat contract_year)
						
						cap{ 
							local tfp_gap_Woold_mean_con = _b[1.connected#1.winner ]
							local tfp_gap_Woold_mean_win = _b[winner]
							local sample_fixed_Woold = e(N)
							cap: drop tagtemp
							cap: drop sumtemp
							cap: drop insampletemp
							gen insampletemp = e(sample)
							gegen tagtemp = tag(internal_firm_id) if insample == 1
							gegen sumtemp = sum(tagtemp)
						}
					
						 cap noisily: reghdfe s_k winner 1.connected#1.winner  if rep == `rep' & isic2_nbr == `sector', absorb(isic3_nbr ///
							agency_name province contract_cat contract_year)
					
					cap{
							local s_k_gap_Woold_mean_con = _b[1.connected#1.winner ]
							local s_k_gap_Woold_mean_win = _b[winner]
						}

						cap: replace fixed_capital_Woold_con2 = exp(-(bk_Woold_crts/(bk_Woold_crts+bl_Woold_crts+bm_Woold_crts))*`s_k_gap_Woold_mean_con' - ///
							1/(bk_Woold_crts+bl_Woold_crts+bm_Woold_crts)*`tfp_gap_Woold_mean_con')-1 if rep == `rep' & isic2_nbr == `sector'
						
						cap: replace fixed_capital_Woold_win2 = exp(-(bk_Woold_crts/(bk_Woold_crts+bl_Woold_crts+bm_Woold_crts))*`s_k_gap_Woold_mean_win' - ///
							1/(bk_Woold_crts+bl_Woold_crts+bm_Woold_crts)*`tfp_gap_Woold_mean_win')-1 if rep == `rep' & isic2_nbr == `sector'

						cap: replace sampleSize_fixed_Woold_2 = `sample_fixed_Woold' if  rep == `rep' & isic2_nbr == `sector'
						cap: replace uniqueFirms_fixed_Woold_2 = sumtemp if  rep == `rep' & isic2_nbr == `sector'
						
						
					* Contract-fixed effects
						local tfp_gap_Woold_mean_con = .
						local tfp_gap_Woold_mean_win = .
						local sample_fixed_Woold = .
						local s_k_gap_Woold_mean_con = .
						local s_k_gap_Woold_mean_win = .
						 cap noisily: reghdfe tfp_Woold winner 1.connected#1.winner  if rep == `rep' & isic2_nbr == `sector' , absorb(contract_link)
						cap{ 
							local tfp_gap_Woold_mean_con = _b[1.connected#1.winner ]
							local tfp_gap_Woold_mean_win = _b[winner]
							local sample_fixed_Woold = e(N)
							cap: drop tagtemp
							cap: drop sumtemp
							cap: drop insampletemp
							gen insampletemp = e(sample)
							gegen tagtemp = tag(internal_firm_id) if insample == 1
							gegen sumtemp = sum(tagtemp)
						}
					
						 cap noisily: reghdfe s_k winner 1.connected#1.winner  if rep == `rep' & isic2_nbr == `sector' , absorb(contract_link)
						cap{
							local s_k_gap_Woold_mean_con = _b[1.connected#1.winner ]
							local s_k_gap_Woold_mean_win = _b[winner]
						}

						cap: replace fixed_capital_Woold_con3 = exp(-(bk_Woold_crts/(bk_Woold_crts+bl_Woold_crts+bm_Woold_crts))*`s_k_gap_Woold_mean_con' - ///
							1/(bk_Woold_crts+bl_Woold_crts+bm_Woold_crts)*`tfp_gap_Woold_mean_con')-1 if rep == `rep' & isic2_nbr == `sector'
						
						cap: replace fixed_capital_Woold_win3 = exp(-(bk_Woold_crts/(bk_Woold_crts+bl_Woold_crts+bm_Woold_crts))*`s_k_gap_Woold_mean_win' - ///
							1/(bk_Woold_crts+bl_Woold_crts+bm_Woold_crts)*`tfp_gap_Woold_mean_win')-1 if rep == `rep' & isic2_nbr == `sector'

						cap: replace sampleSize_fixed_Woold_3 = `sample_fixed_Woold' if  rep == `rep' & isic2_nbr == `sector'
						cap: replace uniqueFirms_fixed_Woold_3 = sumtemp if  rep == `rep' & isic2_nbr == `sector'

				
	
			}
			
			
		}
			gen fixed_welfare_cost_Woold_con1 = (fixed_capital_Woold_con1*cost_share_uncon)*100
			gen fixed_welfare_cost_Woold_win1 = (fixed_capital_Woold_win1*cost_share_uncon)*100
			gen fixed_welfare_cost_Woold_con2 = (fixed_capital_Woold_con2*cost_share_uncon)*100
			gen fixed_welfare_cost_Woold_win2 = (fixed_capital_Woold_win2*cost_share_uncon)*100
			gen fixed_welfare_cost_Woold_con3 = (fixed_capital_Woold_con3*cost_share_uncon)*100
			gen fixed_welfare_cost_Woold_win3 = (fixed_capital_Woold_win3*cost_share_uncon)*100
			
			save "/Volumes/Research Data/Excess/dis_aggcontract_level_excess_costs_v3", replace 
			
			use "/Volumes/Research Data/Excess/dis_aggcontract_level_excess_costs_v3", clear 
			
			keep isic2_nbr rep fixed_capital_Woold_con* fixed_welfare_cost_Woold_con* fixed_capital_Woold_win* fixed_welfare_cost_Woold_win* ///
				sampleSize_fixed_Woold_* uniqueFirms_fixed_Woold*
				
			
			gcollapse (mean) fixed_capital_Woold_con* fixed_welfare_cost_Woold_con* fixed_capital_Woold_win* fixed_welfare_cost_Woold_win* ///
				sampleSize_fixed_Woold_* uniqueFirms_fixed_Woold*, by(isic2_nbr rep)

			save_data "/Volumes/Research Data/Excess/contract_level_excess_costs_v3", replace key(isic2_nbr rep)
			
			
	

end 
program build_misallocation_table	
			
	forvalues i=1/3{
		use  "/Volumes/Research Data/Excess/contract_level_excess_costs_v3", clear
		
		foreach v of varlist fixed_capital* fixed_welfare*{
			winsor2 `v', c(5 95) replace
		}
		
		keep if rep>0

        collapse (mean) fixed_capital_Woold_con`i' fixed_welfare_cost_Woold_con`i' fixed_capital_Woold_win`i' fixed_welfare_cost_Woold_win`i' ///
				 [aweight=sampleSize_fixed_Woold_`i'], by(rep)

        collapse (sd) fixed_capital_Woold_con`i' fixed_welfare_cost_Woold_con`i' fixed_capital_Woold_win`i' fixed_welfare_cost_Woold_win`i' 
		
		
		
        save "/Volumes/Research Data/Excess/SD_excess_`i'", replace

        use "/Volumes/Research Data/Excess/contract_level_excess_costs_v3" if rep==0, clear
		foreach v of varlist fixed_capital* fixed_welfare*{
			winsor2 `v', c(5 95) replace
		}
		
        
        collapse (mean)  fixed_capital_Woold_con`i' fixed_welfare_cost_Woold_con`i' fixed_capital_Woold_win`i' fixed_welfare_cost_Woold_win`i' ///
				 [aweight=sampleSize_fixed_Woold_`i']
		
		append using  "/Volumes/Research Data/Excess/SD_excess_`i'"
		
		gen id = _n
		
		save  "/Volumes/Research Data/Excess/Estimates_excess`i'", replace
	}
	
	use "/Volumes/Research Data/Excess/contract_level_excess_costs_v3" if rep==0, clear
	gcollapse (sum) sampleSize_fixed_Woold*  
	*uniqueFirms_fixed_Woold_*
    gen id = 1

	forvalues i = 1/3{
		merge 1:1 id using "/Volumes/Research Data/Excess/Estimates_excess`i'"	, nogen
	}
	
	
	forvalues i = 1/3{
		rename (fixed_capital_Woold_con`i' fixed_welfare_cost_Woold_con`i' fixed_capital_Woold_win`i' fixed_welfare_cost_Woold_win`i') ///
			(con`i'_Woold_Ec con`i'_Woold_Welfare win`i'_Woold_Ec win`i'_Woold_Welfare)
	}
		
		
	foreach type in win con {

		foreach var in Ec Welfare {

					forvalues i = 1/3{

				qui sum `type'`i'_Woold_`var' if id == 1
				local m_`type'`i'_Woold_`var' = round(`r(mean)', 0.001)

				qui sum `type'`i'_Woold_`var' if id == 2
				local se_`type'`i'_Woold_`var' = round(`r(mean)', 0.001)

				qui sum sampleSize_fixed_Woold_`i'
				local ss_`i'_Woold = `r(mean)'
				
				

				if (abs(`m_`type'`i'_Woold_`var'') >= `se_`type'`i'_Woold_`var''*1.644854 & ///
					abs(`m_`type'`i'_Woold_`var'') < `se_`type'`i'_Woold_`var''*1.95996) {
					local m_`type'`i'_Woold_`var' = "`m_`type'`i'_Woold_`var''*"
				}
				else if (abs(`m_`type'`i'_Woold_`var'') >= `se_`type'`i'_Woold_`var''*1.95996 & ///
					abs(`m_`type'`i'_Woold_`var'') < `se_`type'`i'_Woold_`var''*2.32635) {
					local m_`type'`i'_Woold_`var' = "`m_`type'`i'_Woold_`var''**"
				}
				else if abs(`m_`type'`i'_Woold_`var'') >= `se_`type'`i'_Woold_`var''*2.32635 {
					local m_`type'`i'_Woold_`var' = "`m_`type'`i'_Woold_`var''***"
				
				}
			}
		
			if "`var'" == "Ec" & "`type'" == "win" local panel "Winner"
			else if "`var'" == "Ec" & "`type'"  == "con" local panel "Connected"
			else if "`var'" == "Welfare" local panel ""

			file write excess_costs_table "`panel' & `m_`type'1_Woold_`var'' &  `m_`type'2_Woold_`var'' & `m_`type'3_Woold_`var'' \\" _n
			file write excess_costs_table "& (`se_`type'1_Woold_`var'') & (`se_`type'2_Woold_`var'') & (`se_`type'3_Woold_`var'') \\" _n

			if "`var'" == "Welfare" {
				file write excess_costs_table " & `ss_1_Woold' & `ss_2_Woold' & `ss_3_Woold'" _n
			}
		}
	}
end


* Execute
main
