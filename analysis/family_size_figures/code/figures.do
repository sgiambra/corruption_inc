set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/"

program main
    use "$data_dir/derived_data/name_dataset", clear

    duplicates drop family_id, force

    sum family_size, det

    cumul family_size, gen(c_fam_size_corruption)
    gen id = _n

    rename family_size family_size_corruption

    save_data "../temp/family_size_corruption", ///
        replace key(family_id)

    * Use IPUMS data as reference
    use "$data_dir/IPUMS/ipums_children", clear
    keep if year == 2010 & chborn != 99 & chborn != 98

    * Keep only if at least one sibling
    keep if chborn >= 1

    sum chborn [aweight=perwt], det
    local max_plot = `r(max)'

    cumul chborn [aweight=perwt], gen(c_fam_size_ipums)

    gen id = _n
    merge 1:1 id using "../temp/family_size_corruption", ///
        nogen assert(2 3) keep(2 3)

    stack c_fam_size_corruption family_size_corruption c_fam_size_ipums ///
        chborn, into(c family_size) wide clear

    sort family_size c
    line c_fam_size_corruption c_fam_size_ipums family_size ///
        if family_size <= `max_plot', sort ytitle("") ///
        xtitle("Number of siblings") lpattern(solid dash) ///
        legend(label(1 "This paper data") label(2 "Census 2010 (IPUMS)"))

    graph export "../output/cdf_family_size.png", replace
end

* Execute
main
