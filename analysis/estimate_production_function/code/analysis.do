set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"
    
program main
    local reps 30

    estimate_prod_func, stub("_govSalesMarkup") repsM(`reps') ///
        controlsV("gov_sales_connected") yearD("dyear4 dyear5 dyear6 dyear7 dyear8 dyear9")
        
    estimate_prod_func, stub("_beforeConnection") repsM(`reps') ///
        if_condition("if before_connection == 1") yearD("dyear4 dyear5 dyear6 dyear7 dyear8 dyear9 dyear10 dyear11")
    
    foreach stub in "_full" "_providersOnly" {
        estimate_prod_func, stub(`stub') repsM(`reps') ///
            yearD("dyear4 dyear5 dyear6 dyear7 dyear8 dyear9 dyear10 dyear11")
    }

    estimate_prod_func, stub("_revenueAdj") repsM(`reps') yearD("dyear4 dyear5 dyear6 dyear7 dyear8 dyear9")

    output_elasticities
end

program estimate_prod_func
    syntax, stub(str) repsM(int) yearD(str) [if_condition(str) controlsV(str)]

    * Initialize estimates
    set seed 47
    clear
    gen placeholder = .
    save "../temp/EstimatesBootstrap`stub'", replace emptyok

    use "../temp/balance_sheet`stub'", clear
    qui levelsof isic2_nbr, local(sectors)
    
    foreach sector in `sectors' {

        use "../temp/balance_sheet`stub'" if isic2_nbr == `sector', clear
        gen rep = 0
    
        dis "Specification `stub', Repetition 0, Sector `sector'"

        *=====================================================
        * Wooldridge LP
        *=====================================================

        wooldridgeLP `if_condition', revenue(r) free(free) state(state) proxy(proxy) ///
            inverseVar("state state2 state3 proxy proxy2 proxy3 state_proxy state2_proxy state_proxy2") ///
            controls("`yearD' `controlsV'") maxiter(5000)

        save_woold_estimates, rep(0) yearD(`yearD') sector(`sector') stub(`stub') controlsV(`controlsV')

        *=====================================================
        * OLS
        *=====================================================

        reg r free state proxy dyear2 dyear3 `yearD' `controlsV' `if_condition'

        save_ols_estimates, rep(0) yearD(dyear2 dyear3 `yearD') ///
            sector(`sector') stub(`stub') controlsV(`controlsV')

        qui save_data "../temp/balance_sheet`stub'_sector`sector'", ///
            key(expediente year) replace
    }

    *=====================================================
    * BOOTSTRAPPING
    *=====================================================

    foreach sector in `sectors' {
        * Initialize repetition counter
        local rep = 1

        while `rep' <= `repsM' {
            use "../temp/balance_sheet`stub'" if isic2_nbr == `sector', clear

            bsample, cluster(expediente) idcluster(newid) strata(isic2_nbr)
            qui xtset newid year

            dis "Specification `stub', Repetition `rep', Sector `sector'"

            *=====================================================
            * Wooldridge LP
            *=====================================================

            cap: wooldridgeLP `if_condition', revenue(r) free(free) state(state) proxy(proxy) ///
                inverseVar("state state2 state3 proxy proxy2 proxy3 state_proxy state2_proxy state_proxy2") ///
                controls("`yearD' `controlsV'") maxiter(5000)

            * Collect estimates and run OLS specification only if Wooldridge LP converged
            if !_rc {
                save_woold_estimates, rep(`rep') yearD(`yearD') sector(`sector') stub(`stub') controlsV(`controlsV')

                *=====================================================
                * OLS
                *=====================================================

                qui reg r free state proxy dyear2 dyear3 `yearD' `controlsV' `if_condition'

                save_ols_estimates, rep(`rep') yearD(dyear2 dyear3 `yearD') ///
                    sector(`sector') stub(`stub') controlsV(`controlsV')

                gen rep = `rep'
                qui save_data "../temp/balance_sheet`stub'_sector`sector'_rep`rep'", key(newid year) replace

                local rep = `rep' + 1
            }
        }
    }

    compute_tfp_estimates, stub(`stub') yearD(`yearD') repsM(`repsM') sectors(`sectors')
end

program output_elasticities
    foreach stub in "_govSalesMarkup" "_beforeConnection" "_revenueAdj" "_providersOnly" "_full" {
        
        foreach spec in "Ols" "Woold" {
            use "../temp/EstimatesBootstrap`stub'" if rep>0, clear

            gen breturns_`spec' = bl_`spec' + bm_`spec' + bk_`spec'

            collapse (sd) bl_`spec' bm_`spec' bk_`spec' breturns_`spec' [aweight=firms_`spec']

            save "../temp/EstimatesSD`stub'", replace

            use "../temp/EstimatesBootstrap`stub'" if rep==0, clear
            
            collapse (sum) bfirms=firms_`spec' bsample=sampleSize_`spec'

            gen id = _n
            save "../temp/EstimatesSample`stub'", replace

            use "../temp/EstimatesBootstrap`stub'" if rep==0, clear
            gen breturns_`spec' = bl_`spec' + bm_`spec' + bk_`spec'

            collapse (mean) bl_`spec' bm_`spec' bk_`spec' breturns_`spec' [aweight=firms_`spec']
            append using "../temp/EstimatesSD`stub'"

            gen id = _n
            merge 1:1 id using "../temp/EstimatesSample`stub'"

            reshape long b, i(id) j(var) string

            gen sort_var = 1 if var == "l_`spec'"
            replace sort_var = 2 if var == "m_`spec'"
            replace sort_var = 3 if var == "k_`spec'"
            replace sort_var = 4 if var == "returns_`spec'"
            replace sort_var = 5 if var == "firms"
            replace sort_var = 6 if var == "sample"

            sort sort_var id
            drop if b == . & (var == "firms" | var == "sample")
            mkmat b, mat(el_col)

            mat elasticities = (nullmat(elasticities), el_col)
        }
    }

    mat rownames elasticities = "Labor" "" "Intermediate" "Inputs" "Capital" "" "Returns to" "scale" "Number firms" "Sample size"
    mat colnames elasticities = "OLS (control gov markup)" "Woold (control gov markup)" "OLS (before connection)" "Woold (before connection)" ///
        "OLS (markup adjusted)" "Woold (markup adjusted)" "OLS" "LP-Woold" "OLS (All firms)" "Woold (all firms)"
    esttab matrix(elasticities, fmt(%15.4f)) using "../output/table_elasticities.tex", replace
end

program wooldridgeLP
    syntax [if], revenue(str) free(str) state(str) proxy(str) inverseVar(str) ///
        controls(str) maxiter(str) [and_conditionLP(str)]

    foreach var in `inverseVar' {
        local L_inverseVar = "`L_inverseVar' L_`var'"
        local L2_inverseVar = "`L2_inverseVar' L2_`var'"
    }
    
    * Prepare parameters for the inverse function and lag inverse
    local eq1counter = 0
    foreach var in `inverseVar'{
        local ++eq1counter
        local eq1inverse `eq1inverse' -{a`eq1counter'}*`var'
    }
    local eq2counter = 0
    foreach var in `L_inverseVar'{
        local ++eq2counter
        local eq2inverse `eq2inverse' -{b`eq2counter'}*`var'
    }
    local eq3counter = 0
    foreach var in `controls'{
        local ++eq3counter
        local eqcontrols `eqcontrols' -{d`eq3counter'}*`var'
    }
    
    gmm (eq1: `revenue' -{`free'}*`free' -{`state'}*`state' -{`proxy'}*`proxy' `eq1inverse' `eqcontrols' - {a0})    ///
        (eq2: `revenue' -{`free'}*`free' -{`state'}*`state' -{`proxy'}*`proxy' `eq2inverse' `eqcontrols' - {b0})    ///
        `if' `and_conditionLP', instruments(eq1: `free' `inverseVar' `L_inverseVar' `controls')                     ///
        instruments(eq2: `state' `L_inverseVar' `L2_inverseVar' L_free `controls')                                  ///
        winitial(unadjusted, independent) technique(gn) conv_maxiter(`maxiter')   

    //  Note that inverseVar includes already state and proxy variables so they are not included in eq 1. 
    //  `L_inverseVar' is included in eq1 for overindetification, but could be exlcuded.
end

program save_woold_estimates
    syntax, rep(int) yearD(str) stub(str) sector(str) [controlsV(str)]

    gen in_sample_Woold = e(sample)

    * Observations
    qui distinct expediente if e(sample)
    local firms_Woold=r(ndistinct)

    qui count if e(sample)
    local sampleSize_Woold=r(N)

    preserve
        clear
        set obs 1
        gen isic2_nbr = `sector'
        gen rep = `rep'

        gen firms_Woold=`firms_Woold'
        gen sampleSize_Woold=`sampleSize_Woold'
        
        * Inputs
        gen bl_Woold=_b[free:_cons]
        gen bm_Woold=_b[proxy:_cons]
        gen bk_Woold=_b[state:_cons]
        
        * Polynomial parameters
        forvalues i=1(1)9{
            gen bP`i'_Woold=_b[a`i':_cons]
            gen bPl`i'_Woold=_b[b`i':_cons]
        }
        
        * Constants gmm
        gen ba0c`i'_Woold=_b[a0:_cons]
        gen bb0c`i'_Woold=_b[b0:_cons]
        
        * Year dummies
        local num: word count `yearD'
        forvalues i=1(1)`num'{
            gen by`i'_Woold=_b[d`i':_cons]
        }
        
        local j=`num'+1
        
        * Controls
        local num2: word count `controlsV'
        forvalues i=1(1)`num2'{
            gen bC`i'_Woold=_b[d`j':_cons]
            local j=`j'+1
        }

        append using "../temp/EstimatesBootstrap`stub'"

        save "../temp/EstimatesBootstrap`stub'", replace
    restore
end

program save_ols_estimates
    syntax, rep(int) yearD(str) sector(str) stub(str) [controlsV(str)]

    gen in_sample_Ols = e(sample)

    * Observations
    qui distinct expediente if e(sample)
    local firms_Ols=r(ndistinct)

    qui count if e(sample)
    local sampleSize_Ols=r(N)

    preserve
        clear
        set obs 1
        gen isic2_nbr = `sector'
        gen rep = `rep'

        gen firms_Ols=`firms_Ols'
        gen sampleSize_Ols=`sampleSize_Ols'
        
        * Inputs
        gen bl_Ols=_b[free]
        gen bm_Ols=_b[proxy]
        gen bk_Ols=_b[state]

        * Constant
        gen cons_Ols=_b[_cons]

        * Year dummies
        local i=0
        foreach var in `yearD' {
            local i=`i'+1
            gen by`i'_Ols=_b[`var']
        }
        
        * Controls
        if "`controlsV'" != "" {
            foreach var in `controlsV' {
                gen bC1_Ols=_b[`var']
            }
        }

        qui merge 1:1 isic2_nbr rep using "../temp/EstimatesBootstrap`stub'", nogen

        save "../temp/EstimatesBootstrap`stub'", replace
    restore
end

program compute_tfp_estimates
    syntax, stub(str) yearD(str) repsM(int) sectors(str)

    local first_sector: word 1 of `sectors'
    local remaining_sectors: list sectors- first_sector

    use "../temp/balance_sheet`stub'_sector`first_sector'", clear

    foreach rep of numlist 1(1)`repsM' {
        append using "../temp/balance_sheet`stub'_sector`first_sector'_rep`rep'"
    }

    foreach sector in `remaining_sectors' {
        append using "../temp/balance_sheet`stub'_sector`sector'"
        foreach rep of numlist 1(1)`repsM' {
            append using "../temp/balance_sheet`stub'_sector`sector'_rep`rep'"
        }
    }

    merge m:1 isic2_nbr rep using "../temp/EstimatesBootstrap`stub'", ///
        assert(1 3) keep(3) nogen

    foreach spec in "Ols" "Woold" {
        
        * Year dummies
        if "`spec'"=="Ols" {
            local fullYearD dyear2 dyear3 `yearD'
        }
        else {
            local fullYearD `yearD'
        }
        
        local eqyeardummies ""
        local num: word count `fullYearD'

        forvalues i=1(1)`num' {
            local var: word `i' of `fullYearD'
            replace by`i'_`spec'=0 if `var'==0
            local eqyeardummies `eqyeardummies' -by`i'_`spec'
        }

        if "`stub'"=="_govSalesMarkup" {
            gen tfp_`spec' = r - bl_`spec'*free - bm_`spec'*proxy - bk_`spec'*state - ///
                bC1_`spec'*gov_sales_connected - `eqyeardummies'
        }
        else {
            gen tfp_`spec' = r - bl_`spec'*free - bm_`spec'*proxy - bk_`spec'*state - `eqyeardummies'
        }

        replace tfp_`spec' = . if in_sample_`spec' == 0
    }

    drop placeholder newid

    * There will be firm duplicates
    bys expediente year rep: gen boot_firm_id = _n

    save_data "$data_dir/balance_sheet/derived/balance_sheet`stub'_tfp", /// 
        key(expediente year rep boot_firm_id) replace
end

* Execute  
main
