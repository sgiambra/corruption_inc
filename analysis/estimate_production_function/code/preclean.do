set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    local markup    "1.06"
    local free      "ltot"
    local proxy     "ms2"
    local state     "k"

    build_balance_panels, free(`free') proxy(`proxy') state(`state')
    build_government_sales
    build_robustness_panels, markup(`markup')

    select_industries
end

program build_balance_panels
    syntax, free(str) proxy(str) state(str)

    use provider_id internal_firm_id using ///
        "$data_dir/proc_contracts/tables/providers_registry", clear
    duplicates drop provider_id, force
    rename provider_id firm_id

    merge 1:m firm_id using "$data_dir/balance_sheet/master/balance_sheet_clean", ///
        assert(1 2 3) keep(2 3)

    gen is_provider = _merge == 3
    drop _merge

    * Drop uncertain connections, strategic entrants and created by bureaucrat
    merge m:1 internal_firm_id using ///
        "$data_dir/providers_connections/master/providers_connections_all", ///
        nogen assert(1 2 3) keep(1 3) keepusing(provider_entry_year ///
        strategic_entrant created_by_bureaucrat)
    
    drop if provider_entry_year == 2000
    drop if strategic_entrant == 1
    drop if created_by_bureaucrat == 1

    drop provider_entry_year strategic_entrant created_by_bureaucrat

    * Keep 2-digit sectors that contain at least 750 observations
    restrict_sample, sector_size(750)

    xtset expediente year

    * Generate variables for WooldridgeLP estimation
    qui tab year, gen(dyear)

    rename (`proxy' `free' `state') (proxy free state)

    gen L_free = L.free

    gen state2 = state^2
    gen state3 = state^3
    
    gen proxy2 = proxy^2
    gen proxy3 = proxy^3
    
    gen state_proxy = state*proxy
    gen state2_proxy = state2*proxy
    gen state_proxy2 = state*proxy2
    gen state2_proxy2 = state2*proxy2

    * Generate lag-inverse function
    foreach var of varlist state* proxy* {
        cap: gen L_`var' = L.`var'
        cap: gen L2_`var' = L2.`var'
    }

    save_data "../temp/balance_sheet_full", replace key(expediente year)

    keep if is_provider == 1

    * Additionally drop a few sectors
    restrict_sample, sector_size(750)

    * 20 obs are duplicates in terms of internal_firm_id but none is politically connected
    save_data "../temp/balance_sheet_providersOnly", replace key(expediente year)
end

program build_government_sales
    use "$data_dir/proc_contracts/tables/contracts_master_with_restrictions" ///
        if contract_year>=2009, clear
    
    rename contract_year year
    gcollapse (sum) gov_sales=contract_value, by(internal_firm_id year)

    save_data "../temp/government_sales", replace key(internal_firm_id year)
end

program build_robustness_panels
    syntax, markup(str)

    use "$data_dir/providers_connections/master/providers_connections_all", clear
    merge 1:m internal_firm_id using "../temp/balance_sheet_providersOnly", ///
        nogen assert(1 2 3) keep(2 3)

    gen before_connection = provider_entry_year==. | year<provider_entry_year
    
    restrict_sample, sector_size(750)

    xtset expediente year
    save_data "../temp/balance_sheet_beforeConnection", ///
        replace key(expediente year)

    use "$data_dir/providers_connections/master/providers_connections_all", clear
    merge 1:m internal_firm_id using "../temp/balance_sheet_providersOnly", ///
        nogen assert(1 2 3) keep(2 3)

    * Drop years without contract info
    drop if year < 2009

    * Update variables for WooldridgeLP estimation
    drop dyear* L_* L2_*
    xtset expediente year

    qui tab year, gen(dyear)

    gen L_free = L.free

    foreach var of varlist state* proxy* {
        cap: gen L_`var' = L.`var'
        cap: gen L2_`var' = L2.`var'
    }

    gen after_connection = provider_entry_year!=. & year>=provider_entry_year

    * Drop duplicates (1 firm-year has duplicate)
    duplicates tag internal_firm_id year, gen(dup)
    drop if dup > 0
    drop dup
    merge 1:1 internal_firm_id year using "../temp/government_sales", ///
        nogen assert(1 2 3) keep(1 3)

    replace gov_sales = 0 if gov_sales == .
    gen share_gov = gov_sales/totalRevenue
    replace share_gov = 1 if share_gov > 1

    gen gov_sales_connected = (after_connection==1)*(gov_sales>0)

    restrict_sample, sector_size(750)

    xtset expediente year
    save_data "../temp/balance_sheet_govSalesMarkup", ///
        replace key(expediente year)

    gen gov_sales_ind = gov_sales>0

    gen r_adj = r - ln(1 + gov_sales_ind*(1-1/`markup')) if after_connection == 1
    replace r = r_adj if r_adj != .
    save_data "../temp/balance_sheet_revenueAdj", ///
        replace key(expediente year)
end

program select_industries
    use "../temp/balance_sheet_providersOnly", clear
    keep isic2
    duplicates drop
    
    foreach stub in "_full" "_revenueAdj" "_govSalesMarkup" "_beforeConnection" {
        merge 1:m isic2 using "../temp/balance_sheet`stub'", keep(3) nogen 
        keep isic2
        duplicates drop
    }
    
    save_data "../temp/ListIndustries", key(isic2) replace

    foreach stub in "_providersOnly" "_full" "_revenueAdj" "_govSalesMarkup" "_beforeConnection" {
        use "../temp/balance_sheet`stub'", clear
        merge m:1 isic2 using "../temp/ListIndustries", ///
            assert(1 3) keep(3) nogen

        save_data "../temp/balance_sheet`stub'", replace key(expediente year)
    }
end

program restrict_sample
    syntax, sector_size(int)

    bysort isic2: gen sector_size = _N
    keep if sector_size >= `sector_size'
    drop sector_size
end

* Execute
main
