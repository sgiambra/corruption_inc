set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    foreach plotvar in position agency {
        use "../temp/`plotvar'_contract_value", clear

        gsort -`plotvar'_contract_value
        keep if _n <= 20

        sencode `plotvar'_to_plot, gen(`plotvar'_to_plot_nbr) ///
            label(`plotvar'_to_plot_nbr) gsort(`plotvar'_contract_value)

        qui sum `plotvar'_contract_value
        local max_value = `r(max)' + 20

        twoway bar `plotvar'_contract_value `plotvar'_to_plot_nbr, horizontal barw(0.6) base(0) ///
            || scatter `plotvar'_to_plot_nbr `plotvar'_contract_value, ms(none) mla(count_`plotvar') mlabpos(3) ///
            ylabel(1(1)20, valuelabel ang(h) labsize(small)) xlabel(, labsize(small)) ytitle("") legend(off) ///
            xtitle("Total contract value ($1,000,000)", size(small)) xscale(range(0 `max_value'))

        graph export "../output/bureaucrat_`plotvar'_value.png", replace
    }
end

* Execute
main
