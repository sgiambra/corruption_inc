set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    last_position
    build_positions_agencies
    compute_value
end

program last_position
    use "$data_dir/bureaucrats_scraped/processed_data/bureaucrats_panel" ///
        if bureaucrat == 1, clear
    
    bys bureaucrat_id (year): gen last_year = _n == _N
    keep if last_year == 1

    keep bureaucrat_id position_nbr agency_nbr
    save_data "../temp/bureaucrat_last_position", ///
        replace key(bureaucrat_id)
end

program build_positions_agencies
    use provider_id internal_firm_id using ///
        "$data_dir/proc_contracts/tables/providers_registry", clear
    duplicates drop provider_id, force
    save_data "../temp/providers_registry", replace key(provider_id)

    merge 1:m provider_id using ///
        "$data_dir/providers_connections/provider_owned_by_bureaucrat", ///
        nogen assert(1 3) keep(3)

    rename shareholder_id bureaucrat_id
    
    * Drop strategic and uncertain connections for consistency
    merge m:1 internal_firm_id using ///
        "$data_dir/providers_connections/master/providers_connections_all", ///
        nogen assert(2 3) keep(3) keepusing(provider_entry_year ///
        strategic_entrant created_by_bureaucrat)

    drop if provider_entry_year == 2000
    drop if strategic_entrant == 1
    drop if created_by_bureaucrat == 1

    merge m:1 bureaucrat_id using "../temp/bureaucrat_last_position", ///
        nogen assert(2 3) keep(3)
    merge m:1 position_nbr using ///
        "$data_dir/bureaucrats_scraped/processed_data/positions_list", ///
        nogen assert(2 3) keep(3)
    * We not plot common agencies as it is confidential info
    merge m:1 agency_nbr using ///
        "$data_dir/bureaucrats_scraped/processed_data/agencies_list", ///
        nogen assert(2 3) keep(3)

    bys internal_firm_id (bureaucrat_id): gen nbr_firms_connections = _N

    manual_fixes_position
    bys position_to_plot: gen count_position = _N

    bys agency_to_plot: gen count_agency = _N

    save_data "../temp/positions_agencies", ///
        replace key(bureaucrat_id internal_firm_id)
end

program compute_value
    use "$data_dir/proc_contracts/tables/contracts_master_with_restrictions" ///
        if contract_year>=2009, clear
    
    rename contract_year year
    fcollapse (sum) contract_value, by(contract_link internal_firm_id)

    joinby internal_firm_id using "../temp/positions_agencies"

    save_data "../temp/positions_agencies_contract_value", ///
        replace key(bureaucrat_id internal_firm_id contract_link)

    * For firms connected to multiple bureaucrats give to each a share of the contract value
    foreach plotvar in position agency {
        use "../temp/positions_agencies_contract_value", clear
        keep if `plotvar'_to_plot != ""

        gen `plotvar'_contract_value = contract_value/(1000000*nbr_firms_connections)
    
        collapse (sum) `plotvar'_contract_value (mean) count_`plotvar', by(`plotvar'_to_plot)

        save_data "../temp/`plotvar'_contract_value", replace key(`plotvar'_to_plot)
    }
end

progra manual_fixes_position
    gen position_to_plot = bureaucrat_position
    gen digit = substr(position_to_plot,-2,.)
    destring digit, replace force

    replace position_to_plot = "Coordinator" if strpos(position_to_plot, "COORDINADOR")>0
    replace position_to_plot = "Director" if strpos(position_to_plot, "DIRECTOR")>0
    replace position_to_plot = "Adviser" if strpos(position_to_plot, "ASESOR")>0
    replace position_to_plot = "Governor" if strpos(position_to_plot, "GOBERNADOR")>0
    replace position_to_plot = "Local Council Member" if strpos(position_to_plot, "VOCAL")>0
    replace position_to_plot = "Public Servant 1-4" if strpos(position_to_plot, "SERVIDOR PUBLICO")>0 & (digit <= 4 | digit == .)
    replace position_to_plot = "Public Servant 5-14" if strpos(position_to_plot, "SERVIDOR PUBLICO")>0 & (digit > 4 & digit != .)
    replace position_to_plot = "Analyst" if strpos(position_to_plot, "ANALISTA")>0
    replace position_to_plot = "Sub-Secretary" if strpos(position_to_plot, "SUBSECRETARIO")>0
    replace position_to_plot = "Head Manager" if strpos(position_to_plot, "JEFE")>0
    replace position_to_plot = "General Manager" if strpos(position_to_plot, "GERENTE")>0
    replace position_to_plot = "Technician" if strpos(position_to_plot, "TECNICO")>0
    replace position_to_plot = "Auditor" if strpos(position_to_plot, "AUDITOR")>0
    replace position_to_plot = "Expert" if strpos(position_to_plot, "EXPERT")>0
    replace position_to_plot = "Councilor" if strpos(position_to_plot, "CONCEJAL")>0
    replace position_to_plot = "Assemblyman" if strpos(position_to_plot, "ASAMBLEISTA")>0
    replace position_to_plot = "Sponsorship Assistant" if position_to_plot=="ASISTENTE DE PATROCINIO"
    replace position_to_plot = "Planning and Project Assistant" if position_to_plot=="ASISTENTE DE PLANIFICACION Y PROYECTOS"
    replace position_to_plot = "Minister" if position_to_plot=="MINISTRO"
    replace position_to_plot = "Minister" if position_to_plot=="MINISTRA"
    replace position_to_plot = "Agency/Organization President" if strpos(position_to_plot, "PRESIDENTE")>0
    replace position_to_plot = "Attorney" if strpos(position_to_plot, "PROCURADOR")>0
    replace position_to_plot = "Judge" if strpos(position_to_plot, "JUEZ")>0
    replace position_to_plot = "Rector" if strpos(position_to_plot, "RECTOR")>0
    replace position_to_plot = "Professor" if strpos(position_to_plot, "PROFESOR")>0

    rename agency_name agency_to_plot

    replace agency_to_plot = "Ministry of National Defense" if agency_to_plot == "MINISTERIO DE DEFENSA NACIONAL"
    replace agency_to_plot = "National Assembly" if agency_to_plot == "ASAMBLEA NACIONAL"
    replace agency_to_plot = "Specialty Hospital-Guayaquil" if agency_to_plot == "HOSPITAL DE ESPECIALIDADES GUAYAQUIL DR. ABEL GILBERT PONTON"
    replace agency_to_plot = "Municipal Government-Rioverde" if agency_to_plot == "GOBIERNO AUTONOMO DESCENTRALIZADO MUNICIPAL DE RIOVERDE"
    replace agency_to_plot = "Social Security Institute" if agency_to_plot == "INSTITUTO ECUATORIANO DE SEGURIDAD SOCIAL"
    replace agency_to_plot = "Social Security Institute" if agency_to_plot == "INSTITUTO ECUATORIANO DE SEGURIDAD SOCIAL (IESS)"
    replace agency_to_plot = "Social Security Institute" if agency_to_plot == "IESS INSTITUTO ECUATORIANO DE SEGURIDAD SOCIAL"
    replace agency_to_plot = "Social Security Institute" if agency_to_plot == "IESS"
    replace agency_to_plot = "Public Water and Sewer Utility-Quito" if agency_to_plot == "EMPRESA METROPOLITANA DE AGUA POTABLE Y ALCANTARILLADO DE QUITO EMAAP-Q"
    replace agency_to_plot = "Public Water and Sewer Utility-Guayaquil" if agency_to_plot == "EMPRESA MUNICIPAL DE AGUA POTABLE Y ALCANTARILLADO DE GUAYAQUIL, EP EMAPAG EP"
    replace agency_to_plot = "Public Water and Sewer Utility-Guayaquil" if agency_to_plot == "EMAPAG-EP"
    replace agency_to_plot = "Council of the Judiciary-Orellana" if agency_to_plot == "DIRECCION PROVINCIAL DEL CONSEJO DE LA JUDICATURA - ORELLANA"
    replace agency_to_plot = "University of Guayaquil" if agency_to_plot == "UNIVERSIDAD DE GUAYAQUIL"
    replace agency_to_plot = "TECNISTAMP" if agency_to_plot == "TECNISTAMP"
    replace agency_to_plot = "Municipality-Quito" if agency_to_plot == "MUNICIPIO DEL DISTRITO METROPOLITANO DE QUITO"
    replace agency_to_plot = "Telecommunication Agency" if agency_to_plot == "AGENCIA DE REGULACION Y CONTROL DE LAS TELECOMUNICACIONES ARCOTEL"
    replace agency_to_plot = "National Electricity Council" if agency_to_plot == "CONSEJO NACIONAL DE ELECTRICIDAD CONELEC"
    replace agency_to_plot = "Public Telecommunication Company" if agency_to_plot == "ANDINATEL"
    replace agency_to_plot = "Public Telecommunication Company" if strpos(agency_to_plot, "CNT")>0
    replace agency_to_plot = "Ministry of Education" if agency_to_plot == "MINISTERIO DE EDUCACION"
    replace agency_to_plot = "Council for Higher Education" if agency_to_plot == "CONSEJO DE EDUCACION SUPERIOR - CES"
    replace agency_to_plot = "Council for Higher Education" if agency_to_plot == "CONSEJO DE EDUCACION SUPERIOR-CES"
    replace agency_to_plot = "Ministry of Agriculture and Livestock" if agency_to_plot == "MINISTERIO DE AGRICULTURA, GANADERIA, ACUACULTURA Y PESCA"
    replace agency_to_plot = "Ministry of Public Health" if agency_to_plot == "MINISTERIO DE SALUD PUBLICA"
    replace agency_to_plot = "Secretariat for Water" if agency_to_plot == "SECRETARIA DEL AGUA"
    replace agency_to_plot = "National Electoral Council" if agency_to_plot == "CONSEJO NACIONAL ELECTORAL"
    replace agency_to_plot = "Women Commission" if agency_to_plot == "COMISARIA DE LA MUJER"
    replace agency_to_plot = "National Public Procurement Sustem" if strpos(agency_to_plot, "SERCOP")>0
    replace agency_to_plot = "National Intelligence Agency" if agency_to_plot == "SECRETARIA NACIONAL DE INTELIGENCIA"
    replace agency_to_plot = "Law School JFK" if agency_to_plot == "ESCUELA FISCAL MIXTA JONH F. KENNEDY"
    replace agency_to_plot = "Ministry of Transport and Public Works" if agency_to_plot == "MINISTERIO DE TRANSPORTE Y OBRAS PUBLICAS"
    replace agency_to_plot = "Judiciary Council - Guayas" if agency_to_plot == "DIRECCION PROVINCIAL DEL CONSEJO DE LA JUDICATURA - GUAYAS"
    replace agency_to_plot = "Ministry of Electricity" if agency_to_plot == "MINISTERIO DE ELECTRICIDAD Y ENERGIA RENOVABLE"
end

* Execute
main
