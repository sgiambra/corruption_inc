set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
adopath + ../../../lib/stata/gtools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    build_providers_connections
    build_providers_ownership
end

program build_providers_connections
    use provider_id internal_firm_id using ///
        "$data_dir/proc_contracts/tables/providers_registry", clear
    duplicates drop provider_id, force
    save_data "../temp/providers_registry", replace key(provider_id)

    foreach input_file in provider_owned_by_bureaucrat ///
        provider_owned_by_sibling_of_bureaucrat {

        use "../temp/providers_registry", clear
        merge 1:m provider_id using "$data_dir/providers_connections/`input_file'", ///
            nogen assert(1 3) keep(3)

        replace owner_entry_year = first_year_shareholder if owner_entry_year < first_year_shareholder
        rename owner_entry_year entry_year

        save_data "../temp/`input_file'_all_connections", ///
            replace key(internal_firm_id shareholder_id)

        collapse (min) entry_year (first) provider_id, by(internal_firm_id)
        keep if entry_year >= 2003
        drop entry_year

        save_data "../temp/`input_file'", replace key(internal_firm_id)
    }
    use "../temp/provider_owned_by_bureaucrat", clear
    merge 1:1 internal_firm_id using "../temp/provider_owned_by_sibling_of_bureaucrat", ///
        nogen assert(1 2 3) keep(1 2 3)

    save_data "../temp/list_connected_providers", replace key(internal_firm_id)

    use "../temp/provider_owned_by_bureaucrat_all_connections", clear
    rename entry_year entry_year_direct
    
    merge 1:1 provider_id shareholder_id using ///
        "../temp/provider_owned_by_sibling_of_bureaucrat_all_connections", ///
        assert(1 2 3) keep(1 2 3) nogen

    rename entry_year entry_year_indirect
    egen entry_year = rowmin(entry_year_direct entry_year_indirect)
    drop entry_year_indirect entry_year_direct

    * NB: we have some entries before 2003 if provider has both direct and indirect connections
    * with one of the two happening before 2003 and the other after
    save_data "../temp/shareholders_connections", replace key(provider_id shareholder_id)
end

program build_providers_ownership
    use "../temp/list_connected_providers", clear
    rename provider_id firm_id

    merge 1:m firm_id using "$data_dir/firms_ownership/shareholders_clean", ///
        nogen assert(1 2 3) keep(3)

    save_data "../temp/connected_providers_ownership", ///
        replace key(idshareholder firm_id year)

    rename idshareholder person_id
    merge m:1 person_id using "$data_dir/derived_data/name_dataset", ///
        assert(1 2 3) keep(1 3) nogen keepusing(name)

    rename (person_id firm_id) (shareholder_id provider_id)
    merge m:1 provider_id shareholder_id using "../temp/shareholders_connections", ///
        assert(1 2 3) keep(1 3)
    gen connected_to_bureaucrat = _merge == 3
    drop _merge

    bys provider_id (year shareholder_id): egen validate = max(connected_to_bureaucrat)
    qui sum validate
    assert `r(mean)' == 1
    drop validate

    bys provider_id year (shareholder_id): gen nbr_shareholders = _N

    split name, parse(" ") limit(7)
    gen last_name1 = name1
    gen length_last_name1 = 1

    foreach compound1 in LA LOS SAN SANTA VON VAN DER DEN DI DEL DE {
        replace length_last_name1 = 2 if name1 == "`compound1'"
    }
    foreach compound2 in LA LOS SAN SANTA VON VAN DER DEN DI DEL DE {
        replace length_last_name1 = 3 if length_last_name1 == 2 & name2 == "`compound2'"
    }
    replace last_name1 = name1 + " " + name2 if length_last_name1 == 2
    replace last_name1 = name1 + " " + name2 + " " + name3 if length_last_name1 == 3

    drop name1-name7 length_last_name1

    gen name_noFirstLastName = subinstr(name, last_name1, "", .)

    split name_noFirstLastName, parse(" ") limit(7)
    gen last_name2 = name_noFirstLastName1
    gen length_last_name2 = 1

    foreach compound1 in LA LOS SAN SANTA VON VAN DER DEN DI DEL DE {
        replace length_last_name2 = 2 if name_noFirstLastName1 == "`compound1'"
    }
    foreach compound2 in LA LOS SAN SANTA VON VAN DER DEN DI DEL DE {
        replace length_last_name2 = 3 if length_last_name2 == 2 & name_noFirstLastName2 == "`compound2'"
    }
    replace last_name2 = name_noFirstLastName1 + " " + name_noFirstLastName2 if length_last_name2 == 2
    replace last_name2 = name_noFirstLastName1 + " " + name_noFirstLastName2 + " " + name_noFirstLastName3 if length_last_name2 == 3

    drop name name_noFirstLastName name_noFirstLastName1-name_noFirstLastName7 length_last_name2

    export delimited "../temp/shareholders_connections_names.csv", replace
end

* Execute
main
