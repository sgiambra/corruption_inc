import pandas as pd

df = pd.read_csv("../temp/shareholders_connections_names.csv")
lastName1_lists = df.groupby(['provider_id','year'])['last_name1'].apply(list)
lastName2_lists = df.groupby(['provider_id','year'])['last_name2'].apply(list)

lastName_lists = lastName1_lists + lastName2_lists

df = pd.merge(df,
              pd.DataFrame(lastName_lists, columns=['last_names_list']),
              left_on=['provider_id','year'], 
              right_index=True)

df_bureauc = df.loc[df['connected_to_bureaucrat'] == 1]

df_bureauc.to_csv("../temp/shareholders_connections_names_lists.csv")