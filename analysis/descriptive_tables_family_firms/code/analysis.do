set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
adopath + ../../../lib/stata/gtools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    import delimited "../temp/shareholders_connections_names_lists.csv", clear

    gen lists_no_bureauc_t = subinstr(last_names_list, last_name1, "", 1)
    gen lists_no_bureauc = subinstr(lists_no_bureauc_t, last_name2, "", 1)
    drop lists_no_bureauc_t

    gen matching_name1 = strpos(lists_no_bureauc, last_name1) > 0
    gen matching_name2 = strpos(lists_no_bureauc, last_name2) > 0

    gen two_last_names = matching_name1 == 1 & matching_name2 == 1
    gen one_last_name = (matching_name1 == 1 & matching_name2 == 0) | ///
        (matching_name1 == 0 & matching_name2 == 1)
    gen no_last_names = matching_name1 == 0 & matching_name2 == 0

    keep if entry_year == year

    gen owner_before_bureauc_entry = first_year_shareholder < entry_year

    foreach stub of numlist 0 1 {
        foreach var in two_last_names one_last_name no_last_names {
            cap: matrix drop mat_`var'

            qui sum `var' if owner_before_bureauc_entry == `stub'
            matrix mat_`var' = (`r(mean)')

            qui sum nbr_shareholders if `var' == 1 & owner_before_bureauc_entry == `stub'
            matrix mat_`var' = (nullmat(mat_`var') \ `r(mean)' \ `r(sd)')

            qui sum share if `var' == 1 & owner_before_bureauc_entry == `stub'
            matrix mat_`var' = (nullmat(mat_`var') \ `r(mean)' \ `r(sd)')
        }
        matrix owner_before_bureauc_entry`stub' = (mat_two_last_names, mat_one_last_name, mat_no_last_names)

        matrix rownames owner_before_bureauc_entry`stub' = "Fraction of providers" "Number of shareholders" "" "Shares held" ""
        matrix colnames owner_before_bureauc_entry`stub' = "Share two last names" "Share one last name" "Share no last name"
        esttab matrix(owner_before_bureauc_entry`stub') using "../output/owner_before_bureaucrat_entry`stub'.tex", replace
    }
end

* Execute
main
