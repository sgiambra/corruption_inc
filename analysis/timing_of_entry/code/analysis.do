set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    use "$data_dir/bureaucrats_scraped/parsed/clean/bureaucrats_declaracion", replace

    gen entry_year_month = mofd(date(start_date, "YMD"))
    format entry_year_month %tm

    gen entry_year = year(date(start_date, "YMD"))
    drop if entry_year > 2018
    
    keep if bureaucrat == 1

    gcollapse (min) entry_year_month entry_year, by(bureaucrat_id)

    save_data "../temp/bureaucrat_entries", replace key(bureaucrat_id)

    drop if entry_year < 1970

    gen entry_month = month(dofm(entry_year_month))

    gen nbr_entries = 1
	gen shareEntry=nbr_entries/_N
    graph bar (sum) shareEntry, over(entry_month, label(angle(45) ///
        labsize(small)) relabel(1 "January" 2 "February" 3 "March" 4 "April" 5 "May" ///
        6 "June" 7 "July" 8 "August" 9 "September" 10 "October" 11 "November" ///
        12 "December")) ytitle("Share of appointments")
    graph export "../output/entry_month_distribution.png", replace
end

* Execute
main

