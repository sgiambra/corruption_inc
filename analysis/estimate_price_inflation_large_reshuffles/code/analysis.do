set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    * Electronic-catalog regressions
    use "../temp/electronic_catalog_reg", clear

    reghdfe price_dm ib(0).uncon_vs_years_bef_entry_own uncon_vs_years_bef_entry_per1 ///
        uncon_vs_years_bef_entry_per2 quantity_dm, absorb(year agency) vce(cluster agency)

    outreg2 using "../output/price_table.tex", tex drop(uncon_vs_years_bef_entry_per1 ///
        uncon_vs_years_bef_entry_per2) addtext(Year FE, Yes, Agency FE, Yes) dec(4) replace
    
    gcollapse (mean) av_price_dm=price_dm av_quantity_dm=quantity_dm uncon_vs_years_bef_entry_per1 ///
        uncon_vs_years_bef_entry_per2 uncon_vs_years_bef_entry_own, by(year internal_firm_id)
    
    reghdfe av_price_dm ib(0).uncon_vs_years_bef_entry_own uncon_vs_years_bef_entry_per1 ///
        uncon_vs_years_bef_entry_per2 av_quantity_dm, absorb(year) vce(cluster internal_firm_id)
    
    outreg2 using "../output/price_table.tex", tex drop(uncon_vs_years_bef_entry_per1 ///
        uncon_vs_years_bef_entry_per2) addtext(Year FE, Yes, Agency FE, No) dec(4) append


    * Auction regressions
    use "../temp/auctions_reg", clear

    reghdfe price_dm ib(0).uncon_vs_years_bef_entry_own uncon_vs_years_bef_entry_per1 ///
        uncon_vs_years_bef_entry_per2 quantity_dm, absorb(year agency) vce(cluster agency)

    outreg2 using "../output/price_table.tex", tex drop(uncon_vs_years_bef_entry_per1 ///
        uncon_vs_years_bef_entry_per2) addtext(Year FE, Yes, Agency FE, Yes) dec(4) append
    
    gcollapse (mean) av_price_dm=price_dm av_quantity_dm=quantity_dm uncon_vs_years_bef_entry_per1 ///
        uncon_vs_years_bef_entry_per2 uncon_vs_years_bef_entry_own, by(year internal_firm_id)
    
    reghdfe av_price_dm ib(0).uncon_vs_years_bef_entry_own uncon_vs_years_bef_entry_per1 ///
        uncon_vs_years_bef_entry_per2 av_quantity_dm, absorb(year) vce(robust)
    
    outreg2 using "../output/price_table.tex", tex drop(uncon_vs_years_bef_entry_per1 ///
        uncon_vs_years_bef_entry_per2) addtext(Year FE, Yes, Agency FE, No) dec(4) append
end

* Execute
main    
