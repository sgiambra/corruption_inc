set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    local competitors_threshold 2

    build_individual_connections
    subset_ec_data, competitors_threshold(`competitors_threshold')
    subset_auction_data, competitors_threshold(`competitors_threshold')
end

program build_individual_connections
    use provider_id internal_firm_id using ///
        "$data_dir/proc_contracts/tables/providers_registry", clear
    duplicates drop provider_id, force
    save_data "../temp/providers_registry", replace key(provider_id)

    foreach input_file in provider_is_bureaucrat ///
        provider_sibling_is_bureaucrat {
        
        use "../temp/providers_registry", clear
        merge 1:m provider_id using "$data_dir/providers_connections/`input_file'", ///
            nogen assert(1 3) keep(3)
        
        bys internal_firm_id (entry_year): egen contractor_entry = min(entry_year)
        gen large_reshuffle_at_entry = large_reshuffle if entry_year == contractor_entry

        gcollapse (min) entry_year (max) large_reshuffle_at_entry, by(internal_firm_id)
        
        save_data "../temp/`input_file'", replace key(internal_firm_id)
    }

    use "../temp/provider_is_bureaucrat", clear
    rename (entry_year large_reshuffle_at_entry) (entry_year_person_dir large_reshuffle_person_dir)

    merge 1:1 internal_firm_id using "../temp/provider_sibling_is_bureaucrat", ///
        nogen assert(1 2 3) keep(1 2 3)
    rename (entry_year large_reshuffle_at_entry) (entry_year_person_indir large_reshuffle_person_indir)

    egen entry_year_person = rowmin(entry_year_person_dir entry_year_person_indir)
    egen oneOrMore_large_reshuffle_per = rowmax(large_reshuffle_person_dir large_reshuffle_person_indir)

    save_data "../temp/individual_providers_connections", ///
        replace key(internal_firm_id)
end

program subset_ec_data
    syntax, competitors_threshold(int)

    * Import e-catalog for non-medicine products
    use "$data_dir/proc_contracts/electronic_catalog" if ///
        category1 != "Medicamentos", clear

    * Issue: e-catalog was not used in providers registry 
    * (about 5,000 obs have no internal_firm_id)
    merge m:1 provider_id using "../temp/providers_registry", ///
        nogen assert(1 2 3) keep(1 3)
    replace internal_firm_id = provider_id if internal_firm_id == ""

    merge m:1 internal_firm_id using ///
        "$data_dir/providers_connections/master/providers_connections_all", ///
        assert(1 2 3) keep(1 3)

    gen political_connection_firm = _merge == 3
    drop _merge

    * Drop uncertain connections
    drop if provider_entry_year == 2000
    * Drop strategic entrants
    drop if strategic_entrant == 1
    * Drop created by bureaucrat
    drop if created_by_bureaucrat == 1

    gen years_before_entry_own = political_connection_firm == 1 & year < provider_entry_year

    gen uncon_vs_years_bef_entry_own     = 0 if political_connection_firm == 0
    replace uncon_vs_years_bef_entry_own = 1 if political_connection_firm == 1 & years_before_entry_own == 1
    replace uncon_vs_years_bef_entry_own = 2 if political_connection_firm == 1 & years_before_entry_own == 0

    egen agency = group(agency_id)

    * Add dummy for politically connected individual providers
    merge m:1 internal_firm_id using "../temp/individual_providers_connections", ///
        assert(1 2 3) keep(1 3)
    
    gen political_connection_person = _merge == 3
    drop _merge

    gen years_before_entry_per = political_connection_person == 1 & year < entry_year_person

    * Need to keep separate variables to deal with outreg2
    gen uncon_vs_years_bef_entry_per1 = political_connection_person == 1 & years_before_entry_per == 1
    gen uncon_vs_years_bef_entry_per2 = political_connection_person == 1 & years_before_entry_per == 0

    drop if oneOrMore_large_reshuffles == 0 | oneOrMore_large_reshuffle_per == 0

    // Drop products with less than 2 providers
    egen tag_pf = tag(product_name internal_firm_id year)
    bys product_name year: egen num_providers = total(tag_pf)
    drop if num_providers < `competitors_threshold'

    rename (price_per_unit total_quantity) (price quantity)

    // Price and quantity demeaned
    foreach var in price quantity {
        gen ln_`var' = ln(`var')
        bys product_name year: egen mean_ln_`var' = mean(ln_`var')
        gen `var'_dm = ln_`var' - mean_ln_`var'
    }

    // Removing Outliers
    foreach var in price_dm quantity_dm {
        winsor2 `var', c(1 99) replace
    }

    save "../temp/electronic_catalog_reg", replace
end

program subset_auction_data
    syntax, competitors_threshold(int)

    * Get contract winners
    use "$data_dir/proc_contracts/tables/winners_table", clear
    keep contract_link internal_firm_id
    duplicates drop

    merge m:1 contract_link using "$data_dir/proc_contracts/tables/contracts_table", ///
        nogen assert(2 3) keep(3)

    bys contract_code internal_firm_id: gen to_keep = _n == 1
    drop if to_keep == 0 & dup == 1

    keep if contract_type == "Subasta Inversa Electrónica"

    keep contract_link internal_firm_id contract_year province agency_name
    rename contract_year year

    * Keep only single winner contracts
    duplicates tag contract_link, gen(multiple_winners)
    drop if multiple_winners > 0
    drop multiple_winners
    save_data "../temp/contract_winners", key(contract_link) replace

    * Only consider multi product contracts
    use "$data_dir/proc_contracts/scraped/clean/products" ///
        if product_quantity > 0, clear
    merge m:1 contract_link using "../temp/contract_winners", ///
        nogen assert(1 2 3) keep(3)

    merge m:1 internal_firm_id using "$data_dir/providers_connections/master/providers_connections_all", ///
        assert(1 2 3) keep(1 3)
    
    gen political_connection_firm = _merge == 3
    drop _merge

    * Drop uncertain connections
    drop if provider_entry_year == 2000
    * Drop strategic entrants
    drop if strategic_entrant == 1
    * Drop created by bureaucrat
    drop if created_by_bureaucrat == 1

    gen years_before_entry_own = political_connection_firm == 1 & year < provider_entry_year

    gen uncon_vs_years_bef_entry_own     = 0 if political_connection_firm == 0
    replace uncon_vs_years_bef_entry_own = 1 if political_connection_firm == 1 & years_before_entry_own == 1
    replace uncon_vs_years_bef_entry_own = 2 if political_connection_firm == 1 & years_before_entry_own == 0

    egen agency = group(agency_name)

    * Add dummy for politically connected individual providers
    merge m:1 internal_firm_id using "../temp/individual_providers_connections", ///
        assert(1 2 3) keep(1 3)
    
    gen political_connection_person = _merge == 3
    drop _merge

    gen years_before_entry_per = political_connection_person == 1 & year < entry_year_person

    * Need to keep separate variables to deal with outreg2
    gen uncon_vs_years_bef_entry_per1 = political_connection_person == 1 & years_before_entry_per == 1
    gen uncon_vs_years_bef_entry_per2 = political_connection_person == 1 & years_before_entry_per == 0

    drop if oneOrMore_large_reshuffles == 0 | oneOrMore_large_reshuffle_per == 0

    // Drop products with less than 2 providers
    egen tag_pf = tag(product_description product_id product_unit_size internal_firm_id year)

    * Observations with num_providers = 0 have missing values in one of variables
    bys product_description product_id product_unit_size year: egen num_providers = total(tag_pf)
    drop if num_providers < `competitors_threshold'

    rename (product_unit_price product_quantity) (price quantity)

    // Price and quantity demeaned
    foreach var in price quantity {
        gen ln_`var' = ln(`var')
        bys product_description product_id product_unit_size year: egen mean_ln_`var' = mean(ln_`var')
        gen `var'_dm = ln_`var' - mean_ln_`var'
    }

    // Removing Outliers
    foreach var in price_dm quantity_dm {
        winsor2 `var', c(1 99) replace
    }

    save "../temp/auctions_reg", replace
end

* Execute
main
