set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
adopath + ../../../lib/stata/gtools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    local markup "1.05" 

    build_balance_panels
    build_government_sales
    build_robustness_panels, markup(`markup')
end

program build_balance_panels
    import delimited "$data_dir/price_index_WB/API_FP.CPI.TOTL_DS2_en_csv_v2_126205.csv", ///
        varnames(5) rowrange(5) clear

    foreach v of varlist v* {
       local x : variable label `v'
       rename `v' priceindex`x'
    }
    
    keep if countryname == "Ecuador"
    drop priceindex
    reshape long priceindex, i(countryname) j(year)
    keep year priceindex

    save_data "../temp/priceindex", replace key(year)

    use expediente rucFirm isic using "$data_dir/firms_ownership/directory.dta", replace
    rename rucFirm firm_id
    drop if firm_id == "."
    * Almost 200k rucs become of length 10
    replace firm_id = substr(firm_id, 1, 10) if ///
        substr(firm_id, -3, 3) == "001" & length(firm_id) == 13

    save_data "../temp/list_rucs_directory.dta", key(firm_id) replace

    use "$data_dir/balance_sheet/clean/bal2007_2017", clear
    merge m:1 expediente using "../temp/list_rucs_directory", ///
        nogen assert(1 2 3) keep(3)

    gen isic2 = substr(isic, 1, 3)
    egen isic2_nbr = group(isic2)
    
    gen totalMaterials = materials + otherMaterials + fuel + energy

    * Get price index
    merge m:1 year using "../temp/priceindex", ///
        assert(2 3) keep(3) nogen

    * Follow GNR and drop zeros
    foreach var in capital totalWagesIESS101 totalRevenue totalMaterials {
        replace `var' = `var'/priceindex*100
        winsor2 `var' if `var' > 0, replace cuts(1 99)
        gen ln_`var' = ln(`var')
        gen ln_`var'_zeros = ln(`var' + 1)
    }

    foreach stub in "" "_zeros" {
        rename ln_totalRevenue`stub' r`stub'
        rename ln_totalWagesIESS101`stub' l`stub'
        rename ln_totalMaterials`stub' m`stub'
        rename ln_capital`stub' k`stub'

        gen ll`stub' = l`stub'^2
        gen kk`stub' = k`stub'^2
        gen mm`stub' = m`stub'^2
        gen lk`stub' = l`stub'*k`stub'
        gen lm`stub' = l`stub'*m`stub'
        gen km`stub' = k`stub'*m`stub'
    }

    save_data "../temp/balance_sheet_full", replace key(expediente year)

    use provider_id internal_firm_id using ///
        "$data_dir/proc_contracts/tables/providers_registry", clear
    duplicates drop provider_id, force
    rename provider_id firm_id

    merge 1:m firm_id using "../temp/balance_sheet_full", ///
        assert(1 2 3) keep(2 3)

    gen is_provider = _merge == 3
    drop _merge

    * Drop uncertain connections (this drops 131 obs)
    merge m:1 internal_firm_id using ///
        "$data_dir/providers_connections/master/uncertain_providers_connections_all", ///
        nogen assert(1 2 3) keep(1)
    * Drop strategic firms (this drops 30,902 obs)
    merge m:1 internal_firm_id using ///
        "$data_dir/providers_connections/master/strategic_entrants_all", ///
        nogen assert(1 2 3) keep(1)

    * Keep 2-digit sectors that contain at least 500 observations
    * (drops 16 sectors)
    restrict_sample, sector_size(500)

    xtset expediente year
    save_data "../temp/balance_sheet_full", replace key(expediente year)

    keep if is_provider == 1

    * Additionally drop a few sectors
    restrict_sample, sector_size(500)

    * 20 obs are duplicates in terms of internal_firm_id but none is politically connected
    save_data "../temp/balance_sheet_providersOnly", replace key(expediente year)
end

program build_government_sales
    use "$data_dir/proc_contracts/tables/contracts_master_with_restrictions" ///
        if contract_year>=2009, clear
    
    rename contract_year year
    gcollapse (sum) gov_sales=contract_value, by(internal_firm_id year)

    save_data "../temp/government_sales", replace key(internal_firm_id year)
end

program build_robustness_panels
    syntax, markup(str)

    use "$data_dir/providers_connections/master/providers_connections_all", clear
    merge 1:m internal_firm_id using "../temp/balance_sheet_providersOnly", ///
        nogen assert(1 2 3) keep(2 3)

    gen before_connection = provider_entry_year==. | year<provider_entry_year
    
    restrict_sample, sector_size(500)

    xtset expediente year
    save_data "../temp/balance_sheet_beforeConnection", ///
        replace key(expediente year)

    use "$data_dir/providers_connections/master/providers_connections_all", clear
    merge 1:m internal_firm_id using "../temp/balance_sheet_providersOnly", ///
        nogen assert(1 2 3) keep(2 3)

    gen after_connection = provider_entry_year!=. & year>=provider_entry_year

    * Drop duplicates (1 firm-year has duplicate)
    duplicates tag internal_firm_id year, gen(dup)
    drop if dup > 0
    drop dup
    merge 1:1 internal_firm_id year using "../temp/government_sales", ///
        nogen assert(1 2 3) keep(1 3)

    replace gov_sales = 0 if gov_sales == .
    gen share_gov = gov_sales/totalRevenue
    replace share_gov = 1 if share_gov > 1

    restrict_sample, sector_size(500)

    xtset expediente year

    foreach stub in "" "_zeros" {
        gen r_adj`stub' = r`stub' - ln(1 + share_gov*(1-1/`markup')) if after_connection == 1
        replace r`stub' = r_adj`stub' if r_adj`stub' != .
    }

    save_data "../temp/balance_sheet_revenueAdj", ///
        replace key(expediente year)
end

program restrict_sample
    syntax, sector_size(int)

    bysort isic2: gen sector_size = _N
    keep if sector_size >= `sector_size'
    drop sector_size
end

* Execute
main
