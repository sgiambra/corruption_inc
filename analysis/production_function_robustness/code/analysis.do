set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    local reps 30

    foreach stub in "_govSalesMarkup" "_beforeConnection" ///
        "_revenueAdj" "_providersOnly" "_full" {

        cap: mat drop elasticities

        foreach zeros in "" "_zeros" {
            use "../temp/balance_sheet`stub'", clear

            bootstrap_elasticities, stub(`stub') reps(`reps') zeros(`zeros')

            foreach spec in ols woold levpet {
                use "../temp/simres_`spec'`stub'`zeros'", clear
                
                foreach var in el_l el_m el_k sum {
                    qui mean `var' if rep == 0
                    local `var' = _b[`var']

                    qui sum `var' if rep != 0
                    local sd_`var' = `r(sd)'
                }

                mat el_block_`spec'`zeros' = (`el_l' \ `sd_el_l' \ `el_m' \ `sd_el_m' \ `el_k' \ `sd_el_k' \ `sum' \ `sd_sum')
                mat elasticities = (nullmat(elasticities), el_block_`spec'`zeros')
            }
        }
        
        mat rownames elasticities = "Labor" "" "Intermediate" "Inputs" "Capital" "" "Returns to" "scale"
        mat colnames elasticities = "OLS" "Wooldridge" "Levpet" "OLS (zeros)" "Wooldridge (zeros)" "Levpet (zeros)"

        esttab matrix(elasticities, fmt(%9.4f)) using "../output/elasticities_robustness`stub'.tex", replace
    }
end

program bootstrap_elasticities
    syntax, stub(str) reps(int) [zeros(str)]

    foreach spec in ols woold levpet {
        postfile simres_`spec' el_l el_m el_k sum rep /// 
            using "../temp/simres_`spec'`stub'`zeros'", replace
    }

    estimate_elasticities, rep(0) zeros(`zeros')

    forvalues i = 1/`reps' {
        preserve

            bsample, cluster(expediente) idcluster(newid) strata(isic2_nbr)
            qui xtset newid year

            estimate_elasticities, rep(`i') zeros(`zeros')
        restore
    }

    foreach spec in ols woold levpet {
        postclose simres_`spec'
    }
end

program estimate_elasticities
    syntax, rep(int) [zeros(str)]
    
    * OLS
    qui reg r`zeros' l`zeros' m`zeros' k`zeros'
    
    qui lincom l`zeros' + m`zeros' + k`zeros'

    post simres_ols (_b[l`zeros']) (_b[m`zeros']) (_b[k`zeros']) (`r(estimate)') (`rep')

    * Wooldridge
    qui ivreg2 r`zeros' k`zeros' (l`zeros' m`zeros' = L.k`zeros' L.kk`zeros' L.l`zeros' ///
        L.ll`zeros' L.m`zeros' L.mm`zeros' L.lk`zeros' L.lm`zeros' L.km`zeros'), gmm2s

    qui lincom l`zeros' + m`zeros' + k`zeros'

    post simres_woold (_b[l`zeros']) (_b[m`zeros']) (_b[k`zeros']) (`r(estimate)') (`rep')

    * Levinsohn-Petrin
    qui levpet r`zeros', free(l`zeros') proxy(m`zeros') capital(k`zeros') revenue reps(2)

    qui lincom l`zeros' + m`zeros' + k`zeros'

    post simres_levpet (_b[l`zeros']) (_b[m`zeros']) (_b[k`zeros']) (`r(estimate)') (`rep')
end

* Execute
main
