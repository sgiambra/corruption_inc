set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
adopath + ../../../lib/stata/gtools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    clean_bureaucrats_owners
    clean_shareholders
    build_tax_bureaucrats
end

program clean_bureaucrats_owners
    foreach input_file in provider_owned_by_bureaucrat ///
        provider_owned_by_sibling_of_bureaucrat {
        
        use "$data_dir/providers_connections/`input_file'", clear
        
        gcollapse (min) entry_year=owner_entry_year, by(shareholder_id)
        rename shareholder_id bureaucrat_id
        
        save_data "../temp/`input_file'", replace key(bureaucrat_id)
    }

    use "../temp/provider_owned_by_bureaucrat", clear
    rename entry_year entry_year_direct
    merge 1:1 bureaucrat_id using "../temp/provider_owned_by_sibling_of_bureaucrat", ///
        assert(1 2 3) keep(1 2 3)
    rename entry_year entry_year_indirect

    gen is_bureaucrat = _merge == 1 | _merge == 3
    gen is_bureaucrat_sib = _merge == 2

    gen entry_year = entry_year_direct if is_bureaucrat == 1
    replace entry_year = entry_year_indirect if is_bureaucrat_sib == 1

    drop entry_year_direct entry_year_indirect _merge

    save_data "../temp/bureaucrat_is_relevant_owner", ///
        replace key(bureaucrat_id)
end

program clean_shareholders
    use expediente rucFirm using "$data_dir/firms_ownership/directory.dta", replace
    rename rucFirm firm_id
    drop if firm_id == "."
    * Almost 200k rucs become of length 10
    replace firm_id = substr(firm_id, 1, 10) if ///
        substr(firm_id, -3, 3) == "001" & length(firm_id) == 13
    save_data "../temp/list_rucs_directory", key(firm_id) replace

    use expediente year using "$data_dir/balance_sheet/clean/bal2007_2017", clear
    merge m:1 expediente using "../temp/list_rucs_directory", ///
        nogen assert(1 2 3) keep(3)
    save_data "../temp/balance_sheet", replace key(expediente year)

    use provider_id internal_firm_id using ///
        "$data_dir/proc_contracts/tables/providers_registry", clear
    duplicates drop provider_id, force
    save_data "../temp/providers_registry", replace key(provider_id)

    * Keep relevant shareholders
    use "$data_dir/firms_ownership/shareholders_clean", clear
    keep if share >= 0.2

    * Keep years in which firm has balance sheet info
    merge m:1 firm_id year using "../temp/balance_sheet", ///
        nogen assert(1 2 3) keep(3)
    
    * Keep if shareholder of provider
    rename firm_id provider_id
    merge m:1 provider_id using "../temp/providers_registry", ///
        nogen assert(1 2 3) keep(3)

    keep idshareholder year
    duplicates drop

    save_data "../temp/providers_shareholder_year", ///
        replace key(idshareholder year)
end

program build_tax_bureaucrats
    use "$data_dir/Individuals/irs/clean/all_taxes", clear
    egen income = rowmax(tax_102 tax_107)
    drop tax_102 tax_107

    rename id idshareholder
    save_data "../temp/tax_data", replace key(idshareholder year)

    merge 1:1 idshareholder year using "../temp/providers_shareholder_year", ///
        nogen assert(1 2 3) keep(3)

    rename idshareholder bureaucrat_id
    merge m:1 bureaucrat_id using "../temp/bureaucrat_is_relevant_owner", ///
        nogen assert(1 2 3) keep(1 3)

    gen bureaucrat = 0 if is_bureaucrat == . & is_bureaucrat_sib == .
    replace bureaucrat = 1 if is_bureaucrat == 1 & year < entry_year
    replace bureaucrat = 2 if is_bureaucrat_sib == 1 & year < entry_year

    gen bureaucrat_before_after = 1 if is_bureaucrat == 1 & year < entry_year
    replace bureaucrat_before_after = 2 if is_bureaucrat == 1 & year >= entry_year

    gen bureaucrat_before_after_sib = 1 if is_bureaucrat_sib == 1 & year < entry_year
    replace bureaucrat_before_after_sib = 2 if is_bureaucrat_sib == 1 & year >= entry_year

    merge m:1 year using "$data_dir/balance_sheet/priceindex", ///
        assert(2 3) keep(3) nogen

    replace income = income/priceindex*100

    save_data "../temp/tax_bureaucrats", replace key(bureaucrat_id year)
end

* Execute
main
