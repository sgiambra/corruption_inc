set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
adopath + ../../../lib/stata/gtools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    use "../temp/tax_bureaucrats", clear

    gen ln_income = ln(income + 1)
    label var ln_income "ln(Paid taxes)"

    preserve
    keep if bureaucrat != .
    cdfplot ln_income, by(bureaucrat) legend(label(1 "Not bureaucrat") ///
        label(2 "Bureaucrat") label(3 "Bureaucrat sibling") size(*0.9)) ///
        opt1(lp(longdash_dot solid dash) lc(gs7 gs7 gs7))
    
    graph export "../output/cdf_difference.png", replace
    restore

    preserve
    keep if bureaucrat_before_after != .
    cdfplot ln_income, by(bureaucrat_before_after) ///
        legend(label(1 "Before entry") label(2 "After entry") size(*0.9)) ///
        opt1(lp(solid dash) lc(gs7 gs7))
    
    graph export "../output/cdf_difference_before_vs_after.png", replace

    ksmirnov ln_income, by(bureaucrat_before_after)
    restore

    preserve
    keep if bureaucrat_before_after_sib != .
    cdfplot ln_income, by(bureaucrat_before_after_sib) ///
        legend(label(1 "Before entry") label(2 "After entry") size(*0.9)) ///
        opt1(lp(solid dash) lc(gs7 gs7))
    
    graph export "../output/cdf_difference_before_vs_after_sib.png", replace

    ksmirnov ln_income, by(bureaucrat_before_after_sib)
    restore
end

* Execute
main
