set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
adopath + ../../../lib/stata/gtools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    build_providers_balance
    build_providers_connections

    use "../temp/providers_balance", clear
    merge m:1 provider_id using "../temp/providers_connections", ///
        assert(1 2 3) keep(1 3)
    gen connected = _merge == 3
    gen years_before_entry = connected == 1 & year < entry_year
    drop _merge

    gen uncon_vs_years_bef_entry     = 0 if connected == 0
    replace uncon_vs_years_bef_entry = 1 if connected == 1 & years_before_entry == 1
    replace uncon_vs_years_bef_entry = 2 if connected == 1 & years_before_entry == 0

    save_data "../temp/providers_connections_balance", ///
        key(provider_id year) replace
end

program build_providers_balance
    use provider_id internal_firm_id using ///
        "$data_dir/proc_contracts/tables/providers_registry", clear
    duplicates drop provider_id, force
    save_data "../temp/providers_registry", replace key(provider_id)

    use expediente rucFirm isic using "$data_dir/firms_ownership/directory", replace
    rename rucFirm provider_id
    drop if provider_id == "."
    * Almost 200k rucs become of length 10
    replace provider_id = substr(provider_id, 1, 10) if ///
        substr(provider_id, -3, 3) == "001" & length(provider_id) == 13

    merge 1:1 provider_id using "../temp/providers_registry", ///
        nogen assert(1 2 3) keep(3)

    save_data "../temp/list_rucs_providers", key(provider_id) replace

    use "$data_dir/balance_sheet/clean/bal2007_2017", clear
    merge m:1 expediente using "../temp/list_rucs_providers", ///
        nogen assert(1 2 3) keep(3)

    gen isic3 = substr(isic, 1, 4)
    egen isic3_nbr = group(isic3)

    gen totalMaterials = materials + otherMaterials + fuel + energy

    keep if totalRevenue!=0 & totalMaterials!=0 & capital!=0 & totalWagesIESS101!=0

    save_data "../temp/providers_balance", replace key(provider_id year)
end

program build_providers_connections
    foreach input_file in provider_owned_by_bureaucrat ///
        provider_owned_by_sibling_of_bureaucrat {
        
        use "$data_dir/providers_connections/`input_file'", clear
        replace owner_entry_year = first_year_shareholder if ///
            owner_entry_year < first_year_shareholder
        
        gcollapse (min) entry_year=owner_entry_year, by(provider_id)
        * With no restriction on entry after 2003, entry_year will bunch at 2000
        * This is not an issue since balance data starts in 2007

        save_data "../temp/`input_file'", replace key(provider_id)
    }

    use "../temp/provider_owned_by_bureaucrat", clear
    rename entry_year entry_year_direct
    merge 1:1 provider_id using "../temp/provider_owned_by_sibling_of_bureaucrat", ///
        nogen assert(1 2 3) keep(1 2 3)
    rename entry_year entry_year_indirect

    egen entry_year = rowmin(entry_year_indirect entry_year_direct)
    drop entry_year_direct entry_year_indirect

    save_data "../temp/providers_connections", replace key(provider_id)
end

* Execute
main
