set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    table_difference_mpr
    plot_smooth_production
end

program table_difference_mpr
    use "../temp/providers_connections_balance", clear

    foreach var in capital totalWagesIESS101 totalMaterials {
        gen share_`var' = totalRevenue/`var'
        winsor2 share_`var', replace cuts(1 99)

        gen ln_share_`var' = ln(share_`var')
    }
    gen ln_mrpl = ln_share_totalWagesIESS101
    gen ln_mrpk = ln_share_capital
    gen ln_mrpm = ln_share_totalMaterials

    foreach depvar in ln_mrpl ln_mrpk ln_mrpm {
        qui areg `depvar' i.year, absorb(isic3_nbr)
        predict `depvar'_res, residual

        sum `depvar'_res if connected == 0
        local mean_not_connected `r(mean)'
        sum `depvar'_res if connected == 1
        local mean_connected `r(mean)'

        * Panel A: difference connected vs unconnected
        areg `depvar' connected i.year, absorb(isic3_nbr) cluster(internal_firm_id)

        local diff    = _b[connected]
        local diff_se = _se[connected]

        * Panel B: distinguish between before and after connection
        areg `depvar' ib(0).uncon_vs_years_bef_entry i.year, ///
            absorb(isic3_nbr) cluster(internal_firm_id)

        local diff_before_entry    = _b[1.uncon_vs_years_bef_entry]
        local diff_before_entry_se = _se[1.uncon_vs_years_bef_entry]
        local diff_after_entry     = _b[2.uncon_vs_years_bef_entry]
        local diff_after_entry_se  = _se[2.uncon_vs_years_bef_entry]

        matrix column = (round(`mean_not_connected', 0.001) \ round(`mean_connected', 0.001) \ ///
            round(`diff', 0.001) \ round(`diff_se', 0.001) \ round(`diff_before_entry', 0.001) \ ///
            round(`diff_before_entry_se', 0.001) \ round(`diff_after_entry', 0.001) \ ///
            round(`diff_after_entry_se', 0.001) \ `e(N)')

        matrix table_mpr_diff = (nullmat(table_mpr_diff), column)
    }

    matrix rownames table_mpr_diff = "Not politically connected" "Politically connected" "Difference" "" ///
        "Difference before entry" "" "Difference after entry" "" "Number of observations"
    matrix colnames table_mpr_diff = "Labor" "Capital" "Materials"
    esttab matrix(table_mpr_diff) using "../output/difference_MPR.tex", replace
end

program plot_smooth_production
    foreach stub in "" "_pre_years_only" {
        cap: matrix drop table_production_diff

        if "`stub'" == "" {
            local connection_type "connected"
        }
        if "`stub'" == "_pre_years_only" {
            local connection_type "uncon_vs_years_bef_entry"
        }
        
        use "../temp/providers_connections_balance" if ///
            `connection_type' == 0 | `connection_type' == 1, clear

        gen variable_inputs = totalWagesIESS101 + totalMaterials

        foreach depvar in totalRevenue capital totalWagesIESS101 totalMaterials variable_inputs {
            winsor2 `depvar', cuts(1 99)
            gen ln_`depvar' = ln(`depvar'_w)
            
            qui reg ln_`depvar' i.isic3_nbr i.year
            predict ln_`depvar'_res, residual
        }

        label var ln_totalRevenue_res       "Log residualized revenue"
        label var ln_totalWagesIESS101_res  "Log residualized wages"
        label var ln_totalMaterials_res     "Log residualized materials"
        label var ln_capital_res            "Log residualized capital"
        label var ln_variable_inputs_res    "Log residualized variable inputs"

        foreach depvar in totalWagesIESS101 capital totalMaterials variable_inputs {
            qui sum ln_`depvar'_res, det
            local lower_bound `r(p1)'
            local upper_bound `r(p99)'
            local nbr_obs `r(N)'

            twoway (lpolyci ln_totalRevenue_res ln_`depvar'_res if `connection_type' == 0 & ///
                ln_`depvar'_res > `lower_bound' & ln_`depvar'_res < `upper_bound', fintensity(inten60)) ///
                (lpolyci ln_totalRevenue_res ln_`depvar'_res if `connection_type' == 1 & ///
                ln_`depvar'_res > `lower_bound' & ln_`depvar'_res < `upper_bound', clpattern(dash) clc(black) fintensity(inten60)), ///
                legend(order(2 4) label(2 "Not politically connected") label(4 "Politically connected")) ///
                xtitle(`: var label ln_`depvar'_res') ytitle(`: var label ln_totalRevenue_res')

            graph export "../output/smooth_production_`depvar'`stub'.png", replace

            lpoly ln_totalRevenue_res ln_`depvar'_res if `connection_type' == 0 & ///
                ln_`depvar'_res > `lower_bound' & ln_`depvar'_res < `upper_bound', ///
                gen(`depvar'_not_connected revenue_not_connected) se(revenue_not_connected_se) nograph
            lpoly ln_totalRevenue_res ln_`depvar'_res if `connection_type' == 1 & ///
                ln_`depvar'_res > `lower_bound' & ln_`depvar'_res < `upper_bound', ///
                gen(revenue_connected) at(`depvar'_not_connected) se(revenue_connected_se) nograph

            gen diff_revenue = revenue_not_connected - revenue_connected
            qui sum diff_revenue
            local mean_diff `r(mean)'

            gen se_diff_revenue = sqrt(revenue_not_connected_se^2 + revenue_connected_se^2)
            qui sum se_diff_revenue
            local se_diff `r(mean)'

            matrix column = (round(`mean_diff', 0.001) \ round(`se_diff', 0.001) \ `nbr_obs')
            matrix table_production_diff = (nullmat(table_production_diff), column)

            drop diff_revenue revenue_connected revenue_not_connected se_diff_revenue ///
                revenue_not_connected_se revenue_connected_se
        }
        matrix rownames table_production_diff = "Mean" "SE" "Number observations"
        matrix colnames table_production_diff = "Wages" "Capital" "Materials" "Variable inputs"
        esttab matrix(table_production_diff) using "../output/difference_production_funcrion`stub'.tex", replace
    }
end

* Execute
main
