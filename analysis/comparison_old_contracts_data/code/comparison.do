set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    * New contracts for which we have winner
    use "$data_dir/proc_contracts/tables/winners_table", clear
    collapse (sum) contract_value, by(contract_link internal_firm_id)
    merge m:1 contract_link using "$data_dir/proc_contracts/tables/contracts_table", ///
        nogen assert(2 3) keep(3)

    bys contract_code internal_firm_id: gen to_keep = _n == 1
    drop if to_keep == 0 & dup == 1

    drop if contract_state == "Desierta"
    rename contract_year year
    replace contract_code = subinstr(contract_code, " ","",.)
    keep year contract_code internal_firm_id contract_state contract_type contract_value contract_budget

    save "../temp/new_contracts", replace

    * Old contracts for which we have winner
    use "../temp/old_contracts_by_winner", clear
    rename (firm_id contract_id value budget type_contract) (old_firm_id contract_code old_value old_budget old_type_contract)
    replace contract_code = subinstr(contract_code, " ","",.)
    drop if contract_code == ""
    keep year contract_code old_firm_id old_value old_budget old_type_contract

    save "../temp/old_contracts", replace

    * Comparison
    use "../temp/new_contracts", clear
    merge m:m contract_code year using  "../temp/old_contracts"

    * For matching contracts budget and value are multiples of 10
    keep if _merge == 3

    foreach var in old_value old_budget {
        replace `var' = subinstr(`var', ".", "", .)
        replace `var' = subinstr(`var', ",", ".", .)
        destring `var', replace force
    }
    foreach var in value budget {
        gen ln_ratio_`var' = ln(contract_`var'/old_`var')
        hist ln_ratio_`var'
        graph export "../output/ln_ratio_`var'_hist.png", replace
    }
end

program standardize_ids
    replace provider_id = "0" + provider_id if length(provider_id) == 9 | length(provider_id) == 12
    replace provider_id = substr(provider_id, 1, 10) if substr(provider_id, -3, 3) == "001" & ///
        (length(provider_id) == 13 | length(provider_id) == 14)

    replace provider_id = "." if provider_id == "" | regexm(provider_id , "^[0]+$")
end

program standardize_code
    replace contract_code = subinstr(contract_code, "-","",.)
    replace contract_code = subinstr(contract_code, "_","",.)
    replace contract_code = subinstr(contract_code, "?","",.)
end

main
