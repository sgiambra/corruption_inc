set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries
set excelxlsxlargefile on

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    import excel "${data_dir}/proc_contracts/contracts_2016-2018.xlsx", firstrow allstring clear
    
    rename *, lower
    rename (valoradjudicado presupuesto añoadjudicación) (value budget year)
    
    clean_contracts_data
    destring year, replace
    save_data "../temp/contracts_2016-2018.dta", key(rownum) replace
    
    import excel "${data_dir}/proc_contracts/contracts_2009-2015_exported.xlsx", ///
        sheet("contracts_2009-2015") firstrow allstring clear
    
    rename *, lower
    rename (valoradjudicado presupuesto añoadjudicación) (value budget year)

    replace year = subinstr(year, ".", "", .)
    replace year = "2010" if year == "201"
    destring year, replace force
    drop if year == .

    clean_contracts_data
    save_data "../temp/contracts_2009-2015.dta", key(rownum) replace
    append using "../temp/contracts_2016-2018.dta"

    * Remove 2016 duplicates
    duplicates drop contract_id value firm_id year, force
    save "../temp/old_contracts_by_winner", replace
end

program clean_contracts_data
    rename (rucproveedor códigoproceso) (firm_id contract_id)
    
    drop if firm_id == "" | firm_id == "NO DEFINIDO"
    replace firm_id = substr(firm_id, 1, 13) if strlen(firm_id) > 13
    replace firm_id = "0" + firm_id if strlen(firm_id) == 12
    replace firm_id = "0" + firm_id if strlen(firm_id) == 9
    replace firm_id = firm_id + "001" if strlen(firm_id) == 10 
    
    gen type_contract = "auction" if strpos(tipodecontratación, "Subasta") > 0
    cap: replace type_contract = "random" if strpos(tipodecontratación, "Menor") > 0 & tipodecompra == "Obra"
    replace type_contract = "random" if type_contract == "" & strpos(tipodecontratación, "Menor") > 0 & strpos(contract_id,"MCO") > 0
    replace type_contract = "discretionary" if type_contract == ""
    
    keep budget value year type_contract firm_id contract_id
    duplicates drop contract_id value firm_id year, force

    gen rownum = _n
end

* Execute
main
