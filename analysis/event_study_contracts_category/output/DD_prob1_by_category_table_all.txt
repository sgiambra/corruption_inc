	(1)	(2)	(3)	(4)
VARIABLES	CS all	CS auction	CS discretionary	CS random
				
post_entry	0.0294***	0.0111*	0.0371***	0.0001
	(0.0102)	(0.0063)	(0.0096)	(0.0032)
				
Observations	.	181790	.	181790
Observations	181790	.	181790	.
Number contractors	27838	27838	27838	27838
Connected contractors	4841	4841	4841	4841
Mean before connection	0.218	0.0639	0.164	0.0321
Standard errors in parentheses				
*** p<0.01, ** p<0.05, * p<0.1				
