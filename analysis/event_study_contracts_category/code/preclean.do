cd "/Users/felipe/Dropbox (Brown)/Research/corruption_inc/analysis/event_study_contracts_category/code"

set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    build_balance
    build_providers_panel

    bureaucrats_winners_panel
end

program build_balance
    use provider_id internal_firm_id using ///
        "$data_dir/proc_contracts/tables/providers_registry", clear
    duplicates drop provider_id, force
    save_data "../temp/providers_registry", replace key(provider_id)

    use expediente rucFirm startYear isic using ///
        "$data_dir/firms_ownership/directory", replace
    rename rucFirm provider_id
    drop if provider_id == "."
    * Almost 200k rucs become of length 10
    replace provider_id = substr(provider_id, 1, 10) if ///
        substr(provider_id, -3, 3) == "001" & length(provider_id) == 13

    gen isic1 = substr(isic, 1, 2)

    merge 1:1 provider_id using "../temp/providers_registry", ///
        nogen assert(1 2 3) keep(3)

    * Same entry years for all 4 duplicates
    duplicates drop internal_firm_id, force

    save_data "../temp/firms_entry_years", key(internal_firm_id) replace

    use expediente year using "$data_dir/balance_sheet/clean/bal2007_2017", clear
    merge m:1 expediente using "../temp/firms_entry_years", ///
        nogen assert(1 2 3) keep(3)

    preserve
        keep internal_firm_id startYear isic1
        duplicates drop
        save_data "../temp/list_firm_providers", key(internal_firm_id) replace
    restore

    drop startYear isic1
    save_data "../temp/providers_balance_panel", replace key(internal_firm_id year)
	
end

program build_providers_panel
	* Build Panel 
    use "$data_dir/proc_contracts/tables/contracts_master_with_restrictions" ///
        if contract_year>=2009, clear

    rename contract_year year
    
    replace contract_cat = "random" if contract_cat == "menor_obras"
    replace contract_cat = "discretionary" if contract_cat != "random" & contract_cat != "auction"

        
    * Not use geographic dimension in event study
    gcollapse (sum) contract_value nbr_contracts=id, by(internal_firm_id year contract_cat)
    save "../temp/winners_table", replace
	
    * Create rectangular panel, starting in 2009
    clear
    set obs 9
    gen year = 2008 + _n
    gen joinvar = 1
    save_data "../temp/panel_years", replace key(year)

    use "../temp/winners_table", clear
    keep contract_cat
    duplicates drop
    gen joinvar = 1
    save_data "../temp/panel_categories", replace key(contract_cat)

    use "../temp/providers_registry", clear
    keep internal_firm_id
    duplicates drop
    gen joinvar = 1
    joinby joinvar using "../temp/panel_years"
    joinby joinvar using "../temp/panel_categories"
    drop joinvar

    * _merge == 2 would derive from contracts won in 2008
    merge 1:1 internal_firm_id year contract_cat using "../temp/winners_table", ///
        nogen assert(1 2 3) keep(1 3)
    replace contract_value = 0 if contract_value == .
    replace nbr_contracts = 0 if nbr_contracts == .
	

    save_data "../temp/providers_panel", replace ///
        key(internal_firm_id year contract_cat)
end

program bureaucrats_winners_panel
    use "$data_dir/providers_connections/master/providers_connections_all", clear

    * Keep if _merge == 2 if want to include control group
    merge 1:m internal_firm_id using "../temp/providers_panel", ///
        nogen assert(1 2 3) keep(2 3)
    merge m:1 internal_firm_id using "../temp/list_firm_providers", ///
        nogen assert(1 3) keep(3)
    * Keep if _merge == 1 if want to include firms that do not submit balance sheet in a given year
    merge m:1 internal_firm_id year using "../temp/providers_balance_panel", ///
        nogen assert(1 2 3) keep(3)
    
    * Drop uncertain connections
    drop if provider_entry_year == 2000
    * Drop strategic entrants
    drop if strategic_entrant == 1
    * Drop if created by bureaucrat
    drop if created_by_bureaucrat == 1
	
	rename (nbr_contracts contract_value) (nbr_contracts_ contract_value_)
	
    gen prob1_  = (nbr_contracts_ > 0) & (contract_value_ >= 100)
    gen prob5_  = (nbr_contracts_ > 0) & (contract_value_ >= 800)
    gen prob10_ = (nbr_contracts_ > 0) & (contract_value_ >= 3000)
    gen prob25_ = (nbr_contracts_ > 0) & (contract_value_ >= 15000)
	
  
    keep if year >= startYear

    reshape wide contract_value_ nbr_contracts_ prob1_ prob5_ prob10_ prob25_, i(internal_firm_id year) ///
        j(contract_cat) string

	egen t_nbr_contracts = rowtotal(nbr_contracts*)
	egen t_contract_value = rowtotal(contract_value_*)
	
    gen prob1_all	= (t_nbr_contracts > 0) & (t_contract_value >= 100)
    gen prob5_all	= (t_nbr_contracts > 0) & (t_contract_value >= 800)
    gen prob10_all	= (t_nbr_contracts > 0) & (t_contract_value >= 3000)
    gen prob25_all	= (t_nbr_contracts > 0) & (t_contract_value >= 15000)
	

    * Define treatment status and post_entry
    gen treatment = provider_entry_year != .
    gen post_entry = year >= provider_entry_year & treatment == 1
	
	  * Drop if always treated
	gen relative_ev_year = year - provider_entry_year
	bysort internal_firm_id: egen  m_relative_ev_year=min(relative_ev_year)
	drop if m_relative_ev_year>=0 & m_relative_ev_year!=.
	 
	egen fid=group(internal_firm_id)
	replace provider_entry_year=10000000000 if provider_entry_year==.


    save_data "../temp/providers_connections_all_reg_panel", ///
        replace key(internal_firm_id year)
end

* Execute
main
