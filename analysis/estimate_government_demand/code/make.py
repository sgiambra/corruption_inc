import subprocess
import os
import shutil
import datetime

dir_path = os.path.dirname(os.path.realpath('__file__'))
os.chdir(dir_path)

# Clear ouput and temp directory
clear_dirs = ['../output', '../temp']
for directory in clear_dirs:
    if not os.path.exists(directory):
        os.makedirs(directory)

    for file in os.listdir(directory):
        file_path = os.path.join(directory, file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path): shutil.rmtree(file_path)
        except Exception as e:
            print(e)

def dostata(dofile, *params):
    cmd = ["/Program Files (x86)/Stata15/StataMP-64","/e","do", dofile]         
    for param in params:
        cmd.append(param)
    print('Executing: ' + dofile)
    return (subprocess.call(cmd, cwd=r'../output/'))

do_files = ['preclean', 
            'analysis']

for do_file in do_files:
    dostata('../code/' + do_file + '.do')

with open('../output/make.log', 'w') as outfile:
    print(datetime.datetime.now(), file=outfile)

    for fname in do_files:
        with open('../output/' + fname + '.log') as infile:
            outfile.write(infile.read())
        os.remove('../output/' + fname + '.log')