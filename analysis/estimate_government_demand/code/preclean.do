set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    build_price_index
    build_instrument
end

program build_price_index
    import delimited "$data_dir/price_index_WB/API_FP.CPI.TOTL_DS2_en_csv_v2_126205.csv", ///
        varnames(5) rowrange(5) clear

    foreach v of varlist v* {
       local x : variable label `v'
       rename `v' priceindex`x'
    }
    
    keep if countryname == "Ecuador"
    drop priceindex
    reshape long priceindex, i(countryname) j(year)
    keep year priceindex

    save_data "../temp/priceindex", replace key(year)
end

program build_instrument
    import delimited "$data_dir/tariffs_export_shocks/Concordance_H3_to_CP/JobID-42_Concordance_H3_to_CP.CSV", /// 
        encoding(utf8) stringcol(1 3) clear

    rename (hs2007productcode cpcproductcode) (hs07_str cpc)
    keep hs07_str cpc

    save_data "../temp/hs_cpc_correspondence", replace key(hs07_str)

    import delimited "$data_dir/tariffs_export_shocks/DP_LIVE_23102019175948474.csv", ///
        encoding(utf8) clear
    
    replace location = lower(location)
    keep if time >= 2008
    rename time year

    egen location_nbr = group(location)
    xtset location_nbr year
    gen D_exc = ln(value) - ln(L.value)

    keep location year D_exc
    keep if year >= 2009

    save_data "../temp/change_exchange_rate", replace key(location year)

    use "$data_dir/tariffs_export_shocks/year_origin_destination_hs07_6" /// 
        if origin == 65 & year >= 2009, clear

    build_shocks, trade_var(import)
end

program build_shocks
    syntax, trade_var(str) 
    
    preserve
        decode `trade_var'_val, gen(`trade_var'_val_nbr)
        destring `trade_var'_val_nbr, replace force

        decode dest, gen(location)

        gen hs07_str = string(hs07,"%06.0f")

        bys year hs07: egen tot_`trade_var' = total(`trade_var'_val_nbr)
        gen share_`trade_var' = `trade_var'_val_nbr / tot_`trade_var'

        keep location year hs07_str share_`trade_var'

        merge m:1 location year using "../temp/change_exchange_rate", ///
            nogen assert(1 2 3) keep(3)

        egen country_product = group(location hs07_str)
        xtset country_product year
        
        gen shock_`trade_var' = D_exc*L.share_`trade_var'

        drop if shock_`trade_var' == .

        gcollapse (sum) shock_`trade_var', by(hs07_str year)

        merge m:1 hs07_str using "../temp/hs_cpc_correspondence", ///
            nogen assert(1 2 3) keep(3)

        gcollapse (mean) shock_`trade_var', by(cpc year)

        save_data "../temp/product_`trade_var'_shocks", ///
            replace key(cpc year)
    restore
end

* Execute
main
