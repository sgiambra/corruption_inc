set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    use "$data_dir/proc_contracts/tables/contracts_master_with_restrictions" ///
        if contract_year >= 2009 & id == 1, clear

    rename contract_year year
    gcollapse (mean) year agg_contract_value contract_budget, by(contract_link)

    merge 1:m contract_link using "$data_dir/proc_contracts/scraped/clean/products.dta", ///
        nogen assert(1 2 3) keep(3)

    gen cpc = substr(product_id, 1, 5)

	replace product_unit_price=agg_contract_value/product_quantity
    gcollapse (mean) product_unit_price (sum) product_quantity, by(cpc year)

    merge m:1 year using "../temp/priceindex", ///
        assert(2 3) keep(3) nogen

    replace product_unit_price = product_unit_price/priceindex*100

    merge 1:1 cpc year using "../temp/product_import_shocks", ///
        nogen assert(1 2 3) keep(1 3)

    rename cpc cpc5
    gen cpc = substr(cpc5, 1, 4)

    merge m:1 cpc year using "../temp/product_import_shocks", ///
        nogen assert(1 2 3) keep(1 3)

    drop cpc
    gen cpc = substr(cpc5, 1, 3)

    merge m:1 cpc year using "../temp/product_import_shocks", ///
        nogen assert(1 2 3) keep(1 3)

    drop cpc
    rename cpc5 cpc

    gen cpc2 = substr(cpc, 1, 2)
    egen cpc2_nbr = group(cpc2)

    egen cpc_nbr = group(cpc)
    xtset cpc_nbr year

    gen D_ln_q = ln(product_quantity) - ln(L.product_quantity)
    gen D_ln_p = ln(product_unit_price) - ln(L.product_unit_price)

    build_elasticity_table
end

program build_elasticity_table
    reg D_ln_q D_ln_p, cluster(cpc_nbr)
    outreg2 using "../output/government_demand_elasticity.tex", ///
        replace dec(4) tex addtext(CPC-2 FE, No)

    reg D_ln_q D_ln_p i.cpc2_nbr, cluster(cpc_nbr)
    outreg2 using "../output/government_demand_elasticity.tex", ///
        append dec(4) tex addtext(CPC-2 FE, Yes) drop(i.cpc2_nbr)

    ivreg2 D_ln_q (D_ln_p = shock_import), cluster(cpc_nbr)
    outreg2 using "../output/government_demand_elasticity.tex", ///
        append dec(4) tex addtext(CPC-2 FE, No)

    ivreg2 D_ln_q (D_ln_p = shock_import) i.cpc2_nbr, cluster(cpc_nbr)
    outreg2 using "../output/government_demand_elasticity.tex", ///
        append dec(4) tex addtext(CPC-2 FE, Yes) drop(i.cpc2_nbr)
end

* Execute
main
