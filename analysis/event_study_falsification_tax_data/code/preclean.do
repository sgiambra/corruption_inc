set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    local window 4

    build_balance
    build_providers_panel

    bureaucrats_winners_panel, window(`window')
end

program build_balance
    * Get price index
    import delimited "$data_dir/price_index_WB/API_FP.CPI.TOTL_DS2_en_csv_v2_126205.csv", ///
        varnames(5) rowrange(5) clear

    foreach v of varlist v* {
       local x : variable label `v'
       rename `v' priceindex`x'
    }
    
    keep if countryname == "Ecuador"
    drop priceindex
    reshape long priceindex, i(countryname) j(year)
    keep year priceindex

    save_data "../temp/priceindex", replace key(year)

    use provider_id internal_firm_id using ///
        "$data_dir/proc_contracts/tables/providers_registry", clear
    duplicates drop provider_id, force
    save_data "../temp/providers_registry", replace key(provider_id)

    * Get balance sheet data
    use expediente rucFirm isic startYear active using ///
        "$data_dir/firms_ownership/directory", replace
    rename rucFirm provider_id
    drop if provider_id == "."
    * Almost 200k rucs become of length 10
    replace provider_id = substr(provider_id, 1, 10) if ///
        substr(provider_id, -3, 3) == "001" & length(provider_id) == 13

    gen isic1 = substr(isic, 1, 2)
    save_data "../temp/list_rucs_directory", key(provider_id) replace

    use "$data_dir/balance_sheet/clean/bal2007_2017", clear

    gen totalMaterials = materials + otherMaterials + fuel + energy
    gen variable_inputs = totalWagesIESS101 + totalMaterials

    keep capital totalWagesIESS101 totalRevenue totalMaterials totalAsset variable_inputs expediente year

    merge m:1 expediente using "../temp/list_rucs_directory", ///
        nogen assert(1 2 3) keep(3)
    merge m:1 provider_id using "../temp/providers_registry", ///
        nogen assert(1 2 3) keep(3)

    merge m:1 year using "../temp/priceindex", ///
        assert(2 3) keep(3) nogen

    foreach var in capital totalWagesIESS101 totalRevenue totalMaterials variable_inputs {
        replace `var' = `var'/priceindex*100
    }
    foreach var in capital totalWagesIESS101 totalMaterials variable_inputs {
        gen share_`var' = totalRevenue/`var'
    }

    * Two firms missclassified as same by internal_firm_id
    duplicates drop internal_firm_id year, force

    preserve
        keep internal_firm_id startYear isic1
        duplicates drop internal_firm_id, force
        save_data "../temp/list_firm_providers", key(internal_firm_id) replace
    restore

    drop startYear isic1
    save_data "../temp/providers_balance_panel", replace key(internal_firm_id year)
end

program build_providers_panel
    use "$data_dir/proc_contracts/tables/contracts_master_with_restrictions" ///
        if contract_year>=2009, clear

    rename contract_year year
    gcollapse (sum) contract_value nbr_contracts=id, by(internal_firm_id year)

    save_data "../temp/winners_table", replace key(internal_firm_id year)

    * Create rectangular panel, starting in 2009
    clear
    set obs 9
    gen year = 2008 + _n
    gen joinvar = 1
    save_data "../temp/panel_years", replace key(year)

    use "../temp/providers_registry", clear
    keep internal_firm_id
    duplicates drop
    gen joinvar = 1
    joinby joinvar using "../temp/panel_years"
    drop joinvar

    * _merge == 2 would derive from contracts that have paiments in 2008 or after 2017
    merge 1:1 internal_firm_id year using "../temp/winners_table", ///
        nogen assert(1 2 3) keep(1 3)
    replace contract_value = 0 if contract_value == .
    replace nbr_contracts = 0 if nbr_contracts == .

    save_data "../temp/providers_panel", replace key(internal_firm_id year)
end

program bureaucrats_winners_panel
    syntax, window(int)
    
    use "$data_dir/providers_connections_tax_entry/master/providers_connections_all", clear

    * Keep if _merge == 2 if want to include control group
    merge 1:m internal_firm_id using "../temp/providers_panel", ///
        nogen assert(1 2 3) keep(2 3)
    merge m:1 internal_firm_id using "../temp/list_firm_providers", ///
        nogen assert(1 3) keep(3)
    * Keep if _merge == 1 if want to include firms that do not submit balance sheet in a given year
    merge 1:1 internal_firm_id year using "../temp/providers_balance_panel", ///
        nogen assert(1 2 3) keep(3)
    
    * Drop uncertain connections (first connection year is 2007)
    drop if provider_entry_year == 2007
    * Drop strategic entrants
    drop if strategic_entrant == 1
    * Drop created by bureaucrat
    drop if created_by_bureaucrat == 1
    
    * Drop real treated firms
    merge m:1 internal_firm_id using ///
        "$data_dir/providers_connections/master/providers_connections_all", ///
        nogen assert(1 2 3) keep(1)

    * Just a few gaps so no need to fill them

    gen prob = nbr_contracts > 0

    * Regression window groups
    generate_reggroups, window(`window') ///
        connection_date(provider_entry_year)
        
    replace relative_ev_year_reggroups = 4 if provider_entry_year == . 
    
    keep if year >= startYear

    gen prob_zero_revenue = totalRevenue == 0

    gen priv_sales = totalRevenue - contract_value

    * Note: government share is only defined for observations with positive revenue
    gen share_gov = contract_value/totalRevenue
    winsor2 share_gov, replace cuts(1 99)

    label var prob                  "Prob(Awarded contract)"
    label var contract_value        "Contract value"
    label var nbr_contracts         "Number of contracts"
    label var capital               "Capital"
    label var totalWagesIESS101     "Wages"
    label var totalRevenue          "Revenue"
    label var totalMaterials        "Materials"
    label var variable_inputs       "Variable inputs"
    label var priv_sales            "Private sales"
    label var share_gov             "Share revenue from government sales"

    save_data "../temp/providers_connections_all", replace key(internal_firm_id year)
end

program generate_reggroups
    syntax, window(int) connection_date(str)

    gen relative_ev_year = year - `connection_date'

    sort internal_firm_id relative_ev_year
    gen relevant_time_period = (abs(relative_ev_year) <= `window')
    egen relative_ev_year_reggroups = group(relative_ev_year) if relevant_time_period
    replace relative_ev_year_reggroups = 0 if relative_ev_year < -`window'

    replace relative_ev_year_reggroups = 1000 if relative_ev_year > `window' & `connection_date' != .

    label var relative_ev_year_reggroups    "Years relative to entry into tax system"
end

* Execute
main
