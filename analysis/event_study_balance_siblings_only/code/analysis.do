cd "/Users/felipe/Dropbox (Brown)/Research/corruption_inc/analysis/event_study_balance_siblings_only/code"

set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

program main
    cap: mkdir "../output/tables"
    cap: mkdir "../output/figures"

    use "../temp/providers_connections_all", clear

    * Drop if always treated
	bysort internal_firm_id: egen  m_relative_ev_year=min(relative_ev_year)
	drop if m_relative_ev_year>=0 & m_relative_ev_year!=.
	
	* Keep if Owned only by Sibling
	drop if owned_by_bureaucrat == 1 
		 
	egen fid=group(internal_firm_id)
	replace provider_entry_year=10000000000 if provider_entry_year==.
	
	event_study_figures, window(4)
	
end
 	
program event_study_figures
	syntax, window(integer)


	*===========================================================================
	* Figures: Callaway and Sant'Anna
	*===========================================================================
 
	foreach depvar in prob1 prob5 prob10 prob25 ln_contract_value ln_nbr_contracts {
		
			staggered_cs, y("`depvar'") g("provider_entry_year") t("year") i("fid") estimand("eventstudy") eventTimeStart(-`window') eventTimeEnd(`window')
			matrix coefs = e(b)
			matrix var_cov = e(V)
				
			qui sum `depvar' if relative_ev_year < 0
			forval i = 1/9 {  
				matrix coefs[1,`i']= coefs[1,`i'] + `r(mean)'
			}
		
			coefplot matrix(coefs), v(var_cov) vertical yline(`r(mean)', lpattern(dash)) ytitle(`: var label `depvar'')         ///
			xtitle(`: var label relative_ev_year_reggroups') levels(95 90) ylabel(`ylabel')                      ///
			xlab(1 "-4" 2 "-3" 3 "-2" 4 "-1" 5 "Event" 6"1" 7 "2" 8"3" 9 "4") graphregion(color(white))  
			graph export "../output/figures/`depvar'_sibling.png", replace
		
	}	
end

* Execute
main
