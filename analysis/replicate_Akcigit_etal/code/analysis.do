set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    use "${data_dir}/firms_yearly_master.dta", clear
    merge m:1 expediente using "${data_dir}/derived_data/firms_family_connections.dta", ///
        keep(1 3) assert(1 2 3) keepusing(entry_year entry_highLevel entry_elected exit_year)

    *drop if exit_year < 2007

    gen pc_elected    = year >= entry_elected & _merge == 3
    gen pc_bureaucrat = year >= entry_year & _merge == 3 & entry_elected == .
    drop _merge

    egen isic2_nbr = group(isic2)
    gen isic1 = substr(isic, 1, 1)
    egen isic1_nbr = group(isic1)
    egen province_nbr = group(province)
    drop if age < 0
    gen ln_age = ln(age + 1)

    bys isic2: egen sector_revenue = total(totalRevenue)
    bys isic2: egen sector_wages = total(totalWagesIESS101)
    gen labor_share = sector_wages / sector_revenue

    gen tfp = ln_totalRevenue - labor_share*ln_totalWagesIESS101 - (1-labor_share)*ln_totalAsset
    winsor2 tfp, cuts(1 99) replace

    xtset expediente year
    * Table 6
    reghdfe D.ln_valueAdded pc_elected pc_bureaucrat ln_totalAsset ln_totalWagesIESS101 ln_age, ///
        absorb(year isic2_nbr province_nbr) cluster(expediente)
    outreg2 using "../output/akcigit.tex", replace ///
        addtext(Year FE, YES, Sector FE, YES, Province FE, YES, Firm FE, NO) nor2
    reghdfe D.ln_valueAdded pc_elected pc_bureaucrat ln_totalAsset ln_totalWagesIESS101 ln_age, ///
        absorb(year expediente) cluster(expediente)
    outreg2 using "../output/akcigit.tex", append ///
        addtext(Year FE, YES, Sector FE, NO, Province FE, NO, Firm FE, YES) nor2

    * Table 7
    reghdfe D.tfp pc_elected pc_bureaucrat ln_totalAsset ln_totalWagesIESS101 ln_age, ///
        absorb(year isic2_nbr province_nbr) cluster(expediente)
    outreg2 using "../output/akcigit.tex", append ///
        addtext(Year FE, YES, Sector FE, YES, Province FE, YES, Firm FE, NO) nor2
    reghdfe D.tfp pc_elected pc_bureaucrat ln_totalAsset ln_totalWagesIESS101 ln_age, ///
        absorb(year expediente) cluster(expediente)
    outreg2 using "../output/akcigit.tex", append ///
        addtext(Year FE, YES, Sector FE, NO, Province FE, NO, Firm FE, YES) nor2
end

* Execute
main
