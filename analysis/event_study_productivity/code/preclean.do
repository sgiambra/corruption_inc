set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    local window 4

    build_providers_panel

    foreach stub in "_providersOnly" "_beforeConnection" ///
        "_revenueAdj" "_full" "_govSalesMarkup" {
        
        build_balance, stub(`stub')
        bureaucrats_winners_panel, window(`window') stub(`stub')
    }
end

program build_providers_panel
    use provider_id internal_firm_id using ///
        "$data_dir/proc_contracts/tables/providers_registry", clear
    duplicates drop provider_id, force
    save_data "../temp/providers_registry", replace key(provider_id)

    clear
    set obs 9
    gen year = 2008 + _n
    gen joinvar = 1
    save_data "../temp/panel_years", replace key(year)

    use "../temp/providers_registry", clear
    keep internal_firm_id
    duplicates drop
    gen joinvar = 1
    joinby joinvar using "../temp/panel_years"
    drop joinvar

    save_data "../temp/providers_panel", ///
        replace key(internal_firm_id year)
end

program build_balance
    syntax, stub(str)

    use "$data_dir/balance_sheet/derived/balance_sheet`stub'_tfp" ///
        if is_provider==1, clear
    merge m:1 expediente using "$data_dir/firms_ownership/directory", ///
        nogen assert(2 3) keep(3) keepusing(startYear)

    * Two firms missclassified as same by internal_firm_id
    duplicates drop internal_firm_id year, force

    preserve
        keep internal_firm_id startYear
        duplicates drop internal_firm_id, force
        save_data "../temp/list_firm_providers`stub'", key(internal_firm_id) replace
    restore

    drop startYear
    save_data "../temp/providers_balance_panel`stub'", replace key(internal_firm_id year)
end

program bureaucrats_winners_panel
    syntax, window(int) stub(str)

    use "$data_dir/providers_connections/master/providers_connections_all", clear

    * Keep if _merge == 2 if want to include control group
    merge 1:m internal_firm_id using "../temp/providers_panel", ///
        nogen assert(1 2 3) keep(2 3)
    merge m:1 internal_firm_id using "../temp/list_firm_providers`stub'", ///
        nogen assert(1 3) keep(3)
    * Keep if _merge == 1 if want to include firms that do not submit balance sheet in a given year
    merge 1:1 internal_firm_id year using "../temp/providers_balance_panel`stub'", ///
        nogen assert(1 2 3) keep(3)
    
    * Strategi entrants and uncertain connections already dropped

    generate_reggroups, window(`window') ///
        connection_date(provider_entry_year)

    * Set control group to -1
    replace relative_ev_year_reggroups = 4 if provider_entry_year == .

    keep if year >= startYear

    * Define treatment status and post_entry
    gen treatment = provider_entry_year != .
    gen post_entry = year >= provider_entry_year & treatment == 1

    label var tfp_ols               "TFP (OLS)"
    label var tfp_woold             "TFP (LP-Wooldridge)"

    save_data "../temp/providers_connections_all`stub'", replace key(internal_firm_id year)
end

program generate_reggroups
    syntax, window(int) connection_date(str)

    gen relative_ev_year = year - `connection_date'

    sort internal_firm_id relative_ev_year
    gen relevant_time_period = (abs(relative_ev_year) <= `window')
    egen relative_ev_year_reggroups = group(relative_ev_year) if relevant_time_period
    replace relative_ev_year_reggroups = 0 if relative_ev_year < -`window'

    replace relative_ev_year_reggroups = 1000 if relative_ev_year > `window' & `connection_date' != .

    label var relative_ev_year_reggroups    "Years relative to first political connection"
end

* Execute
main
