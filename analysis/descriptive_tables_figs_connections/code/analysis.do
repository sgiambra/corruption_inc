set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    local max_years 10

    build_tables
    build_draft_figures, max_years(`max_years')
    build_non_draft_figures, max_years(`max_years')
end

program build_tables
    use "../temp/providers_connections_panel", clear

    ** Sample sizes **
    egen tag = tag(internal_firm_id)

    build_sample_size_block
    mat table = (nullmat(table), r(sample_col))

    build_sample_size_block, and_condition("& direct_connection_only==1")
    mat table = (nullmat(table), r(sample_col))

    build_sample_size_block, and_condition("& indirect_connection_only==1")
    mat table = (nullmat(table), r(sample_col))

    build_sample_size_block, and_condition("& both_connections_type==1")
    mat table = (nullmat(table), r(sample_col))

    matrix rownames table = "Nbr non-politically connected" "Not conn to strat bureauc" ///
        "Avg number of connections" "Med number of connections" "Conn to strat bureauc" ///
        "Avg number of connections" "Med number of connections" "Created by bureaucrat" ///
        "Avg number of connections" "Med number of connections"
    matrix colnames table = "All connections" "Only direct" "Only indirect" "Both direct and indirect"

    esttab matrix(table, fmt(%12.3fc)) using "../output/sample_sizes.tex", replace
end

program build_draft_figures
    syntax, max_years(int)

    use "../temp/providers_connections_panel" if ///
        connected != ., clear
    replace years_after_created = `max_years' + 1 if years_after_created > `max_years'

    * Drop strategic firms
    drop if strategic_entrant == 1

    areg prob connected#ib(0).years_after_created ln_assets, ///
        absorb(year) cluster(internal_firm_id)
    
    mat coefs = e(b)
    mat vcov = e(V)

    local last_unc  = `max_years' + 2
    local first_con = `max_years' + 3
    local last_con  = 2*`max_years' + 4

    mat coefs_unc  = coefs[1, 1..`last_unc']'
    mat coefs_conn = coefs[1, `first_con'..`last_con']'
    mat v_unc  = vecdiag(vcov[1..`last_unc', 1..`last_unc'])'
    mat v_conn = vecdiag(vcov[`first_con'..`last_con', `first_con'..`last_con'])'

    clear
    foreach mat in coefs_unc coefs_conn v_unc v_conn {
        svmat `mat'
        rename `mat'1 `mat'
    }
    foreach var in unc conn {
        gen ll_`var' = coefs_`var' - 1.96*sqrt(v_`var')
        gen ul_`var' = coefs_`var' + 1.96*sqrt(v_`var')
    }
    gen years_after_created = _n - 1
    local outside_window = `max_years' + 1
    
    twoway (scatter coefs_unc years_after_created, msymbol(smcircle)) ///
        (scatter coefs_conn years_after_created, msymbol(smtriangle)) ///
        (rcap ul_conn ll_conn years_after_created) (rcap ul_unc ll_unc years_after_created), /// 
        legend(order(1 2) label(1 "Never connected") label(2 "After connection") size(*0.8)) ///
        ytitle("Prob(Awarded contract)") xtitle("Firm age") ///
        xlabel(0(6)`max_years' `outside_window' "> `max_years'")

    graph export "../output/conditional_prob_by_age.png", replace
end

program build_non_draft_figures
    syntax, max_years(int)

    set scheme s1color
    use "../temp/providers_connections_panel", clear
    replace years_after_created = `max_years' + 1 if years_after_created > `max_years'

    areg prob i.rel_ownership#ib(0).years_after_created ln_assets, ///
        absorb(year) cluster(internal_firm_id)
    
    mat coefs = e(b)
    mat vcov = e(V)

    local last_unc = `max_years' + 2
    
    local first_con_bef = `max_years' + 3
    local last_con_bef  = 2*`max_years' + 4
    
    local first_con_aft = 2*`max_years' + 5
    local last_con_aft = 3*`max_years' + 6

    local first_strat_bef = 3*`max_years' + 7
    local last_strat_bef = 4*`max_years' + 8

    local first_strat_aft = 4*`max_years' + 9
    local last_strat_aft = 5*`max_years' + 10

    local first_new = 5*`max_years' + 11
    local last_new = 6*`max_years' + 12

    mat coefs_unc  = coefs[1, 1..`last_unc']'
    mat coefs_con_bef = coefs[1, `first_con_bef'..`last_con_bef']'
    mat coefs_con_aft = coefs[1, `first_con_aft'..`last_con_aft']'
    mat coefs_strat_bef = coefs[1, `first_strat_bef'..`last_strat_bef']'
    mat coefs_strat_aft = coefs[1, `first_strat_aft'..`last_strat_aft']'
    mat coefs_new = coefs[1, `first_new'..`last_new']'

    mat v_unc  = vecdiag(vcov[1..`last_unc', 1..`last_unc'])'
    mat v_con_bef = vecdiag(vcov[`first_con_bef'..`last_con_bef', `first_con_bef'..`last_con_bef'])'
    mat v_con_aft = vecdiag(vcov[`first_con_aft'..`last_con_aft', `first_con_aft'..`last_con_aft'])'
    mat v_strat_bef = vecdiag(vcov[`first_strat_bef'..`last_strat_bef', `first_strat_bef'..`last_strat_bef'])'
    mat v_strat_aft = vecdiag(vcov[`first_strat_aft'..`last_strat_aft', `first_strat_aft'..`last_strat_aft'])'
    mat v_new = vecdiag(vcov[`first_new'..`last_new', `first_new'..`last_new'])'

    clear
    foreach mat in coefs_unc coefs_con_bef coefs_con_aft coefs_strat_bef coefs_strat_aft coefs_new ///
        v_unc v_con_bef v_con_aft v_strat_bef v_strat_aft v_new {
        svmat `mat'
        rename `mat'1 `mat'
    }
    foreach var in unc con_bef con_aft strat_bef strat_aft new {
        gen ll_`var' = coefs_`var' - 1.96*sqrt(v_`var')
        gen ul_`var' = coefs_`var' + 1.96*sqrt(v_`var')
    }
    gen years_after_created = _n - 1
    local outside_window = `max_years' + 1
    
    twoway (scatter coefs_unc years_after_created) (scatter coefs_con_bef years_after_created) ///
        (scatter coefs_con_aft years_after_created) (scatter coefs_strat_bef years_after_created) ///
        (scatter coefs_strat_aft years_after_created) (scatter coefs_new years_after_created), ///
        legend(order(1 2 3 4 5 6) label(1 "Never connected") label(2 "Before connection") ///
            label(3 "After connection") label(4 "Before strategic connection") ///
            label(5 "After strategic connection") label(6 "Created after") size(*0.8)) ///
        ytitle("Prob(Awarded contract)") xtitle("Firm age") ///
        xlabel(0(6)`max_years' `outside_window' "> `max_years'")

    graph export "../output/conditional_prob_by_age_all_categories.png", replace
end

program build_sample_size_block, rclass
    syntax, [and_condition(str)]

    qui distinct internal_firm_id if rel_ownership==0
    local nbr_uncon `r(ndistinct)'

    qui distinct internal_firm_id if (rel_ownership==1 | ///
        rel_ownership==2) `and_condition'
    local nbr_non_strat `r(ndistinct)'

    qui sum distinct_conn_years if tag==1 & (rel_ownership==1 | ///
        rel_ownership==2) `and_condition', det
    local avg_con_non_strat `r(mean)'
    local med_con_non_strat `r(p50)'

    qui distinct internal_firm_id if (rel_ownership==3 | ///
        rel_ownership==4) `and_condition'
    local nbr_strategic `r(ndistinct)'

    qui sum distinct_conn_years if tag==1 & (rel_ownership==3 | ///
        rel_ownership==4) `and_condition', det
    local avg_con_strat `r(mean)'
    local med_con_strat `r(p50)'

    qui distinct internal_firm_id if rel_ownership==5 `and_condition'
    local nbr_created_by_bureauc `r(ndistinct)'

    qui sum distinct_conn_years if tag==1 & rel_ownership==5 `and_condition', det
    local avg_con_created `r(mean)'
    local med_con_created `r(p50)'

    mat sample_col = (floor(`nbr_uncon') \ floor(`nbr_non_strat') \ round(`avg_con_non_strat', .001) \ ///
        round(`med_con_non_strat', .001) \ floor(`nbr_strategic') \ round(`avg_con_strat', .001) \ round(`med_con_strat', .001) \ ///
        floor(`nbr_created_by_bureauc') \ round(`avg_con_created', .001) \ round(`med_con_created', .001))

    return mat sample_col = sample_col
end

* Execute
main
