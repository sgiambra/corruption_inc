set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    build_balance
    build_providers_panel
    bureaucrats_winners_panel
end

program build_balance
    * Get price index
    import delimited "$data_dir/price_index_WB/API_FP.CPI.TOTL_DS2_en_csv_v2_126205.csv", ///
        varnames(5) rowrange(5) clear

    foreach v of varlist v* {
       local x : variable label `v'
       rename `v' priceindex`x'
    }
    
    keep if countryname == "Ecuador"
    drop priceindex
    reshape long priceindex, i(countryname) j(year)
    keep year priceindex

    save_data "../temp/priceindex", replace key(year)

    use provider_id internal_firm_id using ///
        "$data_dir/proc_contracts/tables/providers_registry", clear
    duplicates drop provider_id, force
    save_data "../temp/providers_registry", replace key(provider_id)

    * Get balance sheet data
    use expediente rucFirm isic startYear using ///
        "$data_dir/firms_ownership/directory", replace
    rename rucFirm provider_id
    drop if provider_id == "."
    * Almost 200k rucs become of length 10
    replace provider_id = substr(provider_id, 1, 10) if ///
        substr(provider_id, -3, 3) == "001" & length(provider_id) == 13

    gen isic1 = substr(isic, 1, 2)
    save_data "../temp/list_rucs_directory", key(provider_id) replace

    use expediente year totalRevenue totalAsset using ///
        "$data_dir/balance_sheet/clean/bal2007_2017", clear

    merge m:1 expediente using "../temp/list_rucs_directory", ///
        nogen assert(1 2 3) keep(3)
    merge m:1 provider_id using "../temp/providers_registry", ///
        nogen assert(1 2 3) keep(3)

    merge m:1 year using "../temp/priceindex", ///
        assert(2 3) keep(3) nogen

    foreach var in totalRevenue totalAsset {
        replace `var' = `var'/priceindex*100
    }

    * Two firms missclassified as same by internal_firm_id
    duplicates drop internal_firm_id year, force

    save_data "../temp/providers_balance_panel", ///
        replace key(internal_firm_id year)
end

program build_providers_panel
    use "$data_dir/proc_contracts/tables/contracts_master_with_restrictions" ///
        if contract_year>=2009, clear
    
    rename contract_year year
    gcollapse (sum) contract_value nbr_contracts=id, by(internal_firm_id year)

    save_data "../temp/winners_table", replace key(internal_firm_id year)

    * Create rectangular panel, excluding 2008
    clear
    set obs 9
    gen year = 2008 + _n
    gen joinvar = 1
    save_data "../temp/panel_years", replace key(year)

    use "../temp/providers_registry", clear
    keep internal_firm_id
    duplicates drop
    gen joinvar = 1
    joinby joinvar using "../temp/panel_years"
    drop joinvar

    * _merge == 2 would derive from contracts that have paiments in 2008 or after 2017
    merge 1:1 internal_firm_id year using "../temp/winners_table", ///
        nogen assert(1 2 3) keep(1 3)
    replace contract_value = 0 if contract_value == .
    replace nbr_contracts = 0 if nbr_contracts == .

    save_data "../temp/providers_panel", replace key(internal_firm_id year)
end

program bureaucrats_winners_panel
    use "$data_dir/providers_connections/master/providers_connections_all", clear

    gen direct_connection_only   = provider_entry_year_direct != . & provider_entry_year_indirect == .
    gen indirect_connection_only = provider_entry_year_direct == . & provider_entry_year_indirect != .
    gen both_connections_type    = provider_entry_year_direct != . & provider_entry_year_indirect != .

    merge 1:m internal_firm_id using "../temp/providers_panel", ///
        nogen assert(2 3) keep(2 3)

    gen prob = nbr_contracts > 0

    merge 1:1 internal_firm_id year using "../temp/providers_balance_panel", ///
        nogen assert(1 2 3) keep(3)
    
    * Drop uncertain connections
    drop if provider_entry_year == 2000

    keep if year >= startYear

    gen ln_assets = ln(totalAsset)
    gen years_after_created = year - startYear

    foreach var in direct_connection_only indirect_connection_only both_connections_type {
        replace `var' = 0 if `var' == .
    }
    
    gen connected = .
    replace connected = 1 if year >= provider_entry_year
    replace connected = 0 if direct_connection_only == 0 & /// 
        indirect_connection_only == 0 & both_connections_type == 0

    * Comparison with strategic owners and firms created by bureaucrats
    gen rel_ownership = 0 if provider_entry_year == .
    
    * Firms connected to non-strategic bureaucrat (before or after connection)
    replace rel_ownership = 1 if created_by_bureaucrat == 0 & strategic_entrant == 0 & ///
        provider_entry_year != . & year < provider_entry_year
    replace rel_ownership = 2 if created_by_bureaucrat == 0 & strategic_entrant == 0 & ///
        provider_entry_year != . & year >= provider_entry_year

    * Firms connected to strategic bureaucrat (before or after connection)
    replace rel_ownership = 3 if created_by_bureaucrat == 0 & strategic_entrant == 1 & ///
        provider_entry_year != . & year < provider_entry_year
    replace rel_ownership = 4 if created_by_bureaucrat == 0 & strategic_entrant == 1 & ///
        provider_entry_year != . & year >= provider_entry_year

    * Firms created by bureaucrat
    replace rel_ownership = 5 if created_by_bureaucrat == 1

    save_data "../temp/providers_connections_panel", ///
        replace key(internal_firm_id year)
end

* Execute
main
