set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

program main
    use "../temp/providers_connections_all", clear

    prepare_factor_info relative_ev_year if relevant_time_period
    local reg_opts = r(reg_opts)

    foreach var in prob r l m k ln_variable_inputs ln_totalDebt ln_totalAsset {
        event_study_plot, depvar(`var') saving("`var'_not_bureaucrat") `reg_opts'
    }
end

program prepare_factor_info, rclass
    syntax anything(name=factor_var) [if]

    qui levelsof `factor_var' `if', local(factor_levels)
    local num_factor_levels: word count `factor_levels'
    local median_factor_value = (`num_factor_levels' + 1)/2
    local base_factor_value = `median_factor_value' - 1
    return local reg_opts = "factor_levels(`factor_levels') "               + ///
                            "num_factor_levels(`num_factor_levels') "       + ///
                            "median_factor_value(`median_factor_value')"    + ///
                            "base_factor_value(`base_factor_value')"
end

program event_study_plot
    syntax [if], depvar(str) factor_levels(str) num_factor_levels(str) ///
        median_factor_value(str) base_factor_value(str) saving(str) *
    
    qui levelsof relative_ev_year_reggroups, local(reggroups)
    local nbr_reggroups: word count `reggroups'

    areg `depvar' ib(`base_factor_value').relative_ev_year_reggroups i.year `if', ///
        absorb(internal_firm_id) vce(cluster internal_firm_id)

    matrix coefs = e(b)
    matrix var_cov = e(V)

    qui sum `depvar' if relative_ev_year < 0
    forval i = 1/`nbr_reggroups' {  
        matrix coefs[1,`i']= coefs[1,`i'] + `r(mean)'
    }

    coefplot matrix(coefs), v(var_cov) vertical yline(`r(mean)', lpattern(dash)) ytitle(`: var label `depvar'')              ///
        xtitle(`: var label relative_ev_year_reggroups') baselevels levels(95 90)         ///
        rename(1.relative_ev_year_reggroups="-4" 2.relative_ev_year_reggroups="-3" 3.relative_ev_year_reggroups="-2"    ///
               4.relative_ev_year_reggroups="-1" 5.relative_ev_year_reggroups="Event" 6.relative_ev_year_reggroups="1"  ///
               7.relative_ev_year_reggroups="2" 8.relative_ev_year_reggroups="3" 9.relative_ev_year_reggroups="4")      ///
        drop(0.relative_ev_year_reggroups 1000.relative_ev_year_reggroups *year _cons)

    graph export "../output/`saving'.png", replace
end

* Execute
main
