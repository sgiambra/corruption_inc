set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
adopath + ../../../lib/stata/gtools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    build_balance
    build_providers_panel

    use "$data_dir/providers_connections/master/providers_connections_all", clear
    merge 1:m internal_firm_id using "../temp/providers_panel", ///
        nogen assert(1 2 3) keep(2 3)
    * Drop uncertain connections
    merge m:1 internal_firm_id using ///
        "$data_dir/providers_connections/master/uncertain_providers_connections_all", ///
        nogen assert(1 2 3) keep(1)
    * Drop strategic firms
    merge m:1 internal_firm_id using ///
        "$data_dir/providers_connections/master/strategic_entrants_all", ///
        nogen assert(1 2 3) keep(1)

    gen connected = provider_entry_year != .

    save_data "../temp/providers_connections_panel", ///
        replace key(internal_firm_id year)
end

program build_balance
    * Get price index
    import delimited "$data_dir/price_index_WB/API_FP.CPI.TOTL_DS2_en_csv_v2_126205.csv", ///
        varnames(5) rowrange(5) clear

    foreach v of varlist v* {
       local x : variable label `v'
       rename `v' priceindex`x'
    }
    
    keep if countryname == "Ecuador"
    drop priceindex
    reshape long priceindex, i(countryname) j(year)
    keep year priceindex

    save_data "../temp/priceindex", replace key(year)

    use provider_id internal_firm_id using ///
        "$data_dir/proc_contracts/tables/providers_registry", clear
    duplicates drop provider_id, force
    save_data "../temp/providers_registry", replace key(provider_id)

    * Get balance sheet data
    use expediente rucFirm isic startYear using ///
        "$data_dir/firms_ownership/directory", replace
    rename rucFirm provider_id
    drop if provider_id == "."
    * Almost 200k rucs become of length 10
    replace provider_id = substr(provider_id, 1, 10) if ///
        substr(provider_id, -3, 3) == "001" & length(provider_id) == 13

    gen isic2 = substr(isic, 1, 3)
    egen isic2_nbr = group(isic2)
    save_data "../temp/list_rucs_directory", key(provider_id) replace

    use expediente year capital totalWagesIESS101 totalRevenue materials otherMaterials fuel energy ///
        using "$data_dir/balance_sheet/clean/bal2007_2017", clear

    merge m:1 expediente using "../temp/list_rucs_directory", ///
        nogen assert(1 2 3) keep(3)
    merge m:1 provider_id using "../temp/providers_registry", ///
        nogen assert(1 2 3) keep(3)

    merge m:1 year using "../temp/priceindex", ///
        assert(2 3) keep(3) nogen

    * Two firms missclassified as same by internal_firm_id
    duplicates drop internal_firm_id year, force

    gen totalMaterials = materials + otherMaterials + fuel + energy

    foreach var in capital totalWagesIESS101 totalRevenue totalMaterials {
        replace `var' = `var'/priceindex*100
        winsor2 `var' if `var' > 0, replace cuts(1 99)
        gen ln_`var' = ln(`var')
    }

    preserve
        keep internal_firm_id startYear isic2_nbr
        duplicates drop internal_firm_id, force

        save_data "../temp/providers_start_year", replace key(internal_firm_id)
    restore

    keep internal_firm_id year totalRevenue ln_*

    save_data "../temp/providers_balance_panel", ///
        replace key(internal_firm_id year)
end

program build_providers_panel
    clear
    set obs 11
    gen year = 2006 + _n
    gen joinvar = 1
    save_data "../temp/panel_years", replace key(year)

    use "../temp/providers_registry", clear
    keep internal_firm_id
    duplicates drop
    gen joinvar = 1
    joinby joinvar using "../temp/panel_years"
    drop joinvar

    merge m:1 internal_firm_id using "../temp/providers_start_year", ///
        nogen assert(1 3) keep(3)
    merge 1:1 internal_firm_id year using "../temp/providers_balance_panel", ///
        nogen assert(1 3) keep(1 3)

    keep if year >= startYear

    replace totalRevenue = 0 if totalRevenue == .

    bys internal_firm_id (year): gen exit = totalRevenue > 0 & ///
        totalRevenue[_n+1] <= 0 & totalRevenue[_n+2] <= 0 
    
    * Always drop last two years since not defined
    drop if year == 2016 | year == 2017

    * Drop after last exit
    bys internal_firm_id (year): egen year_exit = max(year*exit)
    replace year_exit = . if year_exit == 0
    drop if year > year_exit

    * Consider only last exit
    replace exit = 0 if year != year_exit

    gen age = year - startYear

    save_data "../temp/providers_panel", replace key(internal_firm_id year)
end

* Execute
main
