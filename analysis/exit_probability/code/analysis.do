set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
adopath + ../../../lib/stata/gtools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    /*  NOTE: by using ihs transformation instead of log we find that actual exit
        probability is slightly higher (for a few ages) than the predicted one
    */
    local max_age 15

    actual_exit_prob, max_age(`max_age')
    predicted_exit_prob, max_age(`max_age')
end

program actual_exit_prob
    syntax, max_age(int)

    use "../temp/providers_connections_panel" if age <= `max_age' & ln_totalRevenue != ., clear

    gcollapse (mean) exit (sem) se_exit=exit, by(connected age)

    reshape wide exit se_exit, i(age) j(connected)

    forvalues conn=0/1 {
        gen ll`conn' = exit`conn' - 1.96*se_exit`conn'
        gen ul`conn' = exit`conn' + 1.96*se_exit`conn'
    }

    twoway (scatter exit0 age, msymbol(smcircle) mc(gs8)) ///
        (scatter exit1 age, msymbol(smtriangle) mc(gs3)) /// 
        (rcap ul0 ll0 age, lc(gs8)) (rcap ul1 ll1 age, lc(gs3)), ///
        legend(order(1 2) label(1 "Non-politically connected") ///
        label(2 "Politically connected") size(*0.8)) ///
        ytitle("Prob(Exit)") xtitle("Firm age")

    graph export "../output/unconditional_exit_prob_by_age.png", replace

    use "../temp/providers_connections_panel", clear

    replace age = 999 if age > `max_age'

    areg exit connected##ib(0).age ln_capital ln_totalWagesIESS101 ln_totalMaterials ///
        i.isic2_nbr, absorb(year) cluster(internal_firm_id)
    
    matrix coefs = e(b)
    matrix vcov = e(V)

    local coefs_start = 2*`max_age' + 7 + 1
    local coefs_end = 3*`max_age' + 7

    matrix coefs_conn  = coefs[1, `coefs_start'..`coefs_end']'
    matrix v_conn  = vecdiag(vcov[`coefs_start'..`coefs_end', `coefs_start'..`coefs_end'])'

    clear
    foreach mat in coefs_conn v_conn {
        svmat `mat'
    }
    gen ll_conn = coefs_conn - 1.96*sqrt(v_conn)
    gen ul_conn = coefs_conn + 1.96*sqrt(v_conn)
    
    gen age = _n
    
    twoway (scatter coefs_conn age, msymbol(smtriangle) mc(gs5)) ///
        (rcap ul_conn ll_conn age, lc(gs5)), yline(0, lp(dash)) /// 
        legend(off) ytitle("Coefficient") xtitle("Firm age")

    graph export "../output/conditional_exit_prob_by_age.png", replace
end

program predicted_exit_prob
    syntax, max_age(int)

    use "../temp/providers_connections_panel" if connected == 0, clear
    replace age = 999 if age > `max_age'

    areg exit ib(0).age ln_capital ln_totalWagesIESS101 ln_totalMaterials ///
        i.isic2_nbr, absorb(year) cluster(internal_firm_id)

    use "../temp/providers_connections_panel" if connected == 1, clear
    replace age = 999 if age > `max_age'
    predict exit_predicted

    keep if exit_predicted != .
    drop if age == 999

    gcollapse (mean) exit exit_predicted (sem) se_exit=exit ///
        se_exit_predicted=exit_predicted, by(age)

    foreach var in exit exit_predicted {
        gen ll_`var' = `var' - 1.96*se_`var'
        gen ul_`var' = `var' + 1.96*se_`var'
    } 

    twoway (scatter exit_predicted age, msymbol(smcircle) mc(gs3)) ///
        (scatter exit age, msymbol(smtriangle) mc(gs8)) /// 
        (rcap ul_exit_predicted ll_exit_predicted age, lc(gs3)) ///
        (rcap ul_exit ll_exit age, lc(gs8)), /// 
        legend(order(1 2) label(1 "Predicted") label(2 "Actual") size(*0.8)) ///
        ytitle("Prob(Exit)") xtitle("Firm age")

    graph export "../output/predicted_vs_actual_exit_prob_by_age.png", replace
end

* Execute
main
