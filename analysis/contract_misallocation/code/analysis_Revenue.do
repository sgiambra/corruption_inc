set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "C:\Dropbox\corruption_inc\"



program main
    local reps 30

    foreach stub in "_providersOnly" "_beforeConnection" ///
        "_revenueAdj" "_full" "_govSalesMarkup" {

        compute_misallocation_estimates, stub(`stub') reps(`reps')
    }

    * Build misallocation table
    file open excess_costs_table using "../output/excess_costs_2.tex", write replace
    file write excess_costs_table "& Flexible OLS & Flexible Woold & Fixed OLS & Fixed Woold \\" _n

    foreach stub in "_providersOnly" "_beforeConnection" ///
        "_revenueAdj" "_full" "_govSalesMarkup" {

        build_misallocation_table, stub(`stub')
    }

    file close excess_costs_table
end

program compute_misallocation_estimates
    syntax, reps(int) stub(str)
    foreach spec in Ols Woold {
	
        use "../temp/providers_balance`stub'_tfp_connections", clear
        qui levelsof isic2_nbr, local(sectors)
		foreach sector in `sectors' {
		
		preserve
	
		keep if isic2_nbr == `sector'

        * Assume CRTS and initialize variables
        gen bk_`spec'_crts = 1 - bl_`spec' - bm_`spec'

        gen flex_capital_`spec' = .
        gen fixed_capital_`spec' = .

        gen sampleSize_flex_`spec' = .
        gen sampleSize_fixed_`spec' = .


        forvalues rep = 0(1)`reps' {
	
                dis "Specification `spec'`stub', Repetition `rep', Sector `sector'"

               qui reg tfp_`spec' connected i.year i.isic3_nbr if ///
                    isic2_nbr == `sector' & rep == `rep' & in_sample_`spec' == 1

                local tfp_gap_`spec'_mean = _b[connected]
                local sample_flex_`spec' = e(N)

                qui reg s_k connected i.year i.isic3_nbr if ///
                    isic2_nbr == `sector' & rep == `rep' & in_sample_`spec' == 1

                local s_k_gap_`spec'_mean = _b[connected]
                local sample_fixed_`spec' = e(N)
			
				
                * Save estimates
                replace flex_capital_`spec' = exp(-`tfp_gap_`spec'_mean')-1 if ///
                    isic2_nbr == `sector' & rep == `rep' & in_sample_`spec' == 1

                replace sampleSize_flex_`spec' = `sample_flex_`spec'' if ///
                    isic2_nbr == `sector' & rep == `rep' & in_sample_`spec' == 1

                replace fixed_capital_`spec' = exp(-(bk_`spec'_crts/(1-bk_`spec'_crts))*`s_k_gap_`spec'_mean' - ///
                    1/(1-bk_`spec'_crts)*`tfp_gap_`spec'_mean')-1 if ///
                    isic2_nbr == `sector' & rep == `rep' & in_sample_`spec' == 1

                replace sampleSize_fixed_`spec' = `sample_fixed_`spec'' if ///
                    isic2_nbr == `sector' & rep == `rep' & in_sample_`spec' == 1
            }
        

        gen flex_welfare_cost_`spec' = (flex_capital_`spec'*cost_share_uncon)*100
        gen fixed_welfare_cost_`spec' = (fixed_capital_`spec'*cost_share_uncon)*100

        gcollapse (mean) flex_capital_`spec' fixed_capital_`spec' flex_welfare_cost_`spec' fixed_welfare_cost_`spec' ///
            firms_`spec' sampleSize_flex_`spec' sampleSize_fixed_`spec', by(isic2_nbr rep)
    
        save_data "../temp/misallocation_Estimates_sectors`stub'_`spec'_`sector'_2", replace key(isic2_nbr rep)
		restore
		}
		
		qui levelsof isic2_nbr, local(sectors)
		clear
		foreach sector in `sectors' { 
		append using "../temp/misallocation_Estimates_sectors`stub'_`spec'_`sector'_2"
		save_data "../temp/misallocation_Estimates_sectors`stub'_`spec'_2", replace key(isic2_nbr rep)
		}
			
        keep if rep>0

        collapse (mean) flex_capital_`spec' fixed_capital_`spec' flex_welfare_cost_`spec' ///
            fixed_welfare_cost_`spec' firms_`spec' [aweight=firms_`spec'], by(rep)

        collapse (sd) flex_capital_`spec' fixed_capital_`spec' flex_welfare_cost_`spec' ///
            fixed_welfare_cost_`spec'

        save "../temp/misallocation_EstimatesSE`stub'_`spec'_2", replace

        use "../temp/misallocation_Estimates_sectors`stub'_`spec'_2" if rep==0, clear
        collapse (sum) sampleSize_flex_`spec' sampleSize_fixed_`spec'
        gen id = _n

        save "../temp/misallocation_EstimatesSample`stub'_`spec'_2", replace

        use "../temp/misallocation_Estimates_sectors`stub'_`spec'_2" if rep==0, clear
        
        collapse (mean) flex_capital_`spec' fixed_capital_`spec' flex_welfare_cost_`spec' ///
            fixed_welfare_cost_`spec' [aweight=firms_`spec']

        append using "../temp/misallocation_EstimatesSE`stub'_`spec'_2"
        gen id = _n

        merge 1:1 id using "../temp/misallocation_EstimatesSample`stub'_`spec'_2", nogen

        save "../temp/misallocation_Estimates`stub'_`spec'_2", replace
    }

    * Save estimates for sectoral analysis
    use "../temp/misallocation_Estimates_sectors`stub'_Ols_2", clear
    merge 1:1 isic2_nbr rep using "../temp/misallocation_Estimates_sectors`stub'_Woold_2", ///
        nogen assert(3) keep(3)

    export delimited "../output/misallocation_Estimates_sectors`stub'_2.csv", replace
end

program build_misallocation_table
    syntax, stub(str)

    use "../temp/misallocation_Estimates`stub'_Ols_2", clear

    merge 1:1 id using "../temp/misallocation_Estimates`stub'_Woold_2", nogen keep(3)

    rename (flex_capital_Ols flex_capital_Woold flex_welfare_cost_Ols flex_welfare_cost_Woold ///
        fixed_capital_Ols fixed_capital_Woold fixed_welfare_cost_Ols fixed_welfare_cost_Woold) ///
        (flex_Ols_Ec flex_Woold_Ec flex_Ols_Welfare flex_Woold_Welfare ///
        fixed_Ols_Ec fixed_Woold_Ec fixed_Ols_Welfare fixed_Woold_Welfare)

    foreach var in Ec Welfare {
        foreach cap_assum in flex fixed {
            foreach spec in Ols Woold {
                qui sum `cap_assum'_`spec'_`var' if id == 1
                local m_`cap_assum'_`spec'_`var' = round(`r(mean)', 0.001)

                qui sum `cap_assum'_`spec'_`var' if id == 2
                local se_`cap_assum'_`spec'_`var' = round(`r(mean)', 0.001)

                qui sum sampleSize_`cap_assum'_`spec'
                local ss_`cap_assum'_`spec' = `r(mean)'

                if (abs(`m_`cap_assum'_`spec'_`var'') >= `se_`cap_assum'_`spec'_`var''*1.644854 & ///
                    abs(`m_`cap_assum'_`spec'_`var'') < `se_`cap_assum'_`spec'_`var''*1.95996) {
                    local m_`cap_assum'_`spec'_`var' = "`m_`cap_assum'_`spec'_`var''*"
                }
                else if (abs(`m_`cap_assum'_`spec'_`var'') >= `se_`cap_assum'_`spec'_`var''*1.95996 & ///
                    abs(`m_`cap_assum'_`spec'_`var'') < `se_`cap_assum'_`spec'_`var''*2.32635) {
                    local m_`cap_assum'_`spec'_`var' = "`m_`cap_assum'_`spec'_`var''**"
                }
                else if abs(`m_`cap_assum'_`spec'_`var'') >= `se_`cap_assum'_`spec'_`var''*2.32635 {
                    local m_`cap_assum'_`spec'_`var' = "`m_`cap_assum'_`spec'_`var''***"
                }
            }
        }

        if "`var'" == "Ec" & "`stub'" == "_providersOnly" local panel "No markup adjustment"
        else if "`var'" == "Ec" & "`stub'" == "_beforeConnection" local panel "Before political connection"
        else if "`var'" == "Ec" & "`stub'" == "_revenueAdj" local panel "Markup adjustment"
        else if "`var'" == "Ec" & "`stub'" == "_full" local panel "All firms"
        else if "`var'" == "Ec" & "`stub'" == "_govSalesMarkup" local panel "Main specification"
        else if "`var'" == "Welfare" local panel ""

        file write excess_costs_table "`panel' & `m_flex_Ols_`var'' & `m_flex_Woold_`var'' & `m_fixed_Ols_`var'' & `m_fixed_Woold_`var'' \\" _n
        file write excess_costs_table "& (`se_flex_Ols_`var'') & (`se_flex_Woold_`var'') & (`se_fixed_Ols_`var'') & (`se_fixed_Woold_`var'') \\" _n

        if "`var'" == "Welfare" {
            file write excess_costs_table "& `ss_flex_Ols' & `ss_flex_Woold' & `ss_fixed_Ols' & `ss_fixed_Woold'" _n
        }
    }
end

* Execute
main
