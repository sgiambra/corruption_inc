cd "/Users/felipe/Dropbox (Brown)/Research/corruption_inc/analysis/event_study_falsification_fake_years/code"

set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

program main

	
    use "../temp/providers_connections_all", clear


    foreach var in prob1 prob5 prob10 prob25  {
	
            qui distinct internal_firm_id
            local nbr_firms `r(ndistinct)'
            
            qui sum `var' if year < provider_entry_year & treatment == 1
            local mean_before_conn `r(mean)'

            qui distinct internal_firm_id if treatment == 1
            local nbr_connected_firms `r(ndistinct)'
			
			qui sum `var'
			local nbr_obs `r(N)'

      		* Callaway and Sant'Anna
			staggered_cs, y("`var'") g("provider_entry_year") t("year") i("fid") estimand("simple") 
			ereturn list 
			matrix coef= e(b)
			matrix colname coef = post_entry 
			erepost b=coef, rename

			outreg2 using "../output/DD_`var'_table.tex",   dec(4) append ctitle(SA) ///
			    adds(Observations, `nbr_obs' , Number contractors, `nbr_firms', Connected contractors, `nbr_connected_firms', ///
                Mean before connection, `mean_before_conn') tex keep(post_entry) 
    }
	

end


* Execute
main
