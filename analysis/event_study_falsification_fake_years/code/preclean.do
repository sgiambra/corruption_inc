cd "/Users/felipe/Dropbox (Brown)/Research/corruption_inc/analysis/event_study_falsification_fake_years/code"
set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    local window 4

    build_balance
    build_providers_panel

    bureaucrats_winners_panel, window(`window')
end

program build_balance
    use provider_id internal_firm_id using ///
        "$data_dir/proc_contracts/tables/providers_registry", clear
    duplicates drop provider_id, force
    save_data "../temp/providers_registry", replace key(provider_id)

    use "$data_dir/balance_sheet/master/balance_sheet_clean" if contractor==1, clear

    * Two firms missclassified as same by internal_firm_id
    duplicates drop internal_firm_id year, force

    preserve
        keep internal_firm_id startYear isic3
        duplicates drop internal_firm_id, force
        save_data "../temp/list_firm_providers", key(internal_firm_id) replace
    restore

    drop startYear isic3
    save_data "../temp/providers_balance_panel", replace key(internal_firm_id year)
end

program build_providers_panel
    use "$data_dir/proc_contracts/tables/contracts_master_with_restrictions" ///
        if contract_year>=2009, clear

    rename contract_year year
    gcollapse (sum) contract_value nbr_contracts=id, by(internal_firm_id year)

    save_data "../temp/winners_table", replace key(internal_firm_id year)

    * Create rectangular panel, starting in 2009
    clear
    set obs 9
    gen year = 2008 + _n
    gen joinvar = 1
    save_data "../temp/panel_years", replace key(year)

    use "../temp/providers_registry", clear
    keep internal_firm_id
    duplicates drop
    gen joinvar = 1
    joinby joinvar using "../temp/panel_years"
    drop joinvar

    * _merge == 2 would derive from contracts that have paiments in 2008 or after 2017
    merge 1:1 internal_firm_id year using "../temp/winners_table", ///
        nogen assert(1 2 3) keep(1 3)
    replace contract_value = 0 if contract_value == .
    replace nbr_contracts = 0 if nbr_contracts == .

    save_data "../temp/providers_panel", replace key(internal_firm_id year)
end

program bureaucrats_winners_panel
    syntax, window(int)

    use "../temp/providers_panel", clear
    merge m:1 internal_firm_id using "../temp/list_firm_providers", ///
        nogen assert(1 3) keep(3)
    * Keep if _merge == 1 if want to include firms that do not submit balance sheet in a given year
    merge 1:1 internal_firm_id year using "../temp/providers_balance_panel", ///
        nogen assert(1 2 3) keep(3)
    
    * No need to drop strategic or uncertain because they are excluded in next merge
    
    * Drop treated firms
    merge m:1 internal_firm_id using ///
        "$data_dir/providers_connections/master/providers_connections_all", ///
        nogen assert(1 2 3) keep(1) keepusing(internal_firm_id)
    
    * Drop also firms connected to families having between 4 and 10 siblings
    merge m:1 internal_firm_id using ///
        "$data_dir/providers_connections_large_families/master/providers_connections_all_medium", ///
        nogen assert(1 2 3) keep(1) keepusing(internal_firm_id)

    gen prob1  = (nbr_contracts > 0) & (contract_value >= 100)
    gen prob5  = (nbr_contracts > 0) & (contract_value >= 800)
    gen prob10 = (nbr_contracts > 0) & (contract_value >= 3000)
    gen prob25 = (nbr_contracts > 0) & (contract_value >= 15000)

    gen ln_contract_value = ln(1 + contract_value)
    gen ln_nbr_contracts  = ln(1 + nbr_contracts)
    
    * Treatment group fake entry year ~real entry distribution
    bys internal_firm_id (year): gen firstob = _n == 1
    gen random_nbr = 100*runiform() if firstob == 1
    bys internal_firm_id (random_nbr): replace random_nbr = random_nbr[1]

    gen provider_entry_year = .
    replace provider_entry_year = 2001 if random_nbr <= 0.2
    replace provider_entry_year = 2002 if random_nbr > 0.2 & random_nbr <= 0.4
    replace provider_entry_year = 2003 if random_nbr > 0.4 & random_nbr <= 0.6
    replace provider_entry_year = 2004 if random_nbr > 0.6 & random_nbr <= 0.8
    replace provider_entry_year = 2005 if random_nbr > 0.8 & random_nbr <= 1.9
    replace provider_entry_year = 2006 if random_nbr > 1.9 & random_nbr <= 2.2
    replace provider_entry_year = 2007 if random_nbr > 2.2 & random_nbr <= 2.8
    replace provider_entry_year = 2008 if random_nbr > 2.8 & random_nbr <= 3.5
    replace provider_entry_year = 2009 if random_nbr > 3.5 & random_nbr <= 5.2
    replace provider_entry_year = 2010 if random_nbr > 5.2 & random_nbr <= 8.6
    replace provider_entry_year = 2011 if random_nbr > 8.6 & random_nbr <= 11.5
    replace provider_entry_year = 2012 if random_nbr > 11.5 & random_nbr <= 13.1
    replace provider_entry_year = 2013 if random_nbr > 13.1 & random_nbr <= 15.2
    replace provider_entry_year = 2014 if random_nbr > 15.2 & random_nbr <= 18.3
    replace provider_entry_year = 2015 if random_nbr > 18.3 & random_nbr <= 20.1
    replace provider_entry_year = 2016 if random_nbr > 20.1 & random_nbr <= 20.4
    replace provider_entry_year = 2017 if random_nbr > 20.4 & random_nbr <= 22.3
    replace provider_entry_year = 2018 if random_nbr > 22.3 & random_nbr <= 22.8
	/*
    replace provider_entry_year = 2001 if random_nbr <= 0.4
    replace provider_entry_year = 2002 if random_nbr > 0.4 & random_nbr <= 0.8
    replace provider_entry_year = 2003 if random_nbr > 0.8 & random_nbr <= 1.2
    replace provider_entry_year = 2004 if random_nbr > 1.2 & random_nbr <= 1.6
    replace provider_entry_year = 2005 if random_nbr > 1.6 & random_nbr <= 2.0
    replace provider_entry_year = 2006 if random_nbr > 2.0 & random_nbr <= 2.4
    replace provider_entry_year = 2007 if random_nbr > 2.4 & random_nbr <= 2.8
    replace provider_entry_year = 2008 if random_nbr > 2.8 & random_nbr <= 3.2
    replace provider_entry_year = 2009 if random_nbr > 3.2 & random_nbr <= 3.6
    replace provider_entry_year = 2010 if random_nbr > 3.6 & random_nbr <= 4.0
    replace provider_entry_year = 2011 if random_nbr > 4.0 & random_nbr <= 4.4
    replace provider_entry_year = 2012 if random_nbr > 4.4 & random_nbr <= 4.8
    replace provider_entry_year = 2013 if random_nbr > 4.8 & random_nbr <= 5.2
    replace provider_entry_year = 2014 if random_nbr > 5.2 & random_nbr <= 5.6
    replace provider_entry_year = 2015 if random_nbr > 5.6 & random_nbr <= 6.0
    replace provider_entry_year = 2016 if random_nbr > 6.0 & random_nbr <= 6.4
    replace provider_entry_year = 2017 if random_nbr > 6.4 & random_nbr <= 6.8
    replace provider_entry_year = 2018 if random_nbr > 6.8 & random_nbr <= 7.2
*/
    drop random_nbr firstob
    
    * Regression window groups
    generate_reggroups, window(`window') ///
        connection_date(provider_entry_year)
        
    replace relative_ev_year_reggroups = 4 if provider_entry_year == . 
    
    keep if year >= startYear

    * Drop if created by bureaucrat
    drop if startYear >= provider_entry_year

    gen prob_zero_revenue = totalRevenue == 0

    gen priv_sales = totalRevenue - contract_value

    * Note: government share is only defined for observations with positive revenue
    gen share_gov = contract_value/totalRevenue
    winsor2 share_gov, replace cuts(1 99)

    label var prob1                 "Prob(Awarded contracts)"
    label var prob5                 "Prob(Awarded contracts)"
    label var prob10                "Prob(Awarded contracts)"
    label var prob25                "Prob(Awarded contracts)"
    label var ln_contract_value     "Log contracts value"
    label var ln_nbr_contracts      "Log number of contracts"
    label var capital               "Capital"
    label var totalWagesIESS101     "Wages"
    label var totalRevenue          "Revenue"
    label var Materials3      		"Materials"
    label var variable_inputs       "Variable inputs"
    label var priv_sales            "Private sales"
    label var share_gov             "Share revenue from government sales"
	
	gen post_entry = relative_ev_year_reggroups > 4
	bysort internal_firm_id: gegen treatment = max(post_entry)
	
    * Drop if always treated
	bysort internal_firm_id: egen  m_relative_ev_year=min(relative_ev_year)
	drop if m_relative_ev_year>=0 & m_relative_ev_year!=.
	 
	egen fid=group(internal_firm_id)
	replace provider_entry_year=10000000000 if provider_entry_year==.
	

    save_data "../temp/providers_connections_all", replace key(internal_firm_id year)
end

program generate_reggroups
    syntax, window(int) connection_date(str)

    gen relative_ev_year = year - `connection_date'

    sort internal_firm_id relative_ev_year
    gen relevant_time_period = (abs(relative_ev_year) <= `window')
    egen relative_ev_year_reggroups = group(relative_ev_year) if relevant_time_period
    replace relative_ev_year_reggroups = 0 if relative_ev_year < -`window'

    replace relative_ev_year_reggroups = 1000 if relative_ev_year > `window' & `connection_date' != .

    label var relative_ev_year_reggroups    "Years relative to first political connection"
end

* Execute
main
