set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
adopath + ../../../lib/stata/gtools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    build_providers_connections
    build_panel
end

program build_providers_connections
    use provider_id internal_firm_id using ///
        "$data_dir/proc_contracts/tables/providers_registry", clear
    duplicates drop provider_id, force
    save_data "../temp/providers_registry", replace key(provider_id)

    foreach input_file in provider_owned_by_bureaucrat ///
        provider_owned_by_sibling_of_bureaucrat {
        
        use "../temp/providers_registry", clear
            merge 1:m provider_id using "$data_dir/providers_connections/`input_file'", ///
        nogen assert(1 3) keep(3)
        
        replace owner_entry_year = first_year_shareholder if ///
            owner_entry_year < first_year_shareholder
        
        gcollapse (min) entry_year=owner_entry_year, by(internal_firm_id)

        save_data "../temp/`input_file'", replace key(internal_firm_id)
    }

    use "../temp/provider_owned_by_bureaucrat", clear
    rename entry_year entry_year_direct
    merge 1:1 internal_firm_id using "../temp/provider_owned_by_sibling_of_bureaucrat", ///
        nogen assert(1 2 3) keep(1 2 3)
    rename entry_year entry_year_indirect

    egen entry_year = rowmin(entry_year_indirect entry_year_direct)
    drop entry_year_direct entry_year_indirect

    save_data "../temp/providers_connections", replace key(internal_firm_id)
end

program build_panel
    use "$data_dir/balance_sheet/derived/balance_sheet_providersOnly_tfp", clear
    merge m:1 internal_firm_id using "../temp/providers_connections", ///
        nogen assert(1 2 3) keep(3)

    gen relative_ev_year = year - entry_year

    keep if totalRevenue != 0 & tfp_ols != . 
    keep if year >= 2009

    bys internal_firm_id (year): egen avg_revenue_before_t = mean(totalRevenue) ///
        if relative_ev_year <= -1
    bys internal_firm_id (avg_revenue_before_t): gen avg_revenue_before = avg_revenue_before_t[1]

    bys internal_firm_id (year): egen avg_revenue_after_t = mean(totalRevenue) ///
        if relative_ev_year > -1
    bys internal_firm_id (avg_revenue_after_t): gen avg_revenue_after = avg_revenue_after_t[1]

    bys internal_firm_id (year): egen avg_tfp_t = mean(tfp_ols) if relative_ev_year <= -1
    bys internal_firm_id (avg_tfp_t): gen avg_tfp = avg_tfp_t[1]

    gen ln_revenue_diff = ln(avg_revenue_after) - ln(avg_revenue_before)
    keep if ln_revenue_diff != .

    gen revenue_asset_ratio = ln(totalRevenue/totalAsset)
    bys internal_firm_id (year): egen avg_revenue_asset_t = mean(revenue_asset_ratio) ///
        if relative_ev_year <= -1
    bys internal_firm_id (avg_revenue_asset_t): gen avg_revenue_asset = avg_revenue_asset_t[1]

    duplicates drop internal_firm_id, force

    label var ln_revenue_diff       "Revenue growth after connection"
    label var avg_tfp               "Average log TFP"
    label var revenue_asset_ratio   "Average log revenue-asset ratio"

    save_data "../temp/providers_connections", replace key(internal_firm_id)
end

* Execute
main
