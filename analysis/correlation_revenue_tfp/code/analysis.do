set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
adopath + ../../../lib/stata/third_party/binsreg
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    use "../temp/providers_connections", clear

    winsor2 avg_tfp, c(1 99) tr replace
    winsor2 ln_revenue_diff, c(1 99) tr replace

    binsreg ln_revenue_diff avg_tfp i.isic2_nbr, ///
        ytitle(`: var label ln_revenue_diff') xtitle(`: var label avg_tfp')
    graph export "../output/corr_revenue_tfp.png", replace

    use "../temp/providers_connections", clear

    winsor2 avg_revenue_asset, c(1 99) tr replace
    winsor2 ln_revenue_diff, c(1 99) tr replace

    binscatter ln_revenue_diff avg_revenue_asset, ///
        ytitle(`: var label ln_revenue_diff') xtitle(`: var label avg_revenue_asset')
    graph export "../output/corr_revenue_ratio_asset.png", replace
end

* Execute
main
