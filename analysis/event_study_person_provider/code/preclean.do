set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    local window            5
    local share_threshold   "0.2"

    build_firm_providers
    build_connections
    match_with_shareholders, min_shares(`share_threshold')
    build_providers_panel
    bureaucrats_winners_panel, window(`window')
end

program build_firm_providers
    use provider_id internal_firm_id using ///
        "$data_dir/proc_contracts/tables/providers_registry", clear
    duplicates drop provider_id, force
    save_data "../temp/providers_registry", replace key(provider_id)

    use rucFirm using "$data_dir/firms_ownership/directory", replace
    rename rucFirm provider_id
    drop if provider_id == "."
    * Almost 200k rucs become of length 10
    replace provider_id = substr(provider_id, 1, 10) if ///
        substr(provider_id, -3, 3) == "001" & length(provider_id) == 13

    merge 1:1 provider_id using "../temp/providers_registry", ///
        assert(1 2 3) keep(3) nogen

    * 8 obs. are duplicates
    duplicates drop internal_firm_id, force

    save_data "../temp/firm_providers", replace key(internal_firm_id)
end

program build_connections
    foreach input_file in provider_is_bureaucrat ///
        provider_sibling_is_bureaucrat {
        
        use "../temp/providers_registry", clear
        merge 1:m provider_id using "$data_dir/providers_connections/`input_file'", ///
            nogen assert(1 3) keep(3)
        
        gcollapse (min) entry_year, by(internal_firm_id)
        
        save_data "../temp/`input_file'", replace key(internal_firm_id)
    }

    use "../temp/provider_is_bureaucrat", clear
    rename entry_year entry_year_person_dir

    merge 1:1 internal_firm_id using "../temp/provider_sibling_is_bureaucrat", ///
        assert(1 2 3) keep(1 2 3)
    rename entry_year entry_year_person_indir

    gen is_bureaucrat = _merge == 1 | _merge == 3
    gen is_sibling = _merge == 2 | _merge == 3
    drop _merge

    egen entry_year = rowmin(entry_year_person_dir entry_year_person_indir)
    drop entry_year_person_dir entry_year_person_indir

    save_data "../temp/individual_providers_connections", ///
        replace key(internal_firm_id)
end

program match_with_shareholders
    syntax, min_shares(str)

    use "$data_dir/firms_ownership/shareholders_clean", clear

    * Keep if shareholder owns more than min threshold
    keep if share >= `min_shares'

    * Keep only first observation for each shareholder
    gcollapse (min) first_year_shareholder=year, by(idshareholder)

    * Rename idshareholder as internal_firm_id to match with connection data
    rename idshareholder internal_firm_id
    
    merge 1:1 internal_firm_id using "../temp/individual_providers_connections", ///
        assert(1 2 3) keep(2 3)

    gen has_firm = _merge == 3
    drop _merge

    drop if has_firm == 1 & entry_year < first_year_shareholder

    save_data "../temp/individual_providers_connections", ///
        replace key(internal_firm_id)
end

program build_providers_panel
    use "$data_dir/proc_contracts/tables/contracts_master_with_restrictions" ///
        if contract_year>=2009, clear
    
    rename contract_year year
    gcollapse (sum) contract_value nbr_contracts=id, by(internal_firm_id year)

    save_data "../temp/winners_table", replace key(internal_firm_id year)

    * Create rectangular panel, starting in 2009
    clear
    set obs 9
    gen year = 2008 + _n
    gen joinvar = 1
    save_data "../temp/panel_years", replace key(year)

    use "../temp/providers_registry", clear
    keep internal_firm_id
    duplicates drop
    
    * Keep only person providers
    merge 1:1 internal_firm_id using "../temp/firm_providers", ///
        nogen assert(1 3) keep(1)
    
    gen joinvar = 1
    joinby joinvar using "../temp/panel_years"
    drop joinvar

    * _merge == 2 would derive from contracts that have paiments in 2008 or after 2017
    merge 1:1 internal_firm_id year using "../temp/winners_table", ///
        nogen assert(1 2 3) keep(1 3)
    replace contract_value = 0 if contract_value == .
    replace nbr_contracts = 0 if nbr_contracts == .

    save_data "../temp/providers_panel", replace key(internal_firm_id year)
end

program bureaucrats_winners_panel
    syntax, window(int)

    use "../temp/individual_providers_connections", clear

    * To keep control group, add _merge == 2
    merge 1:m internal_firm_id using "../temp/providers_panel", ///
        nogen assert(1 2 3) keep(3)

    * Keep only people in name dataset
    // rename internal_firm_id person_id
    // merge m:1 person_id using "$data_dir/derived_data/name_dataset", ///
    //     nogen assert(1 2 3) keep(3)

    // rename person_id internal_firm_id

    gen prob1  = (nbr_contracts > 0) & (contract_value >= 100)
    gen prob5  = (nbr_contracts > 0) & (contract_value >= 800)
    gen prob10 = (nbr_contracts > 0) & (contract_value >= 3000)
    gen prob25 = (nbr_contracts > 0) & (contract_value >= 15000)

    keep if entry_year >= 2008

    generate_reggroups, window(`window') ///
        connection_date(entry_year)

    label var prob1                 "Prob(Awarded contracts)"
    label var prob5                 "Prob(Awarded contracts)"
    label var prob10                "Prob(Awarded contracts)"
    label var prob25                "Prob(Awarded contracts)"

    save_data "../temp/providers_connections_all", replace key(internal_firm_id year)
end

program generate_reggroups
    syntax, window(int) connection_date(str)

    gen relative_ev_year = year - `connection_date'

    sort internal_firm_id relative_ev_year
    gen relevant_time_period = (abs(relative_ev_year) <= `window')
    egen relative_ev_year_reggroups = group(relative_ev_year) if relevant_time_period
    replace relative_ev_year_reggroups = 0 if relative_ev_year < -`window'

    local window_up_bound = `window' - 1
    replace relative_ev_year_reggroups = 1000 if relative_ev_year > `window_up_bound' /// 
        & `connection_date' != .

    label var relative_ev_year_reggroups    "Years relative to first political connection"
end

* Execute
main
