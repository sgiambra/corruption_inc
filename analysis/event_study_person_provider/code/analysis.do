set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

program main
    use "../temp/providers_connections_all", clear

    prepare_factor_info relative_ev_year if relevant_time_period
    local reg_opts = r(reg_opts)

    foreach var in prob1 prob5 prob10 prob25 {
        * All connections
        event_study_plot `var', saving("`var'_person_all") `reg_opts'
        
        * Direct connections
        event_study_plot `var', saving("`var'_person_direct") `reg_opts' ///
            condition("is_bureaucrat == 1 & is_sibling == 0")

        * Indirect connections
        event_study_plot `var', saving("`var'_person_indirect") `reg_opts' ///
            condition("is_bureaucrat == 0 & is_sibling == 1")

        * Both connections
        event_study_plot `var', saving("`var'_person_both") `reg_opts' ///
            condition("is_bureaucrat == 1 & is_sibling == 1")

        ** Distinguish between individual with firms
        event_study_plot `var', saving("`var'_person_with_firm") ///
            `reg_opts' condition("has_firm == 1")

        event_study_plot `var', saving("`var'_person_no_firm") ///
            `reg_opts' condition("has_firm == 0")
    }
end

program prepare_factor_info, rclass
    syntax anything(name=factor_var) [if]

    qui levelsof `factor_var' `if', local(factor_levels)
    local num_factor_levels: word count `factor_levels'
    local median_factor_value = (`num_factor_levels' + 1)/2
    local base_factor_value = `median_factor_value' - 1
    return local reg_opts = "factor_levels(`factor_levels') "               + ///
                            "num_factor_levels(`num_factor_levels') "       + ///
                            "median_factor_value(`median_factor_value')"    + ///
                            "base_factor_value(`base_factor_value')"
end

program event_study_plot
    syntax anything(name=depvar) [if], factor_levels(str) num_factor_levels(str) ///
        median_factor_value(str) base_factor_value(str) saving(str) * [condition(str)]
    
    if "`condition'" != "" {
        local if_condition = "if " + "`condition'"
        local and_condition = "& " + "`condition'"
    }

    qui levelsof relative_ev_year_reggroups, local(reggroups)
    local stagger "1"
    local reggroups: list reggroups- stagger

    local nbr_reggroups: word count `reggroups'

    areg `depvar' i(`reggroups')b(`base_factor_value').relative_ev_year_reggroups i.year `if_condition', ///
        absorb(internal_firm_id) vce(cluster internal_firm_id)

    matrix coefs = e(b)
    matrix var_cov = e(V)

    qui sum `depvar' if relative_ev_year < 0 `and_condition'
    forval i = 1/`nbr_reggroups' {  
        matrix coefs[1,`i']= coefs[1,`i'] + `r(mean)'
    }

    coefplot matrix(coefs), v(var_cov) vertical yline(`r(mean)', lpattern(dash)) ytitle(`: var label `depvar'')              ///
        xtitle(`: var label relative_ev_year_reggroups') baselevels levels(95 90)         ///
        rename(2.relative_ev_year_reggroups="-4" 3.relative_ev_year_reggroups="-3" 4.relative_ev_year_reggroups="-2"    ///
               5.relative_ev_year_reggroups="-1" 6.relative_ev_year_reggroups="Event" 7.relative_ev_year_reggroups="1"  ///
               8.relative_ev_year_reggroups="2" 9.relative_ev_year_reggroups="3" 10.relative_ev_year_reggroups="4")      ///
        drop(0.relative_ev_year_reggroups 1000.relative_ev_year_reggroups *year _cons)

    graph export "../output/`saving'.png", replace
end

* Execute
main
