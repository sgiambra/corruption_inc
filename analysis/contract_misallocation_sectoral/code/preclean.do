set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    build_sectoral_stats

    foreach stub in "_govSalesMarkup" "_beforeConnection" ///
        "_revenueAdj" "_full" "_providersOnly" {

        import delimited "../../contract_misallocation/output/misallocation_Estimates_sectors`stub'.csv", clear

        preserve
            keep if rep>0
            collapse (sd) flex_capital_ols fixed_capital_ols flex_capital_woold ///
                fixed_capital_woold, by(isic2_nbr)

            rename (flex_capital_ols fixed_capital_ols flex_capital_woold fixed_capital_woold) ///
                (flex_capital_ols_se fixed_capital_ols_se flex_capital_woold_se fixed_capital_woold_se)

            save_data "../temp/misallocation_Estimates_sectors`stub'_SE", ///
                key(isic2_nbr) replace
        restore

        keep if rep==0
        merge 1:1 isic2_nbr using "../temp/misallocation_Estimates_sectors`stub'_SE", ///
            nogen assert(3) keep(3)

        merge m:1 isic2_nbr using "../temp/sectoral_stats", ///
            nogen assert(2 3) keep(3)

        * Add Names
        gen code = substr(isic2, 2,3)
        merge 1:1 code using "$data_dir/isic4_names_concordance.dta", ///
            keep(3) assert(2 3) nogen

        replace description = "Video and television programme production, music publishing activities" ///
            if description == "Motion picture, video and television programme production, sound recording and music publishing activities"

        save_data "../temp/misallocation_Estimates_sectors`stub'", replace key(isic2_nbr rep)
    }
end

program build_sectoral_stats
    use "$data_dir/proc_contracts/tables/contracts_master_with_restrictions" ///
        if contract_year>=2009, clear
    
    rename contract_year year
    gcollapse (sum) contract_value, by(internal_firm_id year)

    * Need to merge 1:m because non-contractors have missing internal_firm_id
    merge 1:m internal_firm_id year using ///
        "$data_dir/balance_sheet/master/balance_sheet_clean", ///
        assert(1 2 3) keep(2 3) nogen

    merge m:1 internal_firm_id using ///
        "$data_dir/providers_connections/master/providers_connections_all", ///
        assert(1 2 3) keep(1 3)

    gen connected = _merge == 3
    drop _merge

    replace contract_value = 0 if contract_value == .

    gen count = 1

    collapse (first) isic2 (mean) connected (sum) sectorRevenue=totalRevenue ///
        sectorContractValue=contract_value count_firms=count, by(isic2_nbr year)

    collapse (first) isic2 (mean) connected sectorRevenue ///
        count_firms sectorContractValue, by(isic2_nbr)

    foreach var in sectorRevenue sectorContractValue {
        replace `var' = `var'/1000000
    }

    save_data "../temp/sectoral_stats", replace key(isic2_nbr)
end

* Execute
main
