set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    cap: mkdir "../output/full"
    cap: mkdir "../output/significant"
    cap: mkdir "../output/full/figures"
    cap: mkdir "../output/significant/figures"

    sectoral_ranking
    ranking_correlation
end

program sectoral_ranking
   foreach stub in "_govSalesMarkup" "_beforeConnection" ///
        "_revenueAdj" "_full" "_providersOnly" {
        
        use "../temp/misallocation_Estimates_sectors`stub'", clear

        * Ranking without restrictions
        foreach est in flex_capital_ols fixed_capital_ols flex_capital_woold fixed_capital_woold {
            local rows

            gen ci_`est'_low = `est' - 1.96*`est'_se
            gen ci_`est'_upp = `est' + 1.96*`est'_se

            gen `est'_tstat = abs(`est')/`est'_se

            gen `est'_stars = "*" if `est'_tstat >= 1.644854 & `est'_tstat < 1.95996
            replace `est'_stars = "**" if `est'_tstat >= 1.95996 & `est'_tstat < 2.32635
            replace `est'_stars = "***" if `est'_tstat >= 2.32635

            gsort -`est'
            gen order = _n
            
            export excel order isic2 description `est' ci_`est'_low ci_`est'_upp count_firms connected ///
                sectorRevenue sectorContractValue `est'_stars using "../output/full/ranking_`est'`stub'.xlsx", ///
                replace firstrow(variables)

            plot_coefficients_ranking, est(`est') stub(`stub') signif("full")
        }

        * Ranking estimates for top 20 sectors in terms of procurement spending
        foreach est in flex_capital_ols fixed_capital_ols flex_capital_woold fixed_capital_woold {
            preserve
                gsort -sectorContractValue
                keep if _n <= 20

                gsort -`est'
                gen order = _n

                export excel order isic2 description `est' count_firms connected sectorRevenue sectorContractValue ///
                    using "../output/significant/ranking_`est'`stub'.xlsx", replace firstrow(variables)

                plot_coefficients_ranking, est(`est') stub(`stub') signif("significant")
            restore
        }
    }
end

program ranking_correlation
    local coefficients flex_capital_ols fixed_capital_ols flex_capital_woold fixed_capital_woold 

    use `coefficients' isic2_nbr using "../temp/misallocation_Estimates_sectors_govSalesMarkup", clear
    
    rename (`coefficients') (flex_ols_govSalesMarkup fix_ols_govSalesMarkup ///
        flex_woold_govSalesMarkup fix_woold_govSalesMarkup)
  
    foreach stub in "_providersOnly" "_beforeConnection" "_revenueAdj" "_full" {
        
        merge 1:1 isic2_nbr using "../temp/misallocation_Estimates_sectors`stub'", ///
            keep(3) nogen keepusing(`coefficients') 
        
        rename (`coefficients') (flex_ols`stub' fix_ols`stub' flex_woold`stub' fix_woold`stub')
    }

    corr fix_woold_govSalesMarkup flex_woold_govSalesMarkup
    mat corr_mat = (`r(rho)')

    foreach capital in fix flex {
        corr fix_woold_govSalesMarkup `capital'_ols_govSalesMarkup
        mat corr_mat = (nullmat(corr_mat) \ `r(rho)')
    }

   foreach stub in "_beforeConnection" "_revenueAdj" "_providersOnly" "_full" {
        foreach spec in woold ols {
            foreach capital in fix flex {
                corr fix_woold_govSalesMarkup `capital'_`spec'`stub'
                mat corr_mat = (nullmat(corr_mat) \ `r(rho)')
            }
        }
   }

   esttab matrix(corr_mat, fmt(%9.3f)) using "../output/ranking_correlation.tex", replace
end

program plot_coefficients_ranking
    syntax, est(str) stub(str) signif(str)

    if "`signif'" == "full" {
        local labsize   "tiny"
        local msize     "vsmall"
        local xsize     "4"
    }
    if "`signif'" == "significant" {
        local labsize   "vsmall"
        local msize     "small"
        local xsize     "5.5"
    }

    mkmat `est' ci_`est'_low ci_`est'_upp, matrix(`est')

    qui valuesof description
    local sectors "`r(values)'"
    local nbr_sectors: word count `sectors'

    foreach val of numlist 1/`nbr_sectors' {
        
        gen id`val' = order[`val']
        local label: word `val' of `sectors'
        label variable id`val' "`label'"
        
        local rows `rows' id`val'
    }

    mat rownames `est' = `rows'
    
    coefplot matrix(`est'[,1]), ci((`est'[,2] `est'[,3])) grid(glpattern(dot)) ciopts(recast(rcap)) ///
        sort(, descending) xline(0, lpattern(dash) lcolor(gs10)) ylabel(, labsize(`labsize')) ///
        xsize(`xsize') msize(`msize') msymbol(Oh) xlabel(, labsize(vsmall)) mcolor(black)
    graph export "../output/`signif'/figures/ranking_`est'`stub'.png", replace

    drop order id*
end

* Execute
main
