cd "/Users/felipe/Dropbox (Brown)/Research/corruption_inc/analysis/event_study_balance/code"
set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

program main
    cap: mkdir "../output/tables"
    cap: mkdir "../output/figures"

    use "../temp/providers_connections_all", clear

	event_study_figures, window(4)
	
    foreach depvar in prob1 prob5 prob10 prob25 {
		table_single_dummy, depvar(`depvar')
	}

	build_table_conn_type
end
    
	
program event_study_figures
	syntax, window(integer)


	*===========================================================================
	* Figures: Callaway and Sant'Anna
	*===========================================================================
 
	foreach depvar in prob1 prob5 prob10 prob25 ln_contract_value ln_nbr_contracts {
		
			
			staggered_cs, y("`depvar'") g("provider_entry_year") t("year") i("fid") estimand("eventstudy") eventTimeStart(-`window') eventTimeEnd(`window')
			matrix coefs = e(b)
			matrix var_cov = e(V)
				
			qui sum `depvar' if relative_ev_year < 0
			forval i = 1/9 {  
				matrix coefs[1,`i']= coefs[1,`i'] + `r(mean)'
			}
		
			coefplot matrix(coefs), v(var_cov) vertical yline(`r(mean)', lpattern(dash)) ytitle(`: var label `depvar'')         ///
			xtitle(`: var label relative_ev_year_reggroups') baselevels levels(95 90) ylabel(`ylabel')                      ///
			xlab(1 "-4" 2 "-3" 3 "-2" 4 "-1" 5 "Event" 6"1" 7 "2" 8"3" 9 "4") graphregion(color(white))  
			graph export "../output/figures/`depvar'.png", replace
			
		
	}	
	 
	
end

program table_single_dummy
	syntax, depvar(string)
	*===========================================================================
	* Results: Single Dummy
	*===========================================================================
		* Callaway and Sant'Anna
		staggered_cs, y("`depvar'") g("provider_entry_year") t("year") i("fid") estimand("simple") 
			ereturn list 
			matrix coef= e(b)
			matrix colname coef = afterTreatment 
			erepost b=coef, rename

			outreg2 using "../output/tables/DD_`depvar'_table.tex",  nocons dec(4) replace ctitle(CA)
			

		* Sun and Abraham
		staggered_sa, y("`depvar'") g("provider_entry_year") t("year") i("fid") estimand("simple") 
			ereturn list 
			matrix coef= e(b)
			matrix colname coef = afterTreatment 
			erepost b=coef, rename
			
			outreg2 using "../output/tables/DD_`depvar'_table.tex",  nocons dec(4) append ctitle(SA)

			
		* De chaisemartin and D'haultfoeuille
		qui: sum relative_ev_year
		local max_window = `r(max)'
		did_multiplegt `depvar' fid year afterTreatment ,  breps(30) cluster(fid)  robust_dynamic dynamic(`max_window') average_effect covariances
			matrix coef = e(effect_average)
			matrix colname coef = afterTreatment 
			matrix variance = e(se_effect_average)^2
		*Run regression to create ereturn for outreg2 		
			reg `depvar' afterTreatment, nocons
				erepost b=coef, rename
				erepost V=variance, rename
			outreg2 using "../output/tables/DD_`depvar'_table.tex",  nocons dec(4) append ctitle(DcDh) nor2 noobs



		* TWFE
		reghdfe `depvar' afterTreatment, absorb(fid year) cluster(fid)   // do we want to condition on entry in 2013?
   
		qui distinct internal_firm_id if e(sample)
		local nbr_firms `r(ndistinct)'

		qui sum `depvar' if year < provider_entry_year & treatment == 1 
		local mean_before_conn `r(mean)'

		qui distinct internal_firm_id if treatment == 1 
		local nbr_connected_firms `r(ndistinct)'

	
			outreg2 using "../output/tables/DD_`depvar'_table.tex",  nocons dec(4) ///
				adds(Number contractors, `nbr_firms', Connected contractors, `nbr_connected_firms', ///
				Mean before connection, `mean_before_conn') append tex keep(afterTreatment) ///
				ctitle(TWFE) 


end
	

program build_table_conn_type    
    foreach depvar in prob1 prob5 prob10 prob25 {
        * All connections
			single_dummy_regression, depvar(`depvar') title("All")

        * Owned by bureaucrat
		preserve
			keep if (owned_by_bureaucrat == 1 & owned_by_sibling == 0) | treatment == 0
			single_dummy_regression, depvar(`depvar') title("Only Direct")
		restore
    

        * Owned by sibling of bureaucrat
		preserve
			keep if (owned_by_bureaucrat == 0 & owned_by_sibling == 1) | treatment == 0
			single_dummy_regression, depvar(`depvar') title("Only Indirect")
		restore
     
   

        * Owned by both bureaucrat and sibling of bureaucrat
		preserve
			keep if (owned_by_bureaucrat == 1 & owned_by_sibling == 1) | treatment == 0
			single_dummy_regression, depvar(`depvar') title("Both")
		restore
     
    }
end


program single_dummy_regression
	syntax, depvar(string) title(string) 
	
		qui distinct internal_firm_id
		local nbr_firms `r(ndistinct)'
		
		qui sum `depvar' if year < provider_entry_year & treatment == 1
		local mean_before_conn `r(mean)'

		qui distinct internal_firm_id if treatment == 1
		local nbr_connected_firms `r(ndistinct)'
		
		sum `depvar'
		local nbr_obs `r(N)'
		
		staggered_cs, y("`depvar'") g("provider_entry_year") t("year") i("fid") estimand("simple") 
			
				ereturn list 
				matrix coef= e(b)
				matrix colname coef = afterTreatment 
				erepost b=coef, rename

				outreg2 using "../output/tables/DD_`depvar'_table_connections.tex",  nocons dec(4) append ctitle(`title')  ///
				adds(Observations, `nbr_obs' , Number contractors, `nbr_firms', Connected contractors, `nbr_connected_firms', ///
                Mean before connection, `mean_before_conn') nor2

end



* Execute
main
