set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    build_providers_balance
    build_providers_ownership
    build_contracts
    build_contracts_connections
end

program build_providers_balance
    * Get price index
    import delimited "$data_dir/price_index_WB/API_FP.CPI.TOTL_DS2_en_csv_v2_126205.csv", ///
        varnames(5) rowrange(5) clear

    foreach v of varlist v* {
       local x : variable label `v'
       rename `v' priceindex`x'
    }
    
    keep if countryname == "Ecuador"
    drop priceindex
    reshape long priceindex, i(countryname) j(year)
    keep year priceindex

    save_data "../temp/priceindex", replace key(year)
    
    use "$data_dir/balance_sheet/master/balance_sheet_clean", clear

    merge m:1 internal_firm_id using ///
        "$data_dir/providers_connections/master/providers_connections_all", ///
        nogen assert(1 2 3) keep(1 3)
    
    gen any_connection = provider_entry_year != .

    gen direct_connection_only   = provider_entry_year_direct != . & provider_entry_year_indirect == .
    gen indirect_connection_only = provider_entry_year_direct == . & provider_entry_year_indirect != .
    gen both_connections_type    = provider_entry_year_direct != . & provider_entry_year_indirect != .

    foreach var in strategic_entrant created_by_bureaucrat {
        replace `var' = 0 if `var' == .
    }

    save_data "../temp/firms_balance_connections", replace key(expediente year)

    keep if contractor == 1

    duplicates tag internal_firm_id year, gen(dup)
    drop if dup > 0
    drop dup

    save_data "../temp/contractors_balance_connections", ///
        replace key(internal_firm_id year)
end

program build_providers_ownership
    use "../temp/contractors_balance_connections", clear
    
    merge 1:m firm_id year using "$data_dir/firms_ownership/shareholders_clean", ///
        nogen assert(1 2 3) keep(3)

    save_data "../temp/contractors_balance_ownership", ///
        replace key(idshareholder firm_id year)
end

program build_contracts
    use "$data_dir/proc_contracts/tables/contracts_master_with_restrictions" ///
        if contract_year >= 2009, clear
    
    rename contract_year year
    replace contract_cat = "z_others" if contract_cat == "others"
    replace contract_cat = "random" if contract_cat == "menor_obras"
    replace contract_cat = "disc_" + contract_cat if ///
        contract_cat != "random" & contract_cat != "auction"

    merge m:1 internal_firm_id year using "../temp/contractors_balance_connections", ///
        assert(1 2 3) keep(1 3) keepusing(internal_firm_id)

    gen won_by_firm = _merge == 3
    drop _merge

    collapse (sum) contract_value contract_budget (mean) contract_length year ///
        (first) contract_cat (max) won_by_firm, by(contract_link)

    save_data "../temp/contracts", replace key(contract_link)

    use "$data_dir/proc_contracts/tables/contracts_competitors", clear
    gen id = _n
    gcollapse (count) nbr_competitors=id, by(contract_link)

    merge 1:1 contract_link using "../temp/contracts", ///
        nogen assert(1 2 3) keep(3)

    save_data "../temp/contracts_with_competitors", replace key(contract_link)
end

program build_contracts_connections
    use "$data_dir/proc_contracts/tables/contracts_master_with_restrictions" ///
        if contract_year>=2009, clear
    
    rename contract_year year
    gcollapse (sum) contract_value, by(internal_firm_id year contract_link)

    merge m:1 internal_firm_id year using "../temp/contractors_balance_connections", ///
        nogen assert(1 2 3) keep(3)

    gen connected = provider_entry_year != . & year >= provider_entry_year

    gcollapse (sum) contract_value (max) connected ///
        strategic_entrant created_by_bureaucrat, by(contract_link)

    save_data "../temp/providers_contracts_connections", ///
        replace key(contract_link)
end

* Execute
main
