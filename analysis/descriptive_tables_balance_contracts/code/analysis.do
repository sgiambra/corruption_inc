* NOTE: The sample of connected firms still includes strategic entries

set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    local ref_year      2015

    welfare_parameters
    descriptive_tables_providers, ref_year(`ref_year')
    zeros_table, ref_year(`ref_year')
    descriptives_ownership, ref_year(`ref_year')
    descriptives_contracts
    sample_size_table
end

program welfare_parameters
    * Includes strategic connections
    use "../temp/providers_contracts_connections", clear
    
    gen nbr_contracts = 1
    gcollapse (sum) nbr_contracts contract_value, by(connected)

    foreach var in nbr_contracts contract_value {
        qui sum `var'
        mat welfare_params = (nullmat(welfare_params), `r(sum)')
    }

    egen tot_nbr_contracts = total(nbr_contracts)
    gen share_nbr_contracts = nbr_contracts/tot_nbr_contracts 

    qui sum share_nbr_contracts if connected == 1
    mat welfare_params = (nullmat(welfare_params), `r(mean)')

    use "../temp/contractors_balance_connections", clear
    
    * No distinction before vs after connection as in misallocation analysis
    gen variable_cost_share = (totalWages101 + Materials3)/totalRevenue

    winsor2 variable_cost_share, cuts(1 99) replace

    forvalues conn = 0/1 {
        qui sum variable_cost_share if any_connection == `conn'
        mat welfare_params = (nullmat(welfare_params), `r(mean)')
    }

    mat colnames welfare_params = "Tot nbr contracts" "Tot val contracts" "Share connected" ///
        "Cost share unconnected" "Cost share connected"
    esttab matrix(welfare_params, fmt(%12.0gc)) using "../output/parameters_welfare_analysis.tex", replace
end

program descriptive_tables_providers
    syntax, ref_year(int)

    * Drop strategic entrants from all columns except appendix table
    use "../temp/firms_balance_connections" if year==`ref_year', clear
     
    foreach var in totalRevenue capital totalWages101 Materials3 totalDebt revenue_asset_ratio age {

        qui sum `var'
        
        if "`var'" != "revenue_asset_ratio" & "`var'" != "age" local block = "floor(`r(mean)') \ floor(`r(sd)')"
        else local block = "round(`r(mean)', .001) \ round(`r(sd)', .001)"
        mat block_all_firms = (nullmat(block_all_firms) \ `block')
        
        qui sum `var' if contractor==1
        
        if "`var'" != "revenue_asset_ratio" & "`var'" != "age" local block = "floor(`r(mean)') \ floor(`r(sd)')"
        else local block = "round(`r(mean)', .001) \ round(`r(sd)', .001)"
        mat block_contractors = (nullmat(block_contractors) \ `block')

        qui sum `var' if any_connection==0 & contractor==1
        
        if "`var'" != "revenue_asset_ratio" & "`var'" != "age" local block = "floor(`r(mean)') \ floor(`r(sd)')"
        else local block = "round(`r(mean)', .001) \ round(`r(sd)', .001)"
        mat block_notConnected = (nullmat(block_notConnected) \ `block')

        qui sum `var' if any_connection==1 & contractor==1

        if "`var'" != "revenue_asset_ratio" & "`var'" != "age" local block = "floor(`r(mean)') \ floor(`r(sd)')"
        else local block = "round(`r(mean)', .001) \ round(`r(sd)', .001)"
        mat block_connected = (nullmat(block_connected) \ `block')

        qui sum `var' if any_connection==1 & contractor==1 ///
            & strategic_entrant==0 & created_by_bureaucrat==0 & provider_entry_year!=2000

        if "`var'" != "revenue_asset_ratio" & "`var'" != "age" local block = "floor(`r(mean)') \ floor(`r(sd)')"
        else local block = "round(`r(mean)', .001) \ round(`r(sd)', .001)"
        mat block_connected_restrictions = (nullmat(block_connected_restrictions) \ `block')

        qui sum `var' if owned_by_bureaucrat == 1 & owned_by_sibling == 0 & contractor == 1 ///
            & strategic_entrant==0 & created_by_bureaucrat==0 & provider_entry_year!=2000

        if "`var'" != "revenue_asset_ratio" & "`var'" != "age" local block = "floor(`r(mean)') \ floor(`r(sd)')"
        else local block = "round(`r(mean)', .001) \ round(`r(sd)', .001)"
        mat block_owned_bur = (nullmat(block_owned_bur) \ `block')

        qui sum `var' if owned_by_sibling == 1 & owned_by_bureaucrat == 0 & contractor == 1 ///
            & strategic_entrant==0 & created_by_bureaucrat==0 & provider_entry_year!=2000

        if "`var'" != "revenue_asset_ratio" & "`var'" != "age" local block = "floor(`r(mean)') \ floor(`r(sd)')"
        else local block = "round(`r(mean)', .001) \ round(`r(sd)', .001)"
        mat block_owned_sib = (nullmat(block_owned_sib) \ `block')

        qui sum `var' if owned_by_bureaucrat == 1 & owned_by_sibling == 1 & contractor == 1 ///
            & strategic_entrant==0 & created_by_bureaucrat==0 & provider_entry_year!=2000

        if "`var'" != "revenue_asset_ratio" & "`var'" != "age" local block = "floor(`r(mean)') \ floor(`r(sd)')"
        else local block = "round(`r(mean)', .001) \ round(`r(sd)', .001)"
        mat block_owned_bur_sib = (nullmat(block_owned_bur_sib) \ `block')
    }

    * Sample size
    qui distinct expediente
    local num_all_firms = `r(ndistinct)'

    qui distinct internal_firm_id if contractor==1
    local num_providers = `r(ndistinct)'

    qui distinct internal_firm_id if any_connection==0 & contractor==1
    local num_providers_notConnected = `r(ndistinct)'

    qui distinct internal_firm_id if any_connection==1 & contractor==1
    local num_providers_connected = `r(ndistinct)'

    qui distinct internal_firm_id if any_connection==1 & contractor==1 & ///
        strategic_entrant==0 & created_by_bureaucrat==0 & provider_entry_year!=2000
    local num_connected_restrictions = `r(ndistinct)'

    qui distinct internal_firm_id if  owned_by_bureaucrat==1 & owned_by_sibling==0 & contractor==1 ///
        & strategic_entrant==0 & created_by_bureaucrat==0 & provider_entry_year!=2000
    local num_providers_owned_bur = `r(ndistinct)'

    qui distinct internal_firm_id if owned_by_sibling==1 & owned_by_bureaucrat==0 & contractor==1 ///
        & strategic_entrant==0 & created_by_bureaucrat==0 & provider_entry_year!=2000
    local num_providers_owned_sib = `r(ndistinct)'

    qui distinct internal_firm_id if owned_by_bureaucrat==1 & owned_by_sibling==1 & contractor==1 ///
        & strategic_entrant==0 & created_by_bureaucrat==0 & provider_entry_year!=2000
    local num_providers_owned_bur_sib = `r(ndistinct)'

    ** Main table
    mat sample_size = (`num_all_firms', `num_providers', `num_providers_notConnected', `num_providers_connected', ///
        `num_connected_restrictions', `num_providers_owned_bur', `num_providers_owned_sib', `num_providers_owned_bur_sib')

    mat table_providers = (block_all_firms, block_contractors, block_notConnected, block_connected, ///
        block_connected_restrictions, block_owned_bur, block_owned_sib, block_owned_bur_sib)
    mat table_providers = (table_providers \ sample_size)

    mat rownames table_providers = "Revenue" "" "Capital" "" "Wage bills" "" "Intermediate inputs" "" "Debt" "" ///
        "Revenue-asset ratio" "" "Age" "" "Number contractors"
    mat colnames table_providers =  "All firms" "All contractors" "Not politically connected" "Politically connected" ///
        "Connected restrictions" "Owned by bureaucrat" "Owned by sibling of bureaucrat" "Owned by both"
    esttab matrix(table_providers, fmt(%12.0gc)) using "../output/descriptive_table_providers.tex", replace
end

program zeros_table
    syntax, ref_year(int)

    use "../temp/firms_balance_connections" if year==`ref_year', clear

    foreach var in totalRevenue capital totalWages101 Materials3 totalDebt {
        gen `var'_probZero = `var' == 0
    }
    egen any_probZero = rowtotal(*_probZero)
    replace any_probZero = 1 if any_probZero > 0

    foreach var in totalRevenue capital totalWages101 Materials3 totalDebt any {
        qui sum `var'_probZero
        mat block_zeros_all = (nullmat(block_zeros_all) \ `r(mean)') 

        qui sum `var'_probZero if any_connection == 0
        mat block_zeros_notConnected = (nullmat(block_zeros_notConnected) \ `r(mean)')

        qui sum `var'_probZero if any_connection == 1
        mat block_zeros_connected = (nullmat(block_zeros_connected) \ `r(mean)')
    }
    mat table_zeros = (block_zeros_all, block_zeros_notConnected, block_zeros_connected)

    mat rownames table_zeros = "Revenue" "Capital" "Wage bills" "Intermediate inputs" "Debt" "Any"
    mat colnames table_zeros = "All providers" "Not politically connected" "Politically connected"
    esttab matrix(table_zeros, fmt(%12.0gc)) using "../output/zeros_table_providers.tex", replace
end

program descriptives_ownership
    syntax, ref_year(int)

    * Empirical CDF
    use "$data_dir/firms_ownership/shareholders_clean" if year==`ref_year', clear

    cumul share, gen(cum_allFirms)

    keep cum_allFirms share
    rename share share_allFirms
    keep if share_allFirms > .01
    gen id = _n
    save_data "../temp/data_cdf_all", replace key(cum_allFirms)

    use "../temp/contractors_balance_ownership" if year==`ref_year', clear
    
    cumul share, gen(cum_contractors)

    keep cum_contractors share
    rename share share_contractors
    keep if share_contractors > .01
    gen id = _n

    merge 1:1 id using "../temp/data_cdf_all", ///
        nogen assert(2 3) keep(2 3)

    stack cum_allFirms share_allFirms cum_contractors ///
        share_contractors, into(c share) wide clear

    line cum_allFirms cum_contractors share, sort ytitle("") ///
        xtitle("Shares") legend(label(1 "All firms") label(2 "Contractors"))
    graph export "../output/shares_cdf.png", replace
end

program descriptives_contracts

    foreach stub in firms_only all {
        cap: mat drop contract_desc competitors

        if "`stub'" == "firms_only" local use_cond = "if won_by_firm == 1"
        else if "`stub'" == "all" local use_cond = ""

        use "../temp/contracts" `use_cond', clear

        *By category
        qui levelsof contract_cat, local(categories)

        foreach category in `categories' "" {
            cap: mat drop contract_cat_row

            if "`category'" != "" local cat_cond "if contract_cat == `"`category'"'"
            else local cat_cond = ""

            foreach var in contract_value contract_budget contract_length {

                qui sum `var' `cat_cond'
                mat block = (floor(`r(mean)') \ floor(`r(sd)'))
                mat contract_cat_row = (nullmat(contract_cat_row), block)
            }
            qui count `cat_cond'
            
            mat contracts_in_cat = (`r(N)' \ .)
            mat contract_desc = (nullmat(contract_desc) \ contract_cat_row, contracts_in_cat)
        }

        use "../temp/contracts_with_competitors" `use_cond', clear

        qui levelsof contract_cat, local(categories)

        foreach category in `categories' "" {
            
            if "`category'" != "" local cat_cond "if contract_cat == `"`category'"'"
            else local cat_cond = ""

            qui sum nbr_competitors `cat_cond'
            mat competitors = (nullmat(competitors) \ round(`r(mean)', .001) \ round(`r(sd)', .001))
        }

        mat contract_desc = (contract_desc, competitors)
        
        mat rownames contract_desc = "Auctions" "" "Direct" "contracting" "Quotations" "" ///
            "Lower value," "no constructions" "Publication" "" "Other" "discretionary" "Random" "" "Overall" ""
        mat colnames contract_desc = "Contract value (\$)" "Contract budget (\$)" ///
            "Contract length (days)" "Number contracts" "Number competitors"
        esttab matrix(contract_desc, fmt(%12.0gc)) using "../output/descriptive_table_contracts_`stub'.tex", replace
    }

    use "$data_dir/proc_contracts/electronic_catalog" if ///
        category1 != "Medicamentos", clear

    merge m:1 year using "../temp/priceindex", ///
        assert(2 3) keep(3) nogen

    replace total_value = total_value/priceindex*100

    gcollapse (sum) total_value, by(order_id)
    gen ln_value = ln(total_value)

    cumul ln_value, gen(cum_eccat)

    line cum_eccat ln_value, sort ///
        ytitle("") xtitle("Ln(Contract value)")
    graph export "../output/ec_catalog_cdf.png", replace
    
    * E-catalog table
    use "$data_dir/proc_contracts/electronic_catalog" if category1 != "Medicamentos", clear

    merge m:1 year using "../temp/priceindex", ///
            assert(2 3) keep(3) nogen

    replace total_value = total_value/priceindex*100
    replace price_per_unit = price_per_unit/priceindex*100	
    
    * Identify number of providers (per year)
	bys year provider_id cpc: gen orderProvcpc = _n
	gen first = 1 if orderProvcpc == 1
	
    bys cpc year: egen totProviders = sum(first)
	bys cpc year: gen order = _n
	replace totProviders = . if order > 1

    foreach var in total_value price_per_unit total_quantity totProviders {
        qui sum `var'
        mat col = (round(`r(mean)', .01) \ round(`r(sd)', .01))
        mat ec_table = (nullmat(ec_table), col)
    }

    qui count
    mat col = (`r(N)' \ .)
    mat ec_table = (nullmat(ec_table), col)

    mat rownames ec_table = "Mean" "SD"
    mat colnames ec_table = "Contract value (\$)" "Unit price (\$)" ///
        "Quantity (units)" "Number competitors" "Number of transactions"
    esttab matrix(ec_table, fmt(%12.0gc)) using "../output/ec_table.tex", replace
end

program sample_size_table
    * Note: need to impose condition on year so that the sample is consistent with the
    * one used in the event studies
    use internal_firm_id strategic_entrant created_by_bureaucrat year ///
        provider_entry_year nbr_connections distinct_conn_years any_connection ///
        direct_connection_only indirect_connection_only both_connections_type ///
        using "../temp/contractors_balance_connections" ///
        if provider_entry_year!=2000 & year >= 2009, clear

    ** Sample sizes **
    duplicates drop

    build_sample_size_block
    mat table = (nullmat(table), r(sample_col))

    build_sample_size_block, and_condition("& direct_connection_only==1")
    mat table = (nullmat(table), r(sample_col))

    build_sample_size_block, and_condition("& indirect_connection_only==1")
    mat table = (nullmat(table), r(sample_col))

    build_sample_size_block, and_condition("& both_connections_type==1")
    mat table = (nullmat(table), r(sample_col))

    matrix rownames table = "Nbr non-politically connected" "Not conn to strat bureauc" ///
        "Avg number distinct years" "Avg number of connections" "Conn to strat bureauc" ///
        "Avg number distinct years" "Avg number of connections" "Created by bureaucrat" ///
        "Avg number distinct years" "Avg number of connections"
    matrix colnames table = "All connections" "Only direct" "Only indirect" "Both direct and indirect"

    esttab matrix(table, fmt(%12.3fc)) using "../output/sample_sizes.tex", replace
end

program build_sample_size_block, rclass
    syntax, [and_condition(str)]

    qui distinct internal_firm_id if any_connection==0
    local uncon `r(ndistinct)'

    qui distinct internal_firm_id if any_connection==1 & strategic_entrant==0 & ///
        created_by_bureaucrat==0 `and_condition'
    local non_strat `r(ndistinct)'

    qui sum distinct_conn_years if any_connection==1 & strategic_entrant==0 & ///
        created_by_bureaucrat==0 `and_condition'
    local con_year_non_strat `r(mean)'

    qui sum nbr_connections if any_connection==1 & strategic_entrant==0 & ///
        created_by_bureaucrat==0 `and_condition'
    local con_nbr_non_strat `r(mean)'

    qui distinct internal_firm_id if any_connection==1 & strategic_entrant==1 & ///
        created_by_bureaucrat==0 `and_condition'
    local strategic `r(ndistinct)'

    qui sum distinct_conn_years if any_connection==1 & strategic_entrant==1 & ///
        created_by_bureaucrat==0 `and_condition'
    local con_year_strat `r(mean)'

    qui sum nbr_connections if any_connection==1 & strategic_entrant==1 & ///
        created_by_bureaucrat==0 `and_condition'
    local con_nbr_strat `r(mean)'

    qui distinct internal_firm_id if any_connection==1 & strategic_entrant==0 & ///
        created_by_bureaucrat==1 `and_condition'
    local created_by_bureauc `r(ndistinct)'

    qui sum distinct_conn_years if any_connection==1 & strategic_entrant==0 & ///
        created_by_bureaucrat==1 `and_condition'
    local con_year_created `r(mean)'

    qui sum nbr_connections if any_connection==1 & strategic_entrant==0 & ///
        created_by_bureaucrat==1 `and_condition'
    local con_nbr_created `r(mean)'

    mat sample_col = (floor(`uncon') \ floor(`non_strat') \ round(`con_year_non_strat', .001) \ ///
        round(`con_nbr_non_strat', .001) \ floor(`strategic') \ round(`con_year_strat', .001) \ round(`con_nbr_strat', .001) \ ///
        floor(`created_by_bureauc') \ round(`con_year_created', .001) \ round(`con_nbr_created', .001))

    return mat sample_col = sample_col
end

* Execute
main

