* NOTE: Falsification exercise using years before political connection 
* still gives a positive and significant markup in Wooldridge specification

set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    use "$data_dir/balance_sheet/derived/balance_sheet_govSalesMarkup_tfp", clear

    foreach spec in Ols Woold {
        gen markup_`spec' = bC1_`spec'/(1-bC1_`spec')
    }

    gcollapse (mean) markup_* firms_* sampleSize_*, by(isic2_nbr rep)

    save_data "../temp/markup_Estimates_sectors", replace key(isic2_nbr rep)

    foreach spec in Ols Woold {
        use "../temp/markup_Estimates_sectors" if rep>0, clear

        collapse (mean) markup_`spec' [aweight=firms_`spec'], by(rep)
        collapse (sd) markup_`spec'

        save "../temp/markup_EstimatesSE_`spec'", replace

        use "../temp/markup_Estimates_sectors" if rep==0, clear

        collapse markup_`spec' [aweight=firms_`spec']

        save "../temp/markup_Estimates_`spec'", replace
    }

    foreach stub in "" "SE" {
        use "../temp/markup_Estimates`stub'_Ols", clear
        merge 1:1 _n using "../temp/markup_Estimates`stub'_Woold", nogen

        save "../temp/markup_Estimates`stub'", replace
    }

    use "../temp/markup_Estimates_sectors" if rep==0, clear

    collapse (sum) sampleSize_*
    gen id = _n

    save "../temp/misallocation_EstimatesSample", replace

    use "../temp/markup_Estimates", clear
    append using "../temp/markup_EstimatesSE"

    mkmat markup_Ols markup_Woold, mat(markups)

    mat colnames markups = "Ols" "Woold"
    esttab matrix(markups, fmt(%15.4f)) using "../output/gov_markup_connection.tex", replace
end

* Execute
main
