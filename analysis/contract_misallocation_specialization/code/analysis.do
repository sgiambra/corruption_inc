cd "/Users/felipe/Dropbox (Brown)/Research/corruption_inc/analysis/contract_misallocation_specialization/code"


set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/"


program main
    local reps 30


    foreach stub in  "_govSalesMarkup" {
		foreach specialvar in  dyn_spec_75 dyn_spec_50 spec_75 spec_50 {
			compute_misallocation_estimates, stub(`stub') reps(`reps') special(`specialvar')
		}
    }
		

    * Build misallocation table
    file open excess_costs_table using "../output/excess_costs.tex", write replace
    file write excess_costs_table "& Flexible Woold & Fixed Woold \\"  _n

    foreach spec in "dyn_spec_750" "dyn_spec_751" "dyn_spec_500" "dyn_spec_501" "spec_750" "spec_751" "spec_500" "spec_501" {

        build_misallocation_table, stub("_govSalesMarkup" ) special(`spec')
    }

    file close excess_costs_table

	* Statistics of Specialization
	statistics, stub("_govSalesMarkup")
end

program compute_misallocation_estimates
    syntax, reps(int) stub(str) special(str)

	
		forvalues sp = 1(-1)0{

			use "../temp/providers_balance`stub'_tfp_connections", clear
		
			
			display "Beginning `special' value `sp'"
		
			keep if `special' == `sp' & in_sample_Woold == 1
			
			
			bysort isic2_nbr rep: gen obs = _N
			bysort isic2_nbr: gegen min_obs =min(obs)
			drop if min_obs <15
			
		
			qui levelsof isic2_nbr, local(sectors)
			foreach sector in `sectors' {
				
			
				preserve
					keep if isic2_nbr == `sector'
					
					* Assume CRTS and initialize variables
					gen bk_Woold_crts = bk_Woold 
					gen bl_Woold_crts = bl_Woold 
					gen bm_Woold_crts = bm_Woold 

					gen flex_capital_Woold = .
					gen fixed_capital_Woold = .

					gen sampleSize_flex_Woold = .
					gen sampleSize_fixed_Woold = .


					forvalues rep = 0(1)`reps' {
				
							dis "Specification Woold`stub', Repetition `rep', Sector `sector'"
								qui: reg tfp_Woold connected i.year i.isic3_nbr if  ///
									isic2_nbr == `sector' & rep == `rep'

								local tfp_gap_Woold_mean = _b[connected]
								local sample_flex_Woold = e(N)

								qui reg s_k connected i.year i.isic3_nbr  if ///
									isic2_nbr == `sector' & rep == `rep' 

								local s_k_gap_Woold_mean = _b[connected]
								local sample_fixed_Woold = e(N)
						
							
							* Save estimates
							replace flex_capital_Woold = exp((-`tfp_gap_Woold_mean')/(bk_Woold_crts+bl_Woold_crts+bm_Woold_crts))-1 if ///
								isic2_nbr == `sector' & rep == `rep' & in_sample_Woold == 1

							replace sampleSize_flex_Woold = `sample_flex_Woold' if ///
								isic2_nbr == `sector' & rep == `rep' & in_sample_Woold == 1

							replace fixed_capital_Woold = exp(-(bk_Woold_crts/(bk_Woold_crts+bl_Woold_crts+bm_Woold_crts))*`s_k_gap_Woold_mean' - ///
								1/(bk_Woold_crts+bl_Woold_crts+bm_Woold_crts)*`tfp_gap_Woold_mean')-1 if ///
								isic2_nbr == `sector' & rep == `rep' & in_sample_Woold == 1

							replace sampleSize_fixed_Woold = `sample_fixed_Woold' if ///
								isic2_nbr == `sector' & rep == `rep' & in_sample_Woold == 1
						}
					

					gen flex_welfare_cost_Woold = (flex_capital_Woold*cost_share_uncon)*100
					gen fixed_welfare_cost_Woold = (fixed_capital_Woold*cost_share_uncon)*100

					gcollapse (mean) flex_capital_Woold fixed_capital_Woold flex_welfare_cost_Woold fixed_welfare_cost_Woold ///
						firms_Woold sampleSize_flex_Woold sampleSize_fixed_Woold, by(isic2_nbr rep)
				
					save_data "../temp/misallocation_Estimates_sectors`stub'_Woold_`sector'_`special'`sp'", replace key(isic2_nbr rep)
				restore
				
			}
			
		
			qui levelsof isic2_nbr, local(sectors)
			clear
			foreach sector in `sectors' { 
				cap: append using "../temp/misallocation_Estimates_sectors`stub'_Woold_`sector'_`special'`sp'"
				save_data "../temp/misallocation_Estimates_sectors`stub'_Woold_`special'`sp'", replace key(isic2_nbr rep)
			}
			
				
			keep if rep>0

			collapse (mean) flex_capital_Woold fixed_capital_Woold flex_welfare_cost_Woold ///
				fixed_welfare_cost_Woold firms_Woold [aweight=firms_Woold], by(rep)

			collapse (sd) flex_capital_Woold fixed_capital_Woold flex_welfare_cost_Woold ///
				fixed_welfare_cost_Woold

			save "../temp/misallocation_EstimatesSE`stub'_Woold_`special'`sp'", replace
			
		display	"Yes"

			use "../temp/misallocation_Estimates_sectors`stub'_Woold_`special'`sp'" if rep==0, clear
			collapse (sum) sampleSize_flex_Woold sampleSize_fixed_Woold
			gen id = _n
			

			save "../temp/misallocation_EstimatesSample`stub'_Woold_`special'`sp'", replace
			
		display	"Yes Yes"

			use "../temp/misallocation_Estimates_sectors`stub'_Woold_`special'`sp'" if rep==0, clear
		display	"Yes Yes Yes"

			collapse (mean) flex_capital_Woold fixed_capital_Woold flex_welfare_cost_Woold ///
				fixed_welfare_cost_Woold [aweight=firms_Woold]

			append using "../temp/misallocation_EstimatesSE`stub'_Woold_`special'`sp'"
			gen id = _n

			merge 1:1 id using "../temp/misallocation_EstimatesSample`stub'_Woold_`special'`sp'", nogen

			save "../temp/misallocation_Estimates`stub'_Woold_`special'`sp'", replace
			display "Final"
		}
    

    * Save estimates for sectoral analysis
*    use "../temp/misallocation_Estimates_sectors`stub'_Ols", clear
 *   merge 1:1 isic2_nbr rep using "../temp/misallocation_Estimates_sectors`stub'_Woold", ///
   *     nogen assert(3) keep(3)

  *  export delimited "../output/misallocation_Estimates_sectors`stub'.csv", replace
end

program build_misallocation_table
    syntax, stub(str) special(str)

    use "../temp/misallocation_Estimates`stub'_Woold_`special'", clear

    rename (flex_capital_Woold  flex_welfare_cost_Woold ///
        fixed_capital_Woold fixed_welfare_cost_Woold) ///
        (flex_Woold_Ec flex_Woold_Welfare ///
        fixed_Woold_Ec fixed_Woold_Welfare)

    foreach var in Ec Welfare {
        foreach cap_assum in flex fixed {
			qui sum `cap_assum'_Woold_`var' if id == 1
			local m_`cap_assum'_Woold_`var' = round(`r(mean)', 0.001)

			qui sum `cap_assum'_Woold_`var' if id == 2
			local se_`cap_assum'_Woold_`var' = round(`r(mean)', 0.001)

			qui sum sampleSize_`cap_assum'_Woold
			local ss_`cap_assum'_Woold = `r(mean)'

			if (abs(`m_`cap_assum'_Woold_`var'') >= `se_`cap_assum'_Woold_`var''*1.644854 & ///
				abs(`m_`cap_assum'_Woold_`var'') < `se_`cap_assum'_Woold_`var''*1.95996) {
				local m_`cap_assum'_Woold_`var' = "`m_`cap_assum'_Woold_`var''*"
			}
			else if (abs(`m_`cap_assum'_Woold_`var'') >= `se_`cap_assum'_Woold_`var''*1.95996 & ///
				abs(`m_`cap_assum'_Woold_`var'') < `se_`cap_assum'_Woold_`var''*2.32635) {
				local m_`cap_assum'_Woold_`var' = "`m_`cap_assum'_Woold_`var''**"
			}
			else if abs(`m_`cap_assum'_Woold_`var'') >= `se_`cap_assum'_Woold_`var''*2.32635 {
				local m_`cap_assum'_Woold_`var' = "`m_`cap_assum'_Woold_`var''***"
			
            }
        }

        if "`var'" == "Ec" & "`special'" == "dyn_spec_500" local panel "Dynamic Non-Specialization 50"
        else if "`var'" == "Ec" & "`special'" == "dyn_spec_501" local panel "Dynamic Specialization 50"
        else if "`var'" == "Ec" & "`special'" == "dyn_spec_750" local panel "Dynamic Non-Specialization 75"
        else if "`var'" == "Ec" & "`special'" == "dyn_spec_751" local panel "Dynamic Specialization 75"
        else if "`var'" == "Ec" & "`special'" == "spec_500" local panel "Non-Specialization 50"
	    else if "`var'" == "Ec" & "`special'" == "spec_501" local panel "Specialization 50"
        else if "`var'" == "Ec" & "`special'" == "spec_750" local panel "Non-Specialization 75"
	    else if "`var'" == "Ec" & "`special'" == "spec_751" local panel "Specialization 75"
        else if "`var'" == "Welfare" local panel ""

        file write excess_costs_table "`panel' & `m_flex_Woold_`var'' &  `m_fixed_Woold_`var'' \\" _n
        file write excess_costs_table "& (`se_flex_Woold_`var'') & (`se_fixed_Woold_`var'') \\" _n

        if "`var'" == "Welfare" {
            file write excess_costs_table " & `ss_flex_Woold' & `ss_fixed_Woold'" _n
        }
    }
end

program statistics
	syntax, stub(string)
	
	use "../temp/providers_balance`stub'_tfp_connections", clear
	keep if rep == 0 & in_sample_Woold == 1

	bysort isic2_nbr rep: gen obs = _N
	bysort isic2_nbr: gegen min_obs =min(obs)
	drop if min_obs <30
						
	
	* Distribution of Gov Shares
	matrix col = J(2,5,.)
	sum gov_share, det
	mat col[1,1] = round(`r(p25)',0.01)
	mat col[1,2] = round(`r(p50)',0.01)
	mat col[1,3] = round(`r(p75)',0.01)
	mat col[1,4] = round(`r(p90)',0.01)
	mat col[1,5] = round(`r(p95)',0.01)

	sum gov_share if gov_share >0, det
	mat col[2,1] = round(`r(p25)',0.01)
	mat col[2,2] = round(`r(p50)',0.01)
	mat col[2,3] = round(`r(p75)',0.01)
	mat col[2,4] = round(`r(p90)',0.01)
	mat col[2,5] = round(`r(p95)',0.01)


    mat colnames col = "P25" "Median" "P75" "P90" "P95"
    mat rownames col = "Any Year" "Years with positive sales"
    esttab matrix(col, fmt(%12.0gc)) using "../output/distribution_gov_shares.tex", replace	
	
	
end 

* Execute
main
