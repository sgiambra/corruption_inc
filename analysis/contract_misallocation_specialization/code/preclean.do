cd "/Users/felipe/Dropbox (Brown)/Research/corruption_inc/analysis/contract_misallocation_specialization/code"

set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    local gov_markup "1.06"

    use provider_id internal_firm_id using ///
        "$data_dir/proc_contracts/tables/providers_registry", clear
    duplicates drop provider_id, force
    save_data "../temp/providers_registry", replace key(provider_id)

    build_providers_balance, stub("_govSalesMarkup") ///
        markupToAdjust("`gov_markup'")
    build_tfp_connections, stub("_govSalesMarkup")
end

program build_providers_balance
    syntax, stub(str) [markupToAdjust(str)]
    
    use "$data_dir/balance_sheet/derived/balance_sheet`stub'_tfp", clear

    * Fix missing TFPs
    if "`stub'" == "_beforeConnection" {
        local eqyeardummies_Ols "(by1_Ols + by2_Ols + by3_Ols + by4_Ols + by5_Ols + by6_Ols + by7_Ols + by8_Ols + by9_Ols + by10_Ols)"
        local eqyeardummies_Woold "(by1_Woold + by2_Woold + by3_Woold + by4_Woold + by5_Woold + by6_Woold + by7_Woold + by8_Woold)"

        foreach spec in Ols Woold {
            bys expediente rep (year): replace in_sample_`spec' = 1 if in_sample_`spec'[_n-1] == 1

            replace tfp_`spec' = r - bl_`spec'*free - bm_`spec'*proxy - bk_`spec'*state - ///
                `eqyeardummies_`spec'' if in_sample_`spec' == 1 & tfp_`spec' == .
        }
    }

    if "`markupToAdjust'" != "" {
        gen gov_sales_ind = gov_sales>0
        gen r_adj = r - ln(1 + gov_sales_ind*(1-1/`markupToAdjust')) if after_connection == 1
        replace r = r_adj if r_adj != .
		
    }
	
    gen gov_share = gov_sales/totalRevenue
    replace gov_share = 1 if gov_share > 1 & gov_share !=.
	* Specialized 
	preserve
		
		keep gov_share firm_id year
		gduplicates drop
				
		* Obtain minimum gov share (all the time) and minimum from today onwards
		bysort firm_id: gegen min_gov_share = min(gov_share)
		
		gen dynmin_gov_share = .
		
		forvalues year = 2009(1)2016{
			bysort firm_id: gegen min_gov_share_temp = min(gov_share) if year>=`year'
			replace dynmin_gov_share = min_gov_share_temp if year == `year'
			drop min_gov_share_temp
		}
		
		save_data "../temp/gov_shares", key(firm_id year) replace
		
	restore
	
	merge m:1 firm_id year using "../temp/gov_shares", assert(3) keep(3) nogen
	
	* Gen Specialized
	gen spec_50 = gov_share>=0.5 & gov_share !=.
	gen spec_75 = gov_share>=0.75 & gov_share !=.
	gen dyn_spec_50 = dynmin_gov_share>=0.50 & dynmin_gov_share !=.
	gen dyn_spec_75 = dynmin_gov_share>=0.75 & dynmin_gov_share !=.
	
	

	
    rename firm_id provider_id
    merge m:1 provider_id using "../temp/providers_registry", ///
        nogen assert(1 2 3) keep(3)

    * Capital Share of Revenue Gap
    gen s_k = state-r if rep == 0
    winsor2 s_k, cuts(1 99) replace by(rep)
    bys internal_firm_id year (rep): replace s_k = s_k[1]

    * Cost share
    gen cost_share = (exp(proxy) + exp(free))/exp(r) if rep == 0
    winsor2 cost_share, cuts(1 99) replace by(rep)
    bys internal_firm_id year (rep): replace cost_share = cost_share[1]

    save_data "../temp/providers_balance`stub'_tfp", ///
        replace key(provider_id year rep boot_firm_id)
end

program build_tfp_connections
    syntax, stub(str)

    use "$data_dir/providers_connections/master/providers_connections_all", clear

    merge 1:m internal_firm_id using "../temp/providers_balance`stub'_tfp", ///
        assert(1 2 3) keep(2 3)

    * No need to apply restrictions since already dropped from production estimation

    gen connected = _merge == 3
    drop _merge

    gen years_before_connection = connected == 1 & year < provider_entry_year

    gen uncon_vs_years_bef_entry = 0 if connected == 0
    replace uncon_vs_years_bef_entry = 1 if connected == 1 & years_before_connection == 1
    replace uncon_vs_years_bef_entry = 2 if connected == 1 & years_before_connection == 0

    gen cost_share_uncon = cost_share if connected == 0

    save_data "../temp/providers_balance`stub'_tfp_connections", ///
        replace key(provider_id year rep boot_firm_id)
end

* Execute
main
