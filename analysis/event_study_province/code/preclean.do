set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    local window 4

    build_balance
    build_winners_table
    build_providers_connect_prov
    build_winners_connections
        
    build_same_province_panel, window(`window')
end

program build_balance
    use provider_id internal_firm_id using ///
        "$data_dir/proc_contracts/tables/providers_registry", clear
    duplicates drop provider_id, force
    save_data "../temp/providers_registry", replace key(provider_id)

    use "$data_dir/balance_sheet/master/balance_sheet_clean" if contractor==1, clear

    * Two firms missclassified as same by internal_firm_id
    duplicates drop internal_firm_id year, force

    preserve
        keep internal_firm_id startYear isic3
        duplicates drop internal_firm_id, force
        save_data "../temp/list_firm_providers", key(internal_firm_id) replace
    restore

    drop startYear isic3
    save_data "../temp/providers_balance_panel", replace key(internal_firm_id year)
end

program build_winners_table
    use "$data_dir/proc_contracts/tables/contracts_master_with_restrictions" ///
        if contract_year>=2009, clear
    
    rename contract_year year

    * Keep contracts for which we know province
    encode province, gen(enprov)
    drop if enprov == .

    gcollapse (sum) contract_value nbr_contracts=id, ///
        by(internal_firm_id year enprov)

    save_data "../temp/winners_table", replace ///
        key(internal_firm_id year enprov)
end

program build_providers_connect_prov
    foreach input_file in provider_owned_by_bureaucrat ///
        provider_owned_by_sibling_of_bureaucrat {

        use "../temp/providers_registry", clear
        merge 1:m provider_id using ///
            "$data_dir/providers_connections/`input_file'_province", ///
            nogen assert(1 3) keep(3)

        save_data "../temp/`input_file'", replace ///
            key(internal_firm_id shareholder_id enprov)
    }

    use "../temp/provider_owned_by_bureaucrat", clear
    append using "../temp/provider_owned_by_sibling_of_bureaucrat"

    * Keep firms connected to a single province
    egen distinct_province_t = tag(internal_firm_id enprov)
    bys internal_firm_id (enprov): egen distinct_province = sum(distinct_province_t)
    keep if distinct_province == 1

    gcollapse (min) province_entry_year=owner_entry_year, ///
        by(internal_firm_id enprov)

    * Drop firms with missing info on province
    drop if enprov == 0

    save_data "../temp/providers_entry_province", replace key(internal_firm_id)
end

program build_winners_connections
    * Politically connected firms
    use "../temp/providers_entry_province", clear
    rename enprov enprov_connection

    merge 1:m internal_firm_id using "../temp/winners_table", ///
        nogen assert(1 2 3) keep(3)

    gen same_province = enprov == enprov_connection
    drop enprov_connection

    gcollapse (sum) contract_value nbr_contracts (mean) province_entry_year, ///
        by(internal_firm_id year same_province)

    save_data "../temp/contracts_connected", ///
        replace key(internal_firm_id year same_province)

    * Non-politically connected firms
    use internal_firm_id using "../temp/providers_entry_province", clear
    merge 1:m internal_firm_id using "../temp/winners_table", ///
        nogen assert(1 2 3) keep(2)

    * Randomly assign half of the firm-provinces to "same-province" control group
    gen random = runiform()
    bys internal_firm_id enprov (year): replace random = random[1]

    gen same_province = random >= 0.5

    gcollapse (sum) contract_value nbr_contracts, ///
        by(internal_firm_id year same_province)

    save_data "../temp/contracts_not_connected", ///
        replace key(internal_firm_id year same_province)
end

program build_same_province_panel
    syntax, window(int)
    
    * Create rectangular panel, starting in 2009
    clear
    set obs 9
    gen year = 2008 + _n
    gen joinvar = 1
    save_data "../temp/panel_years", replace key(year)

    clear
    set obs 2
    gen same_province = _n - 1
    gen joinvar = 1
    save_data "../temp/same_provinces", replace key(same_province)

    use "../temp/providers_registry", clear
    keep internal_firm_id
    duplicates drop
    gen joinvar = 1
    joinby joinvar using "../temp/panel_years"
    joinby joinvar using "../temp/same_provinces"
    drop joinvar

    save_data "../temp/panel", replace ///
        key(internal_firm_id year same_province)

    * Connected
    use "../temp/panel", clear
    merge m:1 internal_firm_id using "../temp/providers_entry_province", ///
        nogen assert(1 3) keep(3) keepusing(internal_firm_id)
    merge 1:1 internal_firm_id year same_province using ///
        "../temp/contracts_connected", nogen assert(1 3) keep(1 3)

    save_data "../temp/connected_panel", replace ///
        key(internal_firm_id year same_province)

    * Not connected
    use "../temp/panel", clear

    merge m:1 internal_firm_id using "../temp/providers_entry_province", ///
        nogen assert(1 3) keep(1) keepusing(internal_firm_id)
    merge m:1 internal_firm_id year same_province using ///
        "../temp/contracts_not_connected", nogen assert(1 2 3) keep(1 3)

    append using "../temp/connected_panel"

    replace contract_value = 0 if contract_value == .
    replace nbr_contracts = 0 if nbr_contracts == .

    merge m:1 internal_firm_id using "../temp/list_firm_providers", ///
        nogen assert(1 3) keep(3)
    * Keep if _merge == 1 if want to include firms that do not submit balance sheet in a given year
    merge m:1 internal_firm_id year using "../temp/providers_balance_panel", ///
        nogen assert(1 2 3) keep(3)

    merge m:1 internal_firm_id using ///
        "$data_dir/providers_connections/master/providers_connections_all", ///
        nogen assert(1 2 3) keep(1 3) ///
        keepusing(strategic_entrant provider_entry_year created_by_bureaucrat)
    
    * Drop uncertain connections
    drop if provider_entry_year == 2000
    * Drop strategic entrants
    drop if strategic_entrant == 1
    * Drop if created by bureaucrat
    drop if created_by_bureaucrat == 1

    gen prob = nbr_contracts > 0

    generate_reggroups, window(`window') ///
        connection_date(provider_entry_year)

    * Set control group to -1
    replace relative_ev_year_reggroups = 4 if provider_entry_year == .

    keep if year >= startYear

    label var prob      "Prob(Awarded contract)"

    save_data "../temp/providers_connections_all", replace ///
        key(internal_firm_id year same_province)
end

program generate_reggroups
    syntax, window(int) connection_date(str)

    gen relative_ev_year = year - `connection_date'

    sort internal_firm_id relative_ev_year
    gen relevant_time_period = (abs(relative_ev_year) <= `window')
    egen relative_ev_year_reggroups = group(relative_ev_year) if relevant_time_period
    replace relative_ev_year_reggroups = 0 if relative_ev_year < -`window'

    replace relative_ev_year_reggroups = 1000 if relative_ev_year > `window' & `connection_date' != .

    label var relative_ev_year_reggroups    "Years relative to first political connection"
end

* Execute
main
