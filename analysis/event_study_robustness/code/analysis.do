cd "/Users/felipe/Dropbox (Brown)/Research/corruption_inc/analysis/event_study_robustness/code"

set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

program main
    cap: mkdir "../output/tables"
	
      foreach depvar in prob1 prob5 prob10 prob25 {
	  
        * Panel A: Strategic entrants
        use "../temp/providers_connections_all", clear
					
		keep if  (created_by_bureaucrat==0 ///
		& strategic_entrant==1) | treatment==0

		single_dummy_regression, depvar(`depvar') title("Strategic Entrant")
	
	
   
		 use "../temp/providers_connections_all", clear
        * Panel B: Drop strategic connections from other columns
        drop if strategic_entrant==1 | created_by_bureaucrat==1

        * Large reshuffles
		preserve
			keep if (oneOrMore_large_reshuffles==1 | treatment==0)	
			single_dummy_regression, depvar(`depvar') title("Large Reshuffles")
		restore
		
        * Only one connection year

		preserve
			keep if nbr_connections==1 | treatment==0
			single_dummy_regression, depvar(`depvar') title("Single Entry Year")
		restore
		
        * Drop strategic exit
		preserve
			keep if strategic_exit==0 | treatment==0
			single_dummy_regression, depvar(`depvar') title("No Strategic Exit")
		restore
		
    }
end
    
	


program single_dummy_regression
	syntax, depvar(string) title(string) 
	
		qui distinct internal_firm_id
		local nbr_firms `r(ndistinct)'
		
		qui sum `depvar' if year < provider_entry_year & treatment == 1
		local mean_before_conn `r(mean)'

		qui distinct internal_firm_id if treatment == 1
		local nbr_connected_firms `r(ndistinct)'
		
		sum `depvar'
		local nbr_obs `r(N)'
		
		staggered_cs, y("`depvar'") g("provider_entry_year") t("year") i("fid") estimand("simple") 
			
				ereturn list 
				matrix coef= e(b)
				matrix colname coef = afterTreatment 
				erepost b=coef, rename

				outreg2 using "../output/tables/DD_`depvar'_table.tex",  nocons dec(4) append ctitle(`title')  ///
				adds(Observations, `nbr_obs' , Number contractors, `nbr_firms', Connected contractors, `nbr_connected_firms', ///
                Mean before connection, `mean_before_conn') nor2

end



* Execute
main
