	(1)	(2)	(3)	(4)
VARIABLES	Strategic Entrant	Large Reshuffles	Single Entry Year	No Strategic Exit
				
afterTreatment	0.0610***	0.0318**	0.0268**	0.0462***
	(0.0214)	(0.0151)	(0.0125)	(0.0118)
				
Observations	.	161536	169883	.
Observations	157146	.	.	170473
Number contractors	24111	24750	26029	26184
Connected contractors	1114	1753	3032	3187
Mean before connection	0.170	0.209	0.211	0.198
Standard errors in parentheses				
*** p<0.01, ** p<0.05, * p<0.1				
