set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
   *** Electronic-catalog regressions ***
    use "../temp/electronic_catalog_reg", clear
    descriptives

    mat descript = r(col)

    reghdfe price_dm ib(0).uncon_vs_years_bef_entry_own uncon_vs_years_bef_entry_per1 ///
        uncon_vs_years_bef_entry_per2 quantity_dm, absorb(year agency) vce(cluster agency)

    test 1.uncon_vs_years_bef_entry_own == 2.uncon_vs_years_bef_entry_own

    outreg2 using "../output/price_table.tex", tex drop(uncon_vs_years_bef_entry_per1 ///
        uncon_vs_years_bef_entry_per2) addtext(Year FE, Yes, Agency FE, Yes) ///
        addstat(p-val diff, r(p)) dec(4) replace

    welfare_estimates
    
    gcollapse (mean) av_price_dm=price_dm av_quantity_dm=quantity_dm uncon_vs_years_bef_entry_per1 ///
        uncon_vs_years_bef_entry_per2 uncon_vs_years_bef_entry_own, by(year internal_firm_id)
    
    reghdfe av_price_dm ib(0).uncon_vs_years_bef_entry_own uncon_vs_years_bef_entry_per1 ///
        uncon_vs_years_bef_entry_per2 av_quantity_dm, absorb(year) vce(cluster internal_firm_id)

    test 1.uncon_vs_years_bef_entry_own == 2.uncon_vs_years_bef_entry_own
    
    outreg2 using "../output/price_table.tex", tex drop(uncon_vs_years_bef_entry_per1 ///
        uncon_vs_years_bef_entry_per2) addtext(Year FE, Yes, Agency FE, No) ///
        addstat(p-val diff, r(p)) dec(4) append

    welfare_estimates

    *** Auction regressions ***
    use "../temp/auctions_reg", clear
    descriptives

    mat descript = (descript, r(col))

    mat rownames descript = "Avg nbr orders" "Avg nbr products" "Share wholesale transact"
    mat colnames descript = "E-catalog" "Auctions"
    esttab matrix(descript, fmt(%12.0gc)) using "../output/price_inflation_descriptives.tex", replace

    reghdfe price_dm ib(0).uncon_vs_years_bef_entry_own uncon_vs_years_bef_entry_per1 ///
        uncon_vs_years_bef_entry_per2 quantity_dm, absorb(year agency) vce(cluster agency)

    test 1.uncon_vs_years_bef_entry_own == 2.uncon_vs_years_bef_entry_own

    outreg2 using "../output/price_table.tex", tex drop(uncon_vs_years_bef_entry_per1 ///
        uncon_vs_years_bef_entry_per2) addtext(Year FE, Yes, Agency FE, Yes) ///
        addstat(p-val diff, r(p)) dec(4) append

    welfare_estimates
    
    gcollapse (mean) av_price_dm=price_dm av_quantity_dm=quantity_dm uncon_vs_years_bef_entry_per1 ///
        uncon_vs_years_bef_entry_per2 uncon_vs_years_bef_entry_own, by(year internal_firm_id)
    
    reghdfe av_price_dm ib(0).uncon_vs_years_bef_entry_own uncon_vs_years_bef_entry_per1 ///
        uncon_vs_years_bef_entry_per2 av_quantity_dm, absorb(year) vce(robust)

    test 1.uncon_vs_years_bef_entry_own == 2.uncon_vs_years_bef_entry_own
    
    outreg2 using "../output/price_table.tex", tex drop(uncon_vs_years_bef_entry_per1 ///
        uncon_vs_years_bef_entry_per2) addtext(Year FE, Yes, Agency FE, No) ///
        addstat(p-val diff, r(p)) dec(4) append

    welfare_estimates

    * Save welfare estimates
    mat rownames welfare = "DWLP" "" "dB x MCPF" "" "Total" ""
    esttab matrix(welfare, fmt(%12.3fc)) using "../output/welfare_price_inflation.tex", replace
end

program descriptives, rclass
    cap: mat drop col

    preserve
        gen count = 1
        gcollapse (sum) count, by(internal_firm_id year product_name)

        qui sum count
        mat col = (`r(mean)') 
        drop count

        gen count = 1
        gcollapse (sum) count, by(internal_firm_id year)
        
        qui sum count
        mat col = (col \ `r(mean)') 
    restore

    preserve
        merge m:1 internal_firm_id using "../temp/list_rucs_directory.dta", keep(3)

        qui count if isic0 == "G"
        local wholesale = `r(N)'

        qui count
        local share_whole = `wholesale'/`r(N)'

        mat col = (col \ `share_whole')
    restore

    return mat col = col
end

program welfare_estimates
    * DWLP
    nlcom -1/2*(-0.8)*_b[2.uncon_vs_years_bef_entry_own]^2/(1+(1-0.8)*_b[2.uncon_vs_years_bef_entry_own])*100
    mat V = r(V)
    mat dwlp = (r(b) \ sqrt(V[1,1]))

    * dB x MCPF
    nlcom (1-0.8)*_b[2.uncon_vs_years_bef_entry_own]/(1+(1-0.8)*_b[2.uncon_vs_years_bef_entry_own])*100*1.33
    mat V = r(V)
    mat dB = (r(b) \ sqrt(V[1,1]))

    * Total
    nlcom (1-0.8)*_b[2.uncon_vs_years_bef_entry_own]/(1+(1-0.8)*_b[2.uncon_vs_years_bef_entry_own])*100*1.33 - ///
        1/2*(-0.8)*_b[2.uncon_vs_years_bef_entry_own]^2/(1+(1-0.8)*_b[2.uncon_vs_years_bef_entry_own])*100
    mat V = r(V)
    mat tot = (r(b) \ sqrt(V[1,1]))

    mat col = (dwlp \ dB \ tot)
    mat welfare = (nullmat(welfare), col)
end

* Execute
main    
