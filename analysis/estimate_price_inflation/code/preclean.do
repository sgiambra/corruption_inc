set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    local competitors_threshold 2

    get_firm_sector
    build_individual_connections
    subset_ec_data, competitors_threshold(`competitors_threshold')
    subset_auction_data, competitors_threshold(`competitors_threshold')
end

program get_firm_sector
    use provider_id internal_firm_id using ///
        "$data_dir/proc_contracts/tables/providers_registry", clear
    duplicates drop provider_id, force
    save_data "../temp/providers_registry", replace key(provider_id)

    use expediente rucFirm isic sector using ///
        "$data_dir/firms_ownership/directory", replace
    rename rucFirm firm_id
    drop if firm_id == "."
    * Almost 200k rucs become of length 10
    replace firm_id = substr(firm_id, 1, 10) if ///
        substr(firm_id, -3, 3) == "001" & length(firm_id) == 13

    save_data "../temp/list_rucs_directory.dta", key(firm_id) replace

    rename firm_id provider_id
    merge m:1 provider_id using "../temp/providers_registry", ///
        nogen assert(1 2 3) keep(3)

    duplicates drop internal_firm_id, force

    gen isic0 = substr(isic, 1, 1)

    save_data "../temp/list_rucs_directory.dta", key(internal_firm_id) replace
end

program build_individual_connections
    foreach input_file in provider_is_bureaucrat ///
        provider_sibling_is_bureaucrat {
        
        use "../temp/providers_registry", clear
        merge 1:m provider_id using "$data_dir/providers_connections/`input_file'", ///
            nogen assert(1 3) keep(3)
        
        gcollapse (min) entry_year, by(internal_firm_id)
        
        save_data "../temp/`input_file'", replace key(internal_firm_id)
    }

    use "../temp/provider_is_bureaucrat", clear
    rename entry_year entry_year_person_dir

    merge 1:1 internal_firm_id using "../temp/provider_sibling_is_bureaucrat", ///
        nogen assert(1 2 3) keep(1 2 3)
    rename entry_year entry_year_person_indir

    egen entry_year_person = rowmin(entry_year_person_dir entry_year_person_indir)

    save_data "../temp/individual_providers_connections", ///
        replace key(internal_firm_id)
end

program subset_ec_data
    syntax, competitors_threshold(int)

    * Import e-catalog for non-medicine products
    use "$data_dir/proc_contracts/electronic_catalog" if ///
        category1 != "Medicamentos", clear

    * Issue: e-catalog was not used in providers registry 
    * (about 5,000 obs have no internal_firm_id)
    merge m:1 provider_id using "../temp/providers_registry", ///
        nogen assert(1 2 3) keep(1 3)
    replace internal_firm_id = provider_id if internal_firm_id == ""

    merge m:1 internal_firm_id using ///
        "$data_dir/providers_connections/master/providers_connections_all", ///
        assert(1 2 3) keep(1 3)

    gen political_connection_firm = _merge == 3
    drop _merge

    * Drop uncertain connections
    drop if provider_entry_year == 2000
    * Drop strategic entrants
    drop if strategic_entrant == 1
    * Drop created by bureaucrat
    drop if created_by_bureaucrat == 1

    gen years_before_entry_own = political_connection_firm == 1 & year < provider_entry_year

    gen uncon_vs_years_bef_entry_own     = 0 if political_connection_firm == 0
    replace uncon_vs_years_bef_entry_own = 1 if political_connection_firm == 1 & years_before_entry_own == 1
    replace uncon_vs_years_bef_entry_own = 2 if political_connection_firm == 1 & years_before_entry_own == 0

    egen agency = group(agency_id)

    // Drop products with less than 2 providers
    egen tag_pf = tag(product_name internal_firm_id year)
    bys product_name year: egen num_providers = total(tag_pf)
    drop if num_providers < `competitors_threshold'

    rename (price_per_unit total_quantity) (price quantity)

    build_demeaned_vars

    * Add dummy for politically connected individual providers
    merge m:1 internal_firm_id using "../temp/individual_providers_connections", ///
        assert(1 2 3) keep(1 3)
    
    gen political_connection_person = _merge == 3
    drop _merge

    gen years_before_entry_per = political_connection_person == 1 & year < entry_year_person

    * Need to keep separate variables to deal with outreg2
    gen uncon_vs_years_bef_entry_per1 = political_connection_person == 1 & years_before_entry_per == 1
    gen uncon_vs_years_bef_entry_per2 = political_connection_person == 1 & years_before_entry_per == 0

    save "../temp/electronic_catalog_reg", replace
end

program subset_auction_data
    syntax, competitors_threshold(int)

    * Get contract winners
    use "$data_dir/proc_contracts/tables/contracts_master_with_restrictions", clear

    * make.py doesn't recognize anymore utf8 characters
    keep if strpos(contract_type, "Subasta Inversa Electr") > 0

    keep contract_link internal_firm_id contract_year province agency_name agg_contract_value contract_budget
    duplicates drop
    rename contract_year year
    
    save_data "../temp/contract_winners", key(contract_link) replace

    use "$data_dir/proc_contracts/scraped/clean/products", clear
    merge m:1 contract_link using "../temp/contract_winners", ///
        nogen assert(1 2 3) keep(3)

    merge m:1 internal_firm_id using "$data_dir/providers_connections/master/providers_connections_all", ///
        assert(1 2 3) keep(1 3)
    
    gen political_connection_firm = _merge == 3
    drop _merge

    * Drop uncertain connections
    drop if provider_entry_year == 2000
    * Drop strategic entrants
    drop if strategic_entrant == 1
    * Drop created by bureaucrat
    drop if created_by_bureaucrat == 1

    gen years_before_entry_own = political_connection_firm == 1 & year < provider_entry_year

    gen uncon_vs_years_bef_entry_own     = 0 if political_connection_firm == 0
    replace uncon_vs_years_bef_entry_own = 1 if political_connection_firm == 1 & years_before_entry_own == 1
    replace uncon_vs_years_bef_entry_own = 2 if political_connection_firm == 1 & years_before_entry_own == 0

    egen agency = group(agency_name)

    // Drop products with less than 2 providers
    egen product_name = group(product_description product_id product_unit_size)
    egen tag_pf = tag(product_name internal_firm_id year)

    * Observations with num_providers = 0 have missing values in one of variables
    bys product_name year: egen num_providers = total(tag_pf)
    drop if num_providers < `competitors_threshold'

	drop product_unit_price
	gen product_unit_price=agg_contract_value/product_quantity
	*gen price3=contract_budget/product_quantity
    rename (product_unit_price product_quantity) (price quantity)

    build_demeaned_vars

    * Add dummy for politically connected individual providers
    merge m:1 internal_firm_id using "../temp/individual_providers_connections", ///
        assert(1 2 3) keep(1 3)
    
    gen political_connection_person = _merge == 3
    drop _merge

    gen years_before_entry_per = political_connection_person == 1 & year < entry_year_person

    * Need to keep separate variables to deal with outreg2
    gen uncon_vs_years_bef_entry_per1 = political_connection_person == 1 & years_before_entry_per == 1
    gen uncon_vs_years_bef_entry_per2 = political_connection_person == 1 & years_before_entry_per == 0

    save "../temp/auctions_reg", replace
end

program build_demeaned_vars
    // Price and quantity demeaned
    foreach var in price quantity {
        gen ln_`var' = ln(`var')
        bys product_name year: egen mean_ln_`var' = mean(ln_`var')
        gen `var'_dm = ln_`var' - mean_ln_`var'
    }

    // Removing Outliers
    foreach var in price_dm quantity_dm {
        winsor2 `var', c(1 99) replace
    }
end

* Execute
main
