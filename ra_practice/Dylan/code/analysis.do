set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

program main
    use "http://www.stata-press.com/data/r13/overlap1", clear
    save_data "../temp/overlap1", key(id seq) replace

    use "http://www.stata-press.com/data/r13/overlap2", clear
    
    * Save, using as key the variable id
    

    use "../temp/overlap1", clear
    
    * Merge to overlap2, keeping only matching observations
    
end

* Execute
main
