set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/"

program main
    build_ancillary_data

    foreach level in "bureaucrat" "highLevel_bureaucrat" {
        clean_data, level_bureaucrat(`level')
        merge_data, level_bureaucrat(`level')
    }
end

program build_ancillary_data
    use provider_id internal_firm_id using ///
        "$data_dir/proc_contracts/tables/providers_registry", clear
    duplicates drop provider_id, force
    save_data "../temp/providers_registry", replace key(provider_id)

    * Get balance sheet data
    use rucFirm startYear using "$data_dir/firms_ownership/directory", replace
    rename rucFirm provider_id
    drop if provider_id == "."
    * Almost 200k rucs become of length 10
    replace provider_id = substr(provider_id, 1, 10) if ///
        substr(provider_id, -3, 3) == "001" & length(provider_id) == 13

    merge 1:1 provider_id using "../temp/providers_registry", ///
        nogen assert(1 2 3) keep(3)
    save_data "../temp/providers_registry", replace key(provider_id)
end

program clean_data
    syntax, level_bureaucrat(str)

    foreach input_file in "provider_owned_by_`level_bureaucrat'" ///
        "provider_owned_by_sibling_of_`level_bureaucrat'" {
        
        use "../temp/providers_registry", clear
        merge 1:m provider_id using "$data_dir/providers_connections/`input_file'", ///
            nogen assert(1 3) keep(3)

        * Fix inconsistencies
        replace first_year_shareholder = startYear if first_year_shareholder < startYear

        save_data "../temp/`input_file'", replace key(internal_firm_id shareholder_id)
        
        /*  Drop if owner buys shares after becoming connected (strategic entrants)
            
            Deal with exogeneity assumption
            But what about other side, i.e. owners are taking the active decision to become bureaucrats?
            We can only look at pretrends to exclude endogenous entry
            (Of the two directions this seems the most likely to hold)

            Issue: data on bureaucracy entry spans more years than shareholders data

            Note: by dropping before taking min we include firms potentially "contaminated" if they are
            connected to more than one bureaucrat. Check how many connections on average
        */

        * Strategic dummies
        * NOTE: firms that are created exactly when bureaucrat enters are not classified as strategic
        gen strategic_entrant = owner_entry_year < first_year_shareholder
        gen strategic_exit    = owner_entry_year > last_year_shareholder

        * If strategic entrant, assume connected at time it becomes owner
        replace owner_entry_year = first_year_shareholder if strategic_entrant == 1

        gen created_by_bureaucrat = owner_entry_year == startYear

        * Large reshuffles
        bys internal_firm_id (owner_entry_year): egen contractor_entry = min(owner_entry_year)
        gen large_reshuffle_at_entry = large_reshuffle if owner_entry_year == contractor_entry

        gen id = 1
        
        gcollapse (min) provider_entry_year=owner_entry_year all_large_reshuffles=      ///
            large_reshuffle_at_entry (max) strategic_entrant strategic_exit             ///
            created_by_bureaucrat oneOrMore_large_reshuffles=large_reshuffle_at_entry   ///
            (sum) nbr_connections=id, by(internal_firm_id)

        save_data "../temp/`input_file'_entries", replace key(internal_firm_id)
    }

    * Count number of distinct entry years
    use "../temp/provider_owned_by_`level_bureaucrat'", clear
    append using "../temp/provider_owned_by_sibling_of_`level_bureaucrat'"

    egen distinct_conn_years = tag(internal_firm_id owner_entry_year)

    gcollapse (sum) distinct_conn_years, by(internal_firm_id)

    save_data "../temp/distinc_connection_years", replace key(internal_firm_id)
end

program merge_data
    syntax, level_bureaucrat(str)

    use "../temp/provider_owned_by_`level_bureaucrat'_entries", clear
    rename (provider_entry_year oneOrMore_large_reshuffles all_large_reshuffles ///
            strategic_entrant strategic_exit created_by_bureaucrat nbr_connections) ///
        (provider_entry_year_direct oneOrMore_large_resh_dir all_large_resh_dir ///
            strategic_entrant_dir strategic_exit_dir created_by_bureaucrat_dir nbr_connections_dir)

    merge 1:1 internal_firm_id using "../temp/provider_owned_by_sibling_of_`level_bureaucrat'_entries", ///
        assert(1 2 3) keep(1 2 3)
    rename (provider_entry_year oneOrMore_large_reshuffles all_large_reshuffles ///
            strategic_entrant strategic_exit created_by_bureaucrat nbr_connections) /// 
        (provider_entry_year_indirect oneOrMore_large_resh_ind all_large_resh_ind ///
            strategic_entrant_ind strategic_exit_ind created_by_bureaucrat_ind nbr_connections_ind)
    
    gen owned_by_bureaucrat = _merge == 1 | _merge == 3
    gen owned_by_sibling = _merge == 2 | _merge == 3
    drop _merge

    * Find overall entry year
    egen provider_entry_year = rowmin(provider_entry_year_indirect provider_entry_year_direct)
    
    egen oneOrMore_large_reshuffles = rowmax(oneOrMore_large_resh_dir oneOrMore_large_resh_ind)
    egen all_large_reshuffles       = rowmin(all_large_resh_dir all_large_resh_ind)

    egen strategic_entrant     = rowmax(strategic_entrant_dir strategic_entrant_ind)
    egen strategic_exit        = rowmax(strategic_exit_dir strategic_exit_ind)
    egen created_by_bureaucrat = rowmax(created_by_bureaucrat_dir created_by_bureaucrat_ind)

    egen nbr_connections = rowtotal(nbr_connections_dir nbr_connections_ind)

    drop oneOrMore_large_resh_dir oneOrMore_large_resh_ind all_large_resh_dir ///
        all_large_resh_ind strategic_entrant_dir strategic_entrant_ind ///
        strategic_exit_dir strategic_exit_ind created_by_bureaucrat_dir ///
        created_by_bureaucrat_ind nbr_connections_dir nbr_connections_ind

    merge 1:1 internal_firm_id using "../temp/distinc_connection_years", ///
        nogen assert(3) keep(3)

    if "`level_bureaucrat'" == "highLevel_bureaucrat" {
        local stub = "_highLevel"
    }
    else {
        local stub = ""
    }

    save_data "$data_dir/providers_connections/master/providers_connections_all`stub'", ///
        replace key(internal_firm_id)
end

* Execute
main
