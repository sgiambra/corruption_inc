/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    This do-file matches a supplier to a bureaucrat. 
    We establish that a link exists if:
        - Supplier is a bureaucrat (linking through individual ID)
        - Supplier is owned by a bureaucrat (linking through ownership data and
        individual ID).
        - Supplier is sibling of bureaucrat (linking through family ID and
        individual IDs).
        - Supplier is owned by sibling of bureaucrat (ownership data, family ID,
        and individual IDs).

    Note1: The panel goes from 2000 to 2017 as we have shareholders data between
    these dates.
    Note2: When provider connected to multiple bureaucrats in a given year keep
    all provinces where bureaucrats work
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/"

program main
    local family_threshold "4"
    
    preclean

    foreach level in "bureaucrat" "highLevel_bureaucrat" {
        
        entry_province, level_bureaucrat(`level')

        * Direct links
        provider_is_bureaucrat, level_bureaucrat(`level')
        prov_owned_by_bureauc, min_shares("0.2") level_bureaucrat(`level')

        build_family_panel, level_bureaucrat(`level')

        * Indirect links
        prov_sibl_is_bureauc, level_bureaucrat(`level') ///
            family_threshold(`family_threshold')
        prov_owned_by_sibl_bureauc, min_shares("0.2") ///
            family_threshold(`family_threshold') level_bureaucrat(`level')
    }
end

program preclean
    use "$data_dir/firms_ownership/shareholders_clean", clear
    
    gcollapse (min) first_year_shareholder=year (max) ///
        last_year_shareholder=year, by(firm_id idshareholder)
    rename (firm_id idshareholder) (provider_id shareholder_id)
    
    save_data "../temp/owners_salient_years", ///
        replace key(provider_id shareholder_id)

    use provider_id using "$data_dir/proc_contracts/tables/providers_registry", clear
    drop if provider_id == "."
    * The following restriction is to exclude false positive matches
    drop if length(provider_id) < 5
    duplicates drop
    save_data "../temp/providers_registry", key(provider_id) replace
end

program entry_province
    syntax, level_bureaucrat(str)

    use "$data_dir/bureaucrats_scraped/processed_data/bureaucrats_panel" ///
        if `level_bureaucrat' == 1, clear

    keep bureaucrat_id year enprov
    replace enprov = 0 if enprov == .

    gcollapse (min) entry_year=year, by(bureaucrat_id enprov)
    save_data "../temp/`level_bureaucrat'_entry_province", ///
        replace key(bureaucrat_id enprov)
end
    
program provider_is_bureaucrat
    syntax, level_bureaucrat(str)

    use "../temp/providers_registry", clear
    rename provider_id bureaucrat_id

    merge 1:m bureaucrat_id using "../temp/`level_bureaucrat'_entry_province", ///
        keep(3) assert(1 2 3) nogen

    * Merge to instrument
    * A few have it missing (possibly due to missing agency)
    merge m:1 bureaucrat_id using ///
        "$data_dir/providers_connections/instrument/`level_bureaucrat'_instrument", ///
        nogen assert(1 2 3) keep(1 3) keepusing(large_reshuffle)

    replace large_reshuffle = 0 if large_reshuffle == .
    rename bureaucrat_id provider_id
    
    save_data "$data_dir/providers_connections/provider_is_`level_bureaucrat'_province", ///
        replace key(provider_id enprov)

    gcollapse (min) entry_year (mean) large_reshuffle, by(provider_id)

    save_data "$data_dir/providers_connections/provider_is_`level_bureaucrat'", ///
        replace key(provider_id)
end

program prov_owned_by_bureauc
    syntax, level_bureaucrat(str) min_shares(str)

    use "$data_dir/firms_ownership/shareholders_clean", clear
    
    * Only shareholders of government providers
    rename firm_id provider_id
    merge m:1 provider_id using "../temp/providers_registry", ///
        assert(1 2 3) keep(3) nogen

    * Keep if shareholder owns more than min threshold
    keep if share >= `min_shares'

    rename idshareholder bureaucrat_id
    joinby bureaucrat_id using "../temp/`level_bureaucrat'_entry_province"

    merge m:1 bureaucrat_id using ///
        "$data_dir/providers_connections/instrument/`level_bureaucrat'_instrument", ///
        nogen assert(1 2 3) keep(1 3) keepusing(large_reshuffle)

    replace large_reshuffle = 0 if large_reshuffle == .

    * Some shareholders enter government before becoming shareholders
    * We should potentially drop these
    rename bureaucrat_id shareholder_id

    gcollapse (min) owner_entry_year=entry_year (mean) large_reshuffle, ///
        by(provider_id shareholder_id enprov)

    merge m:1 provider_id shareholder_id using "../temp/owners_salient_years", ///
        nogen assert(2 3) keep(3)

    save_data "$data_dir/providers_connections/provider_owned_by_`level_bureaucrat'_province", ///
        replace key(provider_id shareholder_id enprov)

    gcollapse (min) owner_entry_year (mean) large_reshuffle first_year_shareholder ///
        last_year_shareholder, by(provider_id shareholder_id)

    save_data "$data_dir/providers_connections/provider_owned_by_`level_bureaucrat'", ///
        replace key(provider_id shareholder_id)
end

program build_family_panel
    syntax, level_bureaucrat(str)

    use "../temp/`level_bureaucrat'_entry_province", clear

    merge m:1 bureaucrat_id using ///
        "$data_dir/providers_connections/instrument/`level_bureaucrat'_instrument", ///
        nogen assert(1 2 3) keep(1 3) keepusing(large_reshuffle)

    replace large_reshuffle = 0 if large_reshuffle == .

    rename bureaucrat_id person_id

    * Should not have _merge == 1
    merge m:1 person_id using "$data_dir/derived_data/name_dataset", ///
        assert(1 2 3) keep(3) nogen
    
    * Cannot take mean here as families can be connected to more than one bureaucrat
    gcollapse (min) entry_year (max) large_reshuffle /// 
        (mean) family_size, by(family_id enprov)
    
    save_data "../temp/family_`level_bureaucrat'_entry_province", ///
        replace key(family_id enprov)
end
   
program prov_sibl_is_bureauc
    syntax, level_bureaucrat(str) family_threshold(str)

    use "../temp/providers_registry", clear
    rename provider_id person_id
    
    * Note: _merge == 1 are firm suppliers
    merge 1:1 person_id using "$data_dir/derived_data/name_dataset", ///
        assert(1 2 3) keep(3) nogen keepusing(family_id)
    joinby family_id using "../temp/family_`level_bureaucrat'_entry_province"

    * Consider only families of size smaller than 4
    drop if family_size > `family_threshold'

    rename person_id provider_id
    drop family_id family_size

    save_data "$data_dir/providers_connections/provider_sibling_is_`level_bureaucrat'_province", ///
        replace key(provider_id enprov)

    gcollapse (min) entry_year (max) large_reshuffle, by(provider_id)

    save_data "$data_dir/providers_connections/provider_sibling_is_`level_bureaucrat'", ///
        replace key(provider_id)
end

program prov_owned_by_sibl_bureauc
    syntax, min_shares(str) family_threshold(str) level_bureaucrat(str)

    use "$data_dir/firms_ownership/shareholders_clean", clear
    rename firm_id provider_id
    merge m:1 provider_id using "../temp/providers_registry", ///
        assert(1 2 3) keep(3) nogen

    * Keep if shareholder owns more than min threshold
    keep if share >= `min_shares'
    
    rename idshareholder person_id
    merge m:1 person_id using "$data_dir/derived_data/name_dataset", ///
        assert(1 2 3) keep(3) nogen keepusing(family_id)
    joinby family_id using "../temp/family_`level_bureaucrat'_entry_province"

    * Consider only families of size smaller than 4
    drop if family_size > `family_threshold'

    rename person_id shareholder_id

    gcollapse (min) owner_entry_year=entry_year (mean) large_reshuffle, ///
        by(provider_id shareholder_id enprov)

    merge m:1 provider_id shareholder_id using "../temp/owners_salient_years", ///
        nogen assert(2 3) keep(3)

    save_data "$data_dir/providers_connections/provider_owned_by_sibling_of_`level_bureaucrat'_province", ///
        replace key(provider_id shareholder_id enprov)

    gcollapse (min) owner_entry_year (max) large_reshuffle (mean) first_year_shareholder ///
        last_year_shareholder, by(provider_id shareholder_id)

    save_data "$data_dir/providers_connections/provider_owned_by_sibling_of_`level_bureaucrat'", ///
        replace key(provider_id shareholder_id)
end

* Execute
main
