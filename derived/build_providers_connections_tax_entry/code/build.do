clear all
set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/"

program main
    obtain_tax_entry_year
    
    use "$data_dir/firms_ownership/shareholders_clean", clear
    
    gcollapse (min) first_year_shareholder=year (max) ///
        last_year_shareholder=year, by(firm_id idshareholder)
    rename (firm_id idshareholder) (provider_id shareholder_id)
    
    save_data "../temp/owners_salient_years", ///
        replace key(provider_id shareholder_id)
        
    * Direct links
    prov_owned_by_tax_entry, min_shares("0.2") 
   
end

program obtain_tax_entry_year
    use "$data_dir/Individuals/irs/clean/all_taxes.dta", clear

    rename id shareholder_id

    gcollapse (min) entry_year=year, by(shareholder_id)
        
    save_data "../temp/entry_tax_data", replace key(shareholder_id) 
end 

program prov_owned_by_tax_entry
    syntax, min_shares(str) 
    
    use provider_id using "$data_dir/proc_contracts/tables/providers_registry", clear
    drop if provider_id == "."
    * The following restriction is to exclude false positive matches
    drop if length(provider_id) < 5
    duplicates drop
    save_data "../temp/providers_registry", key(provider_id) replace

    use "$data_dir/firms_ownership/shareholders_clean", clear
    
    * Only shareholders of government providers
    rename firm_id provider_id
    merge m:1 provider_id using "../temp/providers_registry", ///
        assert(1 2 3) keep(3) nogen
        
    * Keep if shareholder owns more than min threshold
    keep if share >= `min_shares'

    * Add Entry year into tax
    rename idshareholder shareholder_id
    merge m:1 shareholder_id using "../temp/entry_tax_data", keep(3) nogen

    gcollapse (min) owner_entry_year=entry_year, ///
        by(provider_id shareholder_id)

    merge m:1 provider_id shareholder_id using "../temp/owners_salient_years", ///
        nogen assert(2 3) keep(3)

    gcollapse (min) owner_entry_year (mean) first_year_shareholder ///
        last_year_shareholder, by(provider_id shareholder_id)
                
    save_data "$data_dir/providers_connections_tax_entry/provider_owned_by_tax_entry", ///
        replace key(provider_id shareholder_id)
end

* Execute
main
