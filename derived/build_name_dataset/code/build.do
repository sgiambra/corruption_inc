clear all
set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/"

program main
    merge_datasets
    clean_tax_data
    
    * Give priority to name in tax records
    use "../temp/tax_data_clean.dta", clear
    merge 1:m person_id using  "../temp/all_names.dta", ///
        keep(1 2 3) assert(1 2 3) nogen
    
    foreach var in is_from_bureauc_data is_from_manager_data is_from_shareh_data is_from_tax_data is_from_providers_data {
        replace `var' = 0 if `var' == .
    }
    fix_compound_names
    
    egen family_id = group(last_names)
    bys family_id: gen family_size = _N

    * For IDs associated to multiple names keep largest family
    * or one of largest at random
    bys person_id: egen max_family_size = max(family_size)
    keep if family_size == max_family_size
    * The wide majority of name permutations involve only changes in first names
    * This implies resulting family size is constant across permutations
    duplicates drop person_id, force

    keep person_id name family_id family_size is_from_bureauc_data is_from_manager_data ///
        is_from_shareh_data is_from_tax_data is_from_providers_data
    save_data "${data_dir}/derived_data/name_dataset", key(person_id) replace

    use "${data_dir}/derived_data/name_dataset", clear
end

program merge_datasets
    /* Note: people with exact same name sometimes change ID. Still merge on ID whenever possible
       since more robust to typing errors. (Also, only candidates files have no ID and they are
       not used in the main analysis.) */

    use "$data_dir/firms_ownership/list_shareholders.dta",clear
    keep if person == 1
    gen is_from_shareh_data = 1
    rename (nameshareholder idshareholder) (shareholder_name person_id)
    fix_name, namevar(shareholder_name)
    keep shareholder_name person_id is_from_shareh_data
    
    save_data "../temp/list_shareholders.dta", key(person_id) replace
    
    use "$data_dir/firms_ownership/list_managers.dta",clear
    rename (namemanager idmanager) (manager_name person_id)
    gen is_from_manager_data = 1
    fix_name, namevar(manager_name)
    standardize_ids

    * A handful of managers have same ID after standardization
    duplicates drop person_id, force
    save_data "../temp/list_managers.dta", key(person_id) replace
    
    use provider_id provider_name_extended using ///
        "$data_dir/proc_contracts/tables/individual_suppliers", clear
    rename (provider_name_extended provider_id) (supplier_name person_id)
    gen is_from_providers_data = 1
    duplicates drop
    drop if supplier_name == ""

    * Here many IDs are associated to different names. Keep all cases to compute family size more conservatively
    save_data "../temp/list_providers.dta", key(person_id supplier_name) replace

    foreach file in resultados declaracion {
        use bureaucrat_id bureaucrat_name_extended using ///
            "$data_dir/bureaucrats_scraped/parsed/clean/bureaucrats_`file'", clear
        duplicates drop
        
        merge m:1 bureaucrat_id using "$data_dir/bureaucrats_scraped/processed_data/bureaucrats_entry_exit", ///
            nogen keep(3) assert(1 2 3) keepusing(bureaucrat_id)
        save_data "../temp/`file'", replace key(bureaucrat_id bureaucrat_name_extended)
    }

    /* Bureaucrats data have typing errors in names. Key on (person_id name) to allow 
       all merges to candidates data */
    use "../temp/resultados", clear
    append using "../temp/declaracion"
    duplicates drop
    
    rename (bureaucrat_id bureaucrat_name_extended) (person_id name)
    gen is_from_bureauc_data = 1

    merge m:1 person_id using "../temp/list_managers.dta", ///
        assert(1 2 3) keep(1 2 3)
    replace name = manager_name if _merge == 3 & name != manager_name & ///
        wordcount(name) != 4 & wordcount(manager_name) == 4
    replace name = manager_name if _merge == 2
    drop manager_name _merge

    merge m:1 person_id using "../temp/list_shareholders.dta", ///
        assert(1 2 3) keep(1 2 3)
    replace name = shareholder_name if _merge == 3 & name != shareholder_name & ///
        wordcount(name) != 4 & wordcount(shareholder_name) == 4
    replace name = shareholder_name if _merge == 2
    drop shareholder_name _merge

    merge m:m person_id using "../temp/list_providers.dta", ///
        assert(1 2 3) keep(1 2 3)
    replace name = supplier_name if _merge == 3 & name != supplier_name & ///
        wordcount(name) != 4 & wordcount(supplier_name) == 4
    replace name = supplier_name if _merge == 2
    drop supplier_name _merge

    * For providers with multiple names per ID keep name with largest number of words
    bys person_id: gen name_length = wordcount(name)
    bys person_id: egen max_name_length = max(wordcount(name))
    drop if name_length < max_name_length
    drop name_length max_name_length
    
    duplicates drop person_id name, force
    save_data "../temp/all_names.dta", key(person_id name) replace
end

program clean_tax_data
    use id name using "${data_dir}/Individuals/irs/clean/all_taxes.dta", clear
    duplicates drop id, force
    rename id person_id
    gen is_from_tax_data = 1
      
    save_data "../temp/tax_data_clean.dta", key(person_id) replace
end

program fix_compound_names
    split name, parse(" ") limit(7)
    do "../code/fix_compound_names.do"

    gen last_names = name1 + name2
    gen length_last_name = 2

    foreach stub of numlist 1/6 {
        foreach compound in LA LOS SAN SANTA VON VAN DER DEN DI DEL DE {
            replace length_last_name = length_last_name + 1 if name`stub' == "`compound'"
        }
    }
    replace last_names = name1 + name2 + name3 if length_last_name == 3
    replace last_names = name1 + name2 + name3 + name4 if length_last_name == 4
    replace last_names = name1 + name2 + name3 + name4 + name5 if length_last_name == 5
    replace last_names = name1 + name2 + name3 + name4 + name5 + name6 if length_last_name == 6
end

program fix_name
    syntax, namevar(varname)
    
    replace `namevar'= upper(ustrto(ustrnormalize(`namevar', "nfd"), "ascii", 2))
    replace `namevar' = subinstr(`namevar', "(", "", .)
    replace `namevar' = subinstr(`namevar', ")", "", .)
    replace `namevar' = subinstr(`namevar', `"""',  "", .)
    replace `namevar' = subinstr(`namevar', "`=char(9)'", "", .)
    replace `namevar' = subinstr(`namevar', ".", "", .)
    replace `namevar' = subinstr(`namevar', ",", "", .)
    replace `namevar' = subinstr(`namevar', "-", "", .)
    replace `namevar' = subinstr(`namevar', "'", "", .)
    replace `namevar' = strtrim(stritrim(`namevar'))
end

program standardize_ids
    replace person_id = "0" + person_id if length(person_id) == 9 | length(person_id) == 12
    replace person_id = substr(person_id, 1, 10) if substr(person_id, -3, 3) == "001" & ///
        (length(person_id) == 13 | length(person_id) == 14)
end

* Execute
main
