clear all
set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
adopath + ../../../lib/stata/gtools

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/"

program main
    local min_nbr_bureaucrats 10
    local min_share_entries "0.5"

    foreach level in "bureaucrat" "highLevel_bureaucrat" {
        build_bureaucrats_instrument, level_bureaucrat(`level') ///
            min_nbr_bureaucrats(`min_nbr_bureaucrats') min_share_entries(`min_share_entries')
    }
end

program build_bureaucrats_instrument
    syntax, level_bureaucrat(str) min_nbr_bureaucrats(int) min_share_entries(str)

    use "$data_dir/bureaucrats_scraped/processed_data/bureaucrats_panel" ///
        if `level_bureaucrat' == 1, clear
    keep bureaucrat_id year agency_nbr
    drop if agency_nbr == .

    save_data "../temp/bureaucrats_panel", ///
        replace key(bureaucrat_id year)

    bys agency_nbr year: gen nbr_bureaucrats_in_agency = _N
    keep agency_nbr year nbr_bureaucrats_in_agency
    gduplicates drop

    bys agency_nbr (year): gen first_obs_agency = _n == 1

    save_data "../temp/nbr_bureaucrats_active", ///
        replace key(agency_nbr year)
    
    use "../temp/bureaucrats_panel", clear
    gcollapse (min) year, by(bureaucrat_id agency_nbr)

    bys agency_nbr year: gen nbr_entries = _N
    
    keep agency_nbr year nbr_entries
    gduplicates drop

    merge 1:1 agency_nbr year using "../temp/nbr_bureaucrats_active", ///
        nogen assert(2 3) keep(2 3)
    replace nbr_entries = 0 if nbr_entries == .

    gen share_entries = nbr_entries/nbr_bureaucrats_in_agency

    gen is_large_agency = nbr_bureaucrats_in_agency >= `min_nbr_bureaucrats'
    gen large_reshuffle = is_large_agency == 1 & first_obs_agency == 0 & /// 
        share_entries >= `min_share_entries'

    count_dropped_obs, unit_obs("agencyYears")

    save_data "../temp/instrument", replace key(agency_nbr year)

    use "../temp/bureaucrats_panel", clear
    bys bureaucrat_id (year agency_nbr): gen first_obs = _n == 1

    keep if first_obs == 1
    drop first_obs

    merge m:1 agency_nbr year using "../temp/instrument", ///
        nogen assert(2 3) keep(3)

    count_dropped_obs, unit_obs("bureaucrats")

    save_data "$data_dir/providers_connections/instrument/`level_bureaucrat'_instrument", ///
        replace key(bureaucrat_id)
end

program count_dropped_obs
    syntax, unit_obs(str)

    qui count
    local nbr_`unit_obs' = `r(N)'
    qui count if is_large_agency == 0
    local nbr_small_agencies = `r(N)'
    
    local share_dropped = round(`nbr_small_agencies'/`nbr_`unit_obs'', .001)
    dis "Share of `unit_obs' dropped because small agency: `share_dropped'%"

    qui count if large_reshuffle == 0
    local nbr_no_reshuffle = `r(N)'

    local share_dropped = round(`nbr_no_reshuffle'/`nbr_`unit_obs'', .001)
    dis "Share of `unit_obs' dropped because of no large reshuffle: `share_dropped'%"
end

* Execute
main
