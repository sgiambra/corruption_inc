set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/"
global output_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/providers_connections_large_families"

program main
    use "$data_dir/firms_ownership/shareholders_clean", clear
    
    gcollapse (min) first_year_shareholder=year (max) ///
        last_year_shareholder=year, by(firm_id idshareholder)
    rename (firm_id idshareholder) (provider_id shareholder_id)
    
    save_data "../temp/owners_salient_years", ///
        replace key(provider_id shareholder_id)

    foreach level in "bureaucrat" {
        
    provider_entries, level_bureaucrat(`level')

    build_family_panel, level_bureaucrat(`level')

    * Indirect links
    prov_owned_by_sibl_bureauc, min_shares("0.2") ///
        min_family("15") level_bureaucrat(`level') stub("large")

    prov_owned_by_sibl_bureauc, min_shares("0.2") min_family("4") ///
        max_family("10") level_bureaucrat(`level') stub("medium")        
    }
end

program provider_entries
    syntax, level_bureaucrat(str)

    use "$data_dir/bureaucrats_scraped/processed_data/bureaucrats_panel" ///
        if `level_bureaucrat' == 1, clear
    keep bureaucrat_id year enprov
    replace enprov = 0 if enprov == .

    gcollapse (min) entry_year=year, by(bureaucrat_id enprov)
    save_data "../temp/`level_bureaucrat'_entry_province", replace key(bureaucrat_id enprov)

    use provider_id using "$data_dir/proc_contracts/tables/providers_registry", clear
    drop if provider_id == "."
    * The following restriction is to exclude false positive matches
    drop if length(provider_id) < 5
    duplicates drop
    save_data "../temp/providers_registry", key(provider_id) replace
end

program build_family_panel
    syntax, level_bureaucrat(str)

    use "../temp/`level_bureaucrat'_entry_province", clear
    rename bureaucrat_id person_id

    * Should not have _merge == 1
    merge m:1 person_id using "$data_dir/derived_data/name_dataset", ///
        assert(1 2 3) keep(3) nogen
    
    gcollapse (min) entry_year (mean) family_size, by(family_id enprov)
    save_data "../temp/family_`level_bureaucrat'_entry_province", ///
        replace key(family_id enprov)
end

program prov_owned_by_sibl_bureauc
    syntax, min_shares(str) min_family(str) level_bureaucrat(str) stub(str) [max_family(str)]

    use "$data_dir/firms_ownership/shareholders_clean", clear
    rename firm_id provider_id
    merge m:1 provider_id using "../temp/providers_registry", ///
        assert(1 2 3) keep(3) nogen

    * Keep if shareholder owns more than min threshold
    keep if share >= `min_shares'
    
    rename idshareholder person_id
    merge m:1 person_id using "$data_dir/derived_data/name_dataset", ///
        assert(1 2 3) keep(3) nogen keepusing(family_id)
    joinby family_id using "../temp/family_`level_bureaucrat'_entry_province"

    keep if family_size > `min_family'
    if "`max_family'" != "" {
        keep if family_size <= `max_family'
    }

    rename person_id shareholder_id

    gcollapse (min) owner_entry_year=entry_year, ///
        by(provider_id shareholder_id enprov)

    merge m:1 provider_id shareholder_id using "../temp/owners_salient_years", ///
        nogen assert(2 3) keep(3)

    save_data "$output_dir/provider_owned_by_sibling_of_`level_bureaucrat'_province_`stub'", ///
        replace key(provider_id shareholder_id enprov)

    gcollapse (min) owner_entry_year (mean) first_year_shareholder ///
        last_year_shareholder, by(provider_id shareholder_id)

    save_data "$output_dir/provider_owned_by_sibling_of_`level_bureaucrat'_`stub'", ///
        replace key(provider_id shareholder_id)
end

* Execute
main
