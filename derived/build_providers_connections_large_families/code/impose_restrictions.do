set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/"
global output_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/providers_connections_large_families"

program main
    preclean

    foreach level in "large" "medium" {
        build_final_dta, family_level(`level')
    }
end

program preclean
    use provider_id internal_firm_id using ///
        "$data_dir/proc_contracts/tables/providers_registry", clear
    duplicates drop provider_id, force
    save_data "../temp/providers_registry", replace key(provider_id)

    use rucFirm startYear using "$data_dir/firms_ownership/directory", replace
    rename rucFirm provider_id
    drop if provider_id == "."
    * Almost 200k rucs become of length 10
    replace provider_id = substr(provider_id, 1, 10) if ///
        substr(provider_id, -3, 3) == "001" & length(provider_id) == 13

    merge 1:1 provider_id using "../temp/providers_registry", ///
        nogen assert(1 2 3) keep(3)
    save_data "../temp/providers_registry", replace key(provider_id)
end

program build_final_dta
    syntax, family_level(str)

    use "../temp/providers_registry", clear
    merge 1:m provider_id using ///
        "$output_dir/provider_owned_by_sibling_of_bureaucrat_`family_level'", ///
        nogen assert(1 3) keep(3)

    * Fix inconsistencies
    replace first_year_shareholder = startYear if first_year_shareholder < startYear

    * Strategic dummies
    gen strategic_entrant = owner_entry_year < first_year_shareholder
    gen strategic_exit    = owner_entry_year > last_year_shareholder

    * If strategic entrant, assume connected at time it becomes owner
    replace owner_entry_year = first_year_shareholder if strategic_entrant == 1

    gen created_by_bureaucrat = owner_entry_year == startYear
    
    gcollapse (min) provider_entry_year=owner_entry_year (max) strategic_entrant ///
        strategic_exit created_by_bureaucrat, by(internal_firm_id)

    save_data "$output_dir/master/providers_connections_all_`family_level'", ///
        replace key(internal_firm_id)
end

* Execute
main
