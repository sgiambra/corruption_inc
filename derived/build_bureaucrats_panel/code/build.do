/*  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    This do-file assembles the bureaucrats_table. The table contains the next
    information:
        {bureaucrat_name, bureaucrat_id, entry_year, exit_year, province, city}
    
    For province and city we take the most repeated observed one.
    For exit year, we take the exit year information from the second wave 
    of scraped data.
    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

clear all
set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/bureaucrats_scraped"

program main
    local flags_list = "highLevel_bureaucrat bureaucrat gov_official elected policeArmy " + ///
                       "academic medic secretary academic_inst medic_inst not_bureaucrat"
    resultados, flags(`flags_list')
    declaracion, flags(`flags_list')

    merge_waves
    build_panel, flags(`flags_list')
end

program resultados 
    syntax, flags(str)
    
    * From the first wave (resultados), we want to retreive the final list of IDS and names we will use 
    * and the first year we observe them in the data 
    use "$data_dir/parsed/clean/bureaucrats_resultados", clear

    drop if year == .
    drop if year < 1970
    
    gduplicates tag bureaucrat_id year, gen(dup)
    drop if dup >= 98

    * For each-ID year get max value of each flag (last nonmissing province and position)
    sort bureaucrat_id year province `flags'
    collapse (max) `flags' (last) province bureaucrat_position ///
        agency_name, by(bureaucrat_id year)

    encode province, g(enprov)
    drop province

    save_data "../temp/resultados_panel", replace key(bureaucrat_id year)

    * Get most common province by ID
    bys bureaucrat_id: gegen mode_p = mode(enprov), minmode 
    replace enprov = . if mode_p != enprov

    gcollapse (min) entry_year_wave1=year (max) `flags' enprov, by(bureaucrat_id)

    save_data "../temp/bureaucrat_resultados", replace key(bureaucrat_id)
end

program declaracion
    syntax, flags(str)
    
    * From the second wave (declaracion), we want the province and city as well as first and last year
    use "$data_dir/parsed/clean/bureaucrats_declaracion", replace
    
    gen entry_year = year(date(start_date, "YMD"))
    gen exit_year = year(date(end_date, "YMD"))

    drop if entry_year > 2018

    encode province, g(enprov)
    encode city, g(encity)
    drop province city declaration_type assets debt net_worth start_date ///
        end_date nationality civil_status bureaucrat_name bureaucrat_name_extended

    save_data "../temp/declaracion_panel", replace key(bureaucrat_id id_nbr)
    
    * Keep most repeated province and city by ID    
    gegen prov_city = group(enprov encity)
    bys bureaucrat_id: gegen mode_pc = mode(prov_city), minmode 
    replace encity = . if mode_pc != prov_city
    replace enprov = . if mode_pc != prov_city
        
    * Collapse info
    gcollapse (min) entry_year_wave2=entry_year (max) exit_year_wave2=exit_year /// 
        `flags' encity enprov, by(bureaucrat_id)

    save_data "../temp/bureaucrat_declaracion", replace key(bureaucrat_id)    
end

program merge_waves
    use "../temp/bureaucrat_resultados", clear
    merge 1:1 bureaucrat_id using "../temp/bureaucrat_declaracion", ///
        nogen keep(1 2 3) assert(1 2 3)
    
    * Obtain minimum entry year
    gegen entry_year = rowmin(entry_year_wave1 entry_year_wave2)
    drop if entry_year < 1970
    * Obtain exit year
    gegen max_entry_year = rowmax(entry_year_wave1 entry_year_wave2)
    gen exit_year = exit_year_wave2 if exit_year_wave2 >= max_entry_year
    replace exit_year = . if exit_year > 2018
    
    drop max_entry_year entry_year_wave1 entry_year_wave2 exit_year_wave2
    save_data "$data_dir/processed_data/bureaucrats_entry_exit", replace key(bureaucrat_id)
end

program build_panel
    syntax, flags(str)

    * Assume bureaucrat maintains last position where she was observed
    use bureaucrat_id exit_year using "$data_dir/processed_data/bureaucrats_entry_exit", clear
    replace exit_year = 2018 if exit_year == .
    rename exit_year year
    save_data "../temp/terminal_years", key(bureaucrat_id) replace

    use "../temp/declaracion_panel", clear
    drop exit_year
    rename entry_year year
    drop if year < 1970
    append using "../temp/resultados_panel"

    decode enprov, gen(province)
    decode encity, gen(city)

    gegen position_nbr = group(bureaucrat_position)
    gegen agency_nbr   = group(agency_name)

    preserve
        keep bureaucrat_position position_nbr
        duplicates drop
        save "$data_dir/processed_data/positions_list", replace
    restore

    preserve
        keep agency_name agency_nbr
        duplicates drop
        save "$data_dir/processed_data/agencies_list", replace
    restore

    sort bureaucrat_id year `flags' province city
    collapse (max) `flags' (last) province city position_nbr ///
        agency_nbr, by(bureaucrat_id year)

    encode province, gen(enprov)
    encode city, gen(encity)
    drop province city

    merge 1:1 bureaucrat_id year using "../temp/terminal_years", ///
        nogen assert(1 2 3) keep(1 2 3)

    gegen bureaucrat_id_nbr = group(bureaucrat_id)
    tsset bureaucrat_id_nbr year
    tsfill
    bys bureaucrat_id_nbr: carryforward `flags' bureaucrat_id encity ///
        enprov position_nbr agency_nbr, replace

    drop bureaucrat_id_nbr
    save_data "$data_dir/processed_data/bureaucrats_panel", ///
        replace key(bureaucrat_id year)
end

* Execute
main
