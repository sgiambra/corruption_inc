* NOTE: Unit of measure of capital is unclear

clear all
set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/"

program main
    use expediente rucFirm using "$data_dir/firms_ownership/directory.dta", replace
    rename rucFirm firm_id
    drop if firm_id == "."
    * Almost 200k rucs become of length 10
    replace firm_id = substr(firm_id, 1, 10) if ///
        substr(firm_id, -3, 3) == "001" & length(firm_id) == 13

    save_data "../temp/list_rucs_directory.dta", key(firm_id) replace

    use "$data_dir/firms_ownership/shareholders_pyramids_cleaned", clear
    replace idshareholder = "0" + idshareholder if length(idshareholder) == 9 | length(idshareholder) == 12
    replace idshareholder = substr(idshareholder, 1, 10) if substr(idshareholder, -3, 3) == "001" & ///
        (length(idshareholder) == 13 | length(idshareholder) == 14)
    merge m:1 expediente using "../temp/list_rucs_directory", ///
        nogen assert(1 2 3) keep(3)

    * Eliminate ~30k duplicates
    gcollapse (sum) share, by(idshareholder firm_id year)
    save_data "$data_dir/firms_ownership/shareholders_clean", ///
        replace key(idshareholder firm_id year)
end

* Execute
main
