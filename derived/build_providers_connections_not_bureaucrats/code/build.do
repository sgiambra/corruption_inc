set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/"

program main
    local min_threshold     "0.2"
    local level             "not_bureaucrat"
    local family_threshold  "4"

    preclean

    entry_province, level_bureaucrat(`level')

    falsif_sample_direct, min_shares(`min_threshold') ///
        level_bureaucrat(`level')

    build_family_panel, level_bureaucrat(`level')

    falsif_sample_indirect, min_shares(`min_threshold') ///
        family_threshold(`family_threshold') level_bureaucrat(`level')
end

program preclean
    use "$data_dir/firms_ownership/shareholders_clean", clear
    
    gcollapse (min) first_year_shareholder=year (max) ///
        last_year_shareholder=year, by(firm_id idshareholder)
    rename (firm_id idshareholder) (provider_id shareholder_id)
    
    save_data "../temp/owners_salient_years", ///
        replace key(provider_id shareholder_id)

    use provider_id using "$data_dir/proc_contracts/tables/providers_registry", clear
    drop if provider_id == "."
    * The following restriction is to exclude false positive matches
    drop if length(provider_id) < 5
    duplicates drop
    save_data "../temp/providers_registry", key(provider_id) replace
end

program entry_province
    syntax, level_bureaucrat(str)

    use "$data_dir/bureaucrats_scraped/processed_data/bureaucrats_panel" ///
        if `level_bureaucrat' == 1, clear
    keep bureaucrat_id year enprov
    replace enprov = 0 if enprov == .

    gcollapse (min) entry_year=year, by(bureaucrat_id enprov)
    save_data "../temp/`level_bureaucrat'_entry_province", ///
        replace key(bureaucrat_id enprov)
end

program falsif_sample_direct
    syntax, level_bureaucrat(str) min_shares(str)

    use "$data_dir/firms_ownership/shareholders_clean", clear
    
    * Only shareholders of government providers
    rename firm_id provider_id
    merge m:1 provider_id using "../temp/providers_registry", ///
        assert(1 2 3) keep(3) nogen

    * Keep if shareholder owns less than min threshold
    keep if share >= `min_shares'

    rename idshareholder bureaucrat_id
    joinby bureaucrat_id using "../temp/`level_bureaucrat'_entry_province"

    rename bureaucrat_id shareholder_id

    gcollapse (min) owner_entry_year=entry_year, ///
        by(provider_id shareholder_id enprov)

    merge m:1 provider_id shareholder_id using "../temp/owners_salient_years", ///
        nogen assert(2 3) keep(3)

    save_data "$data_dir/providers_connections_not_bureaucrat/provider_owned_by_`level_bureaucrat'_province", ///
        replace key(provider_id shareholder_id enprov)

    gcollapse (min) owner_entry_year (mean) first_year_shareholder ///
        last_year_shareholder, by(provider_id shareholder_id)

    save_data "$data_dir/providers_connections_not_bureaucrat/provider_owned_by_`level_bureaucrat'", ///
        replace key(provider_id shareholder_id)
end

program build_family_panel
    syntax, level_bureaucrat(str)

    use "../temp/`level_bureaucrat'_entry_province", clear

    rename bureaucrat_id person_id

    * Should not have _merge == 1
    merge m:1 person_id using "$data_dir/derived_data/name_dataset", ///
        assert(1 2 3) keep(3) nogen
    
    * Cannot take mean here as families can be connected to more than one bureaucrat
    gcollapse (min) entry_year (mean) family_size, by(family_id enprov)
    
    save_data "../temp/family_`level_bureaucrat'_entry_province", ///
        replace key(family_id enprov)
end

program falsif_sample_indirect
    syntax, min_shares(str) family_threshold(str) level_bureaucrat(str)

    use "$data_dir/firms_ownership/shareholders_clean", clear
    rename firm_id provider_id
    merge m:1 provider_id using "../temp/providers_registry", ///
        assert(1 2 3) keep(3) nogen

    * Keep if shareholder owns more than min threshold
    keep if share >= `min_shares'
    
    rename idshareholder person_id
    merge m:1 person_id using "$data_dir/derived_data/name_dataset", ///
        assert(1 2 3) keep(3) nogen keepusing(family_id)
    joinby family_id using "../temp/family_`level_bureaucrat'_entry_province"

    * Consider only families of size smaller than 4
    drop if family_size > `family_threshold'

    rename person_id shareholder_id

    gcollapse (min) owner_entry_year=entry_year, ///
        by(provider_id shareholder_id enprov)

    merge m:1 provider_id shareholder_id using "../temp/owners_salient_years", ///
        nogen assert(2 3) keep(3)

    save_data "$data_dir/providers_connections_not_bureaucrat/provider_owned_by_sibling_of_`level_bureaucrat'_province", ///
        replace key(provider_id shareholder_id enprov)

    gcollapse (min) owner_entry_year (mean) first_year_shareholder ///
        last_year_shareholder, by(provider_id shareholder_id)

    save_data "$data_dir/providers_connections_not_bureaucrat/provider_owned_by_sibling_of_`level_bureaucrat'", ///
        replace key(provider_id shareholder_id)
end

* Execute
main
