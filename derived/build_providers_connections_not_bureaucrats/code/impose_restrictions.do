set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/"
global output_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/providers_connections_not_bureaucrat"

program main
    clean_data
    merge_data
end

program clean_data
    use provider_id internal_firm_id using ///
        "$data_dir/proc_contracts/tables/providers_registry", clear
    duplicates drop provider_id, force
    save_data "../temp/providers_registry", replace key(provider_id)

    use rucFirm startYear using "$data_dir/firms_ownership/directory", replace
    rename rucFirm provider_id
    drop if provider_id == "."
    * Almost 200k rucs become of length 10
    replace provider_id = substr(provider_id, 1, 10) if ///
        substr(provider_id, -3, 3) == "001" & length(provider_id) == 13

    merge 1:1 provider_id using "../temp/providers_registry", ///
        nogen assert(1 2 3) keep(3)
    save_data "../temp/providers_registry", replace key(provider_id)
    
    foreach input_file in provider_owned_by_not_bureaucrat ///
        provider_owned_by_sibling_of_not_bureaucrat {
    
        use "../temp/providers_registry", clear
        merge 1:m provider_id using "$output_dir/`input_file'", ///
            nogen assert(1 2 3) keep(3)

        * Fix inconsistencies
        replace first_year_shareholder = startYear if first_year_shareholder < startYear

        * Strategic dummies
        gen strategic_entrant = owner_entry_year < first_year_shareholder
        gen strategic_exit    = owner_entry_year > last_year_shareholder

        * If strategic entrant, assume connected at time it becomes owner
        replace owner_entry_year = first_year_shareholder if strategic_entrant == 1

        gen created_by_bureaucrat = owner_entry_year == startYear
        
        gcollapse (min) provider_entry_year=owner_entry_year (max) strategic_entrant ///
            strategic_exit created_by_bureaucrat, by(internal_firm_id)

        save_data "../temp/`input_file'_entries", replace key(internal_firm_id)
    }
end

program merge_data
    use "../temp/provider_owned_by_not_bureaucrat_entries", clear
    rename (provider_entry_year strategic_entrant strategic_exit created_by_bureaucrat) ///
        (provider_entry_year_direct strategic_entrant_dir strategic_exit_dir created_by_bureaucrat_dir)

    merge 1:1 internal_firm_id using "../temp/provider_owned_by_sibling_of_not_bureaucrat_entries", ///
        assert(1 2 3) keep(1 2 3)
    rename (provider_entry_year strategic_entrant strategic_exit created_by_bureaucrat) /// 
        (provider_entry_year_indirect strategic_entrant_ind strategic_exit_ind created_by_bureaucrat_ind)
    
    gen owned_by_bureaucrat = _merge == 1 | _merge == 3
    gen owned_by_sibling = _merge == 2 | _merge == 3
    drop _merge

    * Find overall entry year
    egen provider_entry_year = rowmin(provider_entry_year_indirect provider_entry_year_direct)

    egen strategic_entrant     = rowmax(strategic_entrant_dir strategic_entrant_ind)
    egen strategic_exit        = rowmax(strategic_exit_dir strategic_exit_ind)
    egen created_by_bureaucrat = rowmax(created_by_bureaucrat_dir created_by_bureaucrat_ind)

    drop strategic_entrant_dir strategic_entrant_ind strategic_exit_dir ///
        strategic_exit_ind created_by_bureaucrat_dir created_by_bureaucrat_ind

    save_data "$output_dir/master/providers_connections_all", ///
        replace key(internal_firm_id)
end

* Execute
main
