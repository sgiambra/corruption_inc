/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    This do-file identifies which providers are likely "Individuals". 
    The steps are the following:
    1. Drop providers with missing ID
    2. Remove all firms that are registered with supercompañias
    3. Remove providers that are obvious firms (from name)
    4. Add flag unsure_if_individual in case we cannot merge to taxpayers data
    
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

clear all
set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/"

program main
    get_directory_rucs
    clean_tax_data
    use "$data_dir/proc_contracts/tables/providers_registry.dta", clear
    * Drop providers that have missing ID
    drop if provider_id == "."

    * 1. Exclude firms in directorio
    * Note: need to use provider_id since internal_firm_id is only valid internally for contract data analysis
    merge m:1 provider_id using "../temp/list_rucs_directory.dta", keep(1 3) assert(1 2 3)
    bys internal_firm_id: egen to_drop = max(_merge)
    drop if to_drop == 3
    drop to_drop _merge

    local list_terms "SKY INFORMATION SALUD LLC ASOCIACION FUNDACION INSTITUTO COLEGIO JUNTA UNIDAD PARROQUIAL CONSORCIO CONSTRUCTORA " + ///
        "EMPRESA COMPANIA SEGUROS HOSPITAL MUNICIPIO TRANSPORTE COOP PATRONATO GOBIERNO MOTORES AUTOM SERVICIOS MECANICA UNIVERSIDAD INC CORPORATIVO REPUESTOS TELEGRAFO " + ///
        "CENTRO GENERAL AASOCIOADOS CONSULTING OFFICE CONSTRUHIERRO CONSULTORES ACCION INTERNATIONAL ACTIVIDADES PROVEEDOR ADMINISTR SOCIEDAD LTDA INGENIER ESTACION MUNICIP " + ///
        "NACIONALIDAD SUPPLY SAFETY CONSTRUCTOR PANAM PARROQUIA PETROC PETROI PROCESADORA PRODUCCION PROFESIONAL PUCE PUBLICACION PUBLICIDAD RADIO RED ESTACION ESCUELA CUERPO COORPORACION " + ///
        "COMISION COPIADORA DIRECCION CORREO COMERCIALI COMISARIA CLINICA CLUB PHARMA DEPORTIV DISTRITO DISTRIBUID ASISTENCIA ELECTR EMP EQUIPOS FACULTDAD FARMACIA FONDO FUNDA FUND SOLUTION " + ///
        "CABO GRUPO IESS IGLESIA IMPRENTA INST ING SUPER METALES INTERNACIONAL PRODENT FARMAC COMERCIAL CAMARA"

    foreach term in `list_terms' {
        drop if strpos(provider_name_extended, "`term'")>0
    }
    drop if strpos(provider_name_extended, "RED ")>0
    drop if strpos(provider_name_extended, " INC")>0
    drop if strpos(provider_name_extended, " S A")>0
    drop if strpos(provider_name_extended, " S/A")>0
    drop if strpos(provider_name_extended, " SPA")>0
    drop if strpos(provider_name_extended, " SA")==strlen(provider_name_extended)-3
    drop if strpos(provider_name_extended, " SA")==strlen(provider_name_extended)-2

    * Validation using tax data
    rename provider_id person_id
    merge m:1 person_id using "../temp/tax_data_clean.dta", keep(1 3) assert(1 2 3)
    bys internal_firm_id: egen max_merge = max(_merge)
    gen unsure_if_individual = max_merge == 1
    drop _merge max_merge name
    * We cannot classify about 50k IDs

    rename person_id provider_id
    save_data "$data_dir/proc_contracts/tables/individual_suppliers", replace key(provider_id provider_name)  
end

program get_directory_rucs
    use rucFirm using "$data_dir/firms_ownership/directory.dta", replace
    rename rucFirm provider_id
    drop if provider_id == "."
    * Almost 200k rucs become of length 10
    replace provider_id = substr(provider_id, 1, 10) if ///
        substr(provider_id, -3, 3) == "001" & length(provider_id) == 13

    save_data "../temp/list_rucs_directory.dta", key(provider_id) replace
end

program clean_tax_data
    use id name using "${data_dir}/Individuals/irs/clean/all_taxes.dta", clear
    duplicates drop id, force
    rename id person_id
      
    save_data "../temp/tax_data_clean.dta", key(person_id) replace
end

* Execute
main
