set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc"

program main
    use "$data_dir/firms_ownership/shareholders_clean", clear

    gen large_shareholder = share >= .2
    gcollapse (max) large_shareholder, by(firm_id year)

    save_data "../temp/large_shareholders", replace key(firm_id year)

    * Get price index
    import delimited "$data_dir/price_index_WB/API_FP.CPI.TOTL_DS2_en_csv_v2_126205.csv", ///
        varnames(5) rowrange(5) clear

    foreach v of varlist v* {
       local x : variable label `v'
       rename `v' priceindex`x'
    }
    
    keep if countryname == "Ecuador"
    drop priceindex
    reshape long priceindex, i(countryname) j(year)
    keep year priceindex

    save_data "../temp/priceindex", replace key(year)

    use provider_id internal_firm_id using ///
        "$data_dir/proc_contracts/tables/providers_registry", clear
    duplicates drop provider_id, force
    save_data "../temp/providers_registry", replace key(provider_id)

    * Get balance sheet data
    use expediente rucFirm isic startYear active using ///
        "$data_dir/firms_ownership/directory", replace
    rename rucFirm firm_id
    drop if firm_id == "."
    * Almost 200k rucs become of length 10
    replace firm_id = substr(firm_id, 1, 10) if ///
        substr(firm_id, -3, 3) == "001" & length(firm_id) == 13

    save_data "../temp/list_rucs_directory.dta", key(firm_id) replace

    use expediente year capital totalWagesIESS101 totalDirecttWages101 totalWages101 ///
        totalRevenue totalAsset services services2 ///
        totalDebt materials otherMaterials fuel energy using ///
        "$data_dir/balance_sheet/clean/bal2007_2017", clear   

    * Fix negative materials
    replace materials = 0 if materials < 0

    * Materials
    gen Materials1 = materials + otherMaterials + energy + fuel  
    gen Materials2 = materials + otherMaterials + energy + fuel + services
    gen Materials3 = materials + otherMaterials + energy + fuel + services2

    gen variable_inputs = totalWagesIESS101 + Materials3

    merge m:1 expediente using "../temp/list_rucs_directory", ///
        nogen assert(1 2 3) keep(3)

    rename firm_id provider_id
    merge m:1 provider_id using "../temp/providers_registry", ///
        assert(1 2 3) keep(1 3)

    gen contractor = _merge == 3
    drop _merge
    rename provider_id firm_id

    forval i=1/3 {
        local j = `i' + 1
        gen isic`i' = substr(isic, 1, `j')
        egen isic`i'_nbr = group(isic`i')
    }

    * Get price index
    merge m:1 year using "../temp/priceindex", ///
        assert(2 3) keep(3) nogen

    gen revenue_asset_ratio = totalRevenue/totalAsset

    * Follow GNR and drop zeros
    foreach var in capital totalWagesIESS101 totalDirecttWages101 totalWages101 totalRevenue ///
        Materials1 Materials2 Materials3 totalDebt variable_inputs totalAsset revenue_asset_ratio {
        
        replace `var' = `var'/priceindex*100
        winsor2 `var' if `var'>0, replace cuts(1 99)
    }

    foreach var in capital totalWagesIESS101 totalDirecttWages101 totalWages101 totalRevenue ///
        Materials1 Materials2 Materials3 totalDebt variable_inputs totalAsset {

        gen ln_`var' = ln(`var')
    }

    gen age = year-startYear

    rename ln_totalRevenue r
    rename ln_totalWagesIESS101 liess
    rename ln_totalDirecttWages101 ldir
    rename ln_totalWages101 ltot
    rename ln_Materials1 m
    rename ln_Materials2 ms
    rename ln_Materials3 ms2
    rename ln_capital k

    merge 1:1 firm_id year using "../temp/large_shareholders", ///
        nogen assert(1 2 3) keep(1 3)

    label_vars

    save_data "$data_dir/balance_sheet/master/balance_sheet_clean", ///
        replace key(firm_id year)
end

program label_vars
    label var r                     "Ln(Revenue)"
    label var liess                 "Ln(Wages)"
    label var ldir                  "Ln(Wages)"
    label var ltot                  "Ln(Wages)"
    label var m                     "Ln(Intermediate inputs)"
    label var ms                    "Ln(Intermediate inputs)"
    label var ms2                   "Ln(Intermediate inputs)"
    label var k                     "Ln(Capital)"
    label var ln_totalDebt          "Ln(Debt)"
    label var ln_variable_inputs    "Ln(Variable inputs)"
    label var ln_totalAsset         "Ln(Assets)"
    label var revenue_asset_ratio   "Revenue-asset ratio"
end

* Execute
main
