clear all
set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
adopath + ../../../lib/stata/ftools

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/proc_contracts/"

program main
    build_contracts_table
    build_winners_table
end
  
program build_contracts_table
    use "$data_dir/scraped/clean/procurement_links_2008_2017", clear
    
    * Add type of contract from description 
    * Note 1: for some reason 1 link appears in description and not in the list of links
    * Note 2: about 4k contracts have no description page; manually checking a few shows that page doesnt load
    merge 1:1 contract_link using "$data_dir/scraped/clean/descriptions", ///
        assert(1 2 3) keep(3) nogen

    * Create year and year/month info
    gen contract_year = yofd(contract_date)
    gen contract_ym = mofd(contract_date)
    format contract_ym %tm
    rename contract_date contract_ymd

    save_data "$data_dir/tables/contracts_table", replace key(contract_link)
end
 
program build_winners_table
    use "$data_dir/scraped/clean/summary_t1", clear
    fix_name, namevar(provider_name)
    duplicates drop 
    merge m:1 provider_name provider_id using "$data_dir/tables/providers_registry", ///
        assert(2 3) keep(3) nogen keepusing(internal_firm_id)

    rename contract_date contract_paym_start
    * No missings in contracts that appear in t1
    qui count if contract_value == .
    assert `r(N)' == 0

    * Notes: a few contracts with value = 0
    * also likely that some duplicates are still present
    
    append using "../temp/publicacion_winners"
    
    * Add additional contract info to make payment decomposition
    * A few contracts (164) have still missing description page
    merge m:1 contract_link using "$data_dir/tables/contracts_table", ///
        keepusing(contract_ymd contract_budget contract_length advance_percentage ///
        post_percentage) assert(1 2 3) keep(3) nogen
    replace contract_paym_start = contract_ymd if contract_paym_start == .

    * Replace missing values in publicacion contracts (46% obs)
    replace contract_value = contract_budget if contract_value == .

    fcollapse (sum) contract_value (first) contract_paym_start contract_budget ///
        contract_length advance_percentage post_percentage, by(contract_link internal_firm_id)
    save_data "../temp/winners_table", replace key(contract_link internal_firm_id)

    * Get info on contract value from summary_t2 whenever possible
    * Contracts with higher value in t2 have additional complementary contracts
    use "$data_dir/scraped/clean/summary_t2", clear
    fix_name, namevar(provider_name)
    duplicates drop 
    merge m:1 provider_name provider_id using "$data_dir/tables/providers_registry", ///
        assert(2 3) keep(3) nogen keepusing(internal_firm_id)
    fcollapse (sum) contract_value_t2=contract_value, by(contract_link internal_firm_id)
    
    merge 1:1 contract_link internal_firm_id using "../temp/winners_table", ///
        nogen assert(1 2 3) keep(2 3)
    replace contract_value = contract_value_t2 if contract_value_t2 != . & contract_value_t2 != 0
    drop contract_value_t2

    * Fix values of some contracts
    bys contract_link: egen total_contract_value = total(contract_value)
    gen value_to_budget_ratio = total_contract_value/contract_budget

    gen contract_value_new = contract_value*1000 if value_to_budget_ratio < 0.01
    gen diff_value_budget = contract_budget - contract_value_new
    * Fix when overcharge no more than $100 or underbid by no more than $2000 (1,143 contracts)
    replace contract_value = contract_value_new if diff_value_budget >= -100 & diff_value_budget <= 2000

    * Fix when value of multiple products is same as total budget or total value multiplied by quantity (797 contracts)
    gen is_integer = mod(value_to_budget_ratio, 1) == 0
    bys contract_link: gen contract_winners = _N
    replace contract_value = contract_budget/contract_winners if value_to_budget_ratio > 1 & is_integer == 1

    drop total_contract_value value_to_budget_ratio contract_value_new diff_value_budget is_integer contract_winners
    
    bys contract_link: egen total_contract_value = total(contract_value)
    gen value_to_budget_ratio = total_contract_value/contract_budget

    * Drop contracts that still have issues (6,131 contracts)
    drop if value_to_budget_ratio < 0.1 | (value_to_budget_ratio > 1.1 & contract_budget != .)
    drop value_to_budget_ratio total_contract_value

    * Define when contract end
    replace contract_length = 0 if contract_length == .
    gen contract_paym_end = contract_paym_start + contract_length
    format contract_paym_end %td

    replace post_percentage = 0 if post_percentage == .
    replace advance_percentage = 100 - post_percentage if advance_percentage == .
    
    * Separate payment start and end
    gen contract_value_start = advance_percentage*contract_value/100
    gen contract_value_end = post_percentage*contract_value/100

    drop contract_length advance_percentage post_percentage contract_value contract_budget
    
    reshape long contract_value contract_paym, i(contract_link internal_firm_id) j(payment_time) string
    replace payment_time = subinstr(payment_time,"_","",.)
    
    save_data "$data_dir/tables/winners_table", replace key(contract_link internal_firm_id payment_time)
end

program fix_name
    syntax, namevar(varname)
    
    replace `namevar'= upper(ustrto(ustrnormalize(`namevar', "nfd"), "ascii", 2))
    replace `namevar' = subinstr(`namevar', "(", "", .)
    replace `namevar' = subinstr(`namevar', ")", "", .)
    replace `namevar' = subinstr(`namevar', `"""',  "", .)
    replace `namevar' = subinstr(`namevar', "`=char(9)'", "", .)
    replace `namevar' = subinstr(`namevar', ".", "", .)
    replace `namevar' = subinstr(`namevar', ",", "", .)
    replace `namevar' = subinstr(`namevar', "-", "", .)
    replace `namevar' = subinstr(`namevar', "'", "", .)
    replace `namevar' = subinstr(`namevar'," ","",.)
end

* Execute
main
