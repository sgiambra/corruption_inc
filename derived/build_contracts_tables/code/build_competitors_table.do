clear all
set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/proc_contracts/"

program main
    build_proc_competitors
    build_auction_competitors
    build_publicacion_winners

    * Append
    use "../temp/competitors.dta", clear
    append using "../temp/competitors_2017.dta"
    append using "../temp/competitors_missings.dta"
    append using "../temp/competitors_auction.dta"
    append using "../temp/publicacion_winners.dta"

    * A few contracts appear in multiple datasets and with different firm names
    duplicates drop contract_link internal_firm_id, force

    save_data "$data_dir/tables/contracts_competitors.dta", replace key(contract_link internal_firm_id)
end

program build_proc_competitors
    foreach stub in "" "_2017" "_missings" {
        import delimited "$data_dir/scraped/results/competitors`stub'.csv", ///
            delimiter(comma) bindquote(strict) varnames(1) stripquote(yes) encoding(utf8) stringcols(_all) clear
        keep idcontract proveedor ruc
        rename idcontract contract_link

        replace ruc = subinstr(ruc, ".0", "", .)
        gen ruc2 = regexs(2) if regexm(ruc, "^([^0-9]*)([0-9]+)([^0-9]*)$")
        drop ruc
        rename ruc2 ruc

        replace proveedor = subinstr(proveedor, "Noenvióoconfirmótodaslaspropuestas.", "", .)
        replace proveedor = subinstr(proveedor, "Nenvoconfirmtodslsprpsts", "", .)
        replace proveedor = subinstr(proveedor, "Nenvoconfirmtodaslaspropusts", "", .)
        replace proveedor = subinstr(proveedor, "No envió o confirmó todas las propuestas.", "", .)
        replace proveedor = subinstr(proveedor, "Nenvoctlp", "", .)
        replace proveedor = subinstr(proveedor, "Nenvoconfirmtodaslaspropuests", "", .)
        replace proveedor = trim(proveedor)

        gen provider_id = word(proveedor,-1)
        gen has_number = regexm(provider_id, "[0-9]+$")
        replace provider_id = "" if has_number == 0
        drop has_number

        replace proveedor = subinstr(proveedor, provider_id, "" ,.)
        replace proveedor = strtrim(stritrim(proveedor))

        replace provider_id = ruc if ruc != ""
        drop ruc
        rename proveedor provider_name
        
        fix_name, namevar(provider_name)
        reverse_ruc_name, firm_name(provider_name) firm_id(provider_id)

        * If include total score and drop duplicates in term of contract_link provider_name there will be
        * duplicates when there are multi-product contracts with different total_score for different products
        standardize_ids
        duplicates drop

        merge m:1 provider_name provider_id using "$data_dir/tables/providers_registry.dta", ///
            assert(2 3) keep(3) nogen keepusing(internal_firm_id)
        drop provider_id
        * In two contracts we have two different firms to which we assigned same ID
        duplicates drop internal_firm_id, force

        save_data "../temp/competitors`stub'.dta", replace key(contract_link internal_firm_id)
    }
end

program build_auction_competitors
    foreach stub in "" "_2017" {
        import delimited "$data_dir/scraped/competitors_auctions/all_competitors_auction`stub'.csv", ///
            delimiter(comma) varnames(1) encoding(utf8) clear

        keep contract_link firm_name
        rename firm_name provider_name
        gen provider_id = "."
        drop if provider_name == ""
        fix_name, namevar(provider_name)
        duplicates drop contract_link provider_name, force

        merge m:1 provider_name provider_id using "$data_dir/tables/providers_registry.dta", ///
            assert(2 3) keep(3) nogen keepusing(internal_firm_id)
        drop provider_id

        * A few competitors are wrongly associated with same internal_firm_id
        * Would be possible to fix this by using info in https://ecuadorpymes.com/
        duplicates drop contract_link internal_firm_id, force
        save_data "../temp/competitors_auction`stub'.dta", ///
            replace key(contract_link internal_firm_id)
    }

    * Use list of links to get link matching other contract pages
    use "../temp/competitors_auction_2017.dta", clear
    rename contract_link contract_link_auction
    save_data "../temp/competitors_auction_2017.dta", replace key(contract_link_auction internal_firm_id)

    use contract_link_auction contract_link using "$data_dir/scraped/clean/auction_list", clear
    drop if contract_link_auction == ""

    merge 1:m contract_link_auction using "../temp/competitors_auction_2017.dta", ///
        nogen keep(2 3) assert(1 2 3)

    replace contract_link = contract_link_auction if contract_link == ""
    drop contract_link_auction

    append using "../temp/competitors_auction.dta"

    save_data "../temp/competitors_auction.dta", replace key(contract_link internal_firm_id)
end

program build_publicacion_winners
    * Publicacion contracts have always only one competing firm
    import delimited "$data_dir/scraped/publicacion/publicacion_winners.csv", /// 
        varnames(1) encoding(utf8) colrange(2) clear
    drop contract_code

    count if winner_name == "NO DATA"
    * 74,948 (excluding duplicates) have no info on firm and are mainly "desierta" or "por Adjudicar"
    drop if winner_name == "NO DATA"
    drop if winner_name == "ERROR" | winner_name == ""

    rename (winner_id winner_name) (provider_id provider_name)
    
    replace provider_id = subinstr(provider_id, "`=char(9)'", "", .)
    replace provider_id = strtrim(provider_id)
    
    fix_name, namevar(provider_name)
    reverse_ruc_name, firm_name(provider_name) firm_id(provider_id)
    standardize_ids

    drop if provider_name == "" | provider_name == "."
    merge m:1 provider_name provider_id using "$data_dir/tables/providers_registry.dta", ///
        assert(2 3) keep(3) nogen keepusing(internal_firm_id)
    drop provider_id

    * Randomly drop single contract with typo in name and consequent duplicate
    duplicates drop contract_link internal_firm_id, force

    * Use list of links to get link matching other contract pages
    rename contract_link contract_link_publicacion
    save_data "../temp/publicacion_winners.dta", replace key(contract_link_publicacion internal_firm_id)

    use contract_link_publicacion contract_link using "$data_dir/scraped/clean/publicacion_list", clear
    drop if contract_link_publicacion == ""

    merge 1:m contract_link_publicacion using "../temp/publicacion_winners.dta", ///
        nogen keep(2 3) assert(1 2 3)

    replace contract_link = contract_link_publicacion if contract_link == ""
    drop contract_link_publicacion
    
    save_data "../temp/publicacion_winners.dta", replace key(contract_link internal_firm_id)
end

program fix_name
    syntax, namevar(varname)
    
    replace `namevar'= upper(ustrto(ustrnormalize(`namevar', "nfd"), "ascii", 2))
    replace `namevar' = subinstr(`namevar', "(", "", .)
    replace `namevar' = subinstr(`namevar', ")", "", .)
    replace `namevar' = subinstr(`namevar', `"""',  "", .)
    replace `namevar' = subinstr(`namevar', "`=char(9)'", "", .)
    replace `namevar' = subinstr(`namevar', ".", "", .)
    replace `namevar' = subinstr(`namevar', ",", "", .)
    replace `namevar' = subinstr(`namevar', "-", "", .)
    replace `namevar' = subinstr(`namevar', "'", "", .)
    replace `namevar' = subinstr(`namevar'," ","",.)
end

program reverse_ruc_name
    syntax, firm_name(str) firm_id(str)

    gen firm_name_alt = regexs(0) if regexm(`firm_name', "[0-9]+")
    gen reverse = `firm_name' == firm_name_alt
    replace `firm_name' = `firm_id' if reverse == 1
    replace `firm_id' = firm_name_alt if reverse == 1
    drop reverse firm_name_alt
end

program standardize_ids
    replace provider_id = "0" + provider_id if length(provider_id) == 9 | length(provider_id) == 12
    replace provider_id = substr(provider_id, 1, 10) if substr(provider_id, -3, 3) == "001" & ///
        (length(provider_id) == 13 | length(provider_id) == 14)

    replace provider_id = "." if provider_id == "" | regexm(provider_id , "^[0]+$")
end

* Execute
main
