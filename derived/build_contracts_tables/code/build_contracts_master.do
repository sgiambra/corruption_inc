clear all
set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/"

program main
    import delimited "$data_dir/price_index_WB/API_FP.CPI.TOTL_DS2_en_csv_v2_126205.csv", ///
        varnames(5) rowrange(5) clear

    foreach v of varlist v* {
       local x : variable label `v'
       rename `v' priceindex`x'
    }
    
    keep if countryname == "Ecuador"
    drop priceindex
    reshape long priceindex, i(countryname) j(year)
    keep year priceindex
    rename year contract_year

    save_data "../temp/priceindex", replace key(contract_year)

    use "$data_dir/proc_contracts/tables/winners_table", clear
    merge m:1 contract_link using "$data_dir/proc_contracts/tables/contracts_table", ///
        nogen assert(2 3) keep(3)

    drop if contract_state == "Desierta" | contract_state == "Terminado Unilateralmente" | ///
        contract_state == "finalizado por mutuo acuerdo" | contract_state == "Finalizado por mutuo acuerdo"

    gen contract_cat = "auction" if contract_type == "Subasta Inversa Electrónica"
    replace contract_cat = "menor_obras" if contract_type == "Menor Cuantía" & object_type == "Obra"
    replace contract_cat = "menor_notObras" if contract_type == "Menor Cuantía" & object_type != "Obra"
    replace contract_cat = "publicacion" if strpos(contract_type, "Publicación") > 0
    replace contract_cat = "cotizacion" if strpos(contract_type, "Cotización") > 0
    replace contract_cat = "contratacion_d" if strpos(contract_type, "Contratacion directa") > 0
    replace contract_cat = "others" if contract_cat == ""

    bys contract_code internal_firm_id payment_time (contract_link): gen to_keep = _n == 1
    drop if to_keep == 0 & dup == 1
    drop dup to_keep

    egen id = tag(contract_link internal_firm_id)

    merge m:1 contract_year using "../temp/priceindex", ///
        nogen assert(2 3) keep(3)

    replace contract_value = contract_value/priceindex*100
    drop priceindex

    bys contract_link: egen agg_contract_value = total(contract_value)

    build_figures, stub("no_restrictions")

    * Note: We drop contracts with excessivly low or high aggregate value
    preserve
        duplicates drop contract_link, force

        _pctile agg_contract_value, p(1)
        local lower_bound = `r(r1)'

        _pctile agg_contract_value, p(99)
        local upper_bound = `r(r1)'
    restore

    drop if agg_contract_value < `lower_bound' | agg_contract_value > `upper_bound'

    build_figures, stub("with_restrictions")

    save_data "$data_dir/proc_contracts/tables/contracts_master_with_restrictions", replace ///
        key(contract_link internal_firm_id payment_time)
end

program build_figures
    syntax, stub(str)

    preserve
        gcollapse (sum) agg_contract_value nbr_contracts=id (mean) ///
            contract_ym, by(contract_link internal_firm_id)
        
        gcollapse (sum) monthly_contracts=nbr_contracts tot_value=agg_contract_value ///
            (mean) avg_contract_value=agg_contract_value, by(contract_ym)
        
        tsset contract_ym

        twoway tsline monthly_contracts, ytitle("Monthly number of contracts") xtitle("")
        graph export "../output/nbr_contracts_`stub'.png", replace

        twoway tsline tot_value, ytitle("Monthly total contract value") xtitle("")
        graph export "../output/tot_contract_value_`stub'.png", replace

        twoway tsline avg_contract_value, ytitle("Monthly average contract value") xtitle("")
        graph export "../output/avg_contract_value_`stub'.png", replace
    restore

    preserve
        fcollapse (sum) agg_contract_value nbr_contracts=id (mean) contract_ym ///
            (first) contract_cat, by(contract_link internal_firm_id)

        gcollapse (sum) monthly_contracts=nbr_contracts tot_value=agg_contract_value ///
            (mean) avg_contract_value=agg_contract_value, by(contract_ym contract_cat)

        egen contract_type_nbr = group(contract_cat)
        xtset contract_type_nbr contract_ym

        qui levelsof contract_cat, local(contract_cats)
        
        foreach contract_cat in `contract_cats' {
            cap: mkdir "../output/`contract_cat'"

            twoway tsline monthly_contracts if contract_cat == "`contract_cat'", /// 
                ytitle("Monthly number of contracts") xtitle("")
            graph export "../output/`contract_cat'/nbr_contracts_`stub'.png", replace

            twoway tsline tot_value if contract_cat == "`contract_cat'", ///
                ytitle("Monthly total contract value") xtitle("")
            graph export "../output/`contract_cat'/tot_contract_value_`stub'.png", replace

            twoway tsline avg_contract_value if contract_cat == "`contract_cat'", ///
                ytitle("Monthly average contract value") xtitle("")
            graph export "../output/`contract_cat'/avg_contract_value_`stub'.png", replace
        }
    restore
end

* Execute
main
