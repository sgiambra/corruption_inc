set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/balance_sheet"

program main
   local keep_vars = "year expediente ruc sector wageIess101 wageNoIess101 iessContributions101 "          + ///
                    "totalWagesIESS101 totalDirecttWages101 otherWages101 totalWages101 wagesProvisions " + ///
                    "totalRevenue totalSales totalCostExp capital materials otherMaterials fuel energy "  + ///
                    "cumulDeprec shortLiabilities longLiabilities equity totalAsset totalDebt socialCapital " + ///
                    "additional_cost_exp services services2"
    /* Notes: TotalCost TotalExp cannot be computed for bal2007-1*/

    foreach stub in "2007-2" "2007-3" "2008-2" "2008-3" "2009-1" "2009-2" "2010-1" "2010-2" "2011-1" {
        clean_2007_to_2011_part1, stub(`stub') keep_varlist(`keep_vars')
    }
    foreach stub in "2007-1" "2008-1" {
        clean_2007_2008_part2, stub(`stub') keep_varlist(`keep_vars')
    }
    foreach stub in "2010-3" "2011-2" {
        clean_2010_2011_part2, stub(`stub') keep_varlist(`keep_vars')
    }
    foreach stub in "2010-4" "2011-3" "2012" "2013" {
        clean_2010_to_2013, stub(`stub') keep_varlist(`keep_vars')
    }
    foreach year of numlist 2014(1)2017 {
        clean_2014_to_2017, year(`year') keep_varlist(`keep_vars')
    }

    use "../temp/balance_2007-1.dta", clear
    foreach stub in "2007-2" "2007-3" "2008-1" "2008-2" "2008-3" "2009-1" "2009-2" "2010-1" "2010-2" ///
                    "2010-3" "2010-4" "2011-1" "2011-2" "2011-3" "2012" "2013" "2014" "2015" "2016" "2017" {
        append using "../temp/balance_`stub'.dta"
    }

    save_data "${data_dir}/clean/bal2007_2017.dta", key(expediente year) replace
end

program clean_2007_to_2011_part1
    syntax, stub(str) keep_varlist(str)

    local year = substr("`stub'", 1, 4)
    import delimited "${data_dir}/raw/balances_`year'/bal`stub'.txt", encoding(utf16) clear

    rename año year
    rename rama_actividad sector

    *=============================================================
    * Wages
    *=============================================================
    if "`stub'" == "2007-2" | "`stub'" == "2008-2" | "`stub'" == "2009-1" | "`stub'" == "2010-1" {
        gen wageIess101 = sueldos_salarios_y_demas_715 + sueldos_salarios_y_demás_716
        gen otherWages101 = honorarios_profesionales_721 + honorarios_profesionales_722 + ///
                            honorarios_a_extranjeros_723 + honorarios_a_extranjeros_724
    }
    
    if "`stub'" == "2007-3" | "`stub'" == "2008-3" | "`stub'" == "2009-2" | "`stub'" == "2010-2" | "`stub'" == "2011-1" {
        gen wageIess101 = sueldos_salarios_y_demas__715 + sueldos_salarios_y_demas__716
        
        gen otherWages101 = honorarios_profesionales__721 + honorarios_profesionales__722 + ///
                            honorarios_a_extranjeros__723 + honorarios_a_extranjeros__724
    }
    
    gen wageNoIess101        = benef_sociales_indemniz_y_717 + benef_sociales_indemniz_y_718
    gen iessContributions101 = aporte_a_la_seguridad_soc_719 + aporte_a_la_seguridad_soc_720
    
    gen totalWagesIESS101    = wageIess101 + iessContributions101 
    gen totalDirecttWages101 = totalWagesIESS101 + wageNoIess101
    gen totalWages101 = otherWages101 + totalDirecttWages101

    gen wagesProvisions = provisiones_para_jubilaci_737 + provisiones_para_jubilaci_738 + provisiones_para_desahuci_739 + ///
                          provisiones_para_desahuci_740 + provisiones_otras_provisi_743 + provisiones_otras_provisi_744
        
    *=============================================================
    * Materials
    *=============================================================
    
    if "`stub'" == "2007-2" | "`stub'" == "2008-2" | "`stub'" == "2009-1" | "`stub'" == "2010-1" {
        gen materials = inventario_inicial_de_bie_701 +   compras_netas_locales_de_702+importaciones_de_bienes_n_703+ importaciones_de_bienes_n_704+inventario_final_de_biene_705 + ///
                        inventario_inicial_de_mat_706 + compras_netas_locales_de_707 + importaciones_de_materia_708 + inventario_final_de_mater_709 + ///
                        inventario_final_de_mater_709 + inventario_inicial_de_pro_710 + inventario_final_de_produ_711+ inventario_inicial_produc_712+  inventario_final_de_produ_713                            
                        
        egen additional_cost_exp = rowtotal(provisiones_para_jubilaci_737   provisiones_para_jubilaci_738   provisiones_para_desahuci_739 ///
            provisiones_para_desahuci_740   provisiones_para_cuentas_742    provisiones_otras_provisi_743   provisiones_otras_provisi_744   ///
            arrendamiento_mercantil_745 arrendamiento_mercantil_746 arrendamiento_mercantil_747 arrendamiento_mercantil_748 comisiones_local_749 ///
            comisiones_local_750    comisiones_del_exterior_751 comisiones_del_exterior_752 intereses_bancarios_local_753   intereses_bancarios_local_754 ///
            intereses_bancarios_del_e_755   intereses_bancarios_del_e_756   intereses_pagados_a_terce_757   intereses_pagados_a_terce_758 ///
            intereses_pagados_a_terce_759   intereses_pagados_a_terce_760   intereses_pagados_a_terce_761   intereses_pagados_a_terce_762 ///
            intereses_pagados_a_terce_763   intereses_pagados_a_terce_764 depreciacion_de_activos_f_782 depreciacion_de_activos_f_783   ///
            depreciacion_de_activos_f_784   amortizaciones_785  amortizaciones_786) 
            
        egen services = rowtotal(mantenimiento_y_reparacio_727 mantenimiento_y_reparacio_728 promocion_y_publicidad_731  promocion_y_publicidad_732 transporte_735  /// 
            transporte_736 seguros_y_reaseguros_prim_771 seguros_y_reaseguros_prim_772 gastos_indirectos_asignad_773 gastos_indirectos_asignad_774  ///  
            gastos_de_gestion_775  gastos_de_viaje_777 gastos_de_viaje_778  pagos_por_otros_servicios_789 pagos_por_otros_servicios_790   /// 
            pagos_por_otros_bienes_791 pagos_por_otros_bienes_792)  
    
        egen services2 = rowtotal( promocion_y_publicidad_731  promocion_y_publicidad_732 transporte_735  /// 
            transporte_736 seguros_y_reaseguros_prim_771 seguros_y_reaseguros_prim_772 gastos_indirectos_asignad_773 gastos_indirectos_asignad_774  ///  
            gastos_de_gestion_775  gastos_de_viaje_777 gastos_de_viaje_778  pagos_por_otros_servicios_789 pagos_por_otros_servicios_790   /// 
            pagos_por_otros_bienes_791 pagos_por_otros_bienes_792)
                        
    }
    
    if "`stub'" == "2007-3" | "`stub'" == "2008-3" | "`stub'" == "2009-2" | "`stub'" == "2010-2" | "`stub'" == "2011-1" {
        gen materials = inventario_inicial_de_bie_701 +   compras_netas_locales_de__702+importaciones_de_bienes_n_703+ importaciones_de_bienes_n_704+inventario_final_de_biene_705 + ///
                        inventario_inicial_de_mat_706 + compras_netas_locales_de__707 + importaciones_de_materia__708 + inventario_final_de_mater_709 + ///
                        inventario_final_de_mater_709 + inventario_inicial_de_pro_710 + inventario_final_de_produ_711+ inventario_inicial_produc_712+  inventario_final_de_produ_713

        egen additional_cost_exp = rowtotal( arrendamiento_de_inmueble_725  arrendamiento_de_inmueble_726 provisiones_para_jubilaci_737 provisiones_para_jubilaci_738   ///
            provisiones_para_desahuci_739   provisiones_para_desahuci_740   provisiones_para_cuentas__742   provisiones_otras_provisi_743   ///
            provisiones_otras_provisi_744   arrendamiento_mercantil_l_745   arrendamiento_mercantil_l_746   arrendamiento_mercantil_d_747   ///
            arrendamiento_mercantil_d_748   comisiones_local_749    comisiones_local_750    comisiones_del_exterior_751 comisiones_del_exterior_752 ///
            intereses_bancarios_local_753   intereses_bancarios_local_754   intereses_bancarios_del_e_755   intereses_bancarios_del_e_756   ///
            intereses_pagados_a_terce_757   intereses_pagados_a_terce_758   intereses_pagados_a_terce_759   intereses_pagados_a_terce_760   ///
            intereses_pagados_a_terce_761   intereses_pagados_a_terce_762   intereses_pagados_a_terce_763   intereses_pagados_a_terce_764 ///
            perdida_en_venta_de_activ_765   perdida_en_venta_de_activ_766   perdida_en_venta_de_activ_767   perdida_en_venta_de_activ_768   ///
            otras_perdidas_769  otras_perdidas_770 impuestos_contribuciones__776 iva_que_se_carga_al_costo_779 iva_que_se_carga_al_costo_780 ///
            depreciacion_de_activos_f_781 depreciacion_de_activos_f_782 depreciacion_de_activos_f_783 depreciacion_de_activos_f_784 amortizaciones_785 amortizaciones_786)

        egen services = rowtotal(mantenimiento_y_reparacio_727 mantenimiento_y_reparacio_728 promocion_y_publicidad_731  promocion_y_publicidad_732 transporte_735  /// 
            transporte_736 seguros_y_reaseguros_prim_771 seguros_y_reaseguros_prim_772 gastos_indirectos_asignad_773 gastos_indirectos_asignad_774  ///  
            gastos_de_gestion_775 gastos_de_viaje_777 gastos_de_viaje_778  pagos_por_otros_servicios_789 pagos_por_otros_servicios_790  ///  
            pagos_por_otros_bienes_791 pagos_por_otros_bienes_792)
            
        egen services2 = rowtotal(  promocion_y_publicidad_731  promocion_y_publicidad_732 transporte_735  /// 
            transporte_736 seguros_y_reaseguros_prim_771 seguros_y_reaseguros_prim_772 gastos_indirectos_asignad_773 gastos_indirectos_asignad_774  ///  
            gastos_de_gestion_775 gastos_de_viaje_777 gastos_de_viaje_778  pagos_por_otros_servicios_789 pagos_por_otros_servicios_790  ///  
            pagos_por_otros_bienes_791 pagos_por_otros_bienes_792)
    
    }
    
    gen totalCost    = total_costos_797
    gen totalExp     = total_gastos_798
    gen totalCostExp = total_costos_y_gastos_799

    gen otherMaterials = suministros_y_materiales_733 + suministros_y_materiales_734
    gen fuel           = combustibles_729 + combustibles_730
    gen energy         = servicios_publicos_787 + servicios_publicos_788    

    *=============================================================
    * Capital
    *=============================================================
    gen capital           = total_activo_fijos_369
    gen socialCapital     = capital_suscrito_y_o_asig_501
    gen totCurrentCapital = total_activo_corriente_339

    gen cumulDeprec = -depreciacion_acumulada_ac_348

    *=============================================================
    * Sales and others
    *=============================================================
    gen totalRevenue = total_ingresos_699
    gen totalSales   = ventas_netas_locales_grav_601 + ventas_netas_locales_grav_602 + exportaciones_netas_603 + otros_ingresos_provenient_604

    gen shortLiabilities = total_pasivo_corriente_439
    gen longLiabilities  = total_pasivo_largo_plazo_469
    gen equity = total_patrimonio_neto_598

    gen totalAsset = total_del_activo_399
    gen totalDebt  = total_del_pasivo_499
            
    keep `keep_varlist'
    save_data "../temp/balance_`stub'.dta", key(expediente) replace
end

program clean_2007_2008_part2
    syntax, stub(str) keep_varlist(str)

    local year = substr("`stub'", 1, 4)
    import delimited "${data_dir}/raw/balances_`year'/bal`stub'.txt", encoding(utf16) clear

    rename año year
    rename rama_actividad sector

    *=============================================================
    * Wages
    *=============================================================
    gen wageIess101 = sueldos_salarios_y_demas__724 + sueldos_salarios_y_demas__725
    
    gen otherWages101 = honorarioscomisiones_diet_729 + remuneraciones_a_otros_tr_730 + honorarios_a_extranjeros__731

    gen wageNoIess101        = beneficios_sociales_e_ind_727
    gen iessContributions101 = aportes_a_la_seguridad_so_726
    
    gen totalWagesIESS101    = wageIess101 + iessContributions101 
    gen totalDirecttWages101 = totalWagesIESS101 + wageNoIess101
    gen totalWages101 = otherWages101 + totalDirecttWages101

    gen wagesProvisions = gasto_provision_para_jubi_728+provisiones_para_jubilaci_457+provisiones_431
    
    *=============================================================
    * Materials
    *=============================================================
    gen materials = inv_inicial_d_bienes_no_p_711 + compras_netas_locd_bienes_712 + importaciones_de_bienes_n_713 + invfinal_bienes_no_produc_714 + ///
        inventario_inicial_de_mat_715 + compras_netas_locales_de__716 + importaciones_de_materia__717 + inventario_final_de_mater_718 + ///
        inventario_inicial_de_pro_719 + inventario_final_de_produ_720 + inventario_inicial_de_pro_721 + inventario_final_de_produ_722 
                                
    gen otherMaterials = suministros_y_materiales_742
    
    gen fuel = combustibles_737 + lubricantes_738
    gen energy = agua_energia_luz_y_teleco_746

    gen totalCostExp = total_costos_y_gastos_799
    
    egen additional_cost_exp = rowtotal(impuestos_contribuciones__748   depreciacion_de_activos_f_749   depreacelerada_vehiculos__750   ///
        amortizaciones_inversione_751   provision_cuentas_incobra_752   amortizaciones_y_gasto_po_753   ///
        intereses_y_comisiones_ba_754   intereses_y_comisiones_ba_755   intereses_pagados_a_terce_756   intereses_pagados_a_terce_757) 
        
    egen services = rowtotal(mantenimiento_y_reparacio_732 comisión_a_sociedades_735 promocion_y_publicidad_736 seguros_y_reaseguros_prim_741 transporte_743  /// 
        gastos_de_gestionagasajos_744 gastos_de_viaje_745 notarios_y_registradores__747 gasto_provisiones_deducib_760 otros_gastos_locales_761 otros_gastos_exterior_762)   
    
    
    egen services2 = rowtotal(promocion_y_publicidad_736 seguros_y_reaseguros_prim_741 transporte_743  /// 
        gastos_de_gestionagasajos_744 gastos_de_viaje_745 notarios_y_registradores__747 gasto_provisiones_deducib_760 otros_gastos_locales_761 otros_gastos_exterior_762)
    
    *=============================================================
    * Capital
    *=============================================================
    gen cumulDeprec = -(depreciacion_acumulada_ac_355 + depreciacion_acumacelerad_357)      
    
    gen capital           = total_activo_fijo_369
    gen socialCapital     = capital_suscrito_asignado_501
    gen totCurrentCapital = total_activo_corriente_339

    *=============================================================
    * Sales and others
    *=============================================================
    gen totalRevenue = total_ingresos_699
    gen totalSales   = ventas_netas_locales_grav_601 + ventas_netas_locales_grav_602 + exportaciones_netas_603 + ingresos_provenientes_del_604



    gen shortLiabilities = total_pasivo_corriente_439
    gen longLiabilities  = total_pasivo_a_largo_plaz_469
    gen equity = total_patrimonio_neto_598

    gen totalAsset = total_del_activo_399
    gen totalDebt  = total_del_pasivo_499

    keep `keep_varlist'
    save_data "../temp/balance_`stub'.dta", key(expediente) replace
end

program clean_2010_2011_part2 
    syntax, stub(str) keep_varlist(str)

    local year = substr("`stub'", 1, 4)
    import delimited "${data_dir}/raw/balances_`year'/bal`stub'.txt", encoding(utf8) clear

    rename ao year
    rename rama_actividad sector

    *=============================================================
    * Wages
    *=============================================================
    gen wageIess101 = sueldos_y_beneficio_510201 + sueldos_y_beneficio_510301 + sueldos_salarios_y__520101 + sueldos_salarios_y__520201
    gen otherWages101 = honorarios_comision_520105 + honorarios_comision_520205 + honorarios_a_extran_520107 + honorarios_a_extran_520207

    gen wageNoIess101        = beneficios_sociales_520103 + beneficios_sociales_520203
    gen iessContributions101 = aportes_a_la_seguri_520102 + aportes_a_la_seguri_520202
    
    gen totalWagesIESS101    = wageIess101 + iessContributions101 
    gen totalDirecttWages101 = totalWagesIESS101 + wageNoIess101
    gen totalWages101 = otherWages101 + totalDirecttWages101

    gen wagesProvisions = gasto_planes_de_ben_510202 + gasto_planes_de_ben_510302 + gasto_planes_de_ben_520104 + gasto_planes_de_ben_520204
        
    *=============================================================
    * Materials
    *=============================================================
    gen materials      = materiales_utilizad_5101
                                
    gen otherMaterials = suministros_materia_510407+ materiales_52012402 + materiales_52022402
    
    gen fuel           = combustibles_520112 + combustibles_520212 + lubricantes_520213 + lubricantes_520113
    gen energy         = agua_energia_luz_y__520118 + agua_energia_luz_y__520218

    gen totalCostExp = gastos_52 + costo_de_ventas_y_p_51
    gen totalCost =  gastos_52
    gen totalExp =  costo_de_ventas_y_p_51

    egen additional_cost_exp = rowtotal(depreciacion_propie_510401 depreciacion_de_act_510402 deterioro_de_propie_510403 ///
        impuestos_contribuc_520120 depreciaciones_520121    amortizaciones_520122   gasto_deterioro_520123  ///
        gasto_por_reestruct_520125  valor_neto_de_reali_520126 arrendamiento_opera_520209 gastos_financieros_5203 ///
        depreciaciones_520221   amortizaciones_520222   gasto_deterioro_520223  gastos_por_cantidad_520224  ///
        gasto_por_reestruct_520225 valor_neto_de_reali_520226)

    egen services = rowtotal(depreciacion_propie_510401 otros_costos_de_pro_510408 mantenimiento_y_rep_520108 comisiones_520110 promocion_y_publici_520111  /// 
        seguros_y_reaseguro_520114 transporte_520115 gastos_de_gestion_a_520116 gastos_de_viaje_520117 notarios_y_registra_520119 otros_gastos_520127   /// 
        mantenimiento_y_rep_520208 comisiones_520210 promocion_y_publici_520211 seguros_y_reaseguro_520214 transporte_520215 gastos_de_gestion_a_520216   /// 
        gastos_de_viaje_520217 notarios_y_registra_520219 otros_gastos_520227)

    egen services2 = rowtotal(otros_costos_de_pro_510408  promocion_y_publici_520111  /// 
        seguros_y_reaseguro_520114 transporte_520115 gastos_de_gestion_a_520116 gastos_de_viaje_520117 notarios_y_registra_520119 otros_gastos_520127   /// 
        promocion_y_publici_520211 seguros_y_reaseguro_520214 transporte_520215 gastos_de_gestion_a_520216   /// 
        gastos_de_viaje_520217 notarios_y_registra_520219 otros_gastos_520227)
    
    *=============================================================
    * Capital
    *=============================================================
    gen capital           = activo_no_corriente_102
    gen socialCapital     = capital_suscrito_o__30101
    gen totCurrentCapital = activo_corriente_101

    gen cumulDeprec = -(depreciacion_acumula_1020112 + depreciacion_acumula_1020203)
    
    *=============================================================
    * Sales and others
    *=============================================================
    gen totalRevenue = ingresos_de_activid_41 + otros_ingresos_43
    gen totalSales   = venta_de_bienes_4101 + prestacion_de_servi_4102 + contratos_de_constr_4103 + ///
                   descuento_en_ventas_4109 + devoluciones_en_ven_4110 + bonificacion_en_pro_4111 + otras_rebajas_comer_4112

    gen shortLiabilities = pasivo_corriente_201
    gen longLiabilities  = pasivo_no_corriente_202
    gen equity = patrimonio_neto_3
    gen totalAsset = activo_1
    gen totalDebt  = pasivo_2

    keep `keep_varlist'
    save_data "../temp/balance_`stub'.dta", key(expediente) replace
end

program clean_2010_to_2013
    syntax, stub(str) keep_varlist(str)

   
    local year = substr("`stub'", 1, 4)

    if "`stub'" == "2012" | "`stub'" == "2013" {
        import delimited "${data_dir}/raw/balances_`year'/bal`stub'.txt", encoding(utf16) clear
        rename año year
    }
    if "`stub'" == "2010-4" | "`stub'" == "2011-3" {
        import delimited "${data_dir}/raw/balances_`year'/bal`stub'.txt", encoding(utf8) clear
        rename ao year
    }
    
    rename rama_actividad sector
    
    *=============================================================
    * Wages
    *=============================================================
    gen wageIess101          = sueldos_y_benefici_510201 + sueldos_y_benefici_510301 + sueldos_salarios__520101 + sueldos_salarios__520201
    gen wageNoIess101        = beneficios_sociale_520103 + beneficios_sociale_520203
    gen iessContributions101 = aportes_a_la_segur_520102 + aportes_a_la_segur_520202
    
    gen totalWagesIESS101    = wageIess101 + iessContributions101
    gen totalDirecttWages101 = totalWagesIESS101 + wageNoIess101 
    
    gen otherWages101 = honorarios_comisi_520105 + honorarios_comisi_520205 + honorarios_a_extra_520107 + honorarios_a_extra_520207
    gen totalWages101 = otherWages101 + totalDirecttWages101            
    gen wagesProvisions = gasto_planes_de_be_510202 + gasto_planes_de_be_510302 + gasto_planes_de_be_520104 + gasto_planes_de_be_520204

    *=============================================================
    * Materials
    *=============================================================
    gen materials      = materiales_utiliza_5101
    gen otherMaterials = suministros_materi_510407 + materiales_52012402 + materiales_52022402
    gen fuel           = combustibles_520112 + combustibles_520212 + lubricantes_520213 + lubricantes_520113
    gen energy         = agua_energia_luz_520118 + agua_energia_luz_520218

    gen totalCost    = costo_de_ventas_y__51
    gen totalExp     = gastos_52
    gen totalCostExp = gastos_52 + costo_de_ventas_y__51
    
    egen additional_cost_exp = rowtotal(depreciacion_propi_510401   deterioro_o_perdid_510402   deterioro_de_propi_510403   efecto_valor_neto__510404 ///
        arrendamiento_oper_520109 depreciaciones_520121 propiedades_plant_52012101  propiedades_de_inv_52012102 amortizaciones_520122   ///
        intangibles_52012201    otros_activos_52012202  gasto_deterioro_520123  propiedades_plant_52012301  ///
        otros_activos_52012306 impuestos_contrib_520220 depreciaciones_520221   propiedades_plant_52022101  ///
        propiedades_de_inv_52022102 amortizaciones_520222   intangibles_52022201    otros_activos_52022202  ///
        gasto_deterioro_520223  propiedades_plant_52022301 instrumentos_finan_52022303  intangibles_52022304    ///
        cuentas_por_cobrar_52022305 otros_activos_52022306  gastos_por_cantida_520224   mano_de_obra_52022401   ///
        materiales_52022402 costos_de_producci_52022403 gasto_por_reestruc_520225   valor_neto_de_real_520226   ///
        gasto_impuesto_a_l_520227   otros_gastos_520228 gastos_financieros_5203 intereses_520301    comisiones_520302   ///
        gastos_de_financia_520303   diferencia_en_camb_520304   otros_gastos_finan_520305 ) 
        
    egen services = rowtotal(mantenimiento_y_re_510406 otros_costos_de_pr_510408 mantenimiento_y_re_520108 comisiones_520110 promocion_y_public_520111   /// 
       seguros_y_reasegur_520114 transporte_520115 gastos_de_gestion__520116 gastos_de_viaje_520117 notarios_y_registr_520119 costos_de_producci_52012403  /// 
       mantenimiento_y_re_520208 comisiones_520210 seguros_y_reasegur_520214 transporte_520215 gastos_de_gestion__520216 gastos_de_viaje_520217  /// 
       notarios_y_registr_520219 costos_de_producci_52022403)
   
    egen services2 = rowtotal( otros_costos_de_pr_510408  promocion_y_public_520111   /// 
        seguros_y_reasegur_520114 transporte_520115 gastos_de_gestion__520116 gastos_de_viaje_520117 notarios_y_registr_520119 costos_de_producci_52012403  /// 
        seguros_y_reasegur_520214 transporte_520215 gastos_de_gestion__520216 gastos_de_viaje_520217  /// 
        notarios_y_registr_520219 costos_de_producci_52022403)
    
    *=============================================================
    * Capital
    *=============================================================
    gen capital           = activo_no_corrient_102
    gen socialCapital     = capital_suscrito_o_30101
    gen totCurrentCapital = activo_corriente_101
    
    gen cumulDeprec = -(depreciacion_a_1020112 + depreciacion_a_1020203)
    
    *=============================================================
    * Sales and others
    *=============================================================
    gen totalRevenue = ingresos_de_activi_41 + otros_ingresos_43
    gen totalSales   = venta_de_bienes_4101 + prestacion_de_serv_4102 + contratos_de_const_4103 + ///
                       devoluciones_e_4111 + descuento_en_v_4110 + bonificacion_e_4112 + otras_rebajas__4113  /* NOTE: discounts are recorded with negative sign */
        
    gen shortLiabilities = pasivo_corriente_201
    gen longLiabilities  = pasivo_no_corrient_202
    gen equity = patrimonio_neto_3

    gen totalAsset = activo_1
    gen totalDebt  = pasivo_2

    keep `keep_varlist'
    save_data "../temp/balance_`stub'.dta", key(expediente) replace 
end

program clean_2014_to_2017
    syntax, year(int) keep_varlist(str)

    import delimited "${data_dir}/raw/balances_`year'/bal`year'.txt", encoding(utf16) clear

    rename anio year
    rename rama_actividad sector

    *=============================================================
    * Wages
    *=============================================================
    gen wageIess101          = costo_ssa_qcm__7040 + gasto_ssa_qcm__7041 
    gen wageNoIess101        = costo_bsi_qnc__7043 + gasto_bsi_qnc__7044
    gen iessContributions101 = costo_ies__7046 + gasto_ies__7047 
    gen totalWagesIESS101    = wageIess101 + iessContributions101 
    
    gen totalDirecttWages101 = totalWagesIESS101 + wageNoIess101 
    gen otherWages101        = cto_hon_pro_dietas__7049 + gto_hon_pro_dietas__7050 + cto_hon_pro_soc__7052 + gto_hon_pro_soc__7053
    gen totalWages101        = otherWages101 + totalDirecttWages101

    gen wagesProvisions = cto_otr_gto_ben_emp__7061 + gto_otr_gto_ben_emp__7062 + cto_provisiones_jpa__7055 + cto_provisiones_desahucio_7058 + ///
        gto_provisiones_desahucio_7059 + gto_provisiones_jpa__7056

    *=============================================================
    * Materials
    *=============================================================
    gen materials = cto_ivi_bienes_npp__7001 + cto_cln_bienes_npp__7004 + cto_ipr_bienes_npp__7007 + gto_ipr_bienes_npp__7008 - cto_ivf_bienes_npp__7010 + ///
                    cto_ivi_materia_prima__7013 + cto_cln_materia_prima__7016 +cto_ipr_materia_prima__7019 - cto_ivf_materia_prima__7022 + /// 
                    costo_ivi_ppr__7025 - costo_ivf_ppr__7028 +costo_ivi_pte__7031 -costo_ivf_pte__7034 +aju_costo_venta__7037+aju_gasto__7038 

    gen otherMaterials = cto_sum_materiales__7190 + gto_sum_materiales__7191

    gen fuel           = gto_combustibles_lub__7179 + vnd_combustibles_lub__7180
    gen energy         = costo_servicios_publicos__7241 + gasto_servicios_publicos__7242

    gen totalCostExp = totas_costos_gastos__7999
    gen totalCost =  total_costos__7991
    gen totalExp =  total_gastos__7992
    
    if `year'==2014 {
        egen additional_cost_exp = rowtotal(cto_dpr_ace_act_fijos__7064 gto_dpr_ace_act_fijos__7065 vnd_dpr_ace_act_fijos__7066 cto_dpr_no_ace_afj__7067 ///
            gto_dpr_no_ace_afj__7068    vnd_dpr_no_ace_afj__7069    cto_dpr_pin_cto_his__7070   gto_dpr_propiedades_inv_7071    ///
            vnd_gto_dpr_pin_cto_his__7072   cto_dpr_aee_cto_his__7073   gto_dpr_aee_cto_his__7074   vnd_gto_dpr_aee_cto_his__7075   ///
            cto_dpr_rev_afj__7076   gto__dpr_rev_afj__7077  vnd__dpr_rev_afj__7078  cto_dpr_pin_rev__7079   vnd_gto_dpr_pin_rev__7081   ///
            cto_dpr_aee_rev__7082   gto_dpr_aee_rev__7083   vnd_gto_dpr_aee_rev__7084   cto_otr_gto_dpr_rev__7085   gto_otr_gto_dpr_rev__7086   ///
            vnd_gto_otr_gto_dpr_rev__7087   cto_dpr_act_biologicos__7088    gto_dpr_act_biologicos__7089    vnd_dpr_act_biologicos__7090    ///
            cyg_dpr_otr_dpr__7091   gyg_dpr_otr_dpr__7092   vnd_gyg_dpr_otr_dpr__7093   cto_amo_chi_act_int__7094   gto_amo_chi_act_int__7095   ///
            vnd_gto_amo_chi_act_int__7096   cto_amo_activos_eee_7097    gto_amo_aee_chi__7098   vnd_gto_amo_aee_chi__7099   cto_amo_rev_act_in__7100 ///
            gto_amo_rev_act_in__7101    vnd_gto_amo_rev_act_in__7102    gto_amo_aee_rev__7104   vnd_gto_amo_aee_rev__7105   cto_amo_rev_otr__7106   ///
            gto_amo_rev_otr__7107   vnd_gto_amo_rev_otr__7108   cto_amo_otr_amo__7109   gto_amo_otr_amo__7110   vnd_gto_amo_otr_amo__7111    ///
            gto_provision_cta_inb__7113 vnd_provision_cta_inb__7114 gto_provisiones_vtr_inv__7115   cto_provisiones_vtr_inv__7116   ///
            vnd_provisiones_vtr_inv__7117   gto_pnt_det_vanc_vta__7119  vnd_gto_pnt_det_vanc_vta__7120  gto_pnt_det_vact_bio__7122  ///
            vnd_gto_pnt_det_vact_bio__7123  cto_pnt_det_ppe__7124   gto_pnt_det_ppe__7125   vnd_gto_pnt_det_ppe__7126   cto_pnt_det_act_int__7127   ///
            gto_pnt_det_act_int__7128   vnd_gto_pnt_det_act_int__7129   gto_pnt_det_prp_inv__7131   vnd_gto_pnt_det_prp_inv__7132   ///
            gto_pnt_det_aee_rec_min__7134   vnd_gto_pnt_det_aee_remin_7135  gto_pnt_det_ain_no_cte__7137    vnd_gto_pnt_dt_ain_no_cte_7138  ///
            cto_pnt_det_val_otr__7139   gto_pnt_det_val_otr__7140   vnd_gto_pnt_det_val_otr__7141   cto_prv_gar__7142   gto_prv_gar__7143   ///
            vnd_gto_prv_gar__7144   cto_prv_desm__7145  gto_prv_desm__7146  vnd_gto_prv_desm__7147  gto_prv_con_one__7149   vnd_gto_prv_con_one__7150   ///
            gto_prv_res_neg__7152   vnd_gto_prv_res_neg__7153   gto_prv_rmb_cli__7155   vnd_gto_prv_rmb_cli__7156   gto_prv_lit__7158   ///
            vnd_gto_prv_lit__7159   cto_prv_pas_con_com_neg__7160   gto_prv_pas_con_com_neg__7161   vnd_gto_prv_pas_con_cm_ng_7162  ///
            costo_otras_provisiones__7163   gasto_otras_provisiones__7164   vnd_otras_provisiones__7165 cto_perdida_vta_act_rel__7166   ///
            gto_perdida_vta_act_rel__7167   vnd_perdida_vta_act_rel__7168   cto_perdida_vta_act_nre__7169   gto_perdida_vta_act_nre__7170 ///
            cto_mermas__7199    gto_mermas__7200    vnd_mermas__7201 cto_imp_contribucins_otrs_7208 imp_contribuciones_otros__7209  ///
            vnd_imp_contr_otros__7210   cto_opr_sta_rel_loc__7223   gto_opr_sta_rel_loc__7224   vnd_gto_opr_sta_rel_loc__7225   ///
            cto_opr_sta_rel_ext__7226   gto_opr_sta_rel_ext__7227   vnd_gto_opr_sta_rel_ext__7228   cto_opr_sta_n_rel_loc__7229 ///
            gto_opr_sta_n_rel_loc__7230 vnd_gto_opr_sta_n_rel_loc_7231  cto_opr_sta_n_rel_ext__7232 gto_opr_sta_n_rel_ext__7233 ///
            vnd_gto_opr_sta_n_rel_ext_7234 cto_ins_org_sim__7235    gto_ins_org_sim__7236   vnd_gto_ins_org_sim__7237   ///
            iva_que_carga_costo__7238   iva_que_carga_gasto__7239   vnd_iva_que_carga_gasto__7240)
                                            
        egen services = rowtotal(gto_promocion_publicidad__7173 vnd_promocion_publicidad__7174 gasto_transporte__7176 vnd_transporte__7177 /// 
            gastos_viaje_gasto__7182 vnd_gastos_viaje__7183 gastos_gestion__7185 vnd_gastos_gestion__7186 cto_mantenimiento_rpa__7196 gto_mantenimiento_rpa__7197  /// 
            vnd_mantenimiento_rpa__7198 cto_seguros_rea__7202   gto_seguros_rea__7203   vnd_seguros_rea__7204 gto_com_rel__7212 vnd_gto_com_rel__7213  /// 
            cto_comisiones_exterior_7214 gto_comisiones_exterior_7215 vnd_gto_com_ext_rel__7216œ cto_comisiones_locales_7217 gto_com_no_rel__7218  /// 
            vnd_gto_com_no_rel__7219    cto_otr_gto__7247 gto_otr_gto__7248 vnd_gto_otr_gto__7249)
        
        egen services2 = rowtotal(gto_promocion_publicidad__7173 vnd_promocion_publicidad__7174 gasto_transporte__7176 vnd_transporte__7177 /// 
            gastos_viaje_gasto__7182 vnd_gastos_viaje__7183 gastos_gestion__7185 vnd_gastos_gestion__7186    /// 
            cto_seguros_rea__7202   gto_seguros_rea__7203   vnd_seguros_rea__7204 gto_com_rel__7212 vnd_gto_com_rel__7213  /// 
            vnd_gto_com_ext_rel__7216œ  gto_com_no_rel__7218  /// 
            vnd_gto_com_no_rel__7219    cto_otr_gto__7247 gto_otr_gto__7248 vnd_gto_otr_gto__7249)
        
        }
    
        if `year' == 2015 | `year' == 2016 | `year' == 2017 {
            egen additional_cost_exp = rowtotal(cto_dpr_ace_act_fijos__7064 gto_dpr_ace_act_fijos__7065 vnd_dpr_ace_act_fijos__7066 cto_dpr_no_ace_afj__7067    ///
                gto_dpr_no_ace_afj__7068    vnd_dpr_no_ace_afj__7069    cto_dpr_pin_cto_his__7070   gto_dpr_pin_cto_his__7071   vnd_gto_dpr_pin_cto_his__7072   ///
                cto_dpr_aee_cto_his__7073   gto_dpr_aee_cto_his__7074   vnd_gto_dpr_aee_cto_his__7075   cto_dpr_rev_afj__7076   gto__dpr_rev_afj__7077  vnd__dpr_rev_afj__7078  ///
                cto_dpr_pin_rev__7079   gto_dpr_pin_rev__7080   vnd_gto_dpr_pin_rev__7081   cto_dpr_aee_rev__7082   gto_dpr_aee_rev__7083   vnd_gto_dpr_aee_rev__7084   ///
                cto_otr_gto_dpr_rev__7085   gto_otr_gto_dpr_rev__7086   vnd_gto_otr_gto_dpr_rev__7087   cto_dpr_act_biologicos__7088    gto_dpr_act_biologicos__7089    ///
                vnd_dpr_act_biologicos__7090    cyg_dpr_otr_dpr__7091   gyg_dpr_otr_dpr__7092   vnd_gyg_dpr_otr_dpr__7093   cto_amo_chi_act_int__7094   ///
                gto_amo_chi_act_int__7095   vnd_gto_amo_chi_act_int__7096   cto_amo_aee_chi__7097   gto_amo_aee_chi__7098   vnd_gto_amo_aee_chi__7099   ///
                cto_amo_rev_act_in__7100    gto_amo_rev_act_in__7101    vnd_gto_amo_rev_act_in__7102    cto_amo_aee_rev__7103   gto_amo_aee_rev__7104   ///
                vnd_gto_amo_aee_rev__7105   cto_amo_rev_otr__7106   gto_amo_rev_otr__7107   vnd_gto_amo_rev_otr__7108   cto_amo_otr_amo__7109   gto_amo_otr_amo__7110   ///
                vnd_gto_amo_otr_amo__7111   gto_provision_cta_inb__7113 vnd_provision_cta_inb__7114 gto_provisiones_vtr_inv__7115   cto_provisiones_vtr_inv__7116   ///
                vnd_provisiones_vtr_inv__7117   gto_pnt_det_vanc_vta__7119  vnd_gto_pnt_det_vanc_vta__7120  gto_pnt_det_vact_bio__7122  vnd_gto_pnt_det_vact_bio__7123  ///
                cto_pnt_det_ppe__7124   gto_pnt_det_ppe__7125   vnd_gto_pnt_det_ppe__7126   cto_pnt_det_act_int__7127   gto_pnt_det_act_int__7128   vnd_gto_pnt_det_act_int__7129   ///
                gto_pnt_det_prp_inv__7131   vnd_gto_pnt_det_prp_inv__7132   gto_pnt_det_aee_rec_min__7134   vnd_gto_pnt_det_aee_remin_7135  gto_pnt_det_ain_no_cte__7137    ///
                vnd_gto_pnt_dt_ain_no_cte_7138  cto_pnt_det_val_otr__7139   gto_pnt_det_val_otr__7140   vnd_gto_pnt_det_val_otr__7141   cto_prv_gar__7142   gto_prv_gar__7143   ///
                vnd_gto_prv_gar__7144   cto_prv_desm__7145  gto_prv_desm__7146  vnd_gto_prv_desm__7147  gto_prv_con_one__7149   vnd_gto_prv_con_one__7150   gto_prv_res_neg__7152   ///
                vnd_gto_prv_res_neg__7153   gto_prv_rmb_cli__7155   vnd_gto_prv_rmb_cli__7156   gto_prv_lit__7158   vnd_gto_prv_lit__7159   cto_prv_pas_con_com_neg__7160   ///
                gto_prv_pas_con_com_neg__7161   vnd_gto_prv_pas_con_cm_ng_7162  costo_otras_provisiones__7163   gasto_otras_provisiones__7164   vnd_otras_provisiones__7165 ///
                cto_perdida_vta_act_rel__7166   gto_perdida_vta_act_rel__7167   vnd_perdida_vta_act_rel__7168   cto_perdida_vta_act_nre__7169   gto_perdida_vta_act_nre__7170 ///
                vnd_perdida_vta_act_nre__7171   gasto_ame_local__7251   vnd_ame_local__7252 gasto_ame_exterior__7254    vnd_ame_exterior__7255  gto_arr_mer_loc_nre__7257   ///
                vnd_gto_arr_mer_loc_nre__7258   gto_arr_mer_ext_nre__7260   vnd_gto_arr_mer_ext_nre__7261   gto_int_bancarios_local__7263   vnd_int_bancarios_local__7264   ///
                gto_iba_exterior__7266  vnd_iba_exterior__7267  gto_trn_no_rel_loc__7269    vnd_gto_trn_no_rel_loc__7270    gto_trn_no_rel_ext__7272    vnd_gto_trn_no_rel_ext__7273 ///
                gto_int_ifi_rel_loc__7275   vnd_gto_int_ifi_rel_loc__7276   gto_int_ifi_rel_ext__7278   vnd_gto_int_ifi_rel_ext__7279   gto_int_ifi_no_rel_loc__7281    ///
                vnd_gto_int_ifi_no_rel_lc_7282  gto_int_ifi_no_rel_ext__7284    vnd_gto_com_ext_rel__7216  gto_inp_ter_rel_local__7287 vnd_inp_ter_rel_local__7288 ///
                gto_inp_ter_rel_exterios__7290  vnd_inp_ter_rel_exterior__7291  gto_inp_ter_nre_local__7293 vnd_inp_ter_nre_local__7294 gto_inp_ter_nre_exterior__7296  ///
                vnd_inp_ter_nre_exterior__7297  rev_des_prv_rec_vp__7299    vnd_rev_des_prv_rec_vp__7300    int_imp_dev_trn__7302   vnd_int_imp_dev_trn__7303   otr_gto_fin__7305   ///
                vnd_otr_gto_fin__7306   per_min_aso_ncj_vpp__7308   vnd_per_min_aso_ncj_vpp__7309   gto_otr_no_oper__7311   vnd_gto_otr_no_oper__7312   gto_pnt_act_dis__7314   vnd_gto_pnt_act_dis__7315)
            
            egen services = rowtotal(gto_promocion_publicidad__7173 vnd_promocion_publicidad__7174 gasto_transporte__7176 vnd_transporte__7177 /// 
                gastos_viaje_gasto__7182 vnd_gastos_viaje__7183 gastos_gestion__7185 vnd_gastos_gestion__7186 cto_mantenimiento_rpa__7196  /// 
                gto_mantenimiento_rpa__7197 vnd_mantenimiento_rpa__7198 cto_seguros_rea__7202 gto_seguros_rea__7203 vnd_seguros_rea__7204  ///
                cto_com_rel__7211 gto_com_rel__7212 vnd_gto_com_rel__7213 cto_com_ext_rel__7214 gto_com_ext_rel__7215 vnd_gto_com_ext_rel__7216  /// 
                cto_com_no_rel__7217 gto_com_no_rel__7218 vnd_gto_com_no_rel__7219 cto_com_ext_no_rel__7220 gto_com_ext_no_rel__7221  /// 
                vnd_gto_com_ext_no_rel__7222 cto_opr_sta_rel_loc__7223 gto_opr_sta_rel_loc__7224 vnd_gto_opr_sta_rel_loc__7225 cto_opr_sta_rel_ext__7226  ///
                gto_opr_sta_rel_ext__7227 vnd_gto_opr_sta_rel_ext__7228 cto_opr_sta_n_rel_loc__7229 gto_opr_sta_n_rel_loc__7230 vnd_gto_opr_sta_n_rel_loc_7231  ///
                cto_opr_sta_n_rel_ext__7232 gto_opr_sta_n_rel_ext__7233 vnd_gto_opr_sta_n_rel_ext_7234  cto_ins_org_sim__7235   gto_ins_org_sim__7236  ///
                vnd_gto_ins_org_sim__7237  cto_otr_gto__7247 gto_otr_gto__7248 vnd_gto_otr_gto__7249)
        
            egen services2 = rowtotal(gto_promocion_publicidad__7173 vnd_promocion_publicidad__7174 gasto_transporte__7176 vnd_transporte__7177 /// 
                gastos_viaje_gasto__7182 vnd_gastos_viaje__7183 gastos_gestion__7185 vnd_gastos_gestion__7186   /// 
                cto_seguros_rea__7202 gto_seguros_rea__7203 vnd_seguros_rea__7204  ///
                cto_com_rel__7211 gto_com_rel__7212 vnd_gto_com_rel__7213 cto_com_ext_rel__7214 gto_com_ext_rel__7215 vnd_gto_com_ext_rel__7216  /// 
                cto_com_no_rel__7217 gto_com_no_rel__7218 vnd_gto_com_no_rel__7219 cto_com_ext_no_rel__7220 gto_com_ext_no_rel__7221  /// 
                vnd_gto_com_ext_no_rel__7222 cto_opr_sta_rel_loc__7223 gto_opr_sta_rel_loc__7224 vnd_gto_opr_sta_rel_loc__7225 cto_opr_sta_rel_ext__7226  ///
                gto_opr_sta_rel_ext__7227 vnd_gto_opr_sta_rel_ext__7228 cto_opr_sta_n_rel_loc__7229 gto_opr_sta_n_rel_loc__7230 vnd_gto_opr_sta_n_rel_loc_7231  ///
                cto_opr_sta_n_rel_ext__7232 gto_opr_sta_n_rel_ext__7233 vnd_gto_opr_sta_n_rel_ext_7234  cto_ins_org_sim__7235   gto_ins_org_sim__7236  ///
                vnd_gto_ins_org_sim__7237  cto_otr_gto__7247 gto_otr_gto__7248 vnd_gto_otr_gto__7249)
        }
    
    *=============================================================
    * Capital
    *=============================================================

    gen capital           = tot_activo_no_corriente__449
    gen socialCapital     = capital_suscrito_asignado_601
    gen totCurrentCapital = total_activo_corriente_361
    
    if `year'==2015 | `year'==2016 | `year'==2017{  
        gen cumulDeprec = dep_acu_cto_his_384 + dep_acu_ajt_acu_385
    }
    
    if `year'==2014{    
        gen cumulDeprec =tot_dep_acu_rex_rev_act__469
    }

    *=============================================================
    * Sales and others
    *=============================================================
    gen totalRevenue = total_ingresos__6999
    gen totalSales = vln_eaf_tdc__6001 + pre_loc_ser_grav_tar_12__6005+ vln_eaf_tce__6003 + pre_loc_ser_grav_tar_0__6007 ///
                   + exportaciones_netas__6009 +expor_netas_servi__6011 + ing_pre_ser_con__6013 + ing_com_sim__6015 + ///
                   ing_por_arr_ope__6017 
                        
            
    gen shortLiabilities = tot_pasivos_corrientes__550
    gen longLiabilities  = total_pasivos_largo_plazo_589
    gen equity = total_patrimonio_neto__698
    gen totalDebt  = total_pasivos__599
    gen totalAsset = total_activo__499
        
    keep `keep_varlist'
    save_data "../temp/balance_`year'.dta", key(expediente) replace 
end

* Execute
main
