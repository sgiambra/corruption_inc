set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/" 

program main
    compute_dm_prices
    standardize_ids
end

program compute_dm_prices
    forvalues i = 1(1)6{
        import delimited "$data_dir/proc_contracts/CATALOGO ELECTRONICO_`i'.csv", ///
            stringcols(2 5 13 14) clear encoding("utf-8")

        drop if estadodelacompra == "SIN EFECTO"

        rename (rucproveedor rucentidad ordendecompra provincia categorían1 categorían2 ///
            categorían3 nombre_producto cpcorden) (firm_id agency_id order_id province  ///
            category1 category2 category3 product_name cpc)

        // Compute unit price
        replace valoradjudicado = subinstr(valoradjudicado, ".","",.)
        replace valoradjudicado = subinstr(valoradjudicado, ",",".",.)
        destring valoradjudicado, g(total_value)
        drop valoradjudicado

        replace cantidadproducto = subinstr(cantidadproducto, ".","",.)
        replace cantidadproducto = subinstr(cantidadproducto, ",",".",.)
        destring cantidadproducto, g(total_quantity)
        drop cantidadproducto

        gen price_per_unit = total_value / total_quantity
        drop if price_per_unit == .

        // Obtain Year
        gen year = substr(order_id, 4, 4)
        destring year, force replace
        
        gen temp_id = _n
        save_data "../temp/electronic_catalog_`i'", replace key(temp_id)
    }
    // Appending All files
    use "../temp/electronic_catalog_1", clear
    gen catalog_num = 1
    
    forvalues i = 2(1)6{
        append using "../temp/electronic_catalog_`i'", force
        replace catalog_num = `i' if catalog_num == .
    }
    drop temp_id

    gen rownum = _n
    * NB: 3 duplicates in term of (order_id firm_id product_name)
    save_data "../temp/electronic_catalog", replace key(rownum)
end

program standardize_ids
    use rucFirm using "$data_dir/firms_ownership/directory", replace
    rename rucFirm firm_id
    drop if firm_id == "."
    * Almost 200k rucs become of length 10
    replace firm_id = substr(firm_id, 1, 10) if ///
        substr(firm_id, -3, 3) == "001" & length(firm_id) == 13

    save_data "../temp/list_rucs_directory", key(firm_id) replace

    use "../temp/electronic_catalog", clear
    replace firm_id = "0" + firm_id if length(firm_id) == 9 | length(firm_id) == 12
    replace firm_id = substr(firm_id, 1, 10) if substr(firm_id, -3, 3) == "001" & ///
        (length(firm_id) == 13 | length(firm_id) == 14)

    replace firm_id = "." if firm_id == "" | regexm(firm_id , "^[0]+$")

    merge m:1 firm_id using "../temp/list_rucs_directory.dta", ///
        keep(1 3) assert(1 2 3)
    gen individual_provider = _merge == 1
    drop _merge
    rename firm_id provider_id

    save_data "$data_dir/proc_contracts/electronic_catalog", replace key(rownum)
end

* Execute
main
