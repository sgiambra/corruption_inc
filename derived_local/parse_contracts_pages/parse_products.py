import os, re
import pandas as pd
from bs4 import BeautifulSoup

path = '/Users/sgiambra/Desktop/products/'
parsed_path = '/Users/sgiambra/Desktop/products_parsed/'
os.chdir(path)

index = 0
for file in os.listdir(path):
	index += 1
	if not index%100:
		print("Parsing file no: "+str(index))
	else:
		pass
		
	id_col = file.split('&')[0]

	html = open(file, encoding="utf8")
	try:
		soup = BeautifulSoup(html, 'html.parser')
		
		tables = soup.find_all("table")
		
		headers = []
		descriptions = []
		
		if len(tables)>1:
			headRow = soup.find('thead')    
			headers = [th.get_text() for th in headRow.find_all('th')]
			table = tables[1]
			check = str(table)
			
			if 'style="background-color: #A4A4A4"' in check:
				table0 = str(tables[0])
				table1 = str(tables[1])
				soup = str(soup)
				
				soup = soup.replace(table0, '')
				soup = soup.replace(table1, '')
				
				table = BeautifulSoup(soup)
			
			for row in table.find_all('tr'):
				if '<b>TOTAL' in str(row):
					continue
				elif '<b>SUB.TOTAL' in str(row):
					continue
				elif '<b>Lote' in str(row):
					continue
				else:
					description = [td.get_text() for td in row.find_all('td')]
					descriptions.append(description)
		
		if (descriptions == [[]]) or (descriptions == []):
			continue
		else:
			output_desc = pd.DataFrame(descriptions)
			output_desc.columns = headers
			output_desc["ID contract"] = id_col
			
			output_desc.to_json(parsed_path + str(id_col))
	
	except:
		pass