# Task: 
# This code uses Summary page to extract ID and name of winners of contracts and participants
# Date: Jan 28, 2019
from lxml import html, etree
import requests, bs4, re
import os, csv, glob
import urllib
import pandas as pd
import numpy
import sys
import time

directory = "/Users/Samuele/Desktop/Summary/"
parsed = "/Users/Samuele/Desktop/summary_parsed/temp/"

os.chdir(directory)
extensions = ('.html')

index = 0
for file in os.listdir(directory):
    ext = os.path.splitext(file)[-1].lower()
    id_col=file.split('&')[0]
    index += 1
    t = time.time()
    try:
        if not index%100:
            print("Parsing file no: "+str(index))
        else:
            pass
        id_col=file.split('&')[0]
        page = open(file, encoding="utf8")               # Import webpage information output
        soup = bs4.BeautifulSoup(page, "lxml")      # Transform to readble variable
        page.close()
        # Extract Tables: we need tables 0, 1, and 2
        tables = soup.find_all("table")
        # From Table 0, we need 2-12 rows
        headers_t0 = []
        descriptions_t0 = []
        i = 0
        for row in tables[0].find_all('tr'):
            if (i>0) & (i<12):
                header_t0 = [td.get_text().replace("\xa0", "") for td in row.find_all('span',"textoAzul12")]
                if len(header_t0) == 0:
                    header_t0 =  [str(i)]
                headers_t0.append(header_t0)
                description_t0 = [td.get_text().replace("\xa0", "") for td in [row.find_all('td',"cuadroIngresoBA")[-1]]]
                descriptions_t0.append(description_t0)
            i += 1
        output_desc_t0 = pd.DataFrame(descriptions_t0).T
        output_desc_t0.columns = [item for sublist in headers_t0 for item in sublist]
        output_desc_t0["ID contract"] = id_col
        output_desc_t0.to_json(path_or_buf=str(parsed)+"output_desc_t0"+str(id_col))
        del output_desc_t0
        # From Table 1, extract all
        headers_t1 = []
        descriptions_t1 = []
        headers_t1 = [td.get_text().replace("\xa0", "") for td in tables[1].find_all('span',"textoAzul12")]
        descriptions_t1 = [td.get_text().replace("\xa0", "") for td in tables[1].find_all('td',"cuadroIngresoBA")]
        if len(descriptions_t1)==0:
            descriptions_t1 = [['EMPTY'] for _ in range(5)]
        # Dealing with possible multiple rows
        subdescriptions_t1 =  numpy.array_split(numpy.array(descriptions_t1),len(descriptions_t1)/len(headers_t1))
        j = 0
        for sublist in subdescriptions_t1:
            j += 1
            if j == 1:
                output_desc_t1 = pd.DataFrame(sublist).T
                output_desc_t1.columns = headers_t1
                output_desc_t1["ID contract"] = id_col
            else:
                output_desc_new_row_t1 = pd.DataFrame(sublist).T
                output_desc_new_row_t1.columns = headers_t1
                output_desc_new_row_t1["ID contract"] = id_col
                output_desc_t1 = pd.concat([output_desc_t1, output_desc_new_row_t1], axis=0, ignore_index=True)
        output_desc_t1.to_json(path_or_buf=str(parsed)+"output_desc_t1"+str(id_col))
        del output_desc_t1
        # From Table 2, extract all
        headers_t2 = []
        descriptions_t2 = []
        headers_t2 = [td.get_text().replace("\xa0", "") for td in tables[2].find_all('span',"textoAzul10")]
        headers_t2.remove('Anticipo')
        headers_t2.remove('Porcentaje(%)')
        headers_t2.insert(7,'Porcentaje(%)')
        headers_t2.remove('Valor($)')
        headers_t2.insert(8,'Valor($)')
        descriptions_t2 = [td.get_text().replace("\xa0", "") for td in tables[2].find_all('td',"cuadroIngresoBA")]
        if len(descriptions_t2)==0:
            descriptions_t2 = [['EMPTY'] for _ in range(16)]
        # Dealing with possible multiple rows
        subdescriptions_t2 =  numpy.array_split(numpy.array(descriptions_t2),len(descriptions_t2)/len(headers_t2))
        j = 0
        for sublist in subdescriptions_t2:
            j += 1
            if j == 1:
                output_desc_t2 = pd.DataFrame(sublist).T
                output_desc_t2.columns = headers_t2
                output_desc_t2["ID contract"] = id_col
            else:
                output_desc_new_row_t2 = pd.DataFrame(sublist).T
                output_desc_new_row_t2.columns = headers_t2
                output_desc_new_row_t2["ID contract"] = id_col
                output_desc_t2 = pd.concat([output_desc_t2, output_desc_new_row_t2], axis=0, ignore_index=True)
        output_desc_t2.to_json(path_or_buf=str(parsed)+"output_desc_t2"+str(id_col))
        del output_desc_t2
    except:
        pass
    
# Appending all json files to one csv
main_output_folder = "/Users/Samuele/Desktop/summary_parsed/output/"

# List of files - T0
output = "/Users/Samuele/Desktop/summary_parsed/output/t0/"

list_t0 = []
for file in os.listdir(parsed):
    if file.find("output_desc_t0") == 0:
        list_t0.append(file)
    else:
        pass

n = 20000
final_list_t0 = [list_t0[i * n:(i + 1) * n] for i in range((len(list_t0) + n - 1) // n )]  

part = 0
index = 0
for sublist in final_list_t0:
    # T0
    os.chdir(parsed)
    part += 1
    for file in sublist:
        index += 1
        if not index%1000:
            print("Parsing file no: "+str(index))
        else:
            pass
        output_df_new_row = pd.read_json(file)
        if (index-1+n)%n == 0:
            output_df = output_df_new_row
        else:
            output_df = pd.concat([output_df, output_df_new_row], axis=0, ignore_index=True)
        
    os.chdir(output)
    output_df.to_csv('summary_t0_'+str(part)+'.csv', encoding='utf-8', index=False, header=True)
    

os.chdir(output)
results = pd.DataFrame([])
 
for counter, file in enumerate(glob.glob("summary_t0_*")):
    namedf = pd.read_csv(file, skiprows=0)
    results = results.append(namedf)

results.to_csv(str(main_output_folder)+'summary_t0.csv')
del results   


# List of files - T1
output = "/Users/Samuele/Desktop/summary_parsed/output/t1/"

list_t1 = []
for file in os.listdir(parsed):
    if file.find("output_desc_t1") == 0:
        list_t1.append(file)
    else:
        pass

n = 20000
final_list_t1 = [list_t1[i * n:(i + 1) * n] for i in range((len(list_t1) + n - 1) // n )]  

part = 0
index = 0
for sublist in final_list_t1:
    # T1
    os.chdir(parsed)
    part += 1
    for file in sublist:
        index += 1
        if not index%1000:
            print("Parsing file no: "+str(index))
        else:
            pass
        output_df_new_row = pd.read_json(file)
        if (index-1+n)%n == 0:
            output_df = output_df_new_row
        else:
            output_df = pd.concat([output_df, output_df_new_row], axis=0, ignore_index=True)
        
    os.chdir(output)
    output_df.to_csv('summary_t1_'+str(part)+'.csv', encoding='utf-8', index=False, header=True)


os.chdir(output)
results = pd.DataFrame([])
 
for counter, file in enumerate(glob.glob("summary_t1_*")):
    namedf = pd.read_csv(file, skiprows=0)
    results = results.append(namedf)

results.to_csv(str(main_output_folder)+'summary_t1.csv')
del results   

# List of files - T2

output = "/Users/Samuele/Desktop/summary_parsed/output/t2/"

list_t2 = []
for file in os.listdir(parsed):
    if file.find("output_desc_t2") == 0:
        list_t2.append(file)
    else:
        pass

n = 20000
final_list_t2 = [list_t2[i * n:(i + 1) * n] for i in range((len(list_t2) + n - 1) // n )]  

part = 0
index = 0
for sublist in final_list_t2:
    # T2
    os.chdir(parsed)
    part += 1
    for file in sublist:
        index += 1
        if not index%1000:
            print("Parsing file no: "+str(index))
        else:
            pass
        output_df_new_row = pd.read_json(file)
        if (index-1+n)%n == 0:
            output_df = output_df_new_row
        else:
            output_df = pd.concat([output_df, output_df_new_row], axis=0, ignore_index=True)
        
    os.chdir(output)
    output_df.to_csv('summary_t2_'+str(part)+'.csv', encoding='utf-8', index=False, header=True)

os.chdir(output)
results = pd.DataFrame([])
 
for counter, file in enumerate(glob.glob("summary_t2_*")):
    namedf = pd.read_csv(file, skiprows=0)
    results = results.append(namedf)

results.to_csv(str(main_output_folder)+'summary_t2.csv')
del results   

