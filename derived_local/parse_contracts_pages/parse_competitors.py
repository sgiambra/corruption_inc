# Task: 
# This code uses Summary page to extract ID and name of winners of contracts and participants
# Date: Jan 28, 2019
from lxml import html, etree
import requests, bs4, re
import os, csv, glob
import urllib
import pandas as pd
import numpy
import sys

directory = "/Users/Samuele/Downloads/Results/"
parsed = "/Users/Samuele/Downloads/Results_parsed/temp/"

os.chdir(directory)
extensions = ('.html')

skipped = 0
processed = 0
auctions = 0

index = 0
for file in os.listdir(directory):
    id_col=file.split('&')[0]
    index += 1
    print(index)
    try:
        # Pull files
        page = open(file, encoding="utf8") 
        soup = str(bs4.BeautifulSoup(page, "lxml")).replace("<br/>"," ")    # Transform to readble variable
        soup = bs4.BeautifulSoup(soup, "lxml")
        page.close()
        if not "Subasta Inversa" in soup.get_text(separator=' '):
            tables = soup.find_all("table")
            # Find competitors table
            all_headers = []
            
            for table in range(0,len(tables)):
                try:
                    all_headers.append([td.get_text() for td in tables[table].find('tr').find_all('td')])
                except:
                    pass
            competitors_header = [s for s in all_headers if 'Resumen' in str(s)]
            competitors_index = all_headers.index(competitors_header[0]) + 1
            
            # Extracting Table of Participating Firms
            i = 0
            descriptions_firms = []
            headers_firms = [td.get_text() for td in tables[competitors_index].find('tr').find_all('td')]
            num_columns = len(tables[competitors_index].find_all('td'))
            info_competitors = tables[competitors_index].find_all('tr')
            for row in info_competitors:
                i += 1
                if i > 1:
                    descriptions_firms.append([td.get_text() for td in row.find_all('td')])
                else:
                    continue              
            output_descriptions_firms = pd.DataFrame(descriptions_firms)
            output_descriptions_firms.columns = headers_firms
            output_descriptions_firms["ID contract"] = id_col
            output_descriptions_firms.to_json(path_or_buf=str(parsed)+"_competitors_"+str(id_col))
            
            processed += 1
        else:
            auctions += 1
            pass
    except:
        skipped += 1
        pass
    
print("parsed", processed)
print("auctions", auctions)
print("skipped", skipped)

# List of files - Competitors
output = "/Users/Samuele/Downloads/Results_parsed/output/"

list_t1 = []
for file in os.listdir(parsed):
    if file.find("_competitors_") == 0:
        list_t1.append(file)
    else:
        pass

n = 20000
final_list_t1 = [list_t1[i * n:(i + 1) * n] for i in range((len(list_t1) + n - 1) // n )]  

part = 0
index = 0
for sublist in final_list_t1:
    # T1
    os.chdir(parsed)
    part += 1
    for file in sublist:
        index += 1
        if not index%1000:
            print("Parsing file no: "+str(index))
        else:
            pass
        output_df_new_row = pd.read_json(file)
        if (index-1+n)%n == 0:
            output_df = output_df_new_row
        else:
            output_df = pd.concat([output_df, output_df_new_row], axis=0, ignore_index=True, sort= True)
        
    os.chdir(output)
    output_df.to_csv('competitors_'+str(part)+'.csv', encoding='utf-8', index=False, header=True)


os.chdir(output)
results = pd.DataFrame([])
 
for counter, file in enumerate(glob.glob("competitors_*")):
    namedf = pd.read_csv(file, skiprows=0)
    results = results.append(namedf)


results.to_csv(str(output) + 'all_competitors.csv')
del results
