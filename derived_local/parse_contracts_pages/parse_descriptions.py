import os, re
import pandas as pd
from bs4 import BeautifulSoup

path = '/Users/sgiambra/Documents/corruption_inc/contracts/description/'
parsed_path = '/Users/sgiambra/Documents/corruption_inc/contracts/description_parsed/'
os.chdir(path)

index = 0
for file in os.listdir(path):
    index += 1
    if not index%100:
            print("Parsing file no: "+str(index))
    else:
        pass
    
    id_col = file.split('&')[0]
    
    html = open(file, encoding="utf8")
    soup = BeautifulSoup(html, 'html.parser')    # Transform to readble variable
    
    tables = soup.find_all("table")
    
    headers = []
    descriptions = []
    
    if len(tables)>1:
        table = str(tables[1])
        
        if len(tables) > 2:    
            tableToSkip = str(tables[2])    
            table = table.replace(tableToSkip, '')
        if len(tables) == 4:
            nextTableToSkip = str(tables[3])
            table = table.replace(nextTableToSkip, '')
        
        table = BeautifulSoup(table)
        rowsToSkip = table.find_all('tr', style=re.compile('display: none'))
            
        for row in table.find_all('tr'):
            if row in rowsToSkip:
                continue
            else:
                header = [th.get_text() for th in row.find_all('th')]
                headers.append(header)
                description = [td.get_text() for td in row.find_all('td')]
                descriptions.append(description)
    
    output_desc = pd.DataFrame(descriptions).T
    output_desc.columns = [item for sublist in headers for item in sublist]
    output_desc["ID contract"] = id_col
    
    output_desc.to_json(parsed_path + str(id_col))