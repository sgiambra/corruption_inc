import requests
import csv
import os
import time
from lxml import html
import requests
import urllib
import sys
import errno
import argparse
from bs4 import BeautifulSoup
import io
import boto3
import json

# Suppress warnings
requests.packages.urllib3.disable_warnings()

parser = argparse.ArgumentParser(description='This program scrapes from http://[AGENCY NAME].gob.ec/transparencia')
#parser.add_argument("-agency", help="The agency to parse (from [agency].gob.ec). Example: -agency educacion", type=str, required=True)
parser.add_argument("-outputdir", help="The directory to put pdfs and log file. Example: -outputdir ./data", type=str, required=True)
parser.add_argument("-sleep", help="How long to wait between requests", type=float, default=1.0)
args = parser.parse_args()

websites = {"Start_Page" : "http://www.compraspublicas.gob.ec/ProcesoContratacion/compras/PC/informacionProcesoContratacion2.cpe?idSoliCompra=",
            "Results" : "http://www.compraspublicas.gob.ec/ProcesoContratacion/compras/PC/resumenAdjudicacion.cpe?solicitud=",
            "Summary" :	"http://www.compraspublicas.gob.ec/ProcesoContratacion/compras/EC/resumenContractual1.cpe?idSoliCompra=",
            "Award" : "http://www.compraspublicas.gob.ec/ProcesoContratacion/compras/PC/resumenAdjudicacionConsultoria.cpe?solicitud=",
            "Evaluation_1" : "http://www.compraspublicas.gob.ec/ProcesoContratacion/compras/PC/resumenCalificacionContratacionDirecta.cpe?solicitud=",
            "Evaluation_2" : "http://www.compraspublicas.gob.ec/ProcesoContratacion/compras/PC/resumenCalificacionFinalConsultoria.cpe?solicitud="
            "Publicacion" : "https://www.compraspublicas.gob.ec/ProcesoContratacion/compras/PC/resumenAdjudicacionPublicacion.cpe?solicitud="}

start_page_format = {"Descripcion" : "http://www.compraspublicas.gob.ec/ProcesoContratacion/compras/ProcesoContratacion/tab.php?tab=1&id=",
                     "Fechas" : "http://www.compraspublicas.gob.ec/ProcesoContratacion/compras/ProcesoContratacion/tab.php?tab=2&id=",
                     "Productos" : "http://www.compraspublicas.gob.ec/ProcesoContratacion/compras/ProcesoContratacion/tab.php?tab=3&id=",
                     "Parametros" : "http://www.compraspublicas.gob.ec/ProcesoContratacion/compras/ProcesoContratacion/tab.php?tab=4&id=",
                     "Archivos" : "http://www.compraspublicas.gob.ec/ProcesoContratacion/compras/ProcesoContratacion/tab.php?tab=5&id=",
                     "Documentos_Anexos" : "http://www.compraspublicas.gob.ec/ProcesoContratacion/compras/ProcesoContratacion/tab.php?tab=6&id="}

client = boto3.client('lambda', region_name='us-east-1')

datadir = args.outputdir
#make directory for documents
if not os.path.exists(datadir):
    try:
        os.makedirs(datadir)
    except OSError as exc: # Guard against race condition
        if exc.errno != errno.EEXIST:
            raise

def getPageContent(getUrl, saveUrl):
    payload = {"GET_URL" : getUrl, "SAVE_URL" : saveUrl}
    try:
        resp = client.invoke(FunctionName='getPage',
                         InvocationType='RequestResponse',
                         Payload=json.dumps(payload))
        return resp['Payload'].read()
    except Exception as e:
        print e
        return False

def getAllStartPageTabs(id):
    for k, v in start_page_format.items():
        htmlFile = os.path.join(args.outputdir, "Start_Page", id + "&Start_Page&" + k + ".html")
        if not getPageContent(v + id, htmlFile):
            return False
    return True

def getAllPagesForID(id):
    for k, v in websites.items():
        if k == "Start_Page":
            if not getAllStartPageTabs(id):
                return False
        else:
            htmlFile = os.path.join(args.outputdir, k, id + "&" + k + ".html")
            if not getPageContent(v + id, htmlFile):
                return False
    return True

i = 0
curr_time = time.strftime("%Y_%m_%d_%H_%M_%S", time.gmtime())
with open('procurement_all.csv', 'r') as csvfile:
    with open(os.path.join(args.outputdir, "log_" + curr_time + ".csv"), 'wb') as logfile:
        contractreader = csv.reader(csvfile)
        logwriter = csv.writer(logfile, delimiter=',')
        for row in contractreader:
            id = row[-1]
            if getAllPagesForID(id):
                logwriter.writerow([id, "SUCCEEDED"])
                print i, id, "SUCCEEDED"
            else:
                logwriter.writerow([id, "FAILED"])
                print i, id, "FAILED"
            time.sleep(args.sleep)
            i += 1
