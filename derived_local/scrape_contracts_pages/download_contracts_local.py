# Script that can be used to download all contract pages on local machine

# Check paths, input file with links to download, and row corresponding to id
# For Python 2.7 remove encoding utf-8

import requests
import os, csv

# Suppress warnings
requests.packages.urllib3.disable_warnings()

websites = {"Results" : "http://www.compraspublicas.gob.ec/ProcesoContratacion/compras/PC/resumenAdjudicacion.cpe?solicitud=",
            "Summary" :	"http://www.compraspublicas.gob.ec/ProcesoContratacion/compras/EC/resumenContractual1.cpe?idSoliCompra="}

path = '/Users/sgiambra/Documents/scrape_contracts'
os.chdir(path)

with open('./procurement_links_2017.csv', 'r', encoding='utf-8') as csvfile:
    contractreader = csv.reader(csvfile)

    for page, toScrape in websites.items():
        index = 0
        for row in contractreader:
            index += 1
            id = row[-1]
            htmlFile = './output/' + page + '/' + id + '&' + page + ".html"
            with requests.Session() as session:
                
                try:
                    print(index, id)
                    response = requests.get(toScrape + id, verify=False, timeout=5.0)
                    open(htmlFile, 'wb').write(response.content)
                except Exception as e:
                    print(e)
                    