from bs4 import BeautifulSoup
import csv
import os
import sys
import numpy as np
import pandas as pd

headers = []

os.chdir('/Users/felipe/Dropbox (Brown)/Research/Corruption/web_scrapping/get_links/output/auction')

LINK_PREFIX = "informacionProcesoContratacion2.cpe?idSoliCompra="

with open("auction_links_2017.csv", "w",encoding="utf-8") as f:
    wr = csv.writer(f)
    for filename in os.listdir(os.getcwd()):
        if not filename.endswith("txt"):
            continue
        with open(filename, 'r',encoding='utf-8') as htmlFile:
            html = htmlFile.read()
            soup = BeautifulSoup(html, "lxml")
            for table in soup.select("table"):
                # python3 just use th.text
                #headers = [th.text.rstrip() for th in table.select("tr th")]
                #print(headers)
                for row in table.select("tr + tr"):
                    l = [td.text.rstrip() for td in row.find_all("td")]
                    if len(l) >= 7:
                        l = l[:7]
                    for e in row.find_all("td"):
                        for link in e.select('a'):
                            l.append(link.get('href')[len(LINK_PREFIX):-1])
                    if l[2] != "Siguiente": 
                        wr.writerow(l)
