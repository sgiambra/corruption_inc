

# Programmes needed to scrape the webpage
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.chrome.options import Options
from time import sleep
from lxml import html, etree

#IMport driver
driver = webdriver.Chrome('/Users/felipe/Dropbox (Brown)/Research/Corruption/web_scrapping/chromedriver')  # Directory where
driver.implicitly_wait(12)

# Load main page

# For SICM2016
 #   driver.get("https://sicm.compraspublicas.gob.ec/ProcesoContratacion/compras/PC/buscarProceso.cpe?sg=1#")

# This corresponds to regular purchases  
driver.get("https://www.compraspublicas.gob.ec/ProcesoContratacion/compras/PC/buscarProceso.cpe?")
    # Note: Captcha has to be introduced manually in chrome driver once for performing the search.


# This corresponds to special purchases
 #   driver.get("https://www.compraspublicas.gob.ec/ProcesoContratacion/compras/PC/buscarProcesoRE.cpe?op=R#")
    
sleep(30)
# Search for all products

startDate="2017-07-01"
endDate="2018-01-01"

# Get file name
# Change directory accordingly
folder="/Users/felipe/Dropbox (Brown)/Research/Corruption/web_scrapping/get_links/output/"
namefile=str(folder)+startDate+"_"+endDate+"_auction.txt"
text_file = open(namefile, "w", encoding='utf-8')


# loop over results: this will create a text file with the html information.  This code has to be run with TExttoCsv to recover the information in proper format
contracts=0
for i in range(0, 3000):
    contracts=contracts+20
    print("Contracts:"+str(contracts))
    sleep(0.1)
    temp = driver.page_source
    names_info=BeautifulSoup(temp, "lxml")
    divfile=names_info.find_all('div',{'id':'divProcesos'})
    divfile=str(divfile[0])
    # save to .txt file
    text_file = open(namefile, "a", encoding='utf-8')
    text_file.write(divfile)
    text_file.close()
    try:
         driver.execute_script("presentarProcesos("+str(contracts)+")")
    except:
        pass
    
    #.click()
