# Before running in EC2 need to install webdriver and Chrome
# https://medium.com/@praneeth.jm/running-chromedriver-and-selenium-in-python-on-an-aws-ec2-instance-2fb4ad633bb5

# cd/tmp/
# wget https://chromedriver.storage.googleapis.com/2.37/chromedriver_linux64.zip
# unzip chromedriver_linux64.zip
# sudo mv chromedriver /usr/bin/chromedriver
# chromedriver --version

# curl https://intoli.com/install-google-chrome.sh | bash
# sudo mv /usr/bin/google-chrome-stable /usr/bin/google-chrome
# google-chrome --version && which google-chrome

# Also need to update urllib3 and install pandas bs4 and selenium
# sudo pip install --upgrade --ignore-installed urllib3
# sudo pip install bs4
# sudo pip install pandas
# sudo pip install selenium

# import libraries
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.chrome.options import Options
import numpy as np
import pandas as pd
import os
import csv
import requests

URL = "https://www.compraspublicas.gob.ec/ProcesoContratacion/compras/PC/ResumenCalificacionesSubasta.cpe?id={0}"

# Webdriver directory
options = Options()
options.headless = True
driver = webdriver.Chrome(options=options)
driver.implicitly_wait(12)

columns=['firm_name','habilitado','national','reason','rup']

with open('auction_links.csv', 'r')  as csvfile:
    rows = csv.reader(csvfile)
    i = 0
    for row in rows:
        i += 1
        if i>50000: 
            print("Parsing Auction: " +str(i-1))
            link = row[1]
            process_id = row[0]
            response = requests.get(URL.format(link))
            html = response.text
            if "tituloRojo" not in html:
                driver.get(URL.format(link))
                contestants_auction = []
                try:
                    results = driver.find_elements_by_xpath("//*[@id='divListaInv']//*[@class='textoAzul12sinJust']")
                    for result in results:
                        contestants_auction.append(result.text)
                    data = np.array(contestants_auction)
                    num_rows = int(np.prod(data.shape)/5)
                    data = data.reshape((num_rows,5))
                    df = pd.DataFrame(data)
                    df = pd.DataFrame(data,columns=columns)
                    df["contract_link"] = link
                    df["contract_code"] = process_id
                    df.to_json(path_or_buf= "output/competitors_" + str(link))
                    print("Parsed")
                except:
                    continue
            else:
                print("Skipped")
