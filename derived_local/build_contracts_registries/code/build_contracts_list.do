clear all
set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/proc_contracts/"

program main
    build_proc_list
    build_auction_list
    build_public_list
end

program build_proc_list
    * Compile list of procurement contracts 2008-2017
    foreach stub in "" "_2017" {
        import delimited "$data_dir/scraped/links/procurement_links`stub'.csv", ///
            delimiter(comma) bindquote(strict) stripquote(yes) varnames(nonames) encoding(utf8) clear

        rename (v1 v2 v3 v4 v5 v6 v7 v8) (contract_code agency_name contract_object ///
            contract_state province_city contract_budget contract_date_t contract_link)

        drop if contract_link == ""
        duplicates drop

        save_data "../temp/procurement_links`stub'", replace key(contract_link)
    }
    use "../temp/procurement_links.dta", clear
    append using "../temp/procurement_links_2017.dta"

    * 1,718/2 contracts have a duplicate
    duplicates tag contract_code agency_name contract_object contract_state ///
        province_city contract_budget contract_date_t, gen(dup)

    replace contract_budget = subinstr(contract_budget, "$", "", .)
    replace contract_budget = subinstr(contract_budget, ",", "", .)
    destring contract_budget, replace force

    gen contract_date = date(substr(contract_date_t, 1, 11), "YMD")
    format contract_date %td
    drop contract_date_t

    foreach var in agency_name contract_state province_city {
        replace `var' = subinstr(`var', char(10), "", .) 
    }

    split province_city, g(place) p("/") limit(2)
    rename place1 province
    rename place2 city
    drop province_city

    save_data "$data_dir/scraped/clean/procurement_links_2008_2017", replace key(contract_link)

    count
    * Total of 1,136,618 distinct contracts

    * check that number of contracts consistent over time
    gen id = _n
    gen contract_month = mofd(contract_date)
    format contract_month %tm
    collapse (count) nbr_contracts = id, by(contract_month)
    tsset contract_month

    tsline nbr_contracts, ytitle("Number contracts") xtitle("")
    graph export "../output/contracts_number_ts.png", replace
end

program build_auction_list
    * Auctions are subset of procurement contracts
    import delimited "$data_dir/scraped/links/auction_links.csv", ///
        delimiter(comma) bindquote(strict) varnames(1) encoding(utf8) clear

    * Check that contract_link matches with those in master list
    merge 1:1 contract_link using "$data_dir/scraped/clean/procurement_links_2008_2017", ///
        nogen assert(2 3) keep(3) keepusing(contract_link)

    save_data "../temp/auction", replace key(contract_link)

    * Need to impute contract link matching procurement dataset for 2017 auctions
    import delimited "../../scrape_contract_links_2017/output/auction/auction_links_2017.csv", ///
        delimiter(comma) bindquote(strict) stripquote(yes) varnames(nonames) encoding(utf8) clear

    rename (v1 v2 v3 v4 v5 v6 v7 v8) (contract_code agency_name contract_object ///
            contract_state province_city contract_budget contract_date_t contract_link_auction)

    * 161 auctions are not matched to any contract; of these, 88 are cancelado or desierta
    merge 1:1 contract_code agency_name contract_object contract_state province_city contract_budget ///
        contract_date_t using "../temp/procurement_links_2017", nogen assert(1 2 3) keep(3)
    
    keep contract_code contract_link contract_link_auction

    save_data "../temp/auction_2017", replace key(contract_link_auction)

    use "../temp/auction", clear
    append using "../temp/auction_2017"

    save_data "$data_dir/scraped/clean/auction_list", replace key(contract_link)
end

program build_public_list
    * Compile list of publicaciones
    import delimited "$data_dir/scraped/links/publicacion_links.csv", ///
        delimiter(comma) bindquote(strict) varnames(1) encoding(utf8) clear
    drop v1
    rename (id_contract link_contract) (contract_code contract_link)

    * Check that contract_link matches with those in master list
    merge 1:1 contract_link using "$data_dir/scraped/clean/procurement_links_2008_2017", ///
        nogen assert(2 3) keep(3) keepusing(contract_link)

    save_data "../temp/publicacion.dta", replace key(contract_link)

    import delimited "../../scrape_contract_links_2017/output/publicacion/publicacion_links_2017.csv", ///
        delimiter(comma) bindquote(strict) encoding(utf8) clear
    rename (v1 v2 v3 v4 v5 v6 v7 v8) (contract_code agency_name contract_object ///
        contract_state province_city contract_budget contract_date_t contract_link_publicacion)

    * 142 publicaciones are not matched to any contract
    merge 1:1 contract_code agency_name contract_object contract_state province_city contract_budget ///
        contract_date_t using "../temp/procurement_links_2017", nogen assert(1 2 3) keep(3)

    keep contract_code contract_link contract_link_publicacion

    save_data "../temp/publicacion_2017", replace key(contract_link_publicacion)

    * links/publicacion_links_2017_v2.csv appears to have exact same observations but with error in 74 links

    use "../temp/publicacion", clear
    append using "../temp/publicacion_2017"

    save_data "$data_dir/scraped/clean/publicacion_list", replace key(contract_link)
end

* Execute
main
