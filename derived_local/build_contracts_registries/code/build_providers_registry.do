/* 
Notes: 
    The output table contains the list of firms winning or competing for procurement contracts
    The variable provider_id lists all possible IDs with which a given firm appears in the data
    Merges to contract data shoul occur using both firm_name and provider_id
    The variable internal_firm_id should only be used for analysis involving contract data alone

IMPORTANT: need to implement standardize_ids before merges
*/

clear all
set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/"

program main
    build_registries
    get_competitors
    get_winners
    get_directory_rucs
    build_complete_registry
end

program build_registries
    * Import firms from the registry of product suppliers
    import delimited "${data_dir}/proc_contracts/providers_registry/providers_product_code.csv", ///
        delimiter(",") varnames(1) encoding(utf8) clear
    keep ruc razónsocial

    rename (ruc razónsocial) (provider_id provider_name)

    drop if provider_name == "Razón Social" | (provider_name == "" & provider_id == "")
    replace provider_id = subinstr(provider_id, `"""',  "", .)
    fix_name, namevar(provider_name)
    standardize_ids
    duplicates drop

    save_data "../temp/providers_1", replace key(provider_id provider_name)

    * Import firms from registry of providers
    import delimited "$data_dir/proc_contracts/providers_registry/Proveedores_y_Entidades_registradas_y_habilitadas_.csv", ///
        delimiter(";") encoding(utf8) stringcols(10) clear 
    keep razonsocialentidadproveedorsucur rucentidadproveedorsucursal

    rename (razonsocialentidadproveedorsucur rucentidadproveedorsucursal) (provider_name provider_id)
    fix_name, namevar(provider_name)
    standardize_ids
    duplicates drop
    
    replace provider_name = "." if provider_name == ""
    save_data "../temp/providers_2", replace key(provider_id provider_name)
end

program get_competitors
    * Import scraped pages
    foreach stub in "" "_2017" "_missings" {
        import delimited "$data_dir/proc_contracts/scraped/results/competitors`stub'.csv", ///
            delimiter(comma) bindquote(strict) varnames(1) stripquote(yes) encoding(utf8) stringcols(_all) clear
        keep proveedor ruc

        replace ruc = subinstr(ruc, ".0", "", .)
        gen ruc2 = regexs(2) if regexm(ruc, "^([^0-9]*)([0-9]+)([^0-9]*)$")
        drop ruc
        rename ruc2 ruc

        replace proveedor = subinstr(proveedor, "Noenvióoconfirmótodaslaspropuestas.", "", .)
        replace proveedor = subinstr(proveedor, "Nenvoconfirmtodslsprpsts", "", .)
        replace proveedor = subinstr(proveedor, "Nenvoconfirmtodaslaspropusts", "", .)
        replace proveedor = subinstr(proveedor, "No envió o confirmó todas las propuestas.", "", .)
        replace proveedor = subinstr(proveedor, "Nenvoctlp", "", .)
        replace proveedor = subinstr(proveedor, "Nenvoconfirmtodaslaspropuests", "", .)
        replace proveedor = trim(proveedor)

        gen provider_id = word(proveedor,-1)
        gen has_number = regexm(provider_id, "[0-9]+$")
        replace provider_id = "" if has_number == 0
        drop has_number

        replace proveedor = subinstr(proveedor, provider_id, "" ,.)
        replace proveedor = strtrim(stritrim(proveedor))

        replace provider_id = ruc if ruc != ""
        drop ruc
        rename proveedor provider_name
        
        fix_name, namevar(provider_name)
        reverse_ruc_name, firm_name(provider_name) firm_id(provider_id)
        standardize_ids
        
        duplicates drop provider_name provider_id, force

        save_data "../temp/competitors`stub'.dta", replace key(provider_id provider_name)
    }

    foreach stub in "" "_2017" {
        import delimited "$data_dir/proc_contracts/scraped/competitors_auctions/all_competitors_auction`stub'.csv", ///
            delimiter(comma) varnames(1) encoding(utf8) clear
        keep firm_name
        rename firm_name provider_name
        gen provider_id = "."
        drop if provider_name == ""
        fix_name, namevar(provider_name)
        duplicates drop provider_name provider_id, force
        save_data "../temp/competitors_auction`stub'.dta", replace key(provider_id provider_name)
    }
end

program get_winners
    foreach stub in t1 t2 {
        use provider_id provider_name using ///
            "$data_dir/proc_contracts/scraped/clean/summary_`stub'.dta", clear
        
        fix_name, namevar(provider_name)
        reverse_ruc_name, firm_name(provider_name) firm_id(provider_id)
        
        duplicates drop provider_id provider_name, force
        save_data "../temp/summary_`stub'.dta", key(provider_id provider_name) replace
    }

    import delimited "$data_dir/proc_contracts/scraped/publicacion/publicacion_winners.csv", /// 
        varnames(1) encoding(utf8) colrange(2) clear
    drop contract_code contract_link

    drop if winner_name == "NO DATA" | winner_name == "ERROR" | winner_name == ""
    rename (winner_id winner_name) (provider_id provider_name)

    replace provider_id = subinstr(provider_id, "`=char(9)'", "", .)
    replace provider_id = strtrim(provider_id)
    fix_name, namevar(provider_name)
    reverse_ruc_name, firm_name(provider_name) firm_id(provider_id)

    drop if provider_name == "" | provider_name == "."
    standardize_ids
    
    duplicates drop provider_name provider_id, force

    save_data "../temp/publicacion_winners.dta", replace key(provider_id provider_name)
end

program get_directory_rucs
    use rucFirm using "$data_dir/firms_ownership/directory.dta", replace
    rename rucFirm provider_id
    drop if provider_id == "."
    gen internal_firm_id = provider_id

    save_data "../temp/list_rucs_directory.dta", key(provider_id) replace
end

program build_complete_registry
    use "../temp/providers_1", clear
    foreach file in providers_2 competitors competitors_2017 competitors_missings ///
        publicacion_winners competitors_auction competitors_auction_2017 summary_t1 summary_t2 {
        append using "../temp/`file'"
    }
    duplicates drop provider_name provider_id, force

    * When one firm name has one missing ID and one valid ID keep only valid one
    bys provider_name (provider_id): gen second_obs = provider_id[2]
    
    preserve
        keep if provider_id == "." & second_obs != "." & second_obs != ""
        rename second_obs internal_firm_id
        save "../temp/registry_missing_id.dta", replace
    restore
    
    drop if provider_id == "." & second_obs != "." & second_obs != ""
    drop second_obs

    save_data "../temp/providers_registry.dta", replace key(provider_id provider_name)

    * Contractors with same name but different IDs
    duplicates tag provider_name, gen(t)
    keep if t>0
    drop t
    bys provider_name: strgroup provider_id, gen(id_group) threshold(0.25)
    save_data "../temp/verify_id_same_name_providers.dta", replace key(provider_id provider_name)

    merge 1:1 provider_id provider_name using "../temp/providers_registry.dta", ///
        assert(2 3) keep(2 3) nogen
    merge m:1 provider_id using "../temp/list_rucs_directory.dta", ///
        assert(1 2 3) keep(1 3) nogen

    * Within id_group can keep one ID at random. Give priority to ID that match directory data
    bys id_group (internal_firm_id): gen last_obs = internal_firm_id[_N]
    replace internal_firm_id = last_obs if internal_firm_id == ""
    drop last_obs

    * Then get single ID at random within group
    bys id_group (provider_id): gen first_obs = provider_id[1]
    replace internal_firm_id = first_obs if internal_firm_id == ""
    drop first_obs

    * For remaining obs get their provider ID
    replace internal_firm_id = provider_id if id_group == .
    drop id_group

    * If missing ID create one
    egen firm_id_tmp = group(provider_name)
    tostring firm_id_tmp, replace
    replace internal_firm_id = "INT" + firm_id_tmp if internal_firm_id == "."
    drop firm_id_tmp

    * Append back firms with missing ID that had duplicate with valid ID
    append using "../temp/registry_missing_id.dta"

    save_data "$data_dir/proc_contracts/tables/providers_registry.dta", ///
        replace key(provider_name provider_id)
end

program fix_name
    syntax, namevar(varname)

    replace `namevar'= upper(ustrto(ustrnormalize(`namevar', "nfd"), "ascii", 2))
    replace `namevar' = subinstr(`namevar', "(", "", .)
    replace `namevar' = subinstr(`namevar', ")", "", .)
    replace `namevar' = subinstr(`namevar', `"""',  "", .)
    replace `namevar' = subinstr(`namevar', "`=char(9)'", "", .)
    replace `namevar' = subinstr(`namevar', ".", "", .)
    replace `namevar' = subinstr(`namevar', ",", "", .)
    replace `namevar' = subinstr(`namevar', "-", "", .)
    replace `namevar' = subinstr(`namevar', "'", "", .)
    gen `namevar'_extended = `namevar'
    replace `namevar'_extended = stritrim(strtrim(`namevar'_extended))
    replace `namevar' = subinstr(`namevar'," ","",.)
end

program reverse_ruc_name
    syntax, firm_name(str) firm_id(str)

    gen firm_name_alt = regexs(0) if regexm(`firm_name', "[0-9]+")
    gen reverse = `firm_name' == firm_name_alt
    replace `firm_name' = `firm_id' if reverse == 1
    replace `firm_id' = firm_name_alt if reverse == 1
    drop reverse firm_name_alt
end

program standardize_ids
    replace provider_id = "0" + provider_id if length(provider_id) == 9 | length(provider_id) == 12
    replace provider_id = substr(provider_id, 1, 10) if substr(provider_id, -3, 3) == "001" & ///
        (length(provider_id) == 13 | length(provider_id) == 14)

    replace provider_id = "." if provider_id == "" | regexm(provider_id, "^[0]+$")
end

* Execute
main
