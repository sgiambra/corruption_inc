clear all
set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/proc_contracts/"

program main
    build_summary_t0_table
    build_summary_t1_table
    build_summary_t2_table
    build_product_table
    build_descr_table
end

program build_summary_t0_table
    /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    This section assembles summary_t0 table. The table contains the next
    information:
        {agency_name, contract_code, object_type, contract_type, budget,
        advance_percentage, post_percentage, contract_days, contract_link}
    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
    
    foreach stub in "" "_2017" "_missings" {
        import delimited "$data_dir/scraped/summary/summary_t0`stub'.csv", ///
            delimiter(comma) bindquote(strict) varnames(1) stripquote(yes) encoding(utf8) clear

        replace presupuestoreferencial = subinstr(presupuestoreferencial, "$","",.)
        replace presupuestoreferencial = subinstr(presupuestoreferencial, ",","",.)
        destring presupuestoreferencial, replace

        replace anticipo = subinstr(anticipo, "%","",.)
        destring anticipo, replace
         
        replace saldo = subinstr(saldo, "%","",.)
        destring saldo, replace 

        replace plazodeentrega = subinstr(plazodeentrega, "días","",.)
        destring plazodeentrega, replace

        rename (entidadcontratante códigodelproceso tipodecompra tipodeprocedimiento presupuestoreferencial anticipo saldo plazodeentrega idcontract) ///
            (agency_name contract_code object_type contract_type budget advance_percentage post_percentage contract_days contract_link)
        
        keep agency_name contract_code object_type contract_type budget advance_percentage ///
            post_percentage contract_days contract_link

        drop if contract_link == ""
        save_data "../temp/summary_t0`stub'.dta", replace key(contract_link)
    }
    use "../temp/summary_t0.dta", clear
    merge 1:1 contract_link using "../temp/summary_t0_2017.dta", ///
        assert(1 2) keep(1 2) nogen
    * Some contracts were scraped twice but information consistent
    merge 1:1 contract_link using "../temp/summary_t0_missings.dta", ///
        assert(1 2 3) keep(1 2 3) nogen

    save_data "$data_dir/scraped/clean/summary_t0.dta", replace key(contract_link)
end

program build_summary_t1_table
    /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    This section assembles summary_t1 table. The table contains the next
    information:
        {provider_id, provider_name, contract_value, contract_link, contract_date}

    Note: When there are multiple winners or multiple products the value of the contract
    reported could be the total value
    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
    
    foreach stub in "" "_2017" "_missings" {
        import delimited "$data_dir/scraped/summary/summary_t1`stub'.csv", ///
            delimiter(comma) bindquote(strict) varnames(1) stripquote(yes) encoding(utf8) clear

        replace montodeadjudicación = subinstr(montodeadjudicación, ",","",.)
        destring montodeadjudicación, replace force

        gen contract_date = date(fechadeadjudicación, "YMD")
        format contract_date %td

        rename (ruc nombredeladjudicatario montodeadjudicación idcontract) ///
            (provider_id provider_name contract_value contract_link)

        keep provider_id provider_name contract_date contract_value contract_link

        drop if contract_link == ""
        replace provider_id = "" if provider_id == "EMPTY"
        replace provider_name = "" if provider_name == "EMPTY"
        drop if provider_name == "" & provider_id == ""

        save "../temp/summary_t1`stub'.dta", replace
    }
    use "../temp/summary_t1.dta", clear
    merge m:m contract_link using "../temp/summary_t1_2017.dta", ///
        nogen assert(1 2) keep(1 2)
    save "../temp/summary_t1_merged.dta", replace 

    keep contract_link
    duplicates drop
    merge 1:m contract_link using "../temp/summary_t1_missings", ///
        nogen assert(1 2 3) keep(2)
    merge m:m contract_link using "../temp/summary_t1_merged", ///
        nogen assert(1 2) keep(1 2)

    replace provider_id = "0" + provider_id if length(provider_id) == 9 | length(provider_id) == 12
    replace provider_id = substr(provider_id, 1, 10) if substr(provider_id, -3, 3) == "001" & ///
        (length(provider_id) == 13 | length(provider_id) == 14)

    save "$data_dir/scraped/clean/summary_t1.dta", replace
end

program build_summary_t2_table
    /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    This section assembles summary_t2 table. The table contains the next
    information:
        {provider_id, provider_name, contract_value, contract_days, advance_percentage, 
        contract_date, number_deliveries, contract_end_date, contract_manager, 
        contract_link}
    
    Note: When there are multiple winners or multiple products the value of the contract
    reported could be the total value
    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
    
    foreach stub in "" "_2017" "_missings" {
        import delimited "$data_dir/scraped/summary/summary_t2`stub'.csv", ///
            delimiter(comma) bindquote(strict) varnames(1) stripquote(yes) encoding(utf8) clear

        replace montocontractual = subinstr(montocontractual, ",","",.)
        destring montocontractual, replace force

        destring plazocontractualdías, replace force
        destring porcentaje, replace force

        gen contract_date = date(fechadesuscripcióndelcontrato, "YMD")
        format contract_date %td
        gen contract_end_date = date(fechaprevistadeterminacióndecont, "YMD")
        format contract_end_date %td

        rename (ruc razónsocial montocontractual plazocontractualdías porcentaje  ///
            númerodeentregasparciales administradordelcontratousuarion idcontract) ///
            (provider_id provider_name contract_value contract_days advance_percentage ///
            number_deliveries contract_manager contract_link)

        drop if contract_link == ""
        replace provider_id = "" if provider_id == "EMPTY"
        replace provider_name = "" if provider_name == "EMPTY"
        drop if provider_name == "" & provider_id == ""

        keep provider_id provider_name contract_value contract_days advance_percentage contract_date ///
            number_deliveries contract_end_date contract_manager contract_link

        save "../temp/summary_t2`stub'.dta", replace
    }
    use "../temp/summary_t2.dta", clear
    merge m:m contract_link using "../temp/summary_t2_2017.dta", ///
        nogen assert(1 2) keep(1 2)
    save "../temp/summary_t2_merged.dta", replace 

    keep contract_link
    duplicates drop
    merge 1:m contract_link using "../temp/summary_t2_missings", ///
        nogen assert(1 2 3) keep(2)
    merge m:m contract_link using "../temp/summary_t2_merged", ///
        nogen assert(1 2) keep(1 2)

    replace provider_id = "0" + provider_id if length(provider_id) == 9 | length(provider_id) == 12
    replace provider_id = substr(provider_id, 1, 10) if substr(provider_id, -3, 3) == "001" & ///
        (length(provider_id) == 13 | length(provider_id) == 14)

    save "$data_dir/scraped/clean/summary_t2.dta", replace
end

program build_product_table
    /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    This section assembles products table. The table contains the next
    information:
        {product_quantity, product_id, product_description, product_unit_price,
        product_subtotal, product_unit_size, contract_link}
    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
    
    foreach stub in "" "_2017" "_missings" {
        import delimited "$data_dir/scraped/csv/all_products`stub'.csv", ///
            delimiter(comma) bindquote(strict) varnames(1) stripquote(yes) encoding(utf8) clear

        cap: replace cant = cantidad if cant == .
        cap: drop cantidad

        tostring categoría, g(product_id) force format(%17.0g)
        cap: replace product_id = cumid if cumid != ""

        rename preciorefunitario product_unit_price
        if "`stub'" == "" | "`stub'" == "missings" {
            replace product_unit_price = v12 if v12 != "" & product_unit_price == ""
            replace subtotal = v14 if v14 != "" & subtotal == ""
            replace subtotal = v18 if v18 != "" & subtotal == ""
        }
        if "`stub'" == "_2017" {
            replace subtotal = v9 if v9 != "" & subtotal == ""
        }
        foreach var in product_unit_price subtotal {
            replace `var' = subinstr(`var', "USD", "",.)
            replace `var' = subinstr(`var', ",", "",.)
            destring `var', replace force
        }

        replace bienobraservicio = bienservicio if bienservicio != "" & bienobraservicio == ""
        cap: replace bienobraservicio = principioactivo if principioactivo != ""

        replace bienobraservicio = subinstr(bienobraservicio, char(10), "", .)

        rename (cant bienobraservicio subtotal unidadmedida idcontract) ///
            (product_quantity product_description product_subtotal product_unit_size contract_link)
        keep product_quantity product_id product_description product_unit_price product_subtotal ///
            product_unit_size contract_link
        drop if product_unit_price == . & product_subtotal == .

        save "../temp/products`stub'.dta", replace
    }
    use "../temp/products.dta", clear
    merge m:m contract_link using "../temp/products_2017.dta", ///
        nogen assert(1 2) keep(1 2)
    merge m:m contract_link using "../temp/products_missings.dta", ///
        nogen assert(1 2) keep(1 2)

    save "$data_dir/scraped/clean/products.dta", replace
end

program build_descr_table
    /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    This section assembles description table. The table contains the next
    information:
        {agency_name, contract_code, contract_bureaucrat, contract_link, 
        contract_length, contract_budget, object_type, contract_type, 
        call_length}
    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

    foreach stub in "" "_2017" "_missings" {
        import delimited "$data_dir/scraped/csv/all_descriptions`stub'.csv", ///
            delimiter(comma) bindquote(strict) varnames(1) stripquote(yes) encoding(utf8) clear

        split estadodelproceso, p(":") limit(1)
        rename estadodelproceso1 contract_state
        drop estadodelproceso*

        replace contract_state = "Desierta" if substr(contract_state, 1,1) == "D"
        replace contract_state = "Terminada Unilateralmente" if substr(contract_state, 1,1) == "T"
        replace contract_state = "Cancelada" if substr(contract_state, 1,1) == "C"

        split formadepago, p("Anticipo: ") limit(2)
        split formadepago2, p("Saldo: ") limit(2)

        replace formadepago22 = substr(formadepago22, -7, 7)
        replace formadepago21 = subinstr(formadepago21, "%", "", .)
        replace formadepago22 = subinstr(formadepago22, "%", "",.)

        destring formadepago21, replace 
        rename formadepago21 advance_percentage
        destring formadepago22, replace force
        rename formadepago22 post_percentage
        drop formadepago*

        * Fix advance percentage
        replace advance_percentage = 70 if advance_percentage==7000
        replace post_percentage = -200 if advance_percentage==300

        gen check = advance_percentage+post_percentage
        count if check!=100 & check!=99 & check !=.
        drop check

        replace presupuestoreferencialtotalsiniv = subinstr(presupuestoreferencialtotalsiniv, "USD", "",.)
        replace presupuestoreferencialtotalsiniv = subinstr(presupuestoreferencialtotalsiniv, ",", "",.)
        destring presupuestoreferencialtotalsiniv, replace force

        foreach v of varlist plazodeentrega vigenciadeoferta {
            replace `v' = subinstr(`v', "días", "",.)
            replace `v' = subinstr(`v', "dias", "",.)
            destring `v', force replace
        }

        replace tipodecontratación = trim(tipodecontratación)

        rename (entidad código funcionarioencargadodelproceso idcontract plazodeentrega presupuestoreferencialtotalsiniv tipocompra tipodecontratación vigenciadeoferta) ///
            (agency_name contract_code contract_bureaucrat contract_link contract_length contract_budget object_type contract_type call_length)
        
        keep agency_name contract_code contract_bureaucrat contract_link contract_length contract_budget ///
            object_type contract_type call_length contract_state advance_percentage post_percentage
        * Note: A few pages were scraped incorrectly e.g. vIfv7Ji4vVDR9hMEuQUASFTg25Yi1Xe_0mT0YYQzFVE
        drop if contract_code == ""

        save_data "../temp/descriptions`stub'.dta", replace key(contract_link)
    }
    use "../temp/descriptions.dta", clear
    merge 1:1 contract_link using "../temp/descriptions_2017.dta", ///
        nogen assert(1 2) keep(1 2)
    save "../temp/descriptions_merged.dta", replace 

    keep contract_link
    duplicates drop
    merge 1:1 contract_link using "../temp/descriptions_missings", ///
        nogen assert(1 2 3) keep(2)
    merge 1:1 contract_link using "../temp/descriptions_merged", ///
        nogen assert(1 2) keep(1 2)

    save_data "$data_dir/scraped/clean/descriptions.dta", replace key(contract_link)
end
 
* Execute
main 
