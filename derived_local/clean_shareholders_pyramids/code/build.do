clear all
set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/firms_ownership/"

program main
    use "$data_dir/directory_ruc", clear
    drop if rucFirm == "."
    destring rucFirm, force replace
    save_data "../temp/directory_ruc", key(rucFirm) replace

    forvalues year=2000(1)2017{
        use "$data_dir/snapshots_shareholders_2000_2017" if year == `year', clear
        save_data "../temp/shareholders`year'", replace key(idshareholder expediente)
       
        /* Going through the layers */
        gen idshareholderLevel1_backup=idshareholder
        rename idshareholder rucFirm
        destring rucFirm, replace force
        replace rucFirm=0 if rucFirm == .
        rename expediente expedienteFirmLevel0
        rename share shareLevel1
        
        forvalues lev=1(1)10{
            NextShareholderLevel, level(`lev') year(`year')
        }
        
        rename rucFirm idshareholderLevel11
        * Layers and Final Ownership Groups 
        gen nLayers = 1
        forvalues i=1(1)10{
            replace nLayers = `i'+1 if expedienteFirmLevel`i' != 0 & idshareholderLevel`i' != 0
            }
        replace nLayers=11 if idshareholderLevel11!=0

        gen finalGroup = 0
        forvalues i=1(1)11{
            replace finalGroup = 1 if nLayers == `i' & idshareholderLevel`i'==0 & idshareholderLevel`i'_backup!=""
            }

        bysort expedienteFirmLevel0: egen maxGroup = max(finalGroup)
        
        *  Making List of Final Owners 
        gen idFinalShareholder_person = ""
        forvalues i=1(1)11{
            replace idFinalShareholder_person= idshareholderLevel`i'_backup if nLayers == `i' & finalGroup == 0
        }
            
        gen lenID = strlen(idFinalShareholder_person)
        gen finalPerson=(lenID>=10 & lenID>0)
        replace idFinalShareholder_person="" if  finalPerson==0
        replace idFinalShareholder_person="" if finalPerson==0

        gen idFinalShareholder_firm = ""
        forvalues i=1(1)11{
            replace idFinalShareholder_firm= idshareholderLevel`i'_backup if nLayers == `i' & finalPerson==0
        }
        
        forvalues i=1(1)10{
            local j=`i'+1
            replace idFinalShareholder_firm= idshareholderLevel`i'_backup if nLayers == `j' & expedienteFirmLevel`i'!=0 & idFinalShareholder_firm=="" & finalPerson==0
        }
        
        replace idFinalShareholder_firm="" if finalPerson==1
        
        * Tag if Loop *
        egen loop=rowmax(loopError*)
        
        * Shares of Final *
        gen tempProd0 = 1
        forvalues i=1(1)11{
            local j = `i' -1
            gen tempProd`i' = shareLevel`i'*tempProd`j'
            }
        gen shareFinal = . 
        forvalues i=1(1)11{
            replace shareFinal = tempProd`i' if nLayers == `i'
            }
            
        forvalues i=1(1)11{
            local j = `i' -1
            replace shareFinal = tempProd`j' if nLayers == `i' & shareFinal==.
            }
        sum shareFinal
        drop tempProd*

        * Keep Reduced Network *
        gen idFinalShareholder = idFinalShareholder_person + idFinalShareholder_firm
        
        keep capital totalcapital expedienteFirmLevel0 nLayers finalPerson idFinalShareholder *_backup loop shareFinal shareLevel*
        save "../temp/fullpyramid`year'", replace

        keep shareFinal expedienteFirmLevel0 idFinalShareholder capital shareLevel1 finalPerson totalcapital
        
        * Summing How much People Own Out of All Capital */
        gen dilutedCapital=shareFinal*capital/shareLevel1
        bysort finalPerson: egen totalCapitalPerson=sum(dilutedCapital)
        
        drop totalCapitalPerson capital shareLevel1
        rename dilutedCapital capital
        rename expedienteFirmLevel0 expediente
        rename idFinalShareholder idshareholder
        rename shareFinal share
        
        gcollapse (sum) capital share (first) totalcapital, by(idshareholder expediente)
        
        save_data "../temp/pyramid`year'", replace key(idshareholder expediente)
    }
    
    use "../temp/pyramid2000", clear
    gen year = 2000
    forvalues year=2001(1)2017{
        append using "../temp/pyramid`year'"
        replace year = `year' if year == . 
    }
    save_data "$data_dir/shareholders_pyramids_cleaned", replace key(idshareholder expediente year)

    * Double check with data validated with IRS
end

program NextShareholderLevel
    syntax, level(integer) year(integer)

    local levelp1 = `level'+1
    merge m:1 rucFirm using "../temp/directory_ruc", ///
        assert(1 2 3) keep(1 3) keepusing(expediente)
    
    rename rucFirm idshareholderLevel`level'
    replace expediente=0 if _merge==1
    drop _merge

    joinby expediente using "../temp/shareholders`year'", unmatched(master)
    drop _merge

    gen idshareholderLevel`levelp1'_backup=idshareholder
    rename idshareholder rucFirm
    destring rucFirm, replace force
    rename share shareLevel`levelp1'
    rename expediente expedienteFirmLevel`level'
    
    gen equal_error = (idshareholderLevel`level'==rucFirm)

    forvalues i=`level'(-1)1{
        replace equal_error = 1 if idshareholderLevel`i'==rucFirm
    }
    
    replace rucFirm=0 if equal_error==1
    replace rucFirm=0 if rucFirm==.
    gen loopError`level' = equal_error
    drop equal_error 
end

* Execute
main
