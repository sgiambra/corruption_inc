/*  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    This do-file cleans bureaucrats information from
        - declaracion (wave 2)
        - resultados (wave 1)
    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

clear all
set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/bureaucrats_scraped/parsed/"

program main
    clean_declaracion
    clean_resultados
end

program clean_declaracion
    get_list_ids

    import delimited "$data_dir/raw/all_bureaucrats.csv", ///
        delimiter(comma) bindquote(strict) varnames(1) stripquote(yes) encoding(utf8) clear

    rename (v1 v2 v3 v5 v7 v11 v14 v16 v18 v28 v29 v30 v31 v32 v33 v39 v43 v48) ///
        (link_id link_nbr start updated end bureaucrat_id bureaucrat_name nationality ///
        civil_status agency_name bureaucrat_position start_date end_date province city ///
        assets debt net_worth)

    keep link_nbr start updated end bureaucrat_id bureaucrat_name nationality ///
        civil_status agency_name bureaucrat_position start_date end_date province city ///
        assets debt net_worth

    drop if bureaucrat_id == ""
    drop bureaucrat_id

    * Original dataset had almost 3 million obs, whereas total links are now 2,2 million
    duplicates drop

    * Fix IDs (507 of all original links have not been downloaded)
    merge 1:1 link_nbr using "../temp/list_ids.dta", nogen assert(2 3) keep(3)
    replace bureaucrat_id = "0" + bureaucrat_id if length(bureaucrat_id) == 9 | length(bureaucrat_id) == 12
    replace bureaucrat_id = substr(bureaucrat_id, 1, 10) if substr(bureaucrat_id, -3, 3) == "001" & ///
        (length(bureaucrat_id) == 13 | length(bureaucrat_id) == 14)

    foreach v of varlist assets debt net_worth {
        replace `v' = subinstr(`v', ",",".",.)
        destring `v', replace force
    }
    * Type of declaration
    gen declaration_type = "new position" if start =="X"
    replace declaration_type = "update" if updated == "X"
    replace declaration_type = "end position" if end == "X"
    drop start updated end

    fix_name, namevar(bureaucrat_name)

    foreach v of varlist nationality agency_name bureaucrat_position {
        replace `v' = upper(ustrto(ustrnormalize(`v', "nfd"), "ascii", 2))
        replace `v' = strtrim(stritrim(`v'))
        replace `v' = subinstr(`v', "`=char(13)'", "", .)
    }
    manual_fixes_position
    replace bureaucrat_id = trim(bureaucrat_id)

    rename link_nbr id_nbr

    save_data "../temp/bureaucrats_declaracion", key(id_nbr) replace
end

program clean_resultados
    import delimited "$data_dir/raw/people_all.csv", encoding(utf8) stringcols(1) clear 
    drop v7
    rename (v1 v2 v3 v4 v5 v6) (bureaucrat_id bureaucrat_name bureaucrat_position agency_name province year)

    drop if bureaucrat_id == ""
    gen to_drop = 1 if regexm(bureaucrat_id, "^[0]+$")
    drop if to_drop == 1
    drop to_drop
     
    replace bureaucrat_id = "0" + bureaucrat_id if length(bureaucrat_id) == 9 | length(bureaucrat_id) == 12
    replace bureaucrat_id = substr(bureaucrat_id, 1, 10) if substr(bureaucrat_id, -3, 3) == "001" & ///
        (length(bureaucrat_id) == 13 | length(bureaucrat_id) == 14)

    fix_name, namevar(bureaucrat_name)
    
    foreach v of varlist agency_name bureaucrat_position {
        replace `v' = upper(ustrto(ustrnormalize(`v', "nfd"), "ascii", 2))
        replace `v' = strtrim(stritrim(`v'))
        replace `v' = subinstr(`v', "`=char(13)'", "", .)
    }
    manual_fixes_position
    replace bureaucrat_id = trim(bureaucrat_id)

    duplicates drop
    gen id_nbr = _n

    save_data "../temp/bureaucrats_resultados", replace key(id_nbr)
end

program get_list_ids
    import delimited "../../../derived_local/scrape_bureaucrats/people_ids_docids.csv", ///
        delimiter(comma) bindquote(strict) varnames(1) stripquote(yes) clear

    duplicates drop
    rename (v1 v2) (bureaucrat_id link_nbr)

    save_data "../temp/list_ids.dta", replace key(link_nbr)
end

program fix_name
    syntax, namevar(varname)

    replace `namevar'= upper(ustrto(ustrnormalize(`namevar', "nfd"), "ascii", 2))
    replace `namevar' = subinstr(`namevar', "(", "", .)
    replace `namevar' = subinstr(`namevar', ")", "", .)
    replace `namevar' = subinstr(`namevar', `"""',  "", .)
    replace `namevar' = subinstr(`namevar', "`=char(9)'", "", .)
    replace `namevar' = subinstr(`namevar', ".", "", .)
    replace `namevar' = subinstr(`namevar', ",", "", .)
    replace `namevar' = subinstr(`namevar', "-", "", .)
    replace `namevar' = subinstr(`namevar', "'", "", .)
    gen `namevar'_extended = `namevar'
    replace `namevar'_extended = stritrim(strtrim(`namevar'_extended))
    replace `namevar' = subinstr(`namevar'," ","",.)
end

program manual_fixes_position
    replace bureaucrat_position=subinstr(bureaucrat_position, "PUBICO", "PUBLICO", .)
    replace bureaucrat_position=subinstr(bureaucrat_position, "PUBLCIO", "PUBLICO", .)
    replace bureaucrat_position=subinstr(bureaucrat_position, "PUBKICO", "PUBLICO", .)
    replace bureaucrat_position=subinstr(bureaucrat_position, "SERVIDOR PUBLICA", "SERVIDOR PUBLICO", .)
    replace bureaucrat_position=subinstr(bureaucrat_position, "SERV.", "SERVIDOR", .)
    replace bureaucrat_position=subinstr(bureaucrat_position, "PUB.", "PUBLICO", .)
    replace bureaucrat_position=subinstr(bureaucrat_position, "SERVDIDOR", "SERVIDOR", .)
    replace bureaucrat_position=subinstr(bureaucrat_position, "SERVDOR", "SERVIDOR", .)
    replace bureaucrat_position=subinstr(bureaucrat_position, "SP", "SERVIDOR PUBLICO", .)
end

* Execute
main
