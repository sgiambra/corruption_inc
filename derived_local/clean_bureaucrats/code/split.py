import pandas as pd

input_list = ['declaracion', 'resultados']

for input in input_list:
    data = pd.read_stata('../temp/bureaucrats_' + input + '.dta')

    new_data = data["bureaucrat_position"].str.split(" ", expand=True)
    output_dt = pd.DataFrame()
    for i in range(0,46):
        colname = "position" + str(i)
        output_dt[colname]= new_data[i]

    selected_cols = data[['id_nbr', 'agency_name', 'bureaucrat_position']]
    output_dt = output_dt.join(selected_cols)
    
    output_dt.rename(columns={'agency_name': 'institution', 'bureaucrat_position': 'position'}, inplace=True)
    output_dt.to_csv('../temp/' + input + '_split.csv', index=False)