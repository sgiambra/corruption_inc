set more off
adopath + ../../../lib/stata/gslab_misc/ado
adopath + ../../../lib/stata/third_party/stata_tools
preliminaries

global data_dir "/Users/`c(username)'/Dropbox (Brown)/corruption_inc/bureaucrats_scraped/parsed/"

program main
    foreach file in declaracion resultados {
        import delimited using "../temp/`file'_split.csv", varnames(1) clear

        institution_type
        clean_institution_name

        gen bureaucrat      = 0
        gen gov_official    = 0
        gen advisor         = 0
        gen policeArmy      = 0
        gen academic        = 0
        gen medic           = 0
        gen elected         = 0
        gen not_bureaucrat  = 0

        gen analist   = position0=="ANALISTA"
        gen assistant = position0=="ASISTENTE"
        gen secretary = position0=="SECRETARIO" | position0=="SECRETARIA"
        gen security  = position0=="SEGURIDAD"

        high_level_positions
        gov_official_positions
        academic_positions

        replace bureaucrat=1 if gov_official==1 | academic==1 | elected==1

        not_bureaucratic_position

        keep id_nbr academic_inst medic_inst highLevel_bureaucrat bureaucrat ///
            gov_official policeArmy academic medic elected not_bureaucrat secretary
        save_data "../temp/`file'_positions", replace key(id_nbr)

        use "../temp/bureaucrats_`file'", clear
        merge 1:1 id_nbr using "../temp/`file'_positions", ///
            keep(3) assert(3) nogen

        save_data "$data_dir/clean/bureaucrats_`file'", replace key(id_nbr)
    }
end

program institution_type
    gen academic_inst = 0
    replace academic_inst=1 if regexm(institution, "COLEGIO")==1
    replace academic_inst=1 if regexm(institution, "COLG")==1
    replace academic_inst=1 if regexm(institution, "COL.")==1
    replace academic_inst=1 if regexm(institution, "ESCUELA")==1
    replace academic_inst=1 if regexm(institution, "ESC.")==1
    replace academic_inst=1 if regexm(institution, "EDUCATIVA")==1
    replace academic_inst=1 if regexm(institution, "EDUCATIVO")==1
    replace academic_inst=1 if regexm(institution, "UNIVERSIDAD")==1
    replace academic_inst=1 if regexm(institution, "UNIV.")==1
    replace academic_inst=1 if regexm(institution, "ESPOCH")==1
    replace academic_inst=1 if regexm(institution, "EDUCACION")==1
    replace academic_inst=1 if regexm(institution, "ESPOL")==1
    replace academic_inst=1 if regexm(institution, "FACULTAD")==1
    replace academic_inst=1 if regexm(institution, "POLITECNICA")==1
    replace academic_inst=1 if regexm(institution, "INSTITUTO")==1
    replace academic_inst=1 if regexm(institution, "ACADEMIA")==1
    replace academic_inst=1 if regexm(institution, "UTM")==1
    replace academic_inst=1 if regexm(institution, "ULEAM")==1
    replace academic_inst=1 if regexm(institution, "CONSERVATORIO")==1
    replace academic_inst=1 if regexm(institution, "UNACH")==1
    replace academic_inst=1 if regexm(institution, "UTA")==1

    gen medic_inst = 0
    replace medic_inst=1 if regexm(institution, "SALUD")==1
    replace medic_inst=1 if regexm(institution, "SEGURIDAD SOCIAL")==1
    replace medic_inst=1 if regexm(institution, "SEGURO SOCIAL")==1
    replace medic_inst=1 if regexm(institution, "IESS")==1
    replace medic_inst=1 if regexm(institution, "I.E.S.S.")==1
    replace medic_inst=1 if regexm(institution, "MSP")==1
    replace medic_inst=1 if regexm(institution, "HOSPITAL")==1
    replace medic_inst=1 if regexm(institution, "MEDICO")==1
    replace medic_inst=1 if regexm(institution, "CLINICA")==1
    replace medic_inst=1 if regexm(institution, "AMBULATORIA")==1
end

program clean_institution_name
    replace institution="POLICIA NACIONAL DEL ECUADOR" if institution=="POLICIA NACIONAL"
    replace institution="INSTITUTO ECUATORIANO DE SEGURIDAD SOCIAL" if institution=="IESS"
    replace institution="INSTITUTO ECUATORIANO DE SEGURIDAD SOCIAL" if institution=="INSTITUTO ECUATORIANO DE SEGURIDAD SOCIAL IESS"
    replace institution="MINISTERIO DE INCLUSION ECONOMICA Y SOCIAL" if institution=="MIES"
    replace institution="INSTITUTO NACIONAL DE ESTADISTICA Y CENSOS" if institution=="INEC"
    replace institution="INSTITUTO NACIONAL DE ESTADISTICA Y CENSOS" if institution=="INSTITUTO NACIONAL DE ESTADISTICA Y CENSOS - INEC"
    replace institution="INSTITUTO NACIONAL DE ESTADISTICA Y CENSOS" if institution=="INSTITUTO NACIONAL DE ESTADISTICA Y CENSOS INEC"
    replace institution="INSTITUTO NACIONAL DE ESTADISTICA Y CENSOS" if institution=="INSTITUTO NACIONAL DE ESTADISTICAS Y CENSOS INEC"
    replace institution="INSTITUTO NACIONAL DE ESTADISTICA Y CENSOS" if institution=="INSTITUTO NACIONAL DE ESTADISTICAS Y CENSOS"
    replace institution="CONSEJO NACIONAL ELECTORAL" if institution=="CNE"
    replace institution="MINISTERIO DE SALUD PUBLICA" if institution=="MSP"
    replace institution="MINISTERIO DE SALUD PUBLICA" if institution=="MINISTERIO DE SALUD"
    replace institution="MINISTERIO DE SALUD PUBLICA" if institution=="MINISTERIO DE SALUD PUBLICA DEL ECUADOR"
    replace institution="PETROAMAZONAS EP" if institution=="EMPRESA PUBLICA DE EXPLORACION Y EXPLOTACION DE HIDROCARBUROS 'PETROAMAZONAS EP'"
    replace institution="PETROAMAZONAS EP" if institution=="PETROAMAZONAS"
    replace institution="ARMADA NACIONAL DEL ECUADOR" if institution=="ARMADA DEL ECUADOR"
    replace institution="BANECUADOR BP" if institution=="BANECUADOR B.P."
    replace institution="COMISION DE TRANSITO DEL ECUADOR" if institution=="COMISION DE TRANSITO DEL ECUADOR CTE"
    replace institution="CONSEJO NACIONAL DE LA JUDICATURA" if institution=="CONSEJO DE LA JUDICATURA"
    replace institution="CONSEJO NACIONAL ELECTORAL" if institution=="CONCEJO NACIONAL ELECTORAL"
    replace institution="CORPORACION NACIONAL DE TELECOMUNICACIONES" if institution=="CORPORACION NACIONAL DE TELECOMUNICACIONES - CNT EP"
    replace institution="CORPORACION NACIONAL DE TELECOMUNICACIONES" if institution=="CORPORACION NACIONAL DE TELECOMUNICACIONES CNT EP"
    replace institution="CORPORACION NACIONAL DE TELECOMUNICACIONES" if institution=="CNT"
    replace institution="MINISTERIO DE INCLUSION ECONOMICA Y SOCIAL" if institution=="MINISTERIO DE INCLUSION ECONOMICA Y SOCIAL MIES"
    replace institution="MINISTERIO DE INCLUSION ECONOMICA Y SOCIAL" if institution=="MINISTERIO DE INCLUSION ECONOMICA Y SOCIAL, MIES"
    replace institution="MINISTERIO DE INCLUSION ECONOMICA Y SOCIAL" if institution=="MIESS"
    replace institution="MUNICIPIO DEL DISTRITO METROPOLITANO DE QUITO" if institution=="MUNICIPIO DE QUITO"
    replace institution="MUNICIPIO DEL DISTRITO METROPOLITANO DE QUITO" if institution=="MUNICIPIO DEL DISTRITO METROPOLITANO DE QUITO"
    replace institution="ASAMBLEA NACIONAL" if institution=="ASAMBLEA NACIONAL CONSTITUYENTE"
    replace institution="PETROECUADOR EP" if institution=="EMPRESA PUBLICA DE HIDROCARBUROS DEL ECUADOR , EP PETROECUADOR"
    replace institution="PETROECUADOR EP" if institution=="EMPRESA PUBLICA DE HIDROCARBUROS DEL ECUADOR EP PETROECUADOR"
    replace institution="PETROECUADOR EP" if institution=="EMPRESA PUBLICA DE HIDROCARBUROS DEL ECUADOR, EP PETROECUADOR"
    replace institution="PETROECUADOR EP" if institution=="PETROECUADOR"
    replace institution="MINISTERIO DE AGRICULTURA, GANADERIA, ACUACULTURA Y PESCA" if institution=="MINISTERIO DE COORDINACION DE DESARROLLO SOCIAL"
    replace institution="MINISTERIO DE AGRICULTURA, GANADERIA, ACUACULTURA Y PESCA" if institution=="MINISTERIO DE AGRICULTURA Y GANADERIA"
    replace institution="MINISTERIO DE EDUCACION" if institution=="MINISTERIO DE EDUCACION Y CULTURA"
    replace institution="SERVICIO DE RENTAS INTERNAS" if institution=="SERVICIO DE RENTAS INTERNAS SRI"
    replace institution="SUPERINTENDENCIA DE BANCOS Y SEGUROS" if institution=="SUPERINTENDENCIA DE BANCOS"
end

program high_level_positions
    foreach stub of numlist 0(1)15 {
        local stub1 = `stub'+1
        local stub2 = `stub'+2

        replace advisor=1 if position`stub'=="ASESOR" & secretary==0

        replace gov_official=1 if position`stub'=="MINISTRO" & secretary==0 & security==0
        replace gov_official=1 if position`stub'=="MINISTRA" & secretary==0 & security==0
        replace gov_official=1 if position`stub'=="MINISTROA" & secretary==0
        replace gov_official=1 if position`stub'=="VICEMINISTRO" & secretary==0
        replace gov_official=1 if position`stub'=="VICEMINISTRA" & secretary==0
        replace gov_official=1 if position`stub'=="SUBSECRETARIO" & secretary==0
        replace gov_official=1 if position`stub'=="SUBSECRETARIA" & secretary==0
        replace gov_official=1 if position`stub'=="SUBSECRETARIOA" & secretary==0
        replace gov_official=1 if position`stub'=="COORDINADOR" & secretary==0
        replace gov_official=1 if position`stub'=="COORDINADORA" & secretary==0
        replace gov_official=1 if position`stub'=="ASESOR" & secretary==0
        replace gov_official=1 if position`stub'=="ASESORA" & secretary==0
        replace gov_official=1 if position`stub'=="DIRECTOR" & secretary==0
        replace gov_official=1 if position`stub'=="DIRECTORA" & secretary==0
        replace gov_official=1 if position`stub'=="SUBDIRECTOR" & secretary==0
        replace gov_official=1 if position`stub'=="SUBDIRECTORA" & secretary==0
        replace gov_official=1 if position`stub'=="GERENTE" & secretary==0
        replace gov_official=1 if position`stub'=="SERVIDOR" & position`stub1'=="PUBLICO" & position`stub2'=="14" & secretary==0
        replace gov_official=1 if position`stub'=="SERVIDOR" & position`stub1'=="PUBLICO14" & secretary==0
        replace gov_official=1 if position`stub'=="SERVIDOR" & position`stub1'=="PUBLICO" & position`stub2'=="13" & secretary==0
        replace gov_official=1 if position`stub'=="SERVIDOR" & position`stub1'=="PUBLICO13" & secretary==0
        replace gov_official=1 if position`stub'=="SERVIDOR" & position`stub1'=="PUBLICO" & position`stub2'=="12" & secretary==0
        replace gov_official=1 if position`stub'=="SERVIDOR" & position`stub1'=="PUBLICO12" & secretary==0
        replace gov_official=1 if position`stub'=="SERVIDOR" & position`stub1'=="PUBLICO" & position`stub2'=="11" & secretary==0
        replace gov_official=1 if position`stub'=="SERVIDOR" & position`stub1'=="PUBLICO11" & secretary==0
        replace gov_official=1 if position`stub'=="SERVIDOR" & position`stub1'=="PUBLICO" & position`stub2'=="10" & secretary==0
        replace gov_official=1 if position`stub'=="SERVIDOR" & position`stub1'=="PUBLICO10" & secretary==0
        replace gov_official=1 if position`stub'=="SERVIDOR" & position`stub1'=="PUBLICO" & position`stub2'=="9" & secretary==0
        replace gov_official=1 if position`stub'=="SERVIDOR" & position`stub1'=="PUBLICO9" & secretary==0
        replace gov_official=1 if position`stub'=="SERVIDOR" & position`stub1'=="PUBLICO" & position`stub2'=="8" & secretary==0
        replace gov_official=1 if position`stub'=="SERVIDOR" & position`stub1'=="PUBLICO8" & secretary==0
        replace gov_official=1 if position`stub'=="SERVIDOR" & position`stub1'=="PUBLICO" & position`stub2'=="7" & secretary==0
        replace gov_official=1 if position`stub'=="SERVIDOR" & position`stub1'=="PUBLICO7" & secretary==0
        replace gov_official=1 if position`stub'=="SERVIDOR" & position`stub1'=="PUBLICO" & position`stub2'=="6" & secretary==0
        replace gov_official=1 if position`stub'=="SERVIDOR" & position`stub1'=="PUBLICO6" & secretary==0
        replace gov_official=1 if position`stub'=="EXPERTO"
        replace gov_official=1 if position`stub'=="EXPERTA"
        replace gov_official=1 if position`stub'=="JEFE" & secretary==0

        replace gov_official=1 if position`stub'=="NOTARIO" & secretary==0
        replace gov_official=1 if position`stub'=="NOTARIA" & secretary==0
        replace gov_official=1 if position`stub'=="JUEZ" & secretary==0
        replace gov_official=1 if position`stub'=="JUEZA" & secretary==0
        replace gov_official=1 if position`stub'=="CONJUEZ" & secretary==0
        replace gov_official=1 if position`stub'=="CONJUEZA" & secretary==0
        replace gov_official=1 if position`stub'=="FISCAL" & secretary==0
        replace gov_official=1 if position`stub'=="EMBAJADOR" & secretary==0
        replace gov_official=1 if position`stub'=="EMBAJADORA" & secretary==0
        replace gov_official=1 if position`stub'=="NIVEL" & position`stub1'=="JERARQUICO" & position`stub2'=="SUPERIOR"
        replace gov_official=1 if position`stub'=="DIRECTORIO" & secretary==0
        replace gov_official=1 if position`stub'=="SUPERINTENDENTE" & secretary==0
        replace gov_official=1 if position`stub'=="INTENDENTE" & secretary==0
        replace gov_official=1 if position`stub'=="SECRETARIO" & position`stub1'=="NACIONAL"
        replace gov_official=1 if position`stub'=="SECRETARIO" & position`stub1'=="GENERAL"
        replace gov_official=1 if position`stub'=="CONTRALOR"
        replace gov_official=1 if position`stub'=="PROCURADOR"
        replace gov_official=1 if position`stub'=="SUPERVISOR" & secretary==0
        replace gov_official=1 if position`stub'=="SUPERVISORA" & secretary==0
        replace gov_official=1 if position`stub'=="PRESIDENTE"
        replace gov_official=1 if position`stub'=="PRESIDENTA"
        replace gov_official=1 if position`stub'=="VICEPRESIDENTE"
        replace gov_official=1 if position`stub'=="VICEPRESIDENTA"
        replace gov_official=1 if position`stub'=="DEFENSOR" & position`stub1'=="PUBLICO" & secretary==0
        replace gov_official=1 if position`stub'=="TESORERO" & secretary==0
        replace gov_official=1 if position`stub'=="TESORERA" & secretary==0
        replace gov_official=1 if position`stub'=="AUDITOR" & secretary==0
        replace gov_official=1 if position`stub'=="CONCEJAL" & secretary==0

        replace policeArmy=1 if position`stub'=="COMANDANTE" & secretary==0
        replace policeArmy=1 if position`stub'=="CORONEL" & secretary==0
        replace policeArmy=1 if position`stub'=="MAYOR" & secretary==0
        replace policeArmy=1 if position`stub'=="CAPITAN" & secretary==0
        replace policeArmy=1 if position`stub'=="SUBCOMANDANTE" & secretary==0
        replace policeArmy=1 if position`stub'=="GENERAL" & secretary==0 & medic_inst==0
        replace policeArmy=1 if position`stub'=="ALMIRANTE" & secretary==0
        replace policeArmy=1 if position`stub'=="VICEALMIRANTE" & secretary==0
        replace policeArmy=1 if position`stub'=="CONTRALMIRANTE" & secretary==0

        replace academic=1 if position`stub'=="RECTOR" & secretary==0
        replace academic=1 if position`stub'=="VICERRECTOR" & secretary==0
        replace academic=1 if position`stub'=="RECTORA" & secretary==0
        replace academic=1 if position`stub'=="VICERRECTORA" & secretary==0
        replace academic=1 if position`stub'=="DECANO" & secretary==0
        replace academic=1 if position`stub'=="DECANA" & secretary==0
        replace academic=1 if position`stub'=="DIRECTOR" & secretary==0 & academic_inst==1
        replace academic=1 if position`stub'=="DIRECTORA" & secretary==0 & academic_inst==1

        replace medic=1 if position`stub'=="DIRECTOR" & secretary==0 & medic_inst==1
        replace medic=1 if position`stub'=="DIRECTORA" & secretary==0 & medic_inst==1
        replace medic=1 if position`stub'=="GERENTE" & secretary==0 & medic_inst==1
        replace medic=1 if position`stub'=="JEFE" & secretary==0 & medic_inst==1
        replace medic=1 if position`stub'=="COORDINADOR" & secretary==0 & medic_inst==1

        replace elected=1 if position`stub'=="PREFECTO" & assistant==0 & secretary==0 & advisor==0 & security==0
        replace elected=1 if position`stub'=="PREFECTA" & assistant==0 & secretary==0 & advisor==0 & security==0
        replace elected=1 if position`stub'=="SUBPREFECTO" & assistant==0 & secretary==0 & advisor==0 & security==0
        replace elected=1 if position`stub'=="SUBPREFECTA" & assistant==0 & secretary==0 & advisor==0 & security==0
        replace elected=1 if position`stub'=="ALCALDE" & assistant==0 & secretary==0 & advisor==0 & security==0
        replace elected=1 if position`stub'=="ALCALDESA" & assistant==0 & secretary==0 & advisor==0 & security==0
        replace elected=1 if position`stub'=="ALCADE" & assistant==0 & secretary==0 & advisor==0 & security==0
        replace elected=1 if position`stub'=="ASAMBLEISTA" & assistant==0 & secretary==0 & advisor==0 & security==0
        replace elected=1 if position`stub'=="DIPUTADO" & assistant==0 & secretary==0 & advisor==0 & security==0
        replace elected=1 if position`stub'=="DIPUTADA" & assistant==0 & secretary==0 & advisor==0 & security==0
        replace elected=1 if position`stub'=="MAGISTRADO" & assistant==0 & secretary==0 & advisor==0 & security==0
        replace elected=1 if position`stub'=="MAGISTRADA" & assistant==0 & secretary==0 & advisor==0 & security==0
        replace elected=1 if position`stub'=="CANCILLER" & assistant==0 & secretary==0 & advisor==0 & security==0
        replace elected=1 if position`stub'=="CANCILLERA" & assistant==0 & secretary==0 & advisor==0 & security==0
        replace elected=1 if position`stub'=="GOBERNADOR" & assistant==0 & secretary==0 & advisor==0 & security==0
        replace elected=1 if position`stub'=="GOBERNADORA" & assistant==0 & secretary==0 & advisor==0 & security==0
        replace elected=1 if position`stub'=="CONSEJAL" & assistant==0 & secretary==0 & advisor==0 & position!="AUXILIAR DE SERVICIOS SALA DE CONSEJALES"
        replace elected=1 if position`stub'=="CONCEJAL" & assistant==0 & secretary==0 & advisor==0 & position!="AUXILIAR DE SERVICIOS SALA DE CONSEJALES"
        replace elected=1 if position`stub'=="DELEGADO" & assistant==0 & secretary==0 & advisor==0 & security==0
        replace elected=1 if position`stub'=="DELEGADA" & assistant==0 & secretary==0 & advisor==0 & security==0
        replace elected=1 if position`stub'=="VOCAL" & assistant==0 & secretary==0 & advisor==0

        replace assistant=1 if position`stub'=="ASISTENTE" & secretary==0
    }

    replace gov_official=1 if position=="SECRETARIO ADM. PUBLICO Y MINISTRO DE GOBIERNO"
    replace gov_official=1 if position=="SECRETARIO DE ESTADO CON CALIDAD DE MINISTRO"
    replace gov_official=1 if position=="SECRETARIO CON RANGO DE MINISTRO DE ESTADO"
    replace gov_official=1 if position=="SUBSECRETARIO GENERAL VICEMINISTRO"
    replace gov_official=1 if position=="SECRETARIA VICEMINISTRA"

    gen highLevel_bureaucrat = gov_official==1 | policeArmy==1 | academic==1 | medic==1 | elected==1 | advisor==1
end

program gov_official_positions
    replace gov_official=1 if position=="TRABAJADORA SOCIAL"
    replace gov_official=1 if analist==1
    replace gov_official=1 if assistant==1
    replace gov_official=1 if position=="AUXILIAR ADMINISTRATIVO"
    replace gov_official=1 if position=="INSERVIDOR PUBLICOECTOR"
    replace gov_official=1 if position=="ASERVIDOR PUBLICOIRANTE"
    replace gov_official=1 if position=="AUXILIAR DE SERVICIOS"
    replace gov_official=1 if position=="TECNICO"

    replace gov_official=1 if strpos(position, "SERVIDOR PUBLICO")>0
    replace gov_official=1 if strpos(position, "SERVIDOR MUNICIPAL")>0
    replace gov_official=1 if strpos(position, "RELACIONES PUBLICAS")>0
    replace gov_official=1 if strpos(position, "AYUDANTE JUDICIAL") > 0
end

program academic_positions
    replace academic=1 if regexm(position, "PROFESOR")==1
    replace academic=1 if regexm(position, "PROFESORA")==1
    replace academic=1 if regexm(position, "CATEDRATICO ")==1 
end

program not_bureaucratic_position
    replace not_bureaucrat=1 if strpos(position, "CHOFER")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "CONDUCTOR")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "OBRER")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "OFICINISTA")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "CONSERJE")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "OPERADOR")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "PEON")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "GUARDIA")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "DIGITADOR")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "JARDINER")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "MECANICO")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "ALBANIL")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "BOMBER")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "ELECTRICISTA")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "BIBLIOTECAR")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "VACUNADOR")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "BARRENDER")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "COCIN")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "PLOMER")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "CARPINTER")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "LIMPIEZA")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "AMANUENSE")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "VIGILANTE")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "SEGURIDAD")>0 & bureaucrat==0
    replace not_bureaucrat=1 if strpos(position, "MANTENIMIENTO")>0 & bureaucrat==0
end

* Execute
main
