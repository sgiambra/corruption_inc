# Note: the file is designed to work with python 3.X

import requests
import csv
import os
import sys
import json
import pandas as pd

os.chdir('/Users/Samuele/Documents/bitbucket/corruption_inc/derived_local/scrape_bureaucrats')
URL = "http://www.contraloria.gob.ec/WFResultados.aspx?&tipo=dj&ce=&no={0}&length=1000000"

output_df = pd.DataFrame()

with open('first3_lastname.csv', 'r') as namesfile:
    prefixes = csv.reader(namesfile)
    for row in prefixes:
        prefix = row[0]
        with requests.Session() as session:
            try:
                response = requests.get(URL.format(prefix), verify=False)
                sourceCode = response.content
                data = json.loads(sourceCode)
                if data['recordsTotal'] > 1000000:
                    print("Error, didn't get all the records for", prefix)
                for p in data['data']:
                    p = [s for s in p]
                    output_df_new_row = pd.DataFrame(p).T
                    output_df = pd.concat([output_df, output_df_new_row], axis=0, ignore_index=True, sort=True)
                print("Downloaded", prefix)
            except Exception as e:
                print(e)
                print("Error, couldn't get", prefix)
                raise
                    
output_df.to_csv('/Users/Samuele/Dropbox (Brown)/corruption_inc/bureaucrats_scraped/parsed/clean/bureaucrats_resultados.csv', encoding='utf-8', index=False, header=False)