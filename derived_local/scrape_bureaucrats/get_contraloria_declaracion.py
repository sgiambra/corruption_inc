from bs4 import BeautifulSoup
import requests
import csv, os, sys
import json
import pandas as pd

URL = "http://www.contraloria.gob.ec/sistema/WFFormaDeclaracion.aspx?id={0}&td={1}"

SLUG = [u'', u'', u'', u'', u'', u'']

with open('./people_ids_docids.csv', 'r') as csvfile:
    rows = csv.reader(csvfile)
    i = 0
    for row in rows:
        i += 1
        if i >= 500000 and i < 600000:
            id = row[0]
            docid = row[1]

            if int(docid) == 0:
                print i, "Skipping", id, "no docid"
                writer.writerow([id, docid, "No data"])
                continue

            with requests.Session() as session:
                try:
                    response = requests.get(URL.format(id, docid), verify=False)
                    sourceCode = response.content

                    soup = BeautifulSoup(sourceCode, 'html.parser')
                    tds = soup.findAll("td")
                    l = [td.text for td in tds]

                    count = sourceCode.count("<td")

                    if count == 44:
                        l = l[:31] + SLUG + l[31:]
                    elif count == 0:
                        l = [u'' for i in range(50)]
                    
                    l = [id] + [docid] + l
                    l = pd.DataFrame(l).T
                    temp_path = '../temp/' + str(id) + str(docid) + '.json'
                    l.to_json(temp_path)

                    print i, "Downloaded", id, docid

                except Exception as e:
                        print e
                        print i, "Error, couldn't get", id, docid
