import os
from os import listdir
from os.path import isfile, join
import pandas as pd

scraped_path = '/Users/sgiambra/Documents/bureaucrats/'
output_path = '/Users/sgiambra/Documents/bureaucrats_output/partitioned/'

for r in range(30):
    i = r + 1
    print(i)
        
    subdir = scraped_path + 'bureaucrats_' + str(i) + '/temp'
    
    os.chdir(subdir)
    file_list = [f for f in listdir(subdir) if isfile(join(subdir, f))]
    
    n = 10000
    partitions = [file_list[i * n:(i + 1) * n] for i in range((len(file_list) + n - 1) // n )]
    
    part = 0
    index = 0
    for sublist in partitions:
        part += 1
        
        for file in sublist:
            index += 1
            if not index%1000:
                print("Parsing file no: "+str(index))
            else:
                pass
            output_df_new_row = pd.read_json(file)
            if (index-1+n)%n == 0:
                output_df = output_df_new_row
            else:
                output_df = pd.concat([output_df, output_df_new_row], axis=0, ignore_index=True, sort=True)
         
        output_df.to_csv(output_path + 'bureaucrats_' + str(i) + '_' + str(part) + '.csv', encoding='utf-8', index=False, header=True)


os.chdir(output_path)
file_list = [f for f in listdir(output_path) if isfile(join(output_path, f))]

index = 0
for file in file_list:
    index += 1
    final_df_new_row = pd.read_csv(file)

    if index == 1:
        final_df = final_df_new_row
    else:
        final_df = pd.concat([final_df, final_df_new_row], axis=0, ignore_index=True, sort=True)
         
final_df.to_csv(output_path + 'all_bureaucrats.csv', encoding='utf-8', index=False, header=True)