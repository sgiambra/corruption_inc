import glob
import pandas as pd
import numpy as np

path ='/Users/Samuele/Dropbox (Brown)/corruption_inc/proc_contracts/scraped/publicacion/' # use your path
allFiles = glob.glob(path + "/*.csv")

np_array_list = []
for file in allFiles:
    if 'output' in file:
        df = pd.read_csv(file,index_col=None, header=0,encoding = 'utf8')
        np_array_list.append(df.as_matrix())

comb_np_array = np.vstack(np_array_list)
big_frame = pd.DataFrame(comb_np_array)

big_frame.columns = ['contract_link', 'contract_code', 'winner_id', 'winner_name', 'winning_reason']
big_frame = big_frame.drop("winning_reason", axis=1)

big_frame.to_csv(str(path)+'publicacion_winners.csv',encoding='utf-8')
