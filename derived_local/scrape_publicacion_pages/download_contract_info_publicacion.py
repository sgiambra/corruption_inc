# Script can be used to download publication webpages

from bs4 import BeautifulSoup
import requests
import csv
import os
import sys

requests.packages.urllib3.disable_warnings()

URL = "https://www.compraspublicas.gob.ec/ProcesoContratacion/compras/PC/resumenAdjudicacionPublicacion.cpe?solicitud={0}"

linksdir = "/Users/Samuele/Documents/publicacion_winners/"
datadir = "/Users/Samuele/Documents/publicacion_winners/output/"

with open(str(linksdir)+'publicacion_links_2017.csv', 'r', encoding='utf-8') as csvfile:
    with open(str(datadir)+'output_2017.csv', 'w', encoding='utf-8') as outfile:
        rows = csv.reader(csvfile)
        writer = csv.writer(outfile)
        i = 0
        for row in rows:
            link = row[0]
            process_id = row[1]
            i += 1
            
            print("Parsing Publicacion #" +str(i))
            with requests.Session() as session:
                try:
                    response = requests.get(URL.format(link), verify=False)
                    soup = BeautifulSoup(response.text, "html.parser")
                    if "RUC:" in soup.get_text():
                        tables = soup.find_all("table")
                        info = [td.get_text() for td in tables[-3].find_all('td')]
                        _ = writer.writerow([link, process_id, info[2], info[4], info[6]])
                    else:
                        _ = writer.writerow([link, process_id, "NO DATA", "NO DATA", "NO DATA" ])
                except Exception as e:
                    print(e)
                    print("Error, couldn't get", link)
                    _ = writer.writerow([link, process_id, "ERROR", "ERROR", "ERROR" ])
